nds2-client-NEXT
-----------------------------
  * Have nds-client-config return a non-zero exit status when unknown option is specified (fixes #152)
  * Removed dependencies on python's distutils module which has been depricated

nds2-client-0.16.8
-----------------------------
  * Updated catch2 library for M1 chips
  * Updated nds_query and nd2_channel_source to use NDS2SERVER env variable
  * Extract server info from NDSSERVER iff neither  -n or -p are specified
  * Fixed pythoon shebang lines for EL8

nds2-client-0.16.7
-----------------------------
  * Updates to allow retrieving online data from NDS2 servers with sub-second strides.

nds2-client-0.16.6
-----------------------------
  * Extended sybols exposed to Windows (fixes #106)

nds2-client-0.16.5
-----------------------------
  * Updated packaging rules based on lessons learned from LDAS
  * Updated home page url (issue: #98)
  * Updated project source url

nds2-client-0.16.4
-----------------------------
  * Corrected several documentation issues
  * Eliminated warning message by adding template function to specialize on size_t comparison on buffer range checking.

nds2-client-0.16.3
-----------------------------
  * Workaround for #83, errors with invalid hex conversion when nds1 sends bad data.
  * Fix for #87, requesting testpoint data and online data simultaneously
  * Fix for #85, on NDS1 the gap handler returns a the wrong name (includes s/m-trend in the name) when creating a completely synthetic block of data for trend data.
  * Fix for #82, added range checking on intervals to ensure start < end

nds2-client-0.16.2
-----------------------------
  * Corrected testing script to reflect deprication of javax.xml.bind

nds2-client-0.16.1
-----------------------------
  * Added a "connectionless" interface, exposing fetch, iterate, and find_channels (ticket #64).
  * Updated the user documentation.
  * Extended the gap handlers to handle more cases.
  * Updated the gap handling to always return data.  If a gap handler is specified it will always give data.
  * Fix for #74 where a bad channel selection is done when filling in an segment that is a complete gap.
  * Fix for #73 better error messages when a ambigous channel is selected.  The message will list the alterantives.
  * Internal code cleanup, replace new/delete w/ make_unique.  Provide a make_unique for pre-C++14 systems.
  * libndscxx now distinguishes between testpoints and regular channels when connected to a nds1 server.
  * The version information returned by the swig clients is the version of the swig generated bindings not the libndscxx that underpins it.
  * Adding gps times to the abort gap handler error message to help identify gaps.

nds2-client-0.16.0
-----------------------------
  * For the C++ parameters and configuration are now handled by a NDS::parameters object.  This change makes the system
      more flexible for different connection schemes (via host/port/proto or URI or environment or ...)
  * Added better support (and stride constants) to request streaming of data at sub second (ie 16Hz) rates.
  * Renamed the libndswrapxx to libndscxx as it is not longer a wrapper library.
  * Added abi specific namespaces and marked abi_0 as inline.  The intent is to provide new namespaces with
    abi changes.  Old code will have access to old abi versions while new code can use newer features.
  * All internal/helper symbols have been moved out of the toplevel NDS namespace.
  * Added nds2-clientConfig.cmake files as build products, allowing cmake projects to consume nds2-client.
  * Applied clang-tidy fixes to use default constructors/copy operators, nullptr, ...
  * Removed many .sh files that were left overs from the autotools testing setup.
  * Updated NDS::buffer to be constructable from a std::vector<char> as well as a char*.
  * Added at<>() to NDS::buffer for a more native and type safe access to buffer elements.
  * Added cbegin<>()/cend<>() to NDS::buffer for a more native style access the data.
  * Reworked SWIG code so that the bindings on NDS::buffer no longer need a special friend class to get the data.
  * Removed an unused private field from NDS::buffer.
  * Removed sqlite from the client code completely.  Nds1 channels are cached in memory.
  * Updated c++ tests to use an assert that is never remove by a release build.
  * In the swig client bulk nds1 channel reads no longer issue require reading the channel list twice.
  * Adding a layer of abstraction to make it easier for a C++ client to consume the C++ interface provided by the SWIG layers.
  * Updated the C++ layer to C++11 as a requirement
  * The C++ layer is now meant to be consumed directly by clients.
    - Minor api cleanups
       - Using move semantics to remove return parameters
       - vectors of objects are provided instead of an additional layer of abstraction
       - The iterate interface has been changed to produce C++ iterators to allow streams of buffers
         to be consumed by typical C++ code (std algorithms, range based for loops, ...)
  * Removed auto_ptr from the code base.
  * Updated the tests and test harnesses so that python 2 or 3 could be used.
  * Enhanced the documentation to use Doxygen
    - Folded DocBook documentation into Doxygen's mainpage
    - Created manuals for Users, Administrators, and Developers
      which have different levels of detail.
  * Removed config/nds2-client.spec.in as it was for Autotools
  * Created channel_info structure to be used internally by nds1_channel_iterator
  * Added missing cmake build dependency for SL7 (ticket #15)
  * Moved nds_swig.hh to SWIG distribution (ticket #44)
  * Integrated nds_str_helper.hh into nds_channel.hh (ticket #35)
	
nds2-client-0.15.3
-----------------------------
  * Addressed packaging issues for Debian Jessie and previous releases
  * Addressed warnings related to unused variables
  * Fixed #231 by adding numpy dependency to nds2-client-python
  * Static linking of the java client to alleviate matlab library
    compatibility issues
  * Updates to the testing, to allow cases where disconnection is required
  * Added a build target for CI builds in debian
  * Fix for ticket 295, bounded live data retrieval on nds1
	
nds2-client-0.15.2 (svn )
-----------------------------
  * Addressed packaging issues for OSX and Debian

nds2-client-0.15.1 (svn )
-----------------------------
  * Addressed XCode 9.x build issue in a unit test

nds2-client-0.15.0 (svn )
-----------------------------
  * Fixed #284 Removed the bad embedded path in nds-client-config
  * Fixed #291 All installation destinations have been modified to use GNUInstallDirs values
  * Fixed #282 NDS1 connections raise an DAQD_ERROR after a large number of commands.  Transparently cycle the nds1 connection.
  * Expanded testing to support testing of multiple protocol version per protocol when using the replay server
  * Fixed #292 Addressed user-env issues
	
nds2-client-0.14.3 (svn )
-----------------------------
  * Fixed #288 In some circumstances offline nds2 iterate only returns the first chunk of
  available data.
  * Fixed #289 Channel listing with unknown data types now do not raise an error
  in find_channels.

nds2-client-0.14.2 (svn )
-----------------------------
  * Modified to have proper shared library naming conventions
  * Modified to follow libtool versioning rules
  * Modified to have CMake define HAVE_SASL if SASL_FOUND

nds2-client-0.14.1 (svn )
-----------------------------
  * Renamed Install.CMake to Install as cmake is now the build system
  * Corrected where python extentions were installed for Debian
  * Added versioned dependencies for nds2-client subpackages for Debian to support downgrading
  * Added RPM meta-package nds2-client-all to better facilitage meta packaging for administrators

nds2-client-0.14.0 (svn )
-----------------------------
  * updating environment scripts to better reflect what packages get installed (#248, #266, #267)
  * Convert build system to cmake to enable a single consistent build system.
  * Convert tests to ctest so that all platforms can run tests (#219).
  * Update internal/language specific documentation.
  * Expanded the test suite so that matlab/java have tests equivalent to python/c++.
  * Remove the old C SWIG bindings.
  * Change the command timeout to 20s.
  * Fix possible buffer overflow issues when dealing with a large availability list (#276).
  * Updated C++ code to return arrays of shared pointers to the SWIG code.  This is a general solution to the numpy bug of 0.13.2 (#246).
  * Fixed an error where an exception would cause the system to terminate instead of catching the error (#260, #264, #269, #270).
  * Setting a one second epoch in a call to find_channels will return a precise channel list when talking to nds2 (#254).
  * Make offline iterate/fetch match, they fail with a data not found error if they would synthesise 100% of the data (#251).
  * Issuing a windows release of the client.
  * Improving the octave support for the C++ client (#256)
  * Allow python2 code to use unicode strings as arguments to the SWIG calls (#242)
  * Implemented a matlab wrapper over the java interface.

nds2-client-0.13.2 (svn 1743)
-----------------------------
  * Fix bug where the backing store of a numpy array was disappearing. (#246)

nds2-client-0.13.1 (svn 1714)
-----------------------------
  * Added jnilib to jar archive
  * Fixed connection iterator for Python and C++
  * MacPorts now supports the java extensions via subport instead of variant (nd2-client-java)
  * MacPorts now supports the octave extensions via subport instead of variant (nds2-client-octave)
  * Corrected some Java calls for backwards compatibility

nds2-client-013.0 (svn 1706)
-----------------------------
  * Replaced the C SWIG bindings with the newer C++ SWIG bindings which promotes gap handling into the main SWIG interface.
    - Set the default gap handling strategy to be the abort handler, to maintain compatibility with the older SWIG wrapper.
  * Imported basic documention into the SWIG layer to provide language specific help.
    - The initial focus is to get python doc strings working. (#184)
    - Class level javadoc is also generated.
    - Added some pretty printing functionality to C++/Python SWIG layers.
  * Removed the ROVector from the C++ interface due to issues with python wrapping. (#236)
  * Packaging changes for the java interface, a .jar file is now made available along with the .class files.
    - In future release only the .jar file will be produced.

nds2-client-0.12.2 (svn 1674)
-----------------------------
  * Corrected client/swig/python/Makefile.am to define DESTDIR to / if unset (#225)
  * Expanded error message for data on tape for SWIG extensions based on the C interface (#227)
  * Suppress messages going to terminal (#188)
	
nds2-client-0.12.1 (svn 1668)
-----------------------------
  * This addresses some build issues releated to reference platforms use by the LVC
	
nds2-client-0.12.0 (svn 1664)
-----------------------------
  * No longer using sqlite database for nds2
  * Can set epochs [start,stop) ranges to reduce channel list size on nds2 queries (also constrains data)
  * Added a version string to the SWIG interface
  * Added a set_parameter call to allow default settings on the connection to be changed
  * The system now defaults to it being an error condition when is on tape.
    - this can be overridden in the environment or through a call to set_parameter(‘ALLOW_DATA_ON_TAPE’, 1)
  * Added a new swig backend which is released as a seperate interface for testing.
    Currently exposed through the ‘nds’ module/namespace.  It is expected that this backend
    will become the default in the near future.
    - includes full gap handling (for both fetch and iterate)
    - includes the ability to query the nds2 server for preconfigured epochs
    - includes the ability to count the number of channels available
    - includes the ablilty to get detailed channel availability information
  * Added Octave bindings for the C++ interface
  * Fixed python swig bindings to support Python 3.4 on OSX
  * Disabled some unit tests when OSX System Integrity Protection is in effect.

nds2-client-0.11.6 (svn 1496)
-----------------------------
  * Fixed the nds2_connection object now clears the request_in_progress
  flag on errors (#161).
  * Updated test code and docs
  * Updated daq_test.c/nds_query to handle all error codes that the server
  will send back on a check data.

nds2-client-0.11.5 (svn 1455)
-----------------------------
  * Fixed MATLAB 2014b bug seen on Windows (#160)
  * Modified rpm spec file to build binary and noarch in a single pass.

nds2-client-0.11.4 (svn 1444)
-----------------------------
  * Fixed Building for Ubuntu 12.04 (#157)
  * Fixed handling of NDS1 protocol 12.1 status channels 2 response
	when a channel is named (#158)
  * Fixed printing of size_t values as the syntax differs between Unix and
	Windows. (#159)

nds2-client-0.11.3 (svn 1431)
-----------------------------
  * Fixed SWIG issue by requiring SWIG 2.0.11 or later (#144, #145)
  * Fixed nds_query now returns a non-zero exit code when any daq call
	fails (#139)
  * Fixed Java bindings for OSX Yosemite using MATLAB 2014b (#149)
  * Fixed requesting of trend data from nds1 servers (#137)
  * Fixed Python SWIG bindings for UINT-4 ODC Channels (#143)
  * Fixed Total nds1 channel count returned by conn.find_channels()
	to include fast data and uint32 data (#148)
  * Fixed daq_send now returns response code on socket errors (#142)
  * Fixed read_bytes() to return -1 on error, 0 for eof, > 0 for bytes
	read (#141)

nds2-client-0.11.2 (svn 1401)
-----------------------------
  * Fixed issue related to pattern matching and NDS1 servers (#134)
  * Fixed issue related to insufficient memory being allocated for
	  the request buffer when calling nds1_request_data() (#135)

nds2-client-0.11.1 (svn 1395)
-----------------------------
  * Fix man pages and debian package building
  * Check for extra arguments in nds_query list commands. reformat help.

nds2-client-0.11.0 (svn 1380)
-----------------------------
  * Separated the mex based routines from the core client library
    (ticket: 88 )
  * SWIG eliminate all database access during data retrieval
  * SWIG may specify database directory with NDS2_CHANNEL_DB_DIR envvar
  * SWIG can use read-only database
  * SWIG bugs resolved:
    -  68 - Added close method to nds2 SWIG
    - 100 - Added NDS1 fast-writer support
    - 107 - Release python's Global Interpreter Lock
    - 114 - Check channel list hash and count before initiating
	    channel list network transfers
    - 115 - Eliminated redundant on-disk records for NDS1 channel
	    cashes
    - 126 - Eliminated error when attempting to retrieve multiple
	    seconds of NDS1 data
  * Developing tools
    - Added mock nds1 server to expand breadth of unit tests.

nds2-client-0.10.5 (svn 1233)
-----------------------------
  * Fix Windows support
  * Update version number

nds2-client-0.10.5beta2 (svn 1229)
-----------------------------=====
  * Fix mex file builds for older versions of matlab

nds2-client-0.10.5beta1 (svn 1223)
-----------------------------=====
  * Update nds_query (daq_test.c) help and manual for missing command options.
  * Truncate data if server returns more data than requested, suppress message.
  * Add matlab 2014{ab} to version list

nds2-client-0.10.4 (svn 1212)
-----------------------------
  * Make the installation directory available
    via Windows Registry (Ticket #60)
    - MATLAB example:
	winqueryreg 'HKEY_LOCAL_MACHINE'
	'software\wow6432node\ligo\nds2-client'
  * Access nds2-client version from nds_query (Ticket #30)
  * Add nds2-tunnel uses environment variable NDS2_SSH to specify
	the command used to establish the ssh tunnel. The default
	is ssh.
  * Add nds_query now supports fast fetching of channel data from nds2
	servers. For backwards compatability, the option
	--channel-list-pre-fetch has been added. (Ticket #66).
  * Add nds_query now supports --server-version for nds2 servers. This
	option will now return the version and revision information
	associated with the NDS version 2 servers. The values of zero
	for both version and revision are reserved to indicate NDS2
	servers before that do not support this information. (Ticket #31)
  * Add man pages for all utility programs.
  * Fix debian build scripts

nds2-client-0.10.3 (svn 1130)
-----------------------------
  * Update release name, ChangeLog

nds2-client-0.10.3beta2 (svn 1128)
-----------------------------=====
  * Fix HAVE_GSSPI compilation flag in cmake
  * Update release name

nds2-client-0.10.3beta1 (svn 1126)
-----------------------------=====
  * Removed debugging logging (Ticket #53)
  * Replaced strsep() with strtok_t() (Ticket #56)
  * CMake
    - Added rules for building Python extensions
    - Modified method of determining numpy
      include path (Ticket #57)
  * Update debian packaging
    - Depend on swig >= 2.0.4
    - Recommend libsasl2-modules, libsasl2-modules-gssapi-mit
  * Fix size argument types in mex sources.
  * Add logging tests.
  * Fix cleanup on disconnect (Ticket ?)
    - Add methods daq_init, daq_destroy, daq_is_connected
    - Move common close to daq_connect from nds1_disconnect and nds2_disconnect
    - Protect against error on repeated disconnects.
  * Fix sockets left open (Ticket #55)

nds2-client-0.10.2 (svn 1099)
-----------------------------
  * Use AM_CPPFLAGS instead of (depricated) INCLUDE
  * Fix nds-client-config script - "javaclasspath" instead of "javaclassdir",
	return top level directory.

nds2-client-0.10.1 (svn 1092)
-----------------------------
  * Add nds-client-config script
  * Enabled logging for MATLAB users
  * documentation

nds2-client-0.10.0 (svn 1076)
-----------------------------
 * Tag for release.
 * Fixed a compiler error with SWIG 2.0.9.

nds2-client-0.10.0beta6 (svn 1072)
============================
 * Added shutdown(fd, BOTH) before close()
 * Fixed example code in daqc.h
 * Fix CMake lists to include Windows VC libraries at install time.
 * Documentation changes

nds2-client-0.10.0beta5 (svn 1059)
============================
 * Added initializing of logging from environment variable NDS_LOGGING.
 * Fixed target installcheck to work when configured from out of source
   build.
 * Fixed a regression that caused the MATLAB MEX bindings to crash MATLAB
   because they were not linked against libndsclient.
 * Fixed a regression that caused connection.find_channels to return an error
   instead of return normally with an empty channel list if no matching channels
   are found.
 * Fixed an issue that caused the MEX bindings for Octave not to work because
   of undefined symbols.
 * Added software license: GPL version 2.
nds2-client-0.10.0beta4 (svn 1017)
============================
 * Added nds.data_type_to_string and nds.channel_type_to_string.
 * Added signal_gain, signal_slope, signal_offset, and signal_units to channel
   type in SWIG bindings.
 * Added binding for nds.delete_cache.
 * Channel cache database now has a version-specific name
   (i.e., ~/.nds2-client_0.10.0beta2.sqlite).
 * Invoke sed more portably when building SWIG-Java bindings.
 * Do not cache global references to method IDs and classes in JNI code.
   The speedup was not noticeable, and we weren't releasing the references when
   the module was unloaded.
 * connection.find_channels() now supports bash-compatible pattern matching with
   '*' and '?' wildcards and nested lists of alternative patterns. For example,
   the pattern:

     'Hello, {l{on,ov}e}ly {world,planet}.'

   matches:

     'Hello, lovely world.'
 * Renamed nds.CHANNEL_TYPE_RAW, nds.DATA_TYPE_INT16, etc. to
   nds.channel.CHANNEL_TYPE_RAW, nds.channel.DATA_TYPE_INT16 to avoid
   necessity for a class in the 'default namespace' in Java.
 * Added SWIG bindings for bash_pattern.
 * Filled in missing trend channel types for NDS1 servers.
 * Addressed issue #32: "nds.jar has absolute links to the build rather than
   install directory."
 * nds.jar is now installed into $(libdir) instead of $(datadir)/java.
 * Added HTML documentation to build system.
 * On Linux, the rpath is now set when linking the MATLAB mex extensions.
 * nds2-client-user-env.{sh,csh} no longer sets LD_LIBRARY_PATH. All libraries
   and executables should be able to automatically locate the nds2-client
   library via rpath.

nds2-client-0.10.0beta2 (svn 861)
============================
 * Renamed --with-octave option to --enable-mex-octave to match style of
   --enable-swig-octave option.
 * Rewrote Makefile for MEX-Octave interface using libtool, matching style
   of SWIG-Octave interface.
 * The following options now default to 'check':
      --enable-mex-octave
      --enable-swig-octave
      --enable-swig-python
      --enable-swig-java
 * Removed now-unused Octave configuration macros.
 * Removed now-unused Java configuration macros.
 * Compile embedded sqlite engine if embedded one is not found
 * Added informational message to end of configure script to show which
   optional features are enabled.
 * Fixed building swig/octave on Debian Wheezy by replacing some Octave API
   calls that are deprecated in Octave 3.6.
 * In Java bindings, moved enum declarations from the class nds.nds to the
   class nds (which is a top-level class, but shares the same name as the
   package nds). As a result, you can now refer to nds.CHANNEL_TYPE_RAW
   instead of nds.nds.CHANNEL_TYPE_RAW.


nds2-client-0.10.0beta1 (svn 840)
============================
general
-------
 * Modified the CMake rules to build swig/java under Windows 7 64 bit
   - Now properly installs under "\Program Files" directory
   - Prompts user if the binaries should be added to the sytem PATH variable.
 * Modified autotools to calculate additional JAVA configurations
   - Under OSX, the library extension is .jnilib
 * Modified autotools to support parallel builds of the package.
 * MATLAB .m and .c files are no longer installed if MATLAB support is disabled.
 * Modified so nds2_auth is built only when request and for OSX versions 10.5
   and earlier.
 * Corrected saslcflags and sasllibs when configured for gssapi interface.

swig
----
 * Added Sqlite-based channel list cache to SWIG language interface to speed up
   requests for channel metadata.
 * Added user manual for SWIG language interface, featuring example code in Python, Octave, and MATLAB.
 * Fixed an issue that prevented reusing a connection object for multiple requests.
 * Numerous refinements of SWIG language interface.


nds2-client-0.9.1 (svn 740)
============================
<base>
------
 * configure.ac
   - Bump version number to 0.9.1

config
------
 * nds2-client.spec.in
   - Install in /usr
   - Install headers in /usr/include/nds2-client

src/client
----------
 * daqc.h daqc_access.c nds1_access.c nds2_access.c nds2.h 
   - Add daq_set_epoch() function

 * nds1_access.c nds2_access.c 
   - Remove waits from daq_connect (use select intstead)
   - Limit stride to 2Gs
   - Use correct length type for write() on windows.
   - reserve enough space for command

 * daqc_internal.c nds_auth.c
   - fix data type warnings.

src/util
--------
  * daq_test.c
    - Add --epoch argument
    - Prevent SEGV from missing command line arguments
  * nds2_channel_source.c
    - Fix help message

swig
----
  * hava/ octave/ python/
    - new swig wrappers.

nds2-client-0.9.0c (svn 654)
============================
<base>
------
 * configure.ac
   - Bump version number to 0.9.0c
   - require errno.h and stdio.h header files.
   - Disable octave builds on Darwin platforms
 * Addressed several Windows compilation issues.
 * Added support for MATLAB 2012a

config
------
  * ac_nds2c.m4
    - Fix octave extension (.mex) symbol (OCTAVE_EXT).
    - switch to POSIX_C_SOURCE=200112
    - Empty sasl flags no longer get expanded to -L/lib or -I/include
  * ccmex2.m4
    - Fix parsing of matlab startup message to extract versions 2011b and 2012a
    - Added --with-matlab-prefix option to configure prefixes used for
      matlab installations.
  * matlab-mk-makefile
    - Insert $(MEX_EXTRA_CFLAGS) in mex-compile command (to add e.g. -g)
  * nds-client.spec.in
    - add matlab2012b code.

src/client
----------
 * Makefile
   - Added daqc_response.h to list of installed headers.
   - Add nds_log.h to the noinst_HEADER list. 

 * daqc.h
   - C interfaces for C++.
   - Add private data structure and accessors.
   - New data_private structure, move system dependencies to private.

 * daqc_access.c daqc_listener.c daqc_internal.c nds_auth.c 
   nds1_access.c nds2_access.c
   - move error logging to nds_logging package.
   - Move system dependent code to private structure (conceal).
   - Recast data to fix type warnings

 * daqc_internal.c
   - Disable -Wconversion flag arouns ntohs, htons 

 * nds_auth.c
   - Search for GSSAPI authentication made position independent.

src/matlab
----------
 * NDS_GetChannels.c NDS2_GetChannels.c
   - Remove use mxFree to free string allocated by mxAlloc
 * NDS2_GetData.c
   - Recast real data pointer to mxDataReal_type
 * nds_mex_utils.c
   - remove erroneous strdup()

src/octave
----------
 * Makefile.am
   - Disable octave builds on Darwin platforms

src/utils
----------
  * daq_test.c
    - Use new data accessors to fet from private members.
    - Add debugging printouts using new logging mechanism.

nds2-client-0.8.2 (svn 583)
===========================
<base>
------
 * Changes
   - Add changes for nds2-client-0.8.2
 * configure.ac
   - Change version to 0.8.2

src/client
----------
 * daqc_internal.h daqc_access.c
   - Define PRISIZE_T only if SIZEOF_xxx macros have been defined.

nds2-client-0.8.1 (svn 581)
===========================
<base>
------
 * Changes
   - Add changes for nds2-client-0.8.1
 * configure.ac
   - Change version to 0.8.1

config
------
 * matlab-mk-pkg-config
   - Allow x86_64 on Darwin

src/client
----------
 * daqc.h
   - Move include of daq_net.h so that it affects only Windows builds.
 * nds1_access.c
   - Fix "start net-writer" command for online requests.

src/octave
----------
 * Makefile.am
   - Rename make symbols from OCTAVE_CFLAGS and OCTAVE_LDFLAGS to  
     OCTAVECFLAGS and OCTAVELDFLAGS to make automake/libtool happy.

src/util
--------
 * Makefile.am
   - Fix nds2_auth build code - spaces instead of tabs!


nds2-client-0.8.0 (svn 566)
===========================
<base>
------
 * Changes
   - Add changes for nds2-client-0.8.0
 * INSTALL.txt
   -
 * configure.ac Makefile.am CMakeLists.txt
   - Change version to 0.8.0
   - add test for (v)snprintf_s
   - add examples build

config
------
  * matlab.m4 matlab.mk
    - Matlab 2011{a,b} support
  * nds2-client-user-env.sh.in nds2-client-user-env.csh.in
    - prepend directories to path variables if not already there.

config_cmake
------------
  * FindGSSAPI.cmake FindMatlabMulti.cmake
    - win7 64-bit support

debian
------
 * control changelog changelog.lenny changelog.squeeze copyright files 
   nds2-client.dirs nds2-client-all.dirs nds2-client-dev.dirs 
   nds2-client-lib.dirs nds2-client-matlab.dirs rules t_arch
   - debian build code

examples
--------
 * demo1.m demo2.m diff_nds_12.m Makefile.am
   - matlab test scripts

src/client
----------
 * nds_log.c nds_log.h config_cmake.h.in nds2_access.c Makefile.am
   CMakeLists.txt
   - Add log-file.
 * daqc_internal.c daqc_internal.h nds_auth.c channel.h daqc.h daqc_listener.c
   nds1_access.c nds2_access.c trench.h daqc_access.c
   - Windows 7: define socket type, defined DLL_EXPORT.
 * daqc_internal.c daqc_internal.h nds1.h channel.h daqc.h nds1_access.c
   - Support NDS protocol 12.
 * daqc_access.c
   - Add static channel type

src/matlab
----------
 * Makefile.am
   - Added support for MATLAB 2011a and 2011b
 * nds_log_matlab.c
   - Add log-file.
 * NDS2_GetData.c NDS_GetData.c NDS_GetChannels.c NDS2_GetChannels.c 
   nds_mex_utils.h nds_mex_utils.c
   - Compilation fixups
   - Modified to use MATLAB memory management routines
     to prevent crashing under Windows 7.

src/util
--------
 * daq_test.c
   - verify minute trend requests are for multiples of 60s.

nds2-client-0.7.2
=================
<base>
------
 * Changes
   - Add changes for nds2-client-0.7.2
 * configure.ac
   - Bump version

src/client
----------
  * daqc.h daqc_access.c daqc_internal.c daqc_listener.c daqc_listener.h 
    daqc_net.h Makefile.am nds1_access.c nds2_access.c
    - Replace ip address (struct sockaddr_in) with a pointer
    - move socket-dependent includes to daqc_net.h
    - include daqc_net.h in daqc_access.c, daqc_internal.c, daqc_listener.c, 
      nds1_access.c and nds2_access.c 
    - Back out daq_conf change from Makefile.am
  * nds2_access.c daqc_access.c
    - Remove debug prints
    - Force DAQD_SASL error code from nds_authenticate failure.
    - Don't try nds1 protocol if DAQD_SASL error is returned.

nds2-client-0.7.1
=================
config
------
  * configure.ac
    - Define $SED (fix "-e: command not found" messages in configure)
  * config/ac_nds2c.m4
    - Fix quoting

src/client
----------
  * Makefile.am
    - install daq_config.h

nds2-client-0.7.0
=================
** General **
-------------
  * Added Windows XP distribution
  * Added support for MATLAB 2010b

<base>
------
  * configure.ac
    - Update revision number
    - configure header src/client/daq_config.h
    - Get type information as needed for platform independence (size_t,
      int, long, long long, time_t).
    - Check funcs as needed for platform independence (closesocket, sprintf_s)
    - add LT_INIT

config
------
  * matlab.m4 matlab.mk matlab-mk-makefile 
    - add matlab-2010b to supported version list.
    - add client build directory and $DEFS into compilation flags.
  * nds2-client.spec.in
    - add matlab-2010b to supported version list.
    - fix matlab multiple version install
  * ac_nds2c.m4
    - add --with-gssapi configure argument

config_cmake
------------
  * FindSASL.cmake FindGSSAPI.cmake FindMatlabMulti.cmake CMakeLists.txt
    - Cmake build scripts for Windows (and OSX?)

src
---
  * CMakeLists.txt
    - Cmake build scripts for Windows (and others?)

src/client
----------
  * nds_auth.c nds_auth.h nds_os.c nds_os.h
    - New file: Move platform / auth-mechanism dependence to one place.
  * daq_config.h.in
    - new configuration parameters header file
  * daqc_internal.c daqc_internal.h
    - Standard statement ordering.
    - Use windows functions if _WIN32
    - Add internal function _daq_read_cstring(), _daq_cvt_ll_string() and
      nds_perror() for platform independence
    - Add end pointer to _data_get_string
  * channel.h daqc.h daqc_internal.h daqc_listener.h trench.c daqc_access.c
    - Don't include <unistd.h>, <netdb.h> or <inttypes.h> if not defined 
    - Platform independence
  * daqc_listener.c
    - Compile on windows.
  * config_cmake.h.in
  * nds2_access.c nds2_access.h
    - Move auth functions to nds_auth
    - Standard statement ordering.
  * Makefile.am
    - Add nds_os and nds_auth compilation.

src/matlab
----------
  * Makefile.am
    - unparallel
    - Support matlab-2010b
    - Drag in defs
  * CMakeLists.txt
    - Cmake build scripts for Windows (and others?)

src/matlab
----------
  * Makefile.am
    - add client build directory, $DEFS, and -DHAVE_CONFIG_H into 
      compilation flags.

src/util
--------
  * Makefile.am
    - daq_test has been renamed to nds_query

  * daq_test.c
    - Changed name of program to nds_query.
    - Arranged variable declaration to conform to C standard.
    - Add user specified dump format

  * CMakeLists.txt
    - Cmake build scripts for Windows (and others?)

nds2-client-0.6.6
=================
src/client
----------
  * trench.c trench.h
    - Add new function trench_infer_chan_info()
    - Add data type argument to trench_dtype(), min/max channels for int
      data are int4 
    - Remove channel meta-data inference from trench_parse()
  * nds1_access.c
    - Fix null termination of channel name and unit strings in channel
      list unpacking.
  * daqc_access.c
    - Fix data size calculation (avoid roundoff error for rates < 1Hz)
    - Fix channel rate/type inference: Use trench_infer_chan_info for NDS1 
      channels.
    - Fix memory leak in daq_init_channel()

src/matlab
----------
  * NDS_GetData.c
    - Increase throttle limit to 128MB
    - Combine channel selection procedure to a single pass.
    - Change channel selection implementation to use trench_infer_chan_info
    - Fix throttle value printout to MB.
    - Fix allocation size calculation (avoid roundoff error for rates < 1Hz)

src/util
--------
  * daq_test.c
    - Fix buffer loop when partial blocks returned (by NDS1).
    - Fix channel rate/type inference: Use trench_infer_chan_info for NDS1 
      channels.
    - Fix small memory leak.
    - Free channel list storage when finished.

nds2-client-0.6.5
=================
config
------
  * Makefile.am nds2-client-user-env.csh.in nds2-client-user-env.sh.in
    - Add lscsoft standard user environment setup scripts.
  * nds2-client.spec.in
    - install %prefix/etc files in the correct place.

src/client
----------
  * channel.h
    - fix doxygen comment.

src/matlab
----------
  * NDS_GetData.c
    - Loop over data buffer receivs, just in case NDS doesn't give you all
      the requested data.
  * NDS_GetData.c NDS2_GetData.c
    - Remove unused macros.
  * NDS_GetData.m NDS2_GetData.m NDS_GetChannels.m NDS2_GetChannels.m
    - Improve help message formatting.

nds2-client-0.6.4
=================
config
------
  * configure.ac
    - Fix quoting (fix from Adam).
    - Update version number
  * ccmex2.m4
    - Don't add "/x86_64" suffix on Dawwin systems
    - Move "checking usability" status message
  * nds2-client.spec.in
    - Add compatibility and help .m files to matlab-devel file list

src/matlab
----------
  * Makefile.am
    - Pass dejaGnu scripts an absolute test script directory path.
  * testsuite/config/default.exp
    - Specify command as 'sh -c "..."' rather than '(...)' 

src/client
----------
  * daqc.h
    - Fix spelling of comments.
    - Document meaning of *num_channels_received argument to daq_recv_channels
      and daq_recv_channel_list.
    - Document online data access with daq_request_data.

nds2-client-0.6.3
=================
config
------
  * configure.ac
    - update version number to 0.6.3
  * ac_nds2c.m4
    - Test whether the octave installation is compatible with this build
  * nds2-client-spec.in
    - Spell extension correctly
    - remove all share/matlab files from platform-specific rpms
    - add '/*' to matlab directory file lists

src/matlab
----------
  * nds_mex_utils.c nds_mex_utils.h
    - use size_t for array indices.
  * NDS_GetData.c NDS_GetChannels.c NDS2_GetData.c NDS2_GetChannels.c
    - Specify message buffer length as macro and const size_t variable.
    - Use double for rate and signal unit transformations.
    - Use size_t for looping over indices.
    - Destroy temporary array after loop.
    - Use put_mxarray_bool and put_mxarray_str where appropriate.
    - Fix variable type conversion warnings.
    - convert error codes to strings.

nds2-client-0.6.1
=================
config
------
  * configure.ac ax_cflags_gcc_option.m4
    - Force on -Wconversion, -Wformat and -Wlong-long compilation options 
      if valid.
    - Add Mac OSX (darwing) platform detection for nds2_auth build.

src/client
----------
  * daqc_internal.c daqc_internal.h nds1.h nds2.h channel.h daqc.h 
    daqc_listener.c nds1_access.c nds2_access.c daqc_access.c
    - Remove type conversions or make them explicit.
    - change signature of: swap_data(), read_bytes(), _daq_read_string(),
      _daq_get_string(), nds1_receive_reconfigure(), nds2_recv_source_list(),
      nds2_receive_reconfigure(), data_type_size(), data_type_max(), 
      data_type_name(), data_type_word(), daq_get_channel_addr(),
      daq_get_channel_data(), daq_get_data_length(), daq_get_channel_status(),
      daq_get_scaled_data(), daq_recv_source_list(), daq_request_channel()
    - move variable declarations to top of scope

src/matlab
----------
  * NDS2_GetData.c NDS_GetData.c
    - allow row- or column-vector channel lists.
  * NDS_GetSecondTrend.m NDS_GetMinuteTrend.m
    - Add matlab scripts to implement bacward compatible trend data access. 
  * Makefile.am NDS_GetChannels.m NDS_GetData.m NDS2_GetChannels.m 
    NDS2_GetData.m
    - Add help files for matlab commands
  * Use float for rate returned by NDS2_GetData.c.

src/util
--------
  * nds2_auth.c Makefile.am
    - New executable to be built as 32-bit under Mac OSX for use on OSX-10.5
      (Leopard) as work-around for 64-bit GSSAPI ticket bug.
  * nds2_channel_source.c daq_test.c
    - Remove type conversions or make them explicit.

nds2-client-0.6.0
=================
config
------
  * configure.ac matlab.m4 nds-find-requires ccmex2.m4 matlab-mk-makefile
    nds2-client.spec.in matlab.mk matlab-mk-pkg-config ac_nds2c.m4 
    nds2-client.spec ccmex.m4
    - add multi-version matlab build and octave
    - add spec file for rpm
doc
---
  * Doxyfile
    - issue warning if parameter or return values are undocumented.

src/client
----------
   * daqc_listener.h daqc_internal.h daqc_response.h channel.h daqc.h
     - Add/improve doxygen comments.
   * daqc_internal.c nds2_access.c daq_access.c daqc_internal.h
     - Remove read_long (use read_uint4)
     - Replace length fields with uint4_type
     - rename read_string, get_string, wait_data and cvt_string (prefix _daq_)
   * nds1_access.c
     - Infer unknown data types as raw/online depending on start gps.
     - close socket, mark closed if connection fails.
   * nds2_access.c nds2.h
     - Print error codes with daq_strerror()
     - Move channel name string generation into a static function.
     - Add nds2_recv_source_list function.
     - close socket, mark closed if connection fails.
     - destroy sasl context if auth fails.
   * trench.h trench.c Makefile.am
     - New channel specifier parser
   * daqc_access.c daqc.h
     - Add err_num error code
     - Set err_num in functions returning length/-1.
     - Add daq_recv_source_list() function
     - Add/use daq_init_channel function - initialize daq_channel_t structure.
     - move daq_recv_next outside of daq_request_data
     - return DAQD_NOT_CONFIGURED if not nds_v1 or nds_v2
     - add protocol version fallback code.
     - add daq_strerror()
   * daqc.h channel.h
     - move enum chantype from daqc.h to channel.h
     - chantype conversion functions to channel.h

src/matlab
----------
  * NDS_GetChannels.c nds_mex_utils.c nds_mex_utils.h
    - Switch sample rate in channel cell array to float.
    - fix get_mxarray_float to pick correct data type from field.
  * NDS_GetData.c
    - Parse channel names including trend suffix and channel type
    - remove mapping to channel list index (to allow trends)
    - Read sample rate as float
    - enable fall-back to nds1 protocol
  * NDS2_GetData.c
    - print errors with daq_strerror()
    - move daq_recv_next outside of daq_request_data
    - enable fall-back to nds1 protocol
  * testsuite/config/default.exp testsuite/matlab-all/loadability.m
    testsuite/matlab-all/load.exp Makefile.am
    - test suite for matlab installations.

src/octave
----------
   * Makefile.am ../Makefile.am
     - Make octave version of mex files when enables.
     - Diable octave with --without-octave

src/util
--------
  * daq_test.c 
    - Parse channel names including trend suffix and channel type
    - Infer rate and data type of trend channels.
  * nds2_channel_source.c Makefile.am
    - New program to list nds2 channel sources.

nds2-client-0.5.2
=================
src/client
----------
 * Makefile.am
   - Generate nds2-client.pc

src/config
----------
 * configure.ac
   - set version
   - Generate nds2-client.pc
 * ccmex.m4
   - Add maci64 target.

src/matlab
----------
 * NDS2_GetData.c NDS_GetData.c 
   - Missing include <tdio.h>

src/util
--------
 * daq_test.c
   - fix compiler warning: incorrect printf format.
   
nds2-client-0.5.1
=================
src/client
----------
 * daqc.h
   - Fix nds1 protocol
 * daqc_access.c
   - Extend response wait time to 3s from 1s
   - add command text id response fails.
   - Fix bug in daq_recv_channel_list: don't request nds2 channel list after
     nds1.
 * daqc_internal.c
   - print more info when error is encountered
 * nds1_access.c
   - Fix nds1 channel list interface
 * nds2_access.c
   - fix compilation warning: unused variable.

src/matlab
----------
 * NDS2_GetChannels.c
   - test that malloc succedes. Return error if not.
 * NDS2_GetData.c NDS_GetChannels.c NDS_GetData.c
   - Fix compilation warnings

src/util
--------
 * daq_test.c
   - fix compilation warnings.
   - Change default channel type from cOnline -> cUnknown.
   - Change default port to 0.
   - Add -1 and -2 command flags to select nds1/nds2 protocol.
   - test for number of requested channels, fail if 0.
   - get channel count with daq_recv_channel_list(&daq, 0, 0, &n, gps, c_type)

nds2-client-0.5.0
=================
 * Makefile.am
   - Set up for autoreconf
   - Add <cr> to end of file. 
 * configure.ac
   - Set up for sutoreconf
   - Set package name/version
 * config/nds2-client.spec
   - update version.
   - remove unnecessary --without-gds configure option
 * src/client/Makefile.am
   - move missing header files to include.
 * src/matlab/Makefile.am
   - Distribute matlab sources.
 * src/util/Makefile.am
   - Fix nds2-tunnel distribution.

nds2-client-0.4.5
-----------------
 * config/acx_pthread.m4 configure.ac src/client/Makefile.am 
   src/util/Makefile.am
   - use ACX_PTHREAD to configure Posix thread library
 * src/client/daqc.h src/client/daqc_access.c src/client/daqc_access.c
   src/client/nds1_access.c src/client/nds2_access.c
   - Bugfix: Swap bytes for 2nd-last buffer in a trqansfer
   - Move byte-swap code to daq_recv_next src/util/daq_test.c
   - Redefine daq_recv_next return code.
 * src/util/daq_test.c
   - expand channel list.

nds2-client-0.4.4
-----------------
 * src/client/channel.h
   - extend maximum channel length to 64
 * src/client/nds2_access.c
   - Bugfix: Rate is a floating point number - use strtod to read.
 * src/util/daq_test.c
   - Parse default channel-type argument
   - Use daq_recv_channel_list, specify time and default channel type
     in request.


nds2-client-0.4.3
-----------------
 * src/client/daqc_access.c src/client/daqc_internal.c 
   src/client/nds1_access.c src/client/nds2_access.c
   - Work around obsolete values.h header
 * src/client/daqc_internal.c
   - Fix for apple
 * src/matlab/Makefile.am src/util/Makefile.am
   - build in parallel directory

nds2-client-0.4.2
-----------------
 * src/client/daqc_access.c
   - make default channel type cUnknown.

nds2-client-0.4.1
-----------------
 * src/client/daqc_access.c
   - initialize signal parameters when a channels is requested.
 * src/client/nds2_access.c
   - free command buffer if authorization fails
   - use count-channel command to get number of channel entries.
   - unpack {channel_type, data_type} from status block.
   - allow emty lit in recv_channel_list.
 * src/matlab/NDS2_GetChannels.c src/matlab/NDS2_GetData.c
   - New nds2 protocol clients
 * src/matlab/Makefile.am
   - compile/link NDS2_GetChannels and NDS2_GetChannels 
 * src/matlab/nds_mex_utils.h src/matlab/nds_mex_utils.h
   - add put_mxarray_bool and put_mxarray_str functions.
   - include string.h

nds2-client-0.4.0
-----------------
 * configure.ac config/ac_nds2c.m4 config/ccmex.m4 src/Makefile.am
   - Parse --enable-64-bit
   - configure matlab/mex
   - Automake src/matlab/Makefile.am
 * src/client/daqc_access.c
   - fix bug in daq_get_channel_status (missing '!')
 * src/client/daqc_internal.h
   - Fix type of rc
   - use c comment after #endif
 * src/client/nds2_access.c
   - use recv rather than read 1 byte in nds2_gets().
   - iniitialize/set fields not read in by nds2_recv_channel_list.
   - once-only initialization of sasl client.
 * src/matlab/Makefile.am src/matlab/NDS_GetChannels.c src/matlab/NDS_GetData.c
   src/matlab/nds_mex_utils.c src/matlab/nds_mex_utils.h
   - New matlab client interface

nds2-client-0.3.3
-----------------
 * src/client/daqc_access.c
   - redefine get-data protocol to match nds1 (no online word before 
     each block)
 * src/client/nds1.h nds1_access.c
   - remove nds1_recv_next
   - use daq_recv_next instead of nds1_recv_next
 * src/client/nds2.h nds2_access.c
   - remove nds2_recv_next
   - use daq_recv_next instead of nds2_recv_next
   - free command text immediately after send (prevent leak on error)

nds2-client-0.3.0
-----------------
 * configure.ac
   - add call to AC_NDS2C_CHECK_SASL
   - remove unnecessary package checks (comments)
 * client/config/ac_nds2c.m4
   - add AC_NDS2C_CHECK_SASL
 * src/client/Makefile.am
   - Link sasl libraries
 * src/client/daqc.h src/client/daqc_access.c
   - Add authentication context pointer to daq_t
   - Add daq_startup function
   - make daq_send() buffer argument const.
   - make protocol-dependent daq_disconnect
   - test for authentication response
 * src/client/daqc_internal.c
   - Add length/error checking
 * src/client/daqc_response.h
   - Add DAQ_SASL code (authentication requuired)
 * src/client/nds1.h src/client/nds1_access.c
   - add nds1_disconnect, nds1_initalize, nds1_startup
 * src/client/nds2.h src/client/nds2_access.c
   - add protocol-specific nds1_disconnect, nds1_startup
   - add nds2_authenticate (SASL authentication protocol)
 * src/util/Makefile.am
   - include sasl libraries
 * src/util/daq_test.c
   - Add daq_startup to do sasl initialization

nds2-client-0.2.3
-----------------
 * client/daqc_access.c
   - return 1 from data_type_size() for undefined data types.
 * client/daqc_internal.c
   - remove duplicate #defines
   - include <netdb.h>
   - Use union instead of pointer casts.
   - use explicit length data types, e.g. int32_t where needed.
 * util/daq_test.c
   - dump double and int32 type data
   - enforce dt <= end_gps - start_gps
 * util/Makefile.am
   - link daq_test with posix threads

nds2-client-0.2.2
-----------------
 * client/daqc_access.c client/channel.h
   - add 'data_type_code(const char* name)' function.
   - standardize type names to "<type>_<nbytes>", e.g. "int_2" or "real_8"
 * client/daqc_internal.c client/daqc_internal.h
   - skip leading blanks in get_string()
 * client/nds2_access.c
   - Add data type to nds2 channel list string protocol.
   - Add semi-colon to end of get-channels request.
 * util/daq_test.c
   - Print out data type in channel list.
   - Execute a specified command.
 * util/nds2-tunnel util/Makefile.am
   - new script to create an ssh tunnel to the default/specified server.
