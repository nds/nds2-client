//
// Created by jonathan.hanks on 5/4/18.
//

#ifndef NDS2_CLIENT_COMMONSPAN_READER_HH
#define NDS2_CLIENT_COMMONSPAN_READER_HH

#include <algorithm>
#include <stdexcept>

#include "common/utils.hh"

namespace nds_impl
{
    namespace common
    {

        class SpanReader
        {
        public:
            typedef nds_impl::common::Span< char > span_type;

            SpanReader( span_type& s ) : s_{ s }, cur_{ 0 }
            {
            }

            char*
            write_all( const char* begin, const char* end )
            {
                throw std::runtime_error(
                    "Write operations not supported on the spanreader" );
            }

            /**
             * A read the data that is currently available.
             * @param start Start address of the destination
             * @param end Address of the byte past the last in the destination
             * @return A pointer in [start, end] that is the next location
             * @note raises an exception if there is no more data
             */
            char*
            read_available( char* start, char* end )
            {
                span_type::size_type len_requested = end - start;

                if ( remaining( ) == 0 )
                {
                    throw std::range_error( "No more data to read" );
                }

                if ( remaining( ) < len_requested )
                    len_requested = remaining( );
                std::copy( cur_data( ), cur_data( ) + len_requested, start );
                consume( len_requested );
                return start + len_requested;
            }

            /**
             * Read exactly enough data to fill the range [start,end)
             * @param start Start of the destination
             * @param end Byte just past the end of the destination
             * @return end
             */
            char*
            read_exactly( char* start, const char* end )
            {
                span_type::size_type len_requested = end - start;

                auto result = peek( start, end );
                consume( len_requested );
                return result;
            }

            /**
             *  Return the next data (enough to fill [start, end)).  No data
             * will
             *  be removed from the internal buffer.
             *  The data will still be avaialble in the buffered reader for
             * retreival.
             *  @param start Start of the destination
             *  @param end Byte just past the end of the destination
             *  @return end
             */
            char*
            peek( char* start, const char* end )
            {
                span_type::size_type len_requested = end - start;

                while ( remaining( ) < len_requested )
                {
                    throw std::range_error( "Insufficient data for request" );
                }
                std::copy( cur_data( ), cur_data( ) + len_requested, start );
                return start + len_requested;
            }

            /**
             * Read until a value in [begin_set,end_set) is found in the input
             * stream.
             * @tparam It An iterator that dereferences chars
             * @param begin_set Start of the list of terminators
             * @param end_set End of the list of terminators
             * @return A std::vector<char> of the data that was read in,
             * including the terminator that was found.
             *
             * This will retrieve data from the underlying reader as needed
             */
            template < typename It >
            std::vector< char >
            read_until( It begin_set, It end_set )
            {
                std::vector< char > result;

                span_type::size_type cur = 0;
                auto                 rem = remaining( );
                for ( bool match = false; !match; ++cur, --rem )
                {
                    if ( rem == 0 )
                    {
                        throw std::range_error(
                            "Insufficient data for request" );
                    }

                    if ( std::find( begin_set, end_set, cur_data( )[ cur ] ) !=
                         end_set )
                    {
                        match = true;
                    }
                }
                result.resize( cur );
                std::copy( cur_data( ), cur_data( ) + cur, result.begin( ) );
                consume( cur );
                return result;
            }

            /**
             * Read until a value in [begin_set,end_set) is found in the input
             * stream.  This will write a bounded amount of data up to
             * [dest_start, dest_end)
             * @tparam It An iterator that dereferences chars
             * @param begin_set Start of the list of terminators
             * @param end_set End of the list of terminators
             * @param dest_start Start of the destination
             * @param dest_end one past the last destination point
             *
             * This will retrieve data from the underlying reader as needed
             */
            template < typename It, typename OutIt >
            OutIt
            read_until( It    begin_set,
                        It    end_set,
                        OutIt dest_start,
                        OutIt dest_end )
            {
                span_type::size_type cur = 0;
                auto                 rem = remaining( );
                OutIt                dest_cur = dest_start;
                bool                 match = false;
                for ( ; !match && dest_cur != dest_end;
                      ++cur, --rem, ++dest_cur )
                {
                    if ( rem == 0 )
                    {
                        throw std::range_error(
                            "Insufficient data for request" );
                    }

                    if ( std::find( begin_set, end_set, cur_data( )[ cur ] ) !=
                         end_set )
                    {
                        match = true;
                    }
                }
                if ( match )
                {
                    std::copy( cur_data( ), cur_data( ) + cur, dest_start );
                }
                else
                {
                    throw std::range_error(
                        "Unable to find matching sequence" );
                }
                consume( cur );
                return dest_cur;
            }

        private:
            void
            consume( span_type::size_type count )
            {
                if ( count > remaining( ) )
                {
                    count = remaining( );
                }
                cur_ += count;
            }

            span_type::size_type
            remaining( )
            {
                return s_.size( ) - cur_;
            }

            char*
            cur_data( )
            {
                return s_.data( ) + cur_;
            }

            span_type            s_;
            span_type::size_type cur_ = 0;
        };
    }
}

//////////////////////////////////////////
// Tests only after this point.
/////////

#ifdef _NDS_IMPL_ENABLE_CATCH_TESTS_

#include <string>
#include "catch.hpp"

TEST_CASE( "You can create a span reader class", "[common,span_reader]" )
{
    std::string                    tmp{ "abc" };
    nds_impl::common::Span< char > s{ const_cast< char* >( tmp.data( ) ),
                                      tmp.size( ) };
    nds_impl::common::SpanReader r{ s };
}

TEST_CASE( "You can cannot write to a span_reader", "[common,span_reader]" )
{
    std::string                    tmp{ "abc" };
    nds_impl::common::Span< char > s{ const_cast< char* >( tmp.data( ) ),
                                      tmp.size( ) };
    nds_impl::common::SpanReader r{ s };
    std::string                  tmp2{ "ABC" };
    REQUIRE_THROWS( r.write_all( tmp2.data( ), tmp2.data( ) + tmp2.size( ) ) );
}

TEST_CASE( "You can read from a span reader", "[span_reader,read]" )
{
    std::vector< char > dest( 20 );

    auto                           hello = std::string{ "hello world!" };
    nds_impl::common::Span< char > s{ const_cast< char* >( hello.data( ) ),
                                      hello.size( ) };
    nds_impl::common::SpanReader r{ s };

    auto end = r.read_available( dest.data( ), dest.data( ) + dest.size( ) );
    REQUIRE( end != dest.data( ) );
    REQUIRE( end == dest.data( ) + hello.size( ) );
    auto output = std::string( dest.data( ), end );
    REQUIRE( output == hello );
}

TEST_CASE( "You can read the data in small segments from a span reader",
           "[span_reader,read]" )
{
    std::vector< char > dest( 5 );

    auto                           hello = std::string{ "hello world!" };
    nds_impl::common::Span< char > s{ const_cast< char* >( hello.data( ) ),
                                      hello.size( ) };

    nds_impl::common::SpanReader r{ s };

    auto end = r.read_available( dest.data( ), dest.data( ) + dest.size( ) );
    REQUIRE( end != dest.data( ) );
    REQUIRE( end == dest.data( ) + dest.size( ) );
    auto output = std::string( dest.data( ), end );
    REQUIRE( output == hello.substr( 0, dest.size( ) ) );

    end = r.read_available( dest.data( ), dest.data( ) + dest.size( ) );
    REQUIRE( end != dest.data( ) );
    REQUIRE( end == dest.data( ) + dest.size( ) );
    output = std::string( dest.data( ), end );
    REQUIRE( output == hello.substr( dest.size( ), dest.size( ) ) );
}

TEST_CASE( "You can ask for an exact amount of bytes to be returned from a "
           "span_reader",
           "[span_reader,read]" )
{
    std::vector< char > dest( 10 );

    auto                           input = std::string{ "0123456789" };
    nds_impl::common::Span< char > s{ const_cast< char* >( input.data( ) ),
                                      input.size( ) };
    nds_impl::common::SpanReader r{ s };

    auto stride = 4;
    auto end = r.read_exactly( dest.data( ), dest.data( ) + stride );
    REQUIRE( end == dest.data( ) + stride );

    REQUIRE_THROWS(
        r.read_exactly( dest.data( ), dest.data( ) + dest.size( ) ) );
}

TEST_CASE( "You can peek at an exact amount of data without consuming it from "
           "a span reader",
           "[span_reader,read]" )
{
    std::vector< char > dest( 10 );
    auto                input = std::string{ "0123456789" };

    nds_impl::common::Span< char > s{ const_cast< char* >( input.data( ) ),
                                      input.size( ) };
    nds_impl::common::SpanReader r{ s };

    auto stride = 4;
    auto end = r.peek( dest.data( ), dest.data( ) + stride );
    REQUIRE( end == dest.data( ) + stride );
    REQUIRE( std::equal( dest.data( ), dest.data( ) + stride, input.data( ) ) );

    // clear the buffer
    std::fill( dest.begin( ), dest.end( ), 0 );
    REQUIRE(
        !std::equal( dest.data( ), dest.data( ) + stride, input.data( ) ) );

    end = r.read_exactly( dest.data( ), dest.data( ) + stride );
    REQUIRE( end == dest.data( ) + stride );
    REQUIRE( std::equal( dest.data( ), dest.data( ) + stride, input.data( ) ) );
}

TEST_CASE( "You can ask a span reader to read until a sequence is found",
           "[buffered,read_until]" )
{
    auto                           input = std::string{ "0123456789" };
    nds_impl::common::Span< char > s{ const_cast< char* >( input.data( ) ),
                                      input.size( ) };
    nds_impl::common::SpanReader r{ s };

    auto terminators = std::string( "59" );
    auto result = r.read_until( terminators.begin( ), terminators.end( ) );
    REQUIRE( std::string( result.begin( ), result.end( ) ) ==
             std::string( "012345" ) );
}

TEST_CASE( "You can ask a span reader to read until a sequence is found, "
           "with a bound",
           "[span_reader,read_until]" )
{
    std::vector< char >            dest( 5 );
    auto                           input = std::string{ "0123456789abcdef" };
    nds_impl::common::Span< char > s{ const_cast< char* >( input.data( ) ),
                                      input.size( ) };
    nds_impl::common::SpanReader r{ s };

    std::fill( dest.begin( ), dest.end( ), 0 );
    auto terminators = std::string( "29" );
    auto end = r.read_until(
        terminators.begin( ), terminators.end( ), dest.begin( ), dest.end( ) );
    auto length = std::distance( dest.begin( ), end );
    REQUIRE( length == 3 );
    REQUIRE( std::string( dest.data( ), length ) == std::string( "012" ) );
}

TEST_CASE(
    "A bounded span_reader read_until will throw an exception if it cannot "
    "find a sequence",
    "[span_reader,read_until]" )
{
    std::vector< char >            dest( 5 );
    auto                           input = std::string{ "0123456789abcdef" };
    nds_impl::common::Span< char > s{ const_cast< char* >( input.data( ) ),
                                      input.size( ) };
    nds_impl::common::SpanReader r{ s };

    auto terminators = std::string( "Z" );
    REQUIRE_THROWS_AS( r.read_until( terminators.begin( ),
                                     terminators.end( ),
                                     dest.begin( ),
                                     dest.end( ) ),
                       std::range_error );
}

TEST_CASE(
    "A bounded span_reader read_until will throw an exception if it cannot "
    "find a sequence when input is empty",
    "[span_reader,read_until]" )
{
    std::vector< char >            dest( 5 );
    auto                           input = std::string{ "" };
    nds_impl::common::Span< char > s{ const_cast< char* >( input.data( ) ),
                                      input.size( ) };
    nds_impl::common::SpanReader r{ s };

    auto terminators = std::string( "Z" );
    REQUIRE_THROWS_AS( r.read_until( terminators.begin( ),
                                     terminators.end( ),
                                     dest.begin( ),
                                     dest.end( ) ),
                       std::runtime_error );
}

#endif // _NDS_IMPL_ENABLE_CATCH_TESTS_

#endif // NDS2_CLIENT_COMMON_SPAN_READER_HH
