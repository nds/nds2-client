//
// Created by jonathan.hanks on 5/4/18.
//

#ifndef NDS2_CLIENT_NDS1_CHANNEL_LISTING_HH
#define NDS2_CLIENT_NDS1_CHANNEL_LISTING_HH

#include <algorithm>
#include <array>
#include <iterator>

#include "common/utils.hh"
#include "nds1/common/basic_io.hh"

namespace nds_impl
{
    namespace nds1
    {
        namespace v12_2
        {

            typedef std::array< char, 148 > raw_sc2_channel;

            template < typename Reader, typename Transform >
            class ChannelListIterator
            {

            public:
                typedef typename std::result_of< decltype (
                    &Transform::operator( ) )(
                    Transform, raw_sc2_channel ) >::type value_type;
                typedef value_type&             reference;
                typedef value_type*             pointer;
                typedef std::size_t             difference_type;
                typedef std::input_iterator_tag iterator_category;

                ChannelListIterator( Reader& r, Transform& t )
                    : pos_{ 0 }, end_{ 0 }, t_( t ), r_( r ), cur_{}
                {
                }
                ChannelListIterator( Reader& r, Transform& t, int pos, int end )
                    : pos_{ pos }, end_{ end }, t_( t ), r_( r ),
                      cur_( t( read_in_sc2_channel( r ) ) )
                {
                }
                ChannelListIterator( Reader& r, Transform& t, int end )
                    : pos_{ end }, end_{ end }, t_( t ), r_( r ), cur_{}
                {
                }
                ChannelListIterator( ChannelListIterator&& other )
                    : pos_{ other.pos_ }, end_{ other.end_ }, t_( other.t_ ),
                      r_( other.r_ ), cur_{ std::move( other.cur_ ) }
                {
                }

                ChannelListIterator(
                    const ChannelListIterator< Reader, Transform >& other )
                    : pos_{ other.pos_ }, end_{ other.end_ }, t_( other.t_ ),
                      r_( other.r_ ), cur_( other.cur_ )
                {
                }

                bool
                operator==( const ChannelListIterator< Reader, Transform >&
                                other ) const
                {
                    return other.pos_ == pos_;
                }

                bool
                operator!=( const ChannelListIterator< Reader, Transform >&
                                other ) const
                {
                    return other.pos_ != pos_;
                }

                reference operator*( )
                {
                    return cur_;
                }

                ChannelListIterator< Reader, Transform >& operator++( )
                {
                    pos_++;
                    if ( pos_ < end_ )
                    {
                        raw_sc2_channel buf;
                        read_in_sc2_channel( r_, buf );
                        cur_ = t_( buf );
                    }
                    else
                    {
                        cur_ = value_type( );
                    }
                    return *this;
                };

                ChannelListIterator< Reader, Transform > operator++( int )
                {
                    auto result =
                        ChannelListIterator< Reader, Transform >( *this );
                    this->operator++( );
                    return result;
                };

            private:
                int        pos_;
                int        end_;
                Transform& t_;
                Reader&    r_;
                value_type cur_;

                static void
                read_in_sc2_channel( Reader& r, raw_sc2_channel& buf )
                {
                    const size_t name_length = 60;
                    std::array< char, 1 > tmp;
                    r.read_exactly( buf.data( ), buf.data( ) + name_length );
                    // There is a bug in some versions of nds1 in some calls
                    // (mainly named channel listing, but lets handle this
                    // generically)
                    // where a extra '\t' is added after the name field.
                    // if there is a extra tab, just consume it and move on
                    r.peek( tmp.data( ), tmp.data( ) + 1 );
                    if ( tmp[ 0 ] == '\t' )
                    {
                        r.read_exactly( tmp.data( ), tmp.data( ) + 1 );
                    }
                    r.read_exactly( buf.data( ) + name_length,
                                    buf.data( ) + buf.size( ) );
                }

                static raw_sc2_channel
                read_in_sc2_channel( Reader& r )
                {
                    raw_sc2_channel buf;
                    read_in_sc2_channel( r, buf );
                    return buf;
                }
            };

            /**
             * Read a nds1 status channels 2 response stream.
             * @tparam Reader The type of the reader object, it must implement
             * the BufferedReader interface
             * @tparam OutIt Output iterator type used to store channels, it
             * must accept the output type of the transform object.
             * @tparam Transform Callable type that takes a raw_sc2_channel and
             * returns what ever type you want to use.
             * @tparam Predicate Filter function/Predicate callable type that
             * given a value (created by transform) returns true or false
             * @param r The reader object
             * @param it Output iterator
             * @param t Transform object
             * @param p Predicate object/callable
             * @note This function is straight forwared IFF you have the types
             * lined up properly.
             * The logic is essentially:
             * for all input channels
             *  auto tmp = t(raw_sc2_channel)
             *  if p(tmp)
             *  {
             *      *it = tmp;
             *      ++it;
             *  }
             *  Transform should output the same type that is consumed by the
             * predicate and the
             *  output iterator.
             */
            template <
                typename Reader,
                class OutIt,
                class Transform =
                    nds_impl::common::IdentityTransform< raw_sc2_channel >,
                class Predicate =
                    nds_impl::common::TruePredicate< typename std::result_of<
                        decltype ( &Transform::operator( ) )(
                            Transform, raw_sc2_channel ) >::type > >
            void
            read_get_channels( Reader&   r,
                               OutIt     it,
                               Transform t = Transform( ),
                               Predicate p = Predicate( ) )
            {
                typedef nds_impl::nds1::common::Basic_IO< Reader > basic_reader;
                basic_reader                                       io( r );
                auto count = io.read_uint32_hex( );
                ChannelListIterator< Reader, Transform > begin_range(
                    r, t, 0, count );
                ChannelListIterator< Reader, Transform > end_range(
                    r, t, count );
                std::copy_if( begin_range, end_range, it, p );
            }
        }
    }
}

//////////////////////////////////////////
// Tests only after this point.
/////////

#ifdef _NDS_IMPL_ENABLE_CATCH_TESTS_

#include <array>
#include <iterator>
#include <string>
#include <vector>
#include <iostream>

#include "socket/socket.hh"

#include "socket/buffered_reader.hh"
#include "common/status_codes.hh"
#include "common/utils.hh"
#include "nds1/common/utils.hh"

#include "tests/nds1_test_utilities.hh"
#include "tests/dummy_socket.hh"
#include "catch.hpp"

TEST_CASE( "LIST Channels" )
{
    /*nds_impl::FullSocket::FullSocket client;
    client.connect( "127.0.0.1:8088" );

    nds_impl::FullSocket::BufferedReader< nds_impl::FullSocket::FullSocket > r{
    client };

    std::string cmd = "status channels 2;\n";
    r.write_all( cmd.data( ), cmd.data( ) + cmd.size( ) );

    {
        std::array< char, 4 > buf;

        client.read_available( buf.data( ), buf.data( ) + buf.size( ) );
        std::string tmp( buf.data( ), buf.size( ) );
        REQUIRE( tmp == "0000" );
    }*/

    nds_testing::DummySocket s{ nds_testing::generate_sc2_channel_stream(
        340 ) };
    nds_impl::Socket::BufferedReader< nds_testing::DummySocket > r{ s };

    std::vector< std::string > channels;

    using nds_impl::nds1::v12_2::raw_sc2_channel;

    nds_impl::nds1::v12_2::read_get_channels(
        r,
        std::back_inserter( channels ),
        // TransformSC2(),
        []( nds_impl::nds1::v12_2::raw_sc2_channel input ) -> std::string {
            typedef nds_impl::common::Span< char > char_span;
            char_span input_span( input.data( ), input.size( ) );
            char_span name_span =
                nds_impl::nds1::common::identify_padded_string(
                    input.data( ), input.data( ) + 60 );
            return std::string{ name_span.data( ), name_span.size( ) };
        },
        []( std::string& channel_name ) -> bool {
            return true;
            // return ( channel_name.find( "TP" ) != std::string::npos );
        } );
    REQUIRE( channels.size( ) == 340 );
}

#endif // _NDS_IMPL_ENABLE_CATCH_TESTS_

#endif // NDS2_CLIENT_NDS1_CHANNEL_LISTING_HH
