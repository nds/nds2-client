//
// Created by jonathan.hanks on 4/12/17.
//
// This is somewhat based on the GDS socket code, with many adaptations
//

#ifndef NDS_SOCKET_HH
#define NDS_SOCKET_HH

#if _WIN32
//#define WIN32_LEAN_AND_MEAN
//#include <Windows.h>
#include <Winsock2.h>
#include <Ws2tcpip.h>
#else
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#endif

#include <cstring>
#include <sstream>
#include <memory>
#include <vector>

namespace nds_impl
{
    namespace Socket
    {
        namespace detail
        {
#if _WIN32
            typedef SOCKET      socket_type;
            typedef int         socklen_t;
            typedef const char* send_ptr_type;
            typedef char*       recv_ptr_type;
            typedef const char* sockopt_ptr_type;

            static const socket_type BAD_SOCKET = INVALID_SOCKET;

            inline void
            bzero( void* addr, size_t len )
            {
                ::memset( addr, 0, len );
            }

            inline void
            closesocket( socket_type s )
            {
                ::closesocket( s );
            }

            class wsa_init
            {
            public:
                wsa_init( )
                {
                    WORD    wVersionRequested;
                    WSAData wsaData;

                    wVersionRequested = MAKEWORD( 2, 0 );
                    if ( WSAStartup( wVersionRequested, &wsaData ) != 0 )
                    {
                        throw std::runtime_error(
                            "Unable to initialize winsock library" );
                    }
                }

                ~wsa_init( )
                {
                    WSACleanup( );
                }
            };

            static wsa_init _init_obj;

#else
            typedef int         socket_type;
            typedef ::socklen_t socklen_t;
            typedef const void* send_ptr_type;
            typedef void*       recv_ptr_type;
            typedef const void* sockopt_ptr_type;

            static const socket_type BAD_SOCKET = -1;

            inline void
            bzero( void* addr, size_t len )
            {
                ::bzero( addr, len );
            }

            inline void
            closesocket( socket_type s )
            {
                ::close( s );
            }
#endif
        }

        struct Address
        {
            decltype(::sockaddr_in::sin_addr.s_addr ) node;
            unsigned short port;
        };

        class FullSocket
        {
        public:
            typedef detail::socket_type socket_type;

            FullSocket( ) : s_( detail::BAD_SOCKET )
            {
            }
            FullSocket( FullSocket&& other ) : s_{ other.release( ) }
            {
            }

            explicit FullSocket( socket_type s ) : s_( s )
            {
            }

            ~FullSocket( )
            {
                if ( s_ != detail::BAD_SOCKET )
                {
                    detail::closesocket( s_ );
                }
                s_ = detail::BAD_SOCKET;
            }
            void
            reset( socket_type fd )
            {
                s_ = fd;
            }
            socket_type
            release( )
            {
                socket_type tmp = s_;
                s_ = detail::BAD_SOCKET;
                return tmp;
            }

            socket_type
            get( ) const
            {
                return s_;
            }

            void
            swap( FullSocket& other )
            {
                socket_type tmp = other.s_;
                other.s_ = s_;
                s_ = tmp;
            }

            bool
            good( ) const
            {
                return s_ != detail::BAD_SOCKET;
            }

            void bind( const std::string& address );

            unsigned short
            listen( int queue_depth = 100 )
            {
                if ( !good( ) )
                    throw std::runtime_error(
                        "Need to bind a port before listening" );
                auto rc = ::listen( get( ), queue_depth );
                if ( rc < 0 )
                {
                    auto err = errno;
                    if ( err == EADDRINUSE )
                        throw std::runtime_error(
                            "Listen reported address in use" );
                    throw std::runtime_error( "Error on listen" );
                }
                ::sockaddr_in     sock;
                detail::socklen_t len = sizeof( sock );
                if ( getsockname( s_, (struct ::sockaddr*)&sock, &len ) != 0 )
                {
                    throw std::runtime_error(
                        "Unable to retrieve socket info after listen call" );
                }

                return sock.sin_port;
            }
            unsigned short
            listen( const std::string& address, int queue_depth = 100 )
            {
                bind( address );
                return ntohs( listen( queue_depth ) );
            }

            FullSocket accept( );

            void connect( const std::string& target );

            void write_all( const char* start, const char* end );

            char* read_available( char* start, char* end );

            template < typename Cont >
            static std::vector< FullSocket* >
            select( Cont& sockets, long secs, long usec )
            {
                struct ::timeval tv;
                tv.tv_sec = secs;
                tv.tv_usec = usec;

                int    nfds = 0;
                fd_set rd_set;
                fd_set wr_set;
                FD_ZERO( &rd_set );
                FD_ZERO( &wr_set );

                for ( auto& s : sockets )
                {
                    FD_SET( s->get( ), &rd_set );
                    FD_SET( s->get( ), &wr_set );
                    nfds = ( s->get( ) > nfds ? s->get( ) : nfds );
                }
                ++nfds;
                int count = ::select( nfds,
                                      &rd_set,
                                      &wr_set,
                                      nullptr,
                                      ( secs >= 0 ? &tv : nullptr ) );
                std::vector< FullSocket* > results;
                for ( auto& s : sockets )
                {
                    if ( FD_ISSET( s->get( ), &rd_set ) ||
                         FD_ISSET( s->get( ), &wr_set ) )
                    {
                        results.push_back( s );
                    }
                }
                return results;
            }

        private:
            socket_type s_;

            void set_option( int id, int val );

            FullSocket( const FullSocket& other );
            FullSocket& operator=( const FullSocket& other );
        };

        class SocketHandle
        {
        public:
            typedef detail::socket_type socket_type;

            SocketHandle( ) : s_( detail::BAD_SOCKET )
            {
            }

            SocketHandle( socket_type s ) : s_( s )
            {
            }
            SocketHandle( const SocketHandle& other ) = default;

            void
            reset( socket_type fd )
            {
                s_ = fd;
            }

            socket_type
            release( )
            {
                socket_type tmp = s_;
                s_ = detail::BAD_SOCKET;
                return tmp;
            }

            socket_type
            get( ) const
            {
                return s_;
            }

            void
            swap( SocketHandle& other )
            {
                socket_type tmp = other.s_;
                other.s_ = s_;
                s_ = tmp;
            }

            bool
            good( ) const
            {
                return s_ != detail::BAD_SOCKET;
            }

            void write_all( const char* start, const char* end );

            char* read_available( char* start, char* end );

        private:
            socket_type s_;
        };

        inline Address
        parse_address( const std::string& addr )
        {
            Address result{ 0, 0 };

            //--------------------------------  Pick off the port number
            std::string::size_type inx = addr.find( ":" );
            std::string            host = addr;
            if ( inx != std::string::npos )
            {
                host = addr.substr( 0, inx );
                std::istringstream is( addr.substr( inx + 1 ) );
                is >> result.port;
            }

            //--------------------------------  Get the address.
            if ( host.size( ) == 0 )
            {
                result.node = INADDR_ANY;
            }
            else
            {
                struct hostent* hentp = ::gethostbyname( host.c_str( ) );
                if ( !hentp )
                {
                    throw std::runtime_error( "Error in gethostbyname" );
                    // perror("parse_addr: error in gethostbyname");
                    // return serr_failure;
                }
                std::memcpy(
                    &result.node, *hentp->h_addr_list, sizeof( result.node ) );
            }
            return result;
        }

        inline void
        FullSocket::set_option( int id, int val )
        {
            int rc = ::setsockopt(
                s_,
                SOL_SOCKET,
                id,
                reinterpret_cast< detail::sockopt_ptr_type >( &val ),
                sizeof( int ) );
            if ( rc < 0 )
                throw std::runtime_error( "Error setting socket options" );
        }

        inline FullSocket
        FullSocket::accept( )
        {
            auto client = FullSocket( );

            ::sockaddr_in sock;

            detail::bzero( &sock, sizeof( sock ) );
            sock.sin_family = AF_INET;
            sock.sin_addr.s_addr = 0;
            sock.sin_port = 0;
            detail::socklen_t len = 16;

            client.reset(::accept( s_, (struct ::sockaddr*)&sock, &len ) );
            return client;
        }

        inline void
        FullSocket::bind( const std::string& address )
        {
            if ( good( ) )
                throw std::runtime_error(
                    "You cannot bind to a connected socket" );

            ::sockaddr_in sock;
            auto          addr_info = parse_address( address );
            sock.sin_addr.s_addr = addr_info.node;
            sock.sin_port = addr_info.port;

            if ( ( s_ = ::socket( AF_INET, SOCK_STREAM, 0 ) ) < 0 )
            {
                throw std::runtime_error( "Unable to create socket" );
            }
            sock.sin_family = AF_INET;
            detail::socklen_t len = 16;

            set_option( SO_REUSEADDR, 1 );

            if (::bind( s_, (struct ::sockaddr*)&sock, len ) < 0 )
                throw std::runtime_error(
                    "Unable to bind to requested address" );
        }

        inline void
        FullSocket::connect( const std::string& target )
        {
            class address_cleanup
            {
            public:
                void
                operator( )( struct ::addrinfo* p )
                {
                    if ( p )
                        ::freeaddrinfo( p );
                }
            };

            if ( good( ) )
                throw std::runtime_error(
                    "Cannot connect a socket that is already initialized" );

            std::string::size_type sep = target.find( ':' );
            if ( sep == std::string::npos )
            {
                throw std::runtime_error(
                    "Socket connect target has no port specifier" );
            }
            std::string hostname = target.substr( 0, sep );
            std::string port = target.substr( sep + 1 );

            struct ::addrinfo  hints;
            struct ::addrinfo *result = nullptr, *rp = nullptr;
            std::memset( &hints, 0, sizeof( hints ) );
            hints.ai_family = AF_UNSPEC;
            hints.ai_socktype = SOCK_STREAM;
            hints.ai_protocol = IPPROTO_TCP;

            if ( int rc = ::getaddrinfo( hostname.c_str( ),
                                         port.c_str( ),
                                         &hints,
                                         &result ) != 0 )
            {
                throw std::runtime_error( "Unable to lookup address "
                                          "information during connect "
                                          "operation" );
            }
            std::unique_ptr< struct ::addrinfo, address_cleanup > result_(
                result );
            for ( rp = result; rp != nullptr; rp = rp->ai_next )
            {
                FullSocket tmp(::socket(
                    rp->ai_family, rp->ai_socktype, rp->ai_protocol ) );
                if ( !tmp.good( ) )
                {
                    continue;
                }
                if (::connect( tmp.get( ), rp->ai_addr, rp->ai_addrlen ) == 0 )
                {
                    swap( tmp );
                    return;
                }
            }
            throw std::runtime_error( "Unable to connecto to target address" );
        }

        inline void
        FullSocket::write_all( const char* start, const char* end )
        {
            if ( !good( ) )
                throw std::runtime_error(
                    "Cannot send data on a socket that is not opened" );
            auto cur = start;
            while ( cur < end )
            {
                auto count =
                    ::send( s_,
                            static_cast< detail::send_ptr_type >( cur ),
                            end - cur,
                            0 );
                if ( count <= 0 )
                {
                    auto err = errno;
                    if ( err == EINTR || err == EAGAIN )
                        continue;
                    throw std::runtime_error( "Unable to send on socket" );
                }
                else if ( count == 0 )
                {
                    throw std::runtime_error( "Connection closed" );
                }
                else
                {
                    cur += count;
                }
            }
        }

        inline char*
        FullSocket::read_available( char* start, char* end )
        {
            if ( !good( ) )
                throw std::runtime_error(
                    "Cannot read data on a socket that is not opened" );

            auto count = ::recv( s_,
                                 static_cast< detail::recv_ptr_type >( start ),
                                 end - start,
                                 0 );
            if ( count <= 0 )
            {
                throw std::runtime_error( "Unable to read data from socket" );
            }
            return start + count;
        }

        inline void
        SocketHandle::write_all( const char* start, const char* end )
        {
            if ( !good( ) )
                throw std::runtime_error(
                    "Cannot send data on a socket that is not opened" );
            auto cur = start;
            while ( cur < end )
            {
                auto count =
                    ::send( s_,
                            static_cast< detail::send_ptr_type >( cur ),
                            end - cur,
                            0 );
                if ( count <= 0 )
                {
                    auto err = errno;
                    if ( err == EINTR || err == EAGAIN )
                        continue;
                    throw std::runtime_error( "Unable to send on socket" );
                }
                else if ( count == 0 )
                {
                    throw std::runtime_error( "Connection closed" );
                }
                else
                {
                    cur += count;
                }
            }
        }

        inline char*
        SocketHandle::read_available( char* start, char* end )
        {
            if ( !good( ) )
                throw std::runtime_error(
                    "Cannot read data on a socket that is not opened" );

            auto count = ::recv( s_,
                                 static_cast< detail::recv_ptr_type >( start ),
                                 end - start,
                                 0 );
            if ( count <= 0 )
            {
                throw std::runtime_error( "Unable to read data from socket" );
            }
            return start + count;
        }
    }
}

//////////////////////////////////////////
// Tests only after this point.
/////////

#ifdef _NDS_IMPL_ENABLE_CATCH_TESTS_

#include <iostream>
#include <thread>
#include "catch.hpp"

TEST_CASE( "Can create a socket object", "[create_basic]" )
{
    nds_impl::Socket::FullSocket s;
    REQUIRE( !s.good( ) );
}

TEST_CASE( "Can create a socket from a fd number", "[create_from_fd]" )
{
    nds_impl::Socket::FullSocket s{ 4 };
    REQUIRE( s.good( ) );
    REQUIRE( s.get( ) == 4 );
    s.release( );
    REQUIRE( !s.good( ) );
    REQUIRE( s.get( ) == -1 );
}

TEST_CASE( "Can parse and lookup a name", "[ext_network]" )
{
    nds_impl::Socket::Address addr =
        nds_impl::Socket::parse_address( "localhost:10000" );
    REQUIRE( addr.port == 10000 );
    REQUIRE( addr.node == htonl( 0x7f000001 ) );

    addr = nds_impl::Socket::parse_address( "localhost" );
    REQUIRE( addr.port == 0 );
    REQUIRE( addr.node == htonl( 0x7f000001 ) );

    addr = nds_impl::Socket::parse_address( ":5050" );
    REQUIRE( addr.port == 5050 );
    REQUIRE( addr.node == 0 );
}

TEST_CASE( "Test release", "[release]" )
{
    nds_impl::Socket::FullSocket s;

    REQUIRE( s.release( ) == nds_impl::Socket::detail::BAD_SOCKET );
    REQUIRE( s.get( ) == nds_impl::Socket::detail::BAD_SOCKET );

    s.reset( 4 );
    REQUIRE( s.get( ) == 4 );
    REQUIRE( s.release( ) == 4 );
    REQUIRE( s.get( ) == nds_impl::Socket::detail::BAD_SOCKET );
}

TEST_CASE( "Test get", "[get]" )
{
    nds_impl::Socket::FullSocket s1{ 5 };
    nds_impl::Socket::FullSocket s2;

    REQUIRE( s1.get( ) == 5 );
    REQUIRE( s2.get( ) == -1 );

    s2.reset( s1.release( ) );
    REQUIRE( s1.get( ) == -1 );
    REQUIRE( s2.get( ) == 5 );

    s2.release( );
    REQUIRE( s2.get( ) == -1 );
}

TEST_CASE( "Test connect with an initialized socket", "[connect]" )
{
    nds_impl::Socket::FullSocket s{ 5 };
    REQUIRE_THROWS( s.connect( "localhost:5000" ) );
    s.release( );
}

TEST_CASE( "Test connect with out a port specified", "[connect_no_port]" )
{
    nds_impl::Socket::FullSocket s;
    REQUIRE_THROWS( s.connect( "localhost" ) );
}

TEST_CASE( "Test connect with out a host specified", "[connect_no_host]" )
{
    nds_impl::Socket::FullSocket s;
    REQUIRE_THROWS( s.connect( ":5000" ) );
}

TEST_CASE( "Test connect with no address", "[connect_no_address]" )
{
    nds_impl::Socket::FullSocket s1, s2;
    REQUIRE_THROWS( s1.connect( "" ) );
    REQUIRE_THROWS( s2.connect( ":" ) );
}

TEST_CASE( "Test full connection" )
{
    nds_impl::Socket::FullSocket server;

    unsigned short port = server.listen( "127.0.0.1" );
    REQUIRE( port != 0 );
    std::string target_address = "127.0.0.1:";
    target_address += std::to_string( port );

    std::thread server_thread( [&server]( ) {
        std::vector< nds_impl::Socket::FullSocket* > sockets;
        sockets.push_back( &server );
        auto results = nds_impl::Socket::FullSocket::select( sockets, 5, 0 );
        if ( results.empty( ) )
        {
            throw std::runtime_error( "Client did not connect in time" );
        }
        auto        client = server.accept( );
        std::string data{ "hi there" };
        client.write_all( data.data( ), data.data( ) + data.size( ) );
    } );
    nds_impl::Socket::FullSocket client;
    client.connect( target_address );
    std::vector< char > buf( 8 );
    auto end = client.read_available( buf.data( ), buf.data( ) + buf.size( ) );
    REQUIRE( end != buf.data( ) );

    //    std::string tmp( &buf[ 0 ], end );
    //    std::cout << "Read in " << tmp.size( ) << " bytes\n";
    //    std::cout << tmp << std::endl;

    server_thread.join( );
}

TEST_CASE( "Test socket wrapper" )
{
    nds_impl::Socket::FullSocket server;

    unsigned short port = server.listen( "127.0.0.1" );
    REQUIRE( port != 0 );
    std::string target_address = "127.0.0.1:";
    target_address += std::to_string( port );

    std::thread server_thread( [&server]( ) {
        std::vector< nds_impl::Socket::FullSocket* > sockets;
        sockets.push_back( &server );
        auto results = nds_impl::Socket::FullSocket::select( sockets, 5, 0 );
        if ( results.empty( ) )
        {
            throw std::runtime_error( "Client did not connect in time" );
        }
        auto        client = server.accept( );
        std::string data{ "hi there" };
        client.write_all( data.data( ), data.data( ) + data.size( ) );
    } );
    nds_impl::Socket::FullSocket client_;
    client_.connect( target_address );

    nds_impl::Socket::SocketHandle client{ client_.get( ) };
    std::vector< char >            buf( 8 );
    auto end = client.read_available( buf.data( ), buf.data( ) + buf.size( ) );
    REQUIRE( end != buf.data( ) );

    //    std::string tmp( &buf[ 0 ], end );
    //    std::cout << "Read in " << tmp.size( ) << " bytes\n";
    //    std::cout << tmp << std::endl;

    server_thread.join( );
}

#endif // _NDS_IMPL_ENABLE_CATCH_TESTS_

#endif // NDS_SOCKET_HH
