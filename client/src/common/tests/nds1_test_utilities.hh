//
// Created by jonathan.hanks on 5/7/18.
//

#ifndef NDS2_CLIENT_NDS1_TEST_UTILITIES_HH
#define NDS2_CLIENT_NDS1_TEST_UTILITIES_HH

#include <vector>

namespace nds_testing
{

    struct sc2_channel_info
    {
        std::string   name;
        std::uint32_t rate;
        std::uint32_t tpnum;
        std::uint16_t tpnode;
        std::uint16_t type;
        float         gain;
        float         slope;
        float         offset;
        std::string   units;
    };

    std::vector< char > generate_sc2_channel_stream( int count );

    sc2_channel_info get_sc2_channel_info( int chan_num );
}

#endif // NDS2_CLIENT_NDS1_TEST_UTILITIES_HH
