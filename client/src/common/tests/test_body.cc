// This file includes all the nds_impl (ie src/common) tests.  It is seperate
// from test_main so that
// we can get the main functions for catch in a different compilation unit and
// keep the builds from
// getting too slow.

// This define enables the catch tests in nds_impl
#define _NDS_IMPL_ENABLE_CATCH_TESTS_

#include "common/span_reader.hh"
#include "common/status_codes.hh"
#include "common/utils.hh"
#include "nds1/common/basic_io.hh"
#include "nds1/common/utils.hh"
#include "nds1/v12_2/channel_listing.hh"
#include "socket/buffered_reader.hh"
#include "socket/socket.hh"
