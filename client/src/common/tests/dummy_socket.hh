//
// Created by jonathan.hanks on 5/4/18.
//

#ifndef NDS2_CLIENT_TEST_DUMMY_SOCKET_HH
#define NDS2_CLIENT_TEST_DUMMY_SOCKET_HH

#include <iterator>
#include <string>
#include <vector>

namespace nds_testing
{
    struct RecordingDummySocket
    {
        std::vector< char > internal_; // use this unless constructed with a
        // specific vector
        std::vector< char >& s_; // reference the vector to use here

        RecordingDummySocket( ) : internal_( ), s_( internal_ )
        {
        }
        explicit RecordingDummySocket( std::vector< char >& s )
            : internal_( ), s_( s )
        {
        }
        RecordingDummySocket( const RecordingDummySocket& other ) = delete;
        RecordingDummySocket( RecordingDummySocket&& other ) = default;

        void
        write_all( const char* start, const char* end )
        {

            auto dest = std::back_inserter< std::vector< char > >( s_ );
            std::copy( start, end, dest );
        }

        std::string
        str( )
        {
            return std::string( s_.data( ), s_.size( ) );
        }
    };

    struct DummySocket
    {
        typedef std::vector< char > storage;

        storage data_;

        DummySocket( const std::string& data )
            : data_( data.data( ), data.data( ) + data.size( ) )
        {
        }

        DummySocket( const std::vector< char >& data ) : data_{ data }
        {
        }
        DummySocket( std::vector< char >&& data ) : data_{ std::move( data ) }
        {
        }

        template < typename It >
        void
        _append_data( It start, It end )
        {
            storage::size_type cur_size = data_.size( );
            data_.resize( cur_size + std::distance( start, end ) );
            std::copy( start, end, data_.data( ) + cur_size );
        }

        char*
        read_available( char* start, char* end )
        {
            ::size_t len = end - start;
            if ( data_.size( ) == 0 )
                throw std::runtime_error( "Out of test data to read" );
            if ( len > data_.size( ) )
                len = data_.size( );
            std::copy( data_.begin( ), data_.begin( ) + len, start );
            std::move( data_.begin( ) + len, data_.end( ), data_.begin( ) );
            data_.resize( data_.size( ) - len );
            return start + len;
        }
    };
}

#endif // NDS2_CLIENT_TEST_DUMMY_SOCKET_HH
