#!/bin/bash
echo $PYTHONPATH
echo $REPLAY_TEST_DIR
echo $REPLAY_BLOB_REMOTE_CACHE
echo $PYTHON_NAME
echo $LIBTOOL_VAL
echo $CURL

echo $LD_LIBRARY_PATH
echo $DYLD_LIBRARY_PATH

# skip the test if we don't have curl
# if [ -z $CURL ]; then
# 	exit 77
# fi

# # skip the test if we cannot reach the remote blob cache
# curl -f --connect-timeout 5 -o /dev/null $REPLAY_BLOB_REMOTE_CACHE
# if [ $? -ne 0 ]; then
# 	exit 77
# fi

echo ${PYTHON_NAME} -B ${REPLAY_TEST_DIR}/sock_test.py -i ${REPLAY_TEST_DIR}/test-json/nds2-timeout.json ${NDS_QUERY_WRAP} -2 -k 2>&1 | grep "Timed out"
${PYTHON_NAME} -B ${REPLAY_TEST_DIR}/sock_test.py -i ${REPLAY_TEST_DIR}/test-json/nds2-timeout.json ${NDS_QUERY_WRAP} -2 -k 2>&1 | grep "Timed out"

exit $?
