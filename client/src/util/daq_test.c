/* -*- tab-width:8 c-basic-offset:4  indent-tabs-mode:nil -*- */
#if HAVE_CONFIG_H
#include "daq_config.h"
#endif /* HAVE_CONFIG_H */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if HAVE_UNISTD_H
#include <unistd.h>
#else
#include <io.h>
#endif /* HAVE_UNISTD_H */
#if HAVE_INTTYPES_H
#include <inttypes.h>
#else
#include <stdint.h>
#endif /* HAVE_INTTYPES_H */

#include "daqc.h"
#include "daqc_internal.h"
#include "daqc_response.h"
#include "channel.h"
#include "trench.h"

#include "nds_logging.h"

#define MAX_GROUP_LIST 4096
#define MAX_EPOCH_LIST 65536UL
#define MAX_CHANNEL_LIST 209715200
#define MAX_MESSAGE_LEN 128UL

#define NEW_VECT( type, dim ) ( (type*)malloc( dim * sizeof( type ) ) )

#define NDS_EXIT_STATUS_FAIL 99 /* A general failure has occurred */

#define NDSSERVER_ENV "NDSSERVER"

enum pgm_mode
{
    list_group,
    chan_list,
    chan_count,
    chan_crc,
    chan_data,
    server_protocol,
    server_version,
    epoch_list,
    cmd_exec
};

#define DEFAULT_DELTA 2

static int optimize_channel_fetch = 1;

/**
 * @brief set_node_and_port_from_env attempts to extract port & host information
 * from the environment
 * @param node_id pointer to the node (host) pointer
 * @param port_id pointer to put a port number in
 * @note if arguments or environment is invalid/NULL this does nothing
 */
void
set_node_and_port_from_env( const char** node_id, short* port_id )
{
    static char buffer[ 1024 ];
    char*       env = getenv( NDSSERVER_ENV );
    char*       colon = NULL;
    char*       cur = NULL;
    char*       dest = buffer;

    if ( !node_id || !port_id || !env )
    {
        return;
    }
    colon = strchr( env, ':' );
    if ( colon == NULL )
    {
        *node_id = env;
        return;
    }
    if ( ( colon - env ) > 1024 )
    {
        return;
    }
    for ( cur = env; *cur != ':'; )
    {
        *dest++ = *cur++;
    }
    *dest = '\0';
    *node_id = buffer;
    *port_id = (short)strtol( colon + 1, 0, 0 );
}

/************************************************************************
 *                                                                      *
 *       Dump out data                                                  *
 *                                                                      *
 ************************************************************************/
void
dump_data( daq_t* daq, size_t spl, const char* fmt )
{
    uint4_type i;

    for ( i = 0; i < daq->num_chan_request; i++ )
    {

        size_t     j, k, N;
        daq_data_t dtype;

        chan_req_t* chan = daq->chan_req_list + i;
        if ( chan->status < 0 )
        {
            printf(
                "Channel: %s receive error (%i)\n", chan->name, -chan->status );
            continue;
        }

        dtype = chan->data_type;

        N = (size_t)chan->status / data_type_size( dtype );
        printf( "Channel: %s  type: %s  nWords: %" PRISIZE_T "\n",
                chan->name,
                data_type_name( dtype ),
                N );

        switch ( dtype )
        {
        case _16bit_integer: {
            short* p = (short*)NULL;

            const char* sfmt = "%6i";
            if ( fmt && *fmt )
                sfmt = fmt;
            p = (short*)( daq_get_block_data( daq ) + chan->offset );
            for ( j = 0; j < N; j += spl )
            {
                printf( "%5" PRISIZE_T, j );
                for ( k = j; k < j + spl && k < N; ++k )
                {
                    printf( " " );
                    printf( sfmt, (int)*p++ );
                }
                printf( "\n" );
            }
            break;
        }

        case _32bit_uint: {
            uint32_t*   p = (uint32_t*)NULL;
            const char* ifmt = "%8u";
            if ( fmt && *fmt )
                ifmt = fmt;
            p = (uint32_t*)( daq_get_block_data( daq ) + chan->offset );
            for ( j = 0; j < N; j += spl )
            {
                printf( "%5" PRISIZE_T, j );
                for ( k = j; k < j + spl && k < N; ++k )
                {
                    printf( " " );
                    printf( ifmt, (unsigned int)*p++ );
                }
                printf( "\n" );
            }
            break;
        }

        case _32bit_integer: {
            int32_t*    p = (int32_t*)NULL;
            const char* ifmt = "%8i";
            if ( fmt && *fmt )
                ifmt = fmt;
            p = (int32_t*)( daq_get_block_data( daq ) + chan->offset );
            for ( j = 0; j < N; j += spl )
            {
                printf( "%5" PRISIZE_T, j );
                for ( k = j; k < j + spl && k < N; ++k )
                {
                    printf( " " );
                    printf( ifmt, (int)*p++ );
                }
                printf( "\n" );
            }
            break;
        }
        case _32bit_float: {
            float*      p = (float*)NULL;
            const char* ffmt = "%13.7g";
            if ( fmt && *fmt )
                ffmt = fmt;
            p = (float*)( daq_get_block_data( daq ) + chan->offset );
            for ( j = 0; j < N; j += spl )
            {
                printf( "%5" PRISIZE_T, j );
                for ( k = j; k < j + spl && k < N; ++k )
                {
                    printf( " " );
                    printf( ffmt, *p++ );
                }
                printf( "\n" );
            }
            break;
        }
        case _64bit_double: {
            double*     p = (double*)NULL;
            const char* dfmt = "%15.9g";
            if ( fmt && *fmt )
                dfmt = fmt;
            p = (double*)( daq_get_block_data( daq ) + chan->offset );
            for ( j = 0; j < N; j += spl )
            {
                printf( "%5" PRISIZE_T, j );
                for ( k = j; k < j + spl && k < N; ++k )
                {
                    printf( " " );
                    printf( dfmt, *p++ );
                }
                printf( "\n" );
            }
            break;
        }
        default:
            printf( "Channel: %s data of unknown/unsupported type (%i)\n",
                    chan->name,
                    chan->data_type );
        }
    }
}

/************************************************************************
 *                                                                      *
 *       Dump out channel status information                            *
 *                                                                      *
 ************************************************************************/
void
dump_status( daq_t* daq )
{
    size_t i;
    printf( "Data for %i seconds starting at GPS: %i\n",
            daq_get_block_secs( daq ),
            daq_get_block_gps( daq ) );
    printf( "%-40s %-7s   %-8s %s\n", "Channel", "type", "nWords", "units" );
    for ( i = 0; i < daq->num_chan_request; i++ )
    {
        chan_req_t* chan = daq->chan_req_list + i;
        daq_data_t  dtype = chan->data_type;
        if ( chan->status < 0 )
        {
            printf( "%-40s %-7s: %s\n",
                    chan->name,
                    data_type_name( chan->data_type ),
                    daq_strerror( -chan->status ) );
        }
        else
        {
            size_t N = (size_t)chan->status / data_type_size( dtype );
            printf( "%-40s %-7s %8" PRISIZE_T " %s\n",
                    chan->name,
                    data_type_name( chan->data_type ),
                    N,
                    chan->s.signal_units );
        }
    }
}

/************************************************************************
 *                                                                      *
 *       Main Routine                                                   *
 *                                                                      *
 ************************************************************************/
int
main( int argc, const char* argv[] )
{
    enum nds_version vrsn = nds_try;
    enum pgm_mode    mode = chan_data;
    enum chantype    c_type = cUnknown;
    int              daq_test_debug = 0;

    int retval = 0;

    short       port_id = DAQD_PORT;
    const char* cmd = 0;
    const char* node_id = "localhost";
    const char* epoch = 0;
    int         node_or_port_set = 0;
    time_t      gps_start = 0, gps_end = 0, delta = DEFAULT_DELTA;
    int         verbose = 0;
    size_t      spl = 8;
    const char* fmt = "";
    daq_t       daq;
    int         rc;

    int iarg;
    int syntax = 0;

    /*-----------------------------------------------------
     *   Initialize error logging
     *-----------------------------------------------------*/

    nds_logging_init( );

    /*
     *   Initialize nds client library
     */
    rc = daq_startup( );
    if ( rc )
    {
        fprintf( stderr, "Failure in daq_startup. error code: %d.\n", rc );
        return 1;
    }

    /*
     *   Parse the command line arguments
     */
    for ( iarg = 1; iarg < argc && *( argv[ iarg ] ) == '-'; ++iarg )
    {
        if ( !strcmp( argv[ iarg ], "-c" ) )
        {
            mode = cmd_exec;
            if ( ++iarg == argc )
                syntax = 1;
            else
                cmd = argv[ iarg ];
        }
        else if ( !strcmp( argv[ iarg ], "-d" ) )
        {
            if ( ++iarg == argc )
                syntax = 1;
            else
                delta = strtol( argv[ iarg ], 0, 0 );
        }

        /*------------------------------  End debug level             */
        else if ( !strcmp( argv[ iarg ], "--debug" ) )
        {
            if ( ++iarg == argc )
                daq_test_debug = 1;
            else
                daq_test_debug = strtol( argv[ iarg ], 0, 0 );
        }

        /*------------------------------  End gps time               */
        else if ( !strcmp( argv[ iarg ], "-e" ) )
        {
            if ( ++iarg == argc )
                syntax = 1;
            else
                gps_end = strtol( argv[ iarg ], 0, 0 );
        }

        /*------------------------------  Epoch to be interrogated   */
        else if ( !strcmp( argv[ iarg ], "--epoch" ) )
        {
            if ( ++iarg == argc )
                syntax = 1;
            else
                epoch = argv[ iarg ];
        }

        /*------------------------------  Print list of epochs       */
        else if ( !strcmp( argv[ iarg ], "-E" ) ||
                  !strcmp( argv[ iarg ], "--epoch-list" ) )
        {
            mode = epoch_list;
        }

        else if ( !strcmp( argv[ iarg ], "--channel-list-pre-fetch" ) )
        {
            optimize_channel_fetch = 0;
        }
        else if ( !strcmp( argv[ iarg ], "-g" ) )
        {
            mode = list_group;
        }
        else if ( !strcmp( argv[ iarg ], "--help" ) )
        {
            syntax = 1;
            break;
        }
        else if ( !strcmp( argv[ iarg ], "-h" ) ||
                  !strcmp( argv[ iarg ], "--hash" ) )
        {
            mode = chan_crc;
        }
        else if ( !strcmp( argv[ iarg ], "-k" ) ||
                  !strcmp( argv[ iarg ], "--count" ) )
        {
            mode = chan_count;
        }
        else if ( !strcmp( argv[ iarg ], "-l" ) ||
                  !strcmp( argv[ iarg ], "--list" ) )
        {

            if ( mode != chan_crc && mode != chan_count )
                mode = chan_list;
        }
        else if ( !strcmp( argv[ iarg ], "-n" ) )
        {
            node_or_port_set = 1;
            if ( ++iarg == argc )
                syntax = 1;
            else
                node_id = argv[ iarg ];
        }
        else if ( !strcmp( argv[ iarg ], "-p" ) )
        {
            node_or_port_set = 1;
            if ( ++iarg == argc )
                syntax = 1;
            else
                port_id = (short)strtol( argv[ iarg ], 0, 0 );
        }
        else if ( !strcmp( argv[ iarg ], "-s" ) )
        {
            if ( ++iarg == argc )
                syntax = 1;
            else
                gps_start = strtol( argv[ iarg ], 0, 0 );
        }
        else if ( !strcmp( argv[ iarg ], "--sample-format" ) )
        {
            if ( ++iarg == argc )
                syntax = 1;
            else
                fmt = argv[ iarg ];
        }
        else if ( !strcmp( argv[ iarg ], "--samples-per-line" ) )
        {
            if ( ++iarg == argc )
                syntax = 1;
            else
                spl = (size_t)strtol( argv[ iarg ], 0, 0 );
        }
        else if ( !strcmp( argv[ iarg ], "--server-protocol" ) )
        {
            mode = server_protocol;
        }
        else if ( !strcmp( argv[ iarg ], "--server-version" ) )
        {
            mode = server_version;
        }
        else if ( !strcmp( argv[ iarg ], "-t" ) )
        {
            if ( ++iarg == argc )
                syntax = 1;
            else
                c_type = cvt_str_chantype( argv[ iarg ] );
        }
        else if ( !strcmp( argv[ iarg ], "-v" ) )
        {
            verbose++;
        }
        else if ( !strcmp( argv[ iarg ], "--version" ) )
        {
            char* pos = strrchr( argv[ 0 ], '/' );
            if ( pos )
            {
                ++pos;
            }
            printf( "%s (%s) %s\n",
                    ( ( pos ) ? pos : argv[ 0 ] ),
                    PACKAGE_NAME,
                    PACKAGE_VERSION );
            exit( 0 );
        }
        else if ( !strcmp( argv[ iarg ], "-1" ) )
        {
            vrsn = nds_v1;
        }
        else if ( !strcmp( argv[ iarg ], "-2" ) )
        {
            vrsn = nds_v2;
        }
        else
        {
            fprintf( stderr, "Unrecognized command: %s\n", argv[ iarg ] );
            syntax = 1;
            break;
        }
    }
    if ( !node_or_port_set )
    {
        set_node_and_port_from_env( &node_id, &port_id );
    }

    /* optionally turn on debugging
     * Note that this probably needs to be smartened by allowing user to
     * specify a group mask and/or printout levels.
     */
    if ( daq_test_debug )
    {
        nds_logging_enable( NDS_LOG_GROUP_CONNECTION );
        nds_logging_enable( NDS_LOG_GROUP_VERBOSE_ERRORS );
        nds_logging_enable( NDS_LOG_GROUP_TRACE_ENTRY_EXIT );
        nds_logging_enable( NDS_LOG_GROUP_STATUS_UPDATE );
        nds_logging_enable( NDS_LOG_GROUP_USER );
    }

    if ( mode == server_protocol )
    {
        if ( !port_id )
            port_id = DAQD_PORT;
        rc = daq_connect( &daq, node_id, port_id, vrsn );
        if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
        {
            nds_logging_printf(
                "DEBUG: %06d - %s - rc %d\n", __LINE__, __FILE__, rc );
        }
        if ( rc )
        {
            printf( "Error in daq_connect: %s\n", daq_strerror( rc ) );
            return 1;
        }
        else
        {
            printf( "server: %s port: %d protcol: %d\n",
                    node_id,
                    port_id,
                    daq.nds_versn );
            daq_disconnect( &daq );
        }
        exit( 0 );
    }
    else if ( mode == server_version )
    {
        if ( !port_id )
            port_id = DAQD_PORT;
        rc = daq_connect( &daq, node_id, port_id, vrsn );
        if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
        {
            nds_logging_printf(
                "DEBUG: %06d - %s - rc %d\n", __LINE__, __FILE__, rc );
        }
        if ( rc )
        {
            printf( "Error in daq_connect: %s\n", daq_strerror( rc ) );
            return 1;
        }
        else
        {
            printf( "nds protocol: %d version: %d revision: %d\n",
                    daq.nds_versn,
                    daq.nds1_ver,
                    daq.nds1_rev );
            daq_disconnect( &daq );
        }
        exit( 0 );
    }
    else if ( mode == chan_data && iarg == argc )
    {
        fprintf( stderr, "No channels requested!\n" );
        syntax = 1;
    }
    else if ( ( mode == chan_crc || mode == chan_list || mode == chan_count ) &&
              iarg < argc - 1 )
    {
        fprintf( stderr, "Channel lists accept only a single name pattern\n" );
        syntax = 1;
    }
    if ( syntax )
    {
        fprintf( stderr, "Data query syntax: \n" );
        fprintf(
            stderr,
            "nds_query [-n <server>] [-p <port>] [-12v] [--epoch <epik>]\n" );
        fprintf( stderr,
                 "          [-s <start-gps>] [-e <end-gps>] [-d <delta-t>]\n" );
        fprintf( stderr,
                 "          [-t <chan-type>] "
                 "[--channel-list-pre-fetch] <channel-list>\n\n" );

        fprintf( stderr, "Channel List query syntax: \n" );
        fprintf(
            stderr,
            "nds_query [-n <server>] [-p <port>] [-12] [--epoch <epik>]\n" );
        fprintf( stderr, "          [-s <start-gps>] [-t <chan-type>]\n" );
        fprintf( stderr,
                 "          { {--count | -k} | {--hash | -h} | -l }\n" );
        fprintf( stderr, "          [<channel-pattern>]\n\n" );
        fprintf( stderr, "Status list query syntax: \n" );
        fprintf( stderr, "nds_query [-n <server>] [-p <port>] [-12]\n" );
        fprintf(
            stderr,
            "          [--version] [--server-version] [--server-protocol]\n" );
        fprintf( stderr, "          [--epoch-list | -E] [-g]\n\n" );
        fprintf( stderr, "where: \n" );
        fprintf( stderr, "    -E      Request a list of epochs (NDS2 only)\n" );
        fprintf( stderr,
                 "    -g      Request a list of channel groups (NDS1 only)\n" );
        fprintf( stderr, "    -h      Get hash of selected channels\n" );
        fprintf( stderr, "    -k      Get number of selected channels\n" );
        fprintf( stderr, "    -l      List available channels\n" );

        fprintf( stderr, "    -d      Time stride [%i]\n", DEFAULT_DELTA );
        fprintf( stderr, "    -e      End gps time [start_gps + delta]\n" );
        fprintf( stderr, "    -n      Specify server IP address\n" );
        fprintf( stderr, "    -p      Specify port number [%i]\n", DAQD_PORT );
        fprintf( stderr, "    -s      Specify start time (as GPS)\n" );
        fprintf( stderr, "    -t      Specify default channel type.\n" );
        fprintf( stderr, "    -v      Verbose printout (dump channel data)\n" );
        fprintf( stderr, "    -1      NDS1 protocol\n" );
        fprintf( stderr, "    -2      NDS2 protocol\n" );
        fprintf( stderr, "    --channel-list-pre-fetch" );
        fprintf( stderr,
                 "            Force prefetching of the channel list\n" );
        fprintf( stderr,
                 "            when communicating with version 2 servers\n" );
        fprintf(
            stderr,
            "    --epoch limit times to <epik> specified as a run name\n" );
        fprintf( stderr,
                 "            (e.g. 'S6'), 'start-stop' or 'start:length'\n" );
        fprintf( stderr,
                 "    --sample-format    printf format for data dump\n" );
        fprintf( stderr, "    --samples-per-line samples dumped per line\n" );
        fprintf( stderr,
                 "    --version          print version of client and exit\n" );
        fprintf( stderr,
                 "    --server-version   print version of server and exit\n" );
        fprintf( stderr,
                 "    --server-protocol  print protocol used by the "
                 "server and exit\n" );
        fprintf( stderr, "    <channel-list> Space delimited channel list\n" );
        fprintf( stderr,
                 "    <channel-pattern> bash-like regular expression\n" );
        fprintf( stderr,
                 "If the protocol is not specified, nds_query tries\n" );
        fprintf( stderr,
                 "nds2 protocol, and if this fails, reverts to nds1\n" );
        fprintf( stderr, "protocol\n" );
        return 1;
    }
    if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
    {
        nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
    }

    if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
    {
        nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
    }
    /*--------------------------------------------------------------------*
     *                                                                    *
     *      Connect to the NDS server                                     *
     *                                                                    *
     *--------------------------------------------------------------------*/
    if ( !port_id )
        port_id = DAQD_PORT;

    if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
    {
        nds_logging_printf(
            "DEBUG: %06d - %s - port_id %d\n", __LINE__, __FILE__, port_id );
    }
    rc = daq_connect( &daq, node_id, port_id, vrsn );
    if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
    {
        nds_logging_printf(
            "DEBUG: %06d - %s - rc %d\n", __LINE__, __FILE__, rc );
    }
    if ( rc )
    {
        printf( "Error in daq_connect: %s\n", daq_strerror( rc ) );
        return 1;
    }

    /*----------------------------------  request epoch                   */
    if ( epoch != NULL )
    {
        rc = daq_set_epoch( &daq, epoch );
        if ( rc )
        {
            printf( "error in set_epoch: %s\n", daq_strerror( rc ) );
        }
    }

    /*--------------------------------------------------------------------*
     *                                                                    *
     *      Execute a specific command                                    *
     *                                                                    *
     *--------------------------------------------------------------------*/
    if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
    {
        nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
    }
    switch ( mode )
    {
    case cmd_exec: {
        if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
        {
            nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
        }
        rc = daq_send( &daq, (char*)cmd );
        printf( "command sent, rc=%i (%s)\n", rc, daq_strerror( rc ) );
        break;
    }

    /*--------------------------------------------------------------------*
     *                                                                    *
     *      Get a group list in group list mode.                          *
     *                                                                    *
     *--------------------------------------------------------------------*/
    case list_group: {
        int                  i;
        daq_channel_group_t* group_list =
            NEW_VECT( daq_channel_group_t, MAX_GROUP_LIST );
        int nGroups = 0;

        if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
        {
            nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
        }
        rc = daq_recv_channel_groups(
            &daq, group_list, MAX_GROUP_LIST, &nGroups );
        if ( rc )
        {
            printf( "Error in daq_recv_channel_groups: %s\n",
                    daq_strerror( rc ) );
            return 2;
        }
        printf( " Group    ID\n" );
        for ( i = 0; i < nGroups; i++ )
        {
            printf(
                "%8s %5i\n", group_list[ i ].name, group_list[ i ].group_num );
        }
        break;
    }

    /*--------------------------------------------------------------------*
     *                                                                    *
     *      Get the channel list hash                                     *
     *                                                                    *
     *--------------------------------------------------------------------*/
    case chan_crc: {
        unsigned int hash;
        int          len = sizeof( hash );

        if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
        {
            nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
        }
        if ( iarg < argc )
        {
            rc = daq_recv_hash_by_pattern(
                &daq, &hash, &len, gps_start, c_type, argv[ iarg ] );
        }
        else
        {
            rc = daq_recv_channel_hash( &daq, &hash, &len, gps_start, c_type );
        }
        if ( rc )
        {
            printf( "Error reading channel hash: %s\n", daq_strerror( rc ) );
            retval = NDS_EXIT_STATUS_FAIL;
        }
        else
            printf( "Channel hash code: %x\n", hash );
        break;
    }

    /*--------------------------------------------------------------------*
     *                                                                    *
     *      Get the channel list hash                                     *
     *                                                                    *
     *--------------------------------------------------------------------*/
    case epoch_list: {
        int   len;
        char* buf = malloc( MAX_EPOCH_LIST );

        if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
        {
            nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
        }
        rc = daq_recv_epoch_list( &daq, buf, MAX_EPOCH_LIST, &len );
        buf[ MAX_EPOCH_LIST - 1 ] = 0;
        if ( rc )
        {
            printf( "Error reading epoch list: %s\n", daq_strerror( rc ) );
            retval = NDS_EXIT_STATUS_FAIL;
        }
        else
        {
            int ich, last = 0;
            printf( "NDS2 server epoch list:\n" );
            for ( ich = 0; ich < len; ich++ )
            {
                if ( buf[ ich ] == ' ' )
                {
                    buf[ ich ] = 0;
                    if ( ich != last )
                        printf( "  %s\n", buf + last );
                    last = ich + 1;
                }
            }
            if ( last < len )
                printf( "  %s\n", buf + last );
        }
        free( buf );
        break;
    }

    /*--------------------------------------------------------------------*
     *                                                                    *
     *      Get the channel list                                          *
     *                                                                    *
     *--------------------------------------------------------------------*/
    case chan_list:
    case chan_data:
    case chan_count: {
        /*------------------------------  Count the number of channels    */
        int            i;
        int            nAlloc = 0;
        int            nChans = 0;
        daq_channel_t* channel_list = (daq_channel_t*)NULL;

        if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
        {
            nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
        }
        if ( ( optimize_channel_fetch ) && ( mode == chan_data ) &&
             ( daq.nds_versn == nds_v2 ) )
        {
            /*
             * When optimizing the retrieval of channel data for version 2
             * protocol and beyond, do not retrieve the list of channels
             */
        }
        else
        {
            /*
             *  Retrieve a list of channels by first retrieving the number
             *  of channels. Note that the full channel list is fetched unless
             *  there is a single channel (pattern?) specified and the mode is
             *  either non-data or optimized channel fetch is disabled.
             */
            if ( iarg + 1 == argc &&
                 ( optimize_channel_fetch || mode != chan_data ) )
            {
                rc = daq_recv_channels_by_pattern(
                    &daq, 0, 0, &nAlloc, gps_start, c_type, argv[ iarg ] );
            }
            else
            {
                rc = daq_recv_channel_list(
                    &daq, 0, 0, &nAlloc, gps_start, c_type );
            }
            if ( rc )
            {
                printf( "Error reading channel count: %s\n",
                        daq_strerror( rc ) );
                retval = NDS_EXIT_STATUS_FAIL;
                break;
            }
            if ( mode == chan_count )
            {
                printf( "Number of channels: %i\n", nAlloc );
                break;
            }

            /*------------------------------  Allocate a channel list        */
            if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
            {
                nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
            }
            if ( nAlloc > MAX_CHANNEL_LIST )
            {
                printf(
                    "The number of channels (%i) is larger than the max (%i)\n",
                    nAlloc,
                    MAX_CHANNEL_LIST );
                printf( "    Something must be wrong... Terminating\n" );
                break;
            }
            channel_list = NEW_VECT( daq_channel_t, ( size_t )( nAlloc ) );

            /*-----------------------------  Read in the channel list        */
            if ( iarg + 1 == argc &&
                 ( optimize_channel_fetch || mode != chan_data ) )
            {
                rc = daq_recv_channels_by_pattern( &daq,
                                                   channel_list,
                                                   nAlloc,
                                                   &nChans,
                                                   gps_start,
                                                   c_type,
                                                   argv[ iarg ] );
            }
            else
            {
                rc = daq_recv_channel_list(
                    &daq, channel_list, nAlloc, &nChans, gps_start, c_type );
            }
            if ( rc )
            {
                printf( "Error reading channel list: %s\n",
                        daq_strerror( rc ) );
                retval = NDS_EXIT_STATUS_FAIL;
                free( channel_list );
                break;
            }
        } // if optimize_channel_fetch ...
        /*-------------------------------------------------------------------*
         *                                                                   *
         *      Dump the channel list in list mode                           *
         *                                                                   *
         *-------------------------------------------------------------------*/
        if ( mode == chan_list )
        {

            if ( nChans > nAlloc )
            {
                /*-----------------------------------------------------------*
                 * Make sure we do not go beyond our alloted memory.
                 *-----------------------------------------------------------*/
                nChans = nAlloc;
            }
            printf( "Number of channels received = %i\n", nChans );
            printf( "Channel                  Rate  chan_type\n" );
            for ( i = 0; i < nChans; i++ )
            {
                printf( "%-40s %5g %8s %9s\n",
                        channel_list[ i ].name,
                        channel_list[ i ].rate,
                        cvt_chantype_str( channel_list[ i ].type ),
                        data_type_name( channel_list[ i ].data_type ) );
            }
            free( channel_list );
            break;
        }

        /*-------------------------------------------------------------------*
         *                                                                   *
         *     Get and dump the channel data for the specified channels      *
         *                                                                   *
         *-------------------------------------------------------------------*/
        {
            int nMTrend = 0, nChan = 0;
            for ( i = iarg; i < argc; ++i )
            {
                struct trench_struct tch;
                int                  j, chanok = 0;
                daq_channel_t        chan;

                trench_init( &tch );
                trench_parse( &tch, argv[ i ] );
                if ( daq.nds_versn == nds_v1 )
                {
                    for ( j = 0; j < nChans; ++j )
                    {
                        if ( !trench_cmp_base( &tch, channel_list[ j ].name ) )
                        {
                            trench_infer_chan_info(
                                &tch,
                                c_type,
                                channel_list[ j ].rate,
                                channel_list[ j ].data_type );
                            daq_init_channel( &chan,
                                              tch.str,
                                              tch.ctype,
                                              tch.rate,
                                              tch.dtype );
                            chanok = 1;
                            break;
                        }
                    }
                }
                else
                {
                    if ( channel_list )
                    {
                        for ( j = 0; j < nChans; ++j )
                        {
                            if ( tch.ctype != cUnknown &&
                                 tch.ctype != channel_list[ j ].type )
                                continue;

                            if ( !strcmp( tch.str, channel_list[ j ].name ) )
                            {
                                if ( channel_list[ j ].type == cMTrend )
                                    nMTrend++;
                                daq_init_channel( &chan,
                                                  argv[ i ],
                                                  channel_list[ j ].type,
                                                  channel_list[ j ].rate,
                                                  channel_list[ j ].data_type );
                                chanok = 1;
                                break;
                            }
                        }
                    }
                    else
                    {
                        daq_init_channel(
                            &chan, tch.str, tch.ctype, tch.rate, tch.dtype );
                        chanok = 1;
                    }
                }

                if ( chanok )
                {
                    nChan++;
                    daq_request_channel_from_chanlist( &daq, &chan );
                }
                else
                {
                    printf( "Channel: %s not in list... Ignored.\n",
                            argv[ i ] );
                }
                trench_destroy( &tch );
            }

            /*-------------------------------------------------------------------*
             *                                                                   *
             *     Get and dump the channel data for the specified channels *
             *                                                                   *
             *-------------------------------------------------------------------*/
            if ( !gps_end )
            {
                gps_end = gps_start + (time_t)delta;
            }
            else if ( gps_end < gps_start )
            {
                gps_end += gps_start;
            }
            if ( gps_end < gps_start + delta )
            {
                delta = gps_end - gps_start;
            }
            if ( nMTrend != 0 &&
                 ( ( gps_start % 60 ) != 0 || ( delta % 60 ) != 0 ) )
            {
                printf( "Minute trend requests start & end on a GPS minute\n" );
                return 2;
            }

            /*   Check the data availability first */
            rc = daq_request_check( &daq, gps_start, gps_end );
            if ( rc == DAQD_ONTAPE )
            {
                printf( "Warning Data On Tape: %s\n", daq_strerror( rc ) );
            }
            else if ( rc == DAQD_COMMAND_SYNTAX || rc == DAQD_NOT_SUPPORTED )
            {
                printf( "Warning: Can not check data availability: %s\n",
                        daq_strerror( rc ) );
            }
            else if ( rc != DAQD_OK )
            {
                char msg[ MAX_MESSAGE_LEN ];
                int  msglen;
                printf( "Data not available - request ignored: %s\n",
                        daq_strerror( rc ) );
                free( channel_list );
                if ( !daq_get_last_message(
                         &daq, msg, MAX_MESSAGE_LEN, &msglen ) )
                {
                    printf( "last server error: %s\n", msg );
                }
                daq_disconnect( &daq );
                daq_recv_shutdown( &daq );
                return rc;
            }

            /*   Now get the data data */
            rc = daq_request_data( &daq, gps_start, gps_end, delta );
            if ( rc )
            {
                char msg[ MAX_MESSAGE_LEN ];
                int  msglen;
                printf( "Error in daq_request_data: %s\n", daq_strerror( rc ) );
                if ( !daq_get_last_message(
                         &daq, msg, MAX_MESSAGE_LEN, &msglen ) )
                {
                    printf( "last server error: %s\n", msg );
                }
                retval = NDS_EXIT_STATUS_FAIL;
            }
            else
            {
                time_t gps, dt = delta;
                for ( gps = gps_start; gps < gps_end; gps += dt )
                {
                    int rc = daq_recv_next( &daq );
                    if ( rc )
                    {
                        printf( "Error in daq_recv_next: %s\n",
                                daq_strerror( rc ) );
                        retval = NDS_EXIT_STATUS_FAIL;
                        break;
                    }
                    dump_status( &daq );
                    if ( verbose )
                        dump_data( &daq, spl, fmt );
                    dt = (time_t)daq_get_block_secs( &daq );
                }
            }
            free( channel_list );
        }
    }
    case server_protocol:
    case server_version:
        // Nothing needs to be done here
        break;
    }

    /*--------------------------------------------------------------------*
     *                                                                    *
     *     All done, go away...                                           *
     *                                                                    *
     *--------------------------------------------------------------------*/
    if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
    {
        nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
    }

    daq_disconnect( &daq );
    if ( nds_logging_check( NDS_LOG_GROUP_USER, 10 ) )
    {
        nds_logging_printf( "DEBUG: %06d - %s\n", __LINE__, __FILE__ );
    }
    daq_recv_shutdown( &daq );

    return retval;
}
