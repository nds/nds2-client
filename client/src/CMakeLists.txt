#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
ADD_SUBDIRECTORY(common)
ADD_SUBDIRECTORY(client)
add_subdirectory(libndscxx)
ADD_SUBDIRECTORY(util)
