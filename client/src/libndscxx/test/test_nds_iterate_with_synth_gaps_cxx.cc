#include "nds.hh"
#include "nds_helper.hh"

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <exception>

#include "nds_testing.hh"
#include "test_macros.hh"

#ifndef __GNUC__
#define __attribute__( x ) /* Nothing */
#endif /* __GNUC__ */

void
my_terminate_handler( )
{
    std::cerr << "Something called terminate" << std::endl;
}

template < typename T >
class SimpleMisMatchPredicate
{
    T val_;

public:
    SimpleMisMatchPredicate( T val ) : val_( val )
    {
    }
    bool
    operator( )( const T& other ) const
    {
        return val_ != other;
    }
};

bool
is_gap_second( NDS::buffer::gps_second_type input_gps )
{
    NDS::buffer::gps_second_type gps = input_gps - 1770000000;
    if ( ( gps < 1 ) || ( gps == 9 ) || ( gps == 18 ) ||
         ( gps >= 32 && gps < 40 ) || ( gps >= 50 && gps < 61 ) ||
         ( gps > 67 ) )
        return true;
    return false;
}

template < typename T >
bool
check_data( NDS::buffer& buf, T val )
{
    bool                   result = true;
    NDS::buffer::size_type samples_per_sec = buf.seconds_to_samples( 1 );

    T* data = const_cast< T* >( buf.cbegin< T >( ) );
    for ( NDS::buffer::gps_second_type cur_gps = buf.Start( );
          cur_gps < buf.Stop( );
          ++cur_gps )
    {
        T expected =
            ( is_gap_second( cur_gps ) ? static_cast< T >( 0.0 ) : val );
        T* end = data + samples_per_sec;
        T* mismatch =
            std::find_if( data, end, SimpleMisMatchPredicate< T >( expected ) );
        if ( mismatch != end )
        {
            std::cerr << "Found mismatch for " << cur_gps
                      << " expected that it is a "
                      << ( is_gap_second( cur_gps ) ? "gap" : "data" )
                      << " segment" << std::endl;
            result = false;
        }
        data = end;
    }
    return result;
}

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::set_terminate( my_terminate_handler );

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TRY;
    if ( find_opt( "-proto-1", args ) )
    {
        proto = connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = connection::PROTOCOL_TWO;
    }
    else if ( std::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps( std::getenv( "NDS_TEST_PROTO" ) );
        std::cerr << "NDS_TEST_PROTO = " << ps << std::endl;
        if ( ps.compare( "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }

    std::cerr << "Connecting to " << hostname << ":" << port << " " << proto
              << std::endl;
    pointer< connection > conn(
        make_unique_ptr< connection >( hostname, port, proto ) );

    bool no_gaps = true;
    if ( find_opt( "-default-gap-handling", args ) )
    {
        // Use default gap handling, which should be
        // abort handler with iterate_use_gap_handlers as false
        no_gaps = false;
    }
    else
    {
        conn->parameters( ).set( "ITERATE_USE_GAP_HANDLERS", "true" );
        if ( find_opt( "-no-gap", args ) )
        {
            conn->parameters( ).set( "ITERATE_USE_GAP_HANDLERS", "false" );
            no_gaps = false;
        }
        NDS_ASSERT(
            conn->parameters( ).set( "GAP_HANDLER", "STATIC_HANDLER_ZERO" ) );
    }

    NDS::connection::channel_names_type chans;
    chans.push_back( "X1:PEM-1" );
    chans.push_back( "X1:PEM-2" );

    int                          i = 0;
    NDS::buffer::gps_second_type cur_gps __attribute__( ( unused ) ) =
        1770000000;

    auto stream = conn->iterate(
        NDS::request_period( 1770000000, 1770000070, 10 ), chans );
    for ( auto bufs : stream )
    {
        NDS_ASSERT( bufs->at( 0 ).Start( ) >= cur_gps );
        if ( no_gaps )
        {
            NDS_ASSERT( bufs->at( 0 ).Start( ) == cur_gps );
        }
        NDS::buffer::gps_second_type delta __attribute__( ( unused ) ) =
            bufs->at( 0 ).Stop( ) - bufs->at( 0 ).Start( );
        NDS_ASSERT( bufs->at( 0 ).Samples( ) == 256 * delta );
        NDS_ASSERT( check_data< float >( bufs->at( 0 ), 1.5 ) );
        NDS_ASSERT( bufs->at( 1 ).Samples( ) == 512 * delta );
        NDS_ASSERT( check_data< double >( bufs->at( 1 ), 2.75 ) );
        cur_gps = bufs->at( 1 ).Stop( );
        ++i;
    }

    NDS_ASSERT( i >= 5 );
    if ( no_gaps )
    {
        NDS_ASSERT( cur_gps == 1770000070 );
    }

    // make sure we can do other operations
    conn->find_channels( NDS::channel_predicate(
        NDS::epoch( 1770000000, 1770000000 + 70 ), "X1:PEM-1" ) );
    conn->find_channels( NDS::channel_predicate(
        "X1:PEM-2", NDS::epoch( 1770000000, 1770000000 + 70 ) ) );
}
