#include "nds.hh"
#include "daq_config.h"

#include <iostream>

using namespace std;

int
main( int argc, char* argv[] )
{
    std::string ver = NDS::version( );
    std::string expected( PACKAGE_VERSION );

    if ( ver == expected )
        return 0;
    cerr << "Expected " << expected << " got " << ver << endl;
    return 1;
}
