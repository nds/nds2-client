#include <vector>
#include <sstream>

#include <stdlib.h>

#include "environmental_support.hh"

int
set_env_val( const std::string& key, const std::string& val )
{
#if _WIN32
    return static_cast< int >( _putenv_s( key.c_str( ), val.c_str( ) ) );
#else
    return setenv( key.c_str( ), val.c_str( ), 1 );
#endif
}
