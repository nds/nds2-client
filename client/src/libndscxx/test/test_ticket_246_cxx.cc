#include "nds.hh"
#include "nds_testing.hh"

#include <iostream>
#include <sstream>
#include <cstdlib>
#include <string>

using namespace NDS;

/*
This python code would fail
*/

static const buffer::gps_second_type START = 1163880064;
static const buffer::gps_second_type END = START + 30;
static const std::string CHANNEL_NAME( "H1:DAQ-BCST0_FAST_DATA_CRC" );

void
check_data( const buffers_type& Buf )
{
    size_t i;
    size_t step = Buf[ 0 ].DataTypeSize( );
    size_t end = 3;

    NDS_ASSERT( step == 4 );
    for ( i = 0; i < end; i += step )
    {
        NDS_ASSERT( Buf[ 0 ].at< float >( i ) ==
                    Buf[ 0 ].at< float >( i + 1 ) );
    }
}

buffers_type
let_original_data_go_out_of_scope( connection& Conn )
{
    connection::channel_names_type channel_names;
    channel_names.push_back( CHANNEL_NAME );
    buffers_type buf = Conn.fetch( START, END, channel_names );

    check_data( buf );

    return buf;
}

int
main( int argc, char* argv[] )
{

    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );
        ps >> port;
    }
    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::string(::getenv( "NDS_TEST_HOST" ) );
    }
    std::cerr << "connecting to " << hostname << ":" << port << std::endl;

    pointer< connection > conn( make_unique_ptr< connection >(
        hostname, port, connection::PROTOCOL_TRY ) );

    buffers_type buf = let_original_data_go_out_of_scope( *conn );
    check_data( buf );

    return 0;
}
