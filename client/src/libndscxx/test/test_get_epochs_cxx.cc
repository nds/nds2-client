#include "nds.hh"

#include <cstdlib>
#include <memory>
#include <iostream>
#include <sstream>

#include "nds_testing.hh"
#include "test_macros.hh"

template < typename T >
std::string
to_string( std::shared_ptr< T > obj )
{
    std::ostringstream os;
    os << *obj;
    return os.str( );
}
template < typename T >
std::string
to_string( const T& obj )
{
    std::ostringstream os;
    os << obj;
    return os.str( );
}

NDS::epochs_type
expected_epochs( NDS::connection::protocol_type proto )
{
    NDS::epochs_type exp;
    std::string      expected;

    if ( proto == NDS::connection::PROTOCOL_ONE )
    {
        exp.emplace_back( NDS::epoch( "ALL", 0, NDS::buffer::GPS_INF ) );
        expected = std::string( "( < epoch ALL >, )" );
    }
    else
    {
        exp.emplace_back( NDS::epoch( "ALL", 0, 1999999999 ) );
        exp.emplace_back( NDS::epoch( "ER2", 1025636416, 1028563232 ) );
        exp.emplace_back( NDS::epoch( "ER3", 1042934416, 1045353616 ) );
        exp.emplace_back( NDS::epoch( "ER4", 1057881616, 1061856016 ) );
        exp.emplace_back( NDS::epoch( "ER5", 1073606416, 1078790416 ) );
        exp.emplace_back( NDS::epoch( "ER6", 1102089216, 1102863616 ) );
        exp.emplace_back( NDS::epoch( "ER7", 1116700672, 1118330880 ) );
        exp.emplace_back( NDS::epoch( "ER8", 1123856384, 1126621184 ) );
        exp.emplace_back( NDS::epoch( "NONE", 0, 0 ) );
        exp.emplace_back( NDS::epoch( "O1", 1126621184, 1999999999 ) );
        exp.emplace_back( NDS::epoch( "S5", 815153408, 880920032 ) );
        exp.emplace_back( NDS::epoch( "S6", 930960015, 971654415 ) );
        exp.emplace_back( NDS::epoch( "S6a", 930960015, 935798415 ) );
        exp.emplace_back( NDS::epoch( "S6b", 937785615, 947203215 ) );
        exp.emplace_back( NDS::epoch( "S6c", 947635215, 961545615 ) );
        exp.emplace_back( NDS::epoch( "S6d", 961545615, 971654415 ) );
        expected =
            std::string( "( < epoch ALL >, < epoch ER2 >, < epoch ER3 >," );
    }
    std::cerr << exp << std::endl;
    std::cerr << expected.size( ) << std::endl;
    std::string actual( to_string( exp ).substr( 0, expected.size( ) ) );
    std::cerr << expected << std::endl;
    std::cerr << actual << std::endl;
    NDS_ASSERT( expected == actual );
    expected = "< epoch ALL >";
    NDS_ASSERT( expected == to_string( exp[ 0 ] ) );
    return exp;
}

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TRY;
    if ( find_opt( "-proto-1", args ) )
    {
        proto = connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = connection::PROTOCOL_TWO;
    }
    else if ( std::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps( std::getenv( "NDS_TEST_PROTO" ) );
        std::cerr << "NDS_TEST_PROTO = " << ps << std::endl;
        if ( ps.compare( "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }

    std::cerr << "Connecting to " << hostname << ":" << port << " " << proto
              << std::endl;
    pointer< connection > conn =
        make_unique_ptr< connection >( hostname, port, proto );

    NDS::epochs_type epochs = conn->get_epochs( );

    NDS::epochs_type ref = expected_epochs( conn->parameters( ).protocol( ) );

    std::cout << "got " << epochs.size( ) << " entries, expecting "
              << ref.size( ) << std::endl;

    std::cout << epochs << std::endl;

    for ( int i = 0; i < epochs.size( ); ++i )
    {
        std::cout << epochs[ i ].name << "=" << epochs[ i ].gps_start << "-"
                  << epochs[ i ].gps_stop << std::endl;
    }

    NDS_ASSERT( epochs.size( ) == ref.size( ) );
    for ( int i = 0; i < epochs.size( ); ++i )
    {
        NDS_ASSERT( epochs[ i ].name == ref[ i ].name );
        NDS_ASSERT( epochs[ i ].gps_start == ref[ i ].gps_start );
        NDS_ASSERT( epochs[ i ].gps_stop == ref[ i ].gps_stop );
    }
    std::cout << "Epochs verified" << std::endl;

    return 0;
}
