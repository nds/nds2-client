#include "nds.hh"
#include "nds_buffer.hh"
#include "nds_connection.hh"
#include "nds_gap_handler.hh"
#include "nds_memory.hh"
#include "nds_testing.hh"

#include <cmath>

#include <vector>
#include <utility>
#include <iostream>

#include <limits>

#ifndef __GNUC__
#define __attribute__( x ) /* Nothing */
#endif /* __GNUC__ */

using namespace NDS;

#define RAINBOW_INT16 static_cast< ::int16_t >( 1 )
#define RAINBOW_INT32 static_cast< ::int32_t >( 2 )
#define RAINBOW_INT64 static_cast< ::int64_t >( 3 )
#define RAINBOW_FLOAT32 static_cast< float >( 4.0 )
#define RAINBOW_FLOAT64 static_cast< double >( 5.0 )
#define RAINBOW_COMPLEX_R static_cast< float >( 6.0 )
#define RAINBOW_COMPLEX_I static_cast< float >( 6.5 )
#define RAINBOW_UINT32 static_cast< ::uint32_t >( 7 )

#define RAINBOW_INT16_2 static_cast< ::int16_t >( 1 * 10 )
#define RAINBOW_INT32_2 static_cast< ::int32_t >( 2 * 10 )
#define RAINBOW_INT64_2 static_cast< ::int64_t >( 3 * 10 )
#define RAINBOW_FLOAT32_2 static_cast< float >( 4.0 * 10 )
#define RAINBOW_FLOAT64_2 static_cast< double >( 5.0 * 10 )
#define RAINBOW_COMPLEX_R_2 static_cast< float >( 6.0 * 10 )
#define RAINBOW_COMPLEX_I_2 static_cast< float >( 6.5 * 10 )
#define RAINBOW_UINT32_2 static_cast< ::uint32_t >( 7 * 10 )

#define RAINBOW_INT16_3 static_cast< ::int16_t >( 1 * 100 )
#define RAINBOW_INT32_3 static_cast< ::int32_t >( 2 * 100 )
#define RAINBOW_INT64_3 static_cast< ::int64_t >( 3 * 100 )
#define RAINBOW_FLOAT32_3 static_cast< float >( 4.0 * 100 )
#define RAINBOW_FLOAT64_3 static_cast< double >( 5.0 * 100 )
#define RAINBOW_COMPLEX_R_3 static_cast< float >( 6.0 * 100 )
#define RAINBOW_COMPLEX_I_3 static_cast< float >( 6.5 * 100 )
#define RAINBOW_UINT32_3 static_cast< ::uint32_t >( 7 * 100 )

const buffer::size_type TEST_BUFFER_SIZE = 100;

typedef enum {
    ZERO_VAL = 0,
    ONE_VAL,
    NAN_VAL,
    POS_INF_VAL,
    NEG_INF_VAL,
    RAINBOW_VAL,
    RAINBOW_VAL2,
    RAINBOW_VAL3
} fixed_val;

detail::fixed_point_gap_handler::static_val
create_vals( double value )
{
    // std::cerr << "create_vals " << value << std::endl;
    detail::fixed_point_gap_handler::static_val val;
    val.int16val = static_cast< ::int16_t >( value );
    val.int32val = static_cast< ::int32_t >( value );
    val.int64val = static_cast< ::int64_t >( value );
    val.float32val = static_cast< float >( value );
    val.float64val = value;
    val.complexrval = static_cast< float >( value );
    val.complexival = static_cast< float >( value );
    val.uint32val = static_cast< ::uint32_t >( value >= 0.0 ? value : 0.0 );
    return val;
}

detail::fixed_point_gap_handler::static_val
create_vals( fixed_val target_val )
{
    detail::fixed_point_gap_handler::static_val val;
    switch ( target_val )
    {
    default:
    case ZERO_VAL:
        return create_vals( 0.0 );
    case ONE_VAL:
        return create_vals( 1.0 );
    case NAN_VAL:
        val = create_vals( 0.0 );
        val.float32val = val.complexrval = val.complexival =
            std::numeric_limits< float >::quiet_NaN( );
        val.float64val = std::numeric_limits< double >::quiet_NaN( );
        break;
    case POS_INF_VAL:
        val.int16val = std::numeric_limits< ::int16_t >::max( );
        val.int32val = std::numeric_limits< ::int32_t >::max( );
        val.int64val = std::numeric_limits< ::int64_t >::max( );
        val.float32val = val.complexrval = val.complexival =
            std::numeric_limits< float >::infinity( );
        val.float64val = std::numeric_limits< double >::infinity( );
        val.uint32val = std::numeric_limits< ::uint32_t >::max( );
        break;
    case NEG_INF_VAL:
        val.int16val = std::numeric_limits< ::int16_t >::min( );
        val.int32val = std::numeric_limits< ::int32_t >::min( );
        val.int64val = std::numeric_limits< ::int64_t >::min( );
        val.float32val = val.complexrval = val.complexival =
            -std::numeric_limits< float >::infinity( );
        val.float64val = -std::numeric_limits< double >::infinity( );
        val.uint32val = std::numeric_limits< ::uint32_t >::min( );
        break;
    case RAINBOW_VAL:
        val.int16val = RAINBOW_INT16;
        val.int32val = RAINBOW_INT32;
        val.int64val = RAINBOW_INT64;
        val.float32val = RAINBOW_FLOAT32;
        val.float64val = RAINBOW_FLOAT64;
        val.complexrval = RAINBOW_COMPLEX_R;
        val.complexival = RAINBOW_COMPLEX_I;
        val.uint32val = RAINBOW_UINT32;
        break;
    case RAINBOW_VAL2:
        val.int16val = RAINBOW_INT16_2;
        val.int32val = RAINBOW_INT32_2;
        val.int64val = RAINBOW_INT64_2;
        val.float32val = RAINBOW_FLOAT32_2;
        val.float64val = RAINBOW_FLOAT64_2;
        val.complexrval = RAINBOW_COMPLEX_R_2;
        val.complexival = RAINBOW_COMPLEX_I_2;
        val.uint32val = RAINBOW_UINT32_2;
        break;
    case RAINBOW_VAL3:
        val.int16val = RAINBOW_INT16_3;
        val.int32val = RAINBOW_INT32_3;
        val.int64val = RAINBOW_INT64_3;
        val.float32val = RAINBOW_FLOAT32_3;
        val.float64val = RAINBOW_FLOAT64_3;
        val.complexrval = RAINBOW_COMPLEX_R_3;
        val.complexival = RAINBOW_COMPLEX_I_3;
        val.uint32val = RAINBOW_UINT32_3;
        break;
    }
    return val;
}

std::vector< channel::data_type >
create_types_vector( )
{
    std::vector< channel::data_type > types;
    types.push_back( channel::DATA_TYPE_INT16 );
    types.push_back( channel::DATA_TYPE_INT32 );
    types.push_back( channel::DATA_TYPE_INT64 );
    types.push_back( channel::DATA_TYPE_FLOAT32 );
    types.push_back( channel::DATA_TYPE_FLOAT64 );
    types.push_back( channel::DATA_TYPE_COMPLEX32 );
    types.push_back( channel::DATA_TYPE_UINT32 );
    return types;
}

std::ostream&
operator<<( std::ostream&                                      os,
            const detail::fixed_point_gap_handler::static_val& val )
{
    os << "(" << val.int16val << ", " << val.int32val << ", " << val.int64val
       << ", ";
    os << val.float32val << ", " << val.float64val << ", ";
    os << val.complexrval << ", " << val.complexival << ", " << val.uint32val
       << ")";
    return os;
}

std::unique_ptr< detail::fixed_point_gap_handler >
create_fixed_point_gap_handler(
    detail::fixed_point_gap_handler::static_val& val )
{
    return NDS::detail::make_unique< detail::fixed_point_gap_handler >( val );
}

buffer
create_test_buffer( channel::data_type DataType )
{
    channel ch( "X1:TEST_CHAN",
                channel::CHANNEL_TYPE_RAW,
                DataType,
                1024.0,
                1.0,
                1.0,
                0.0,
                "counts" );
    std::vector< unsigned char > data( TEST_BUFFER_SIZE * ch.DataTypeSize( ),
                                       0x55 );
    buffer retval( ch, 100000, 0, std::move( data ) );

    return retval;
}

template < typename T >
T
putSample( buffer& cur_buffer, buffer::size_type sample, T val )
{
    T* tmp = const_cast< T* >( cur_buffer.cbegin< T >( ) );
    if ( sample >= cur_buffer.Samples( ) )
        throw std::range_error( "out of range" );
    tmp[ sample ] = val;
    return val;
}

const std::pair< float, float >&
putSampleCmplx( buffer&           cur_buffer,
                buffer::size_type sample,
                const std::pair< float, float >& val )
{
    float* tmp = const_cast< float* >(
        reinterpret_cast< const float* >( cur_buffer.cbegin< void >( ) ) );
    if ( sample >= cur_buffer.Samples( ) )
        throw std::range_error( "out of range" );
    tmp[ sample * 2 ] = val.first;
    tmp[ sample * 2 + 1 ] = val.second;
    return val;
}

template < typename T >
T
getSample( const buffer& cur_buffer, buffer::size_type sample )
{
    return cur_buffer.at< T >( sample );
}

std::pair< float, float >
getSampleCmplx( const buffer& cur_buffer, buffer::size_type sample )
{
    std::complex< float > tmp =
        cur_buffer.at< std::complex< float > >( sample );
    return std::pair< float, float >( tmp.real( ), tmp.imag( ) );
}

void
storeSample( buffer&                                            cur_buffer,
             buffer::size_type                                  sample_offset,
             const detail::fixed_point_gap_handler::static_val& ref )
{
    switch ( cur_buffer.DataType( ) )
    {
    default:
    case channel::DATA_TYPE_UNKNOWN:
        break;
    case channel::DATA_TYPE_INT16:
        putSample< ::int16_t >( cur_buffer, sample_offset, ref.int16val );
        break;
    case channel::DATA_TYPE_INT32:
        putSample< ::int32_t >( cur_buffer, sample_offset, ref.int32val );
        break;
    case channel::DATA_TYPE_INT64:
        putSample< ::int64_t >( cur_buffer, sample_offset, ref.int64val );
        break;
    case channel::DATA_TYPE_FLOAT32:
        putSample< float >( cur_buffer, sample_offset, ref.float32val );
        break;
    case channel::DATA_TYPE_FLOAT64:
        putSample< double >( cur_buffer, sample_offset, ref.float64val );
        break;
    case channel::DATA_TYPE_COMPLEX32:
        putSampleCmplx(
            cur_buffer,
            sample_offset,
            std::pair< float, float >( ref.complexrval, ref.complexival ) );
        break;
    case channel::DATA_TYPE_UINT32:
        putSample< ::uint32_t >( cur_buffer, sample_offset, ref.uint32val );
        break;
    }
}

void
fill_buffer( buffer&                                            cur_buffer,
             const detail::fixed_point_gap_handler::static_val& val,
             buffer::size_type                                  start = 0,
             buffer::size_type end = 0xffffffffffffff )
{
    buffer::size_type cur = start;
    buffer::size_type max =
        ( end > cur_buffer.Samples( ) ? cur_buffer.Samples( ) : end );
    for ( ; cur < max; ++cur )
    {
        storeSample( cur_buffer, cur, val );
    }
}

detail::fixed_point_gap_handler::static_val&
loadSample( const buffer&                                cur_buffer,
            buffer::size_type                            sample_offset,
            detail::fixed_point_gap_handler::static_val& dest )
{
    switch ( cur_buffer.DataType( ) )
    {
    default:
    case channel::DATA_TYPE_UNKNOWN:
        break;
    case channel::DATA_TYPE_INT16:
        dest.int16val = getSample< ::int16_t >( cur_buffer, sample_offset );
        break;
    case channel::DATA_TYPE_INT32:
        dest.int32val = getSample< ::int32_t >( cur_buffer, sample_offset );
        break;
    case channel::DATA_TYPE_INT64:
        dest.int64val = getSample< ::int64_t >( cur_buffer, sample_offset );
        break;
    case channel::DATA_TYPE_FLOAT32:
        dest.float32val = getSample< float >( cur_buffer, sample_offset );
        break;
    case channel::DATA_TYPE_FLOAT64:
        dest.float64val = getSample< double >( cur_buffer, sample_offset );
        break;
    case channel::DATA_TYPE_COMPLEX32:
    {
        std::pair< float, float > tmp(
            getSampleCmplx( cur_buffer, sample_offset ) );
        dest.complexrval = tmp.first;
        dest.complexival = tmp.second;
    }
    break;
    case channel::DATA_TYPE_UINT32:
        dest.uint32val = getSample< ::uint32_t >( cur_buffer, sample_offset );
        break;
    }
    return dest;
}

template < typename T >
void
compare_float( T v1, T v2 )
{
    double zero = 0.0;

    if ( std::isnan( v1 ) && std::isnan( v2 ) )
        return;
    if ( v1 == -1.0 / zero && v2 == -1.0 / zero )
        return;
    if ( v1 == 1.0 / zero && v2 == 1.0 / zero )
        return;
    NDS_ASSERT( v1 == v2 );
}

template < typename T >
void
compare_float_ne( T v1, T v2 )
{
    NDS_ASSERT( v1 != v2 );
}

void
compare_static_vals( const detail::fixed_point_gap_handler::static_val& v1,
                     const detail::fixed_point_gap_handler::static_val& v2 )
{
    NDS_ASSERT( v1.int16val == v2.int16val );
    NDS_ASSERT( v1.int32val == v2.int32val );
    NDS_ASSERT( v1.int64val == v2.int64val );
    compare_float< float >( v1.float32val, v2.float32val );
    compare_float< double >( v1.float64val, v2.float64val );
    compare_float< float >( v1.complexrval, v2.complexrval );
    compare_float< float >( v1.complexival, v2.complexival );
    NDS_ASSERT( v1.uint32val == v2.uint32val );
}

void
compare_static_vals_ne( const detail::fixed_point_gap_handler::static_val& v1,
                        const detail::fixed_point_gap_handler::static_val& v2,
                        channel::data_type data_type )
{
    if ( data_type == channel::DATA_TYPE_INT16 )
        NDS_ASSERT( v1.int16val != v2.int16val );
    else
        NDS_ASSERT( v1.int16val == v1.int16val );

    if ( data_type == channel::DATA_TYPE_INT32 )
        NDS_ASSERT( v1.int32val != v2.int32val );
    else
        NDS_ASSERT( v1.int32val == v1.int32val );

    if ( data_type == channel::DATA_TYPE_INT64 )
        NDS_ASSERT( v1.int64val != v2.int64val );
    else
        NDS_ASSERT( v1.int64val == v1.int64val );

    if ( data_type == channel::DATA_TYPE_FLOAT32 )
        NDS_ASSERT( v1.float32val != v2.float32val );
    else
        NDS_ASSERT( v1.float32val == v1.float32val );

    if ( data_type == channel::DATA_TYPE_FLOAT64 )
        NDS_ASSERT( v1.float64val != v2.float64val );
    else
        NDS_ASSERT( v1.float64val == v1.float64val );

    if ( data_type == channel::DATA_TYPE_COMPLEX32 )
    {
        NDS_ASSERT( v1.complexrval != v2.complexrval );
        NDS_ASSERT( v1.complexival != v2.complexival );
    }
    else
    {
        NDS_ASSERT( v1.complexrval == v2.complexrval );
        NDS_ASSERT( v1.complexival == v2.complexival );
    }

    if ( data_type == channel::DATA_TYPE_UINT32 )
        NDS_ASSERT( v1.uint32val != v2.uint32val );
    else
        NDS_ASSERT( v1.uint32val == v1.uint32val );
}

void
test_create_vals( )
{
    compare_static_vals(
        create_vals( ZERO_VAL ),
        detail::fixed_point_gap_handler::static_val(
            detail::fixed_point_gap_handler::static_val::ZERO_VAL ) );
    compare_static_vals(
        create_vals( ONE_VAL ),
        detail::fixed_point_gap_handler::static_val(
            detail::fixed_point_gap_handler::static_val::ONE_VAL ) );
    compare_static_vals(
        create_vals( NAN_VAL ),
        detail::fixed_point_gap_handler::static_val(
            detail::fixed_point_gap_handler::static_val::NAN_VAL ) );
    compare_static_vals(
        create_vals( POS_INF_VAL ),
        detail::fixed_point_gap_handler::static_val(
            detail::fixed_point_gap_handler::static_val::POS_INF_VAL ) );
    compare_static_vals(
        create_vals( NEG_INF_VAL ),
        detail::fixed_point_gap_handler::static_val(
            detail::fixed_point_gap_handler::static_val::NEG_INF_VAL ) );

    compare_static_vals( create_vals( 0.0 ),
                         detail::fixed_point_gap_handler::static_val( 0.0 ) );
    compare_static_vals( create_vals( 1.0 ),
                         detail::fixed_point_gap_handler::static_val( 1.0 ) );
    compare_static_vals( create_vals( -1.0 ),
                         detail::fixed_point_gap_handler::static_val( -1.0 ) );
    compare_static_vals( create_vals( 5.0 ),
                         detail::fixed_point_gap_handler::static_val( 5.0 ) );
    {
        detail::fixed_point_gap_handler::static_val val( -1.0 );
        NDS_ASSERT( val.uint32val == 0 );
    }
    {
        detail::fixed_point_gap_handler::static_val val( 35000 );
        NDS_ASSERT( val.int16val == std::numeric_limits< ::int16_t >::max( ) );
    }
    {
        detail::fixed_point_gap_handler::static_val val( -35000 );
        NDS_ASSERT( val.int16val == std::numeric_limits< ::int16_t >::min( ) );
        NDS_ASSERT( val.int32val == -35000 );
        NDS_ASSERT( val.uint32val == 0 );
    }
    {
        detail::fixed_point_gap_handler::static_val val( -3500000000000000 );
        NDS_ASSERT( val.int16val == std::numeric_limits< ::int16_t >::min( ) );
        NDS_ASSERT( val.int32val == std::numeric_limits< ::int32_t >::min( ) );
        NDS_ASSERT( val.uint32val == 0 );
    }
}

void
test_static_rainbow( )
{
    std::vector< channel::data_type > types( create_types_vector( ) );

    for ( std::vector< channel::data_type >::iterator cur_type = types.begin( );
          cur_type != types.end( );
          ++cur_type )
    {
        buffer test_buf = create_test_buffer( *cur_type );

        detail::fixed_point_gap_handler::static_val ref_pt(
            create_vals( RAINBOW_VAL ) );

        detail::fixed_point_gap_handler::static_val vals =
            create_vals( RAINBOW_VAL );
        pointer< detail::gap_handler > handler(
            create_fixed_point_gap_handler( vals ) );
        std::unique_ptr< detail::delayed_gap_handler > delay
            __attribute__( ( unused ) ) = handler->fill_gap( test_buf, 0, 50 );
        NDS_ASSERT( delay == nullptr );
        for ( buffer::size_type sample = 0; sample < 50; ++sample )
        {
            detail::fixed_point_gap_handler::static_val test_pt(
                create_vals( RAINBOW_VAL ) );
            loadSample( test_buf, sample, test_pt );
            compare_static_vals( ref_pt, test_pt );
        }
        for ( buffer::size_type sample = 50, max = test_buf.Samples( );
              sample < max;
              ++sample )
        {
            detail::fixed_point_gap_handler::static_val test_pt(
                create_vals( RAINBOW_VAL ) );
            loadSample( test_buf, sample, test_pt );
            compare_static_vals_ne( ref_pt, test_pt, *cur_type );
        }
    }
}

void
test_continuation_handler( )
{
    std::vector< channel::data_type > types( create_types_vector( ) );

    for ( std::vector< channel::data_type >::iterator cur_type = types.begin( );
          cur_type != types.end( );
          ++cur_type )
    {
        buffer test_buf1 = create_test_buffer( *cur_type );

        const detail::fixed_point_gap_handler::static_val ref_pt1(
            create_vals( RAINBOW_VAL ) );
        const detail::fixed_point_gap_handler::static_val ref_pt2(
            create_vals( RAINBOW_VAL2 ) );
        const detail::fixed_point_gap_handler::static_val ref_pt3(
            create_vals( RAINBOW_VAL3 ) );

        // fill the first part of the buffer with RAINBOW_VAL
        fill_buffer( test_buf1, ref_pt1, 0, 10 );
        // fill the last of the buffer with RAINBOW_VAL2
        fill_buffer( test_buf1, ref_pt2, 50, TEST_BUFFER_SIZE );

        // run the gap handler
        pointer< detail::gap_handler > handler(
            NDS::detail::make_unique< detail::continuation_gap_handler >(
                ref_pt3 ) );

        pointer< detail::delayed_gap_handler > delay1(
            handler->fill_gap( test_buf1, 10, 50 ) );
        // run the delayed handler if it exists - it should
        ( *delay1 )( );

        // with this setup we should see ref_pt1 from [0,50) and ref_pt2 from
        // [50, TEST_BUFFER_SIZE)
        for ( buffer::size_type cur = 0; cur < 50; ++cur )
        {
            detail::fixed_point_gap_handler::static_val test_pt(
                create_vals( RAINBOW_VAL ) );

            loadSample( test_buf1, cur, test_pt );
            compare_static_vals( ref_pt1, test_pt );
        }
        for ( buffer::size_type cur = 50; cur < TEST_BUFFER_SIZE; ++cur )
        {
            detail::fixed_point_gap_handler::static_val test_pt(
                create_vals( RAINBOW_VAL2 ) );

            loadSample( test_buf1, cur, test_pt );
            compare_static_vals( ref_pt2, test_pt );
        }
        // now test gaps at the start which will need the default value
        buffer test_buf2 = create_test_buffer( *cur_type );
        fill_buffer( test_buf2, ref_pt2, 50, TEST_BUFFER_SIZE );

        pointer< detail::delayed_gap_handler > delay2(
            handler->fill_gap( test_buf2, 0, 50 ) );
        ( *delay2 )( );
        // with this setup we should see ref_pt3 (gap handler default) from
        // [0,50) and
        // ref_pt2 from [50, TEST_BUFFER_SIZE)
        for ( buffer::size_type cur = 0; cur < 50; ++cur )
        {
            detail::fixed_point_gap_handler::static_val test_pt(
                create_vals( RAINBOW_VAL3 ) );

            loadSample( test_buf2, cur, test_pt );
            compare_static_vals( ref_pt3, test_pt );
        }
        for ( buffer::size_type cur = 50; cur < TEST_BUFFER_SIZE; ++cur )
        {
            detail::fixed_point_gap_handler::static_val test_pt(
                create_vals( RAINBOW_VAL2 ) );

            loadSample( test_buf2, cur, test_pt );
            compare_static_vals( ref_pt2, test_pt );
        }
    }
}

int
main( int argc, char* argv[] )
{
    // test creating static points/values
    test_create_vals( );
    // test the static gap handler
    test_static_rainbow( );
    // test the continuation gap handler
    test_continuation_handler( );
    return 0;
}
