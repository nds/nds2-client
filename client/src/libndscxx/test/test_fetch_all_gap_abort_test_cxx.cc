#include "nds.hh"

#include <algorithm>
#include <memory>
#include <limits>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    auto args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    NDS::connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    NDS::connection::protocol_type proto = NDS::connection::PROTOCOL_TWO;

    NDS::connection conn( hostname, port );
    NDS_ASSERT( conn.parameters( ).set( "GAP_HANDLER", "ABORT_HANDLER" ) );

    auto channels = NDS::connection::channel_names_type{ "X1:PEM-1" };
    try
    {
        conn.fetch( 1219524930, 1219524930 + 4, channels );
        NDS_ASSERT( false );
    }
    catch ( NDS::connection::daq_error& err )
    {
        std::string err_msg{ err.what( ) };
        std::cout << err_msg << std::endl;
        NDS_ASSERT( err_msg.find( "There is a gap in X1:PEM-1 from gps "
                                  "1219524930 to 1219524934" ) >= 0 );
    }
    return 0;
}