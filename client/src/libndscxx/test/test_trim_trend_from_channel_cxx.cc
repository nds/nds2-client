#include "nds.hh"

#include <string>
#include <utility>
#include <vector>

#include "nds_testing.hh"

#include "nds_channel_selection.hh"

int
main( int, char** )
{
    typedef std::pair< std::string, std::string > test;

    std::vector< test > tests = {
        { "", "" },
        { "no changes here", "no changes here" },
        { "no changes m-trend here no delim",
          "no changes m-trend here no delim" },
        { "no changes s-trend here no delim",
          "no changes s-trend here no delim" },
        { "X1:TEST,16", "X1:TEST,16" },
        { "X1:TEST%16", "X1:TEST%16" },
        { "X1:TEST,16%m-trend", "X1:TEST,16" },
        { "X1:TEST%m-trend%16", "X1:TEST%16" },
        { "X1:TEST%s-trend%16", "X1:TEST%16" },
        { "X1:TEST,m-trend%16", "X1:TEST%16" },
        { "X1:TEST,s-trend%16", "X1:TEST%16" },
        { "X1:TEST%m-trend,16", "X1:TEST,16" },
        { "X1:TEST%s-trend,16", "X1:TEST,16" },
        { "X1:TEST%16,m-trend", "X1:TEST%16" },
        { "X1:TEST%16,s-trend", "X1:TEST%16" },

        { "X1:TEST,16%s-trend", "X1:TEST,16" },
    };
    for ( const auto& entry : tests )
    {
        auto results = NDS::detail::filter_trend_from_string( entry.first );
        std::cout << "got: " << results << " expecting: " << entry.second
                  << "\n";
        NDS_ASSERT( results == entry.second );

        auto chan1in = NDS::channel( entry.first,
                                     NDS::channel::CHANNEL_TYPE_RAW,
                                     NDS::channel::DATA_TYPE_FLOAT32,
                                     16,
                                     1.,
                                     1.,
                                     0.,
                                     "" );
        auto chan1exp = NDS::channel( entry.second,
                                      NDS::channel::CHANNEL_TYPE_RAW,
                                      NDS::channel::DATA_TYPE_FLOAT32,
                                      16,
                                      1.,
                                      1.,
                                      0.,
                                      "" );
        auto chan2in = NDS::channel( entry.first,
                                     NDS::channel::CHANNEL_TYPE_ONLINE,
                                     NDS::channel::DATA_TYPE_FLOAT64,
                                     256,
                                     0.9,
                                     0.8,
                                     0.5,
                                     "units" );
        auto chan2exp = NDS::channel( entry.second,
                                      NDS::channel::CHANNEL_TYPE_ONLINE,
                                      NDS::channel::DATA_TYPE_FLOAT64,
                                      256,
                                      0.9,
                                      0.8,
                                      0.5,
                                      "units" );

        auto chan1out = NDS::detail::strip_trend_from_name( chan1in );
        auto chan2out = NDS::detail::strip_trend_from_name( chan2in );

        NDS_ASSERT( chan1out.Name( ) == chan1exp.Name( ) );
        NDS_ASSERT( chan1out.Type( ) == chan1exp.Type( ) );
        NDS_ASSERT( chan1out.DataType( ) == chan1exp.DataType( ) );
        NDS_ASSERT( chan1out.SampleRate( ) == chan1exp.SampleRate( ) );
        NDS_ASSERT( chan1out.Gain( ) == chan1exp.Gain( ) );
        NDS_ASSERT( chan1out.Slope( ) == chan1exp.Slope( ) );
        NDS_ASSERT( chan1out.Offset( ) == chan1exp.Offset( ) );
        NDS_ASSERT( chan1out.Units( ) == chan1exp.Units( ) );

        NDS_ASSERT( chan2out.Name( ) == chan2exp.Name( ) );
        NDS_ASSERT( chan2out.Type( ) == chan2exp.Type( ) );
        NDS_ASSERT( chan2out.DataType( ) == chan2exp.DataType( ) );
        NDS_ASSERT( chan2out.SampleRate( ) == chan2exp.SampleRate( ) );
        NDS_ASSERT( chan2out.Gain( ) == chan2exp.Gain( ) );
        NDS_ASSERT( chan2out.Slope( ) == chan2exp.Slope( ) );
        NDS_ASSERT( chan2out.Offset( ) == chan2exp.Offset( ) );
        NDS_ASSERT( chan2out.Units( ) == chan2exp.Units( ) );
    }
    return 0;
}
