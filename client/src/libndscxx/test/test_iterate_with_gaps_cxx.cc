#include "nds.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

#ifndef __GNUC__
#define __attribute__( x ) /* Nothing */
#endif /* __GNUC__ */

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TRY;
    if ( find_opt( "-proto-1", args ) )
    {
        proto = connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = connection::PROTOCOL_TWO;
    }
    else if (::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps(::getenv( "NDS_TEST_PROTO" ) );
        if ( ps.compare( "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }

    pointer< connection > conn =
        make_unique_ptr< connection >( hostname, port, proto );

    connection::channel_names_type cn;
    cn.push_back(
        "H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend" );

    // This channel has an availability with a gap
    // H-H1_T:1115756400-1116285600 H-H1_T:1116286200-1118538000
    NDS::buffer::gps_second_type start = 1116286100, end = start + 200,
                                 stride = 50;
    NDS::buffer::gps_second_type last_start = 0;
    int samples_per_segment __attribute__( ( unused ) ) = stride;

    if ( proto == connection::PROTOCOL_ONE )
    {
        start = 1130797740;
        stride = 3000;
        end = start + stride * 3;

        cn.clear( );
        cn.push_back( "X1:CDS-DACTEST_COSINE_OUT_DQ.n,m-trend" );
    }

    // --------------------------------------------------------------------

    conn->parameters( ).set( "ITERATE_USE_GAP_HANDLERS", "false" );
    auto stream1 = conn->iterate( NDS::request_period( start, end ), cn );

    NDS::buffers_type bufs;
    {
        auto it = stream1.begin( );
        bufs = *( *it );
        NDS_ASSERT( it != stream1.end( ) );
        ++it;
        NDS_ASSERT( it == stream1.end( ) );

        std::cout << "Retreived a buffer " << bufs[ 0 ].Start( ) << "-"
                  << bufs[ 0 ].Stop( ) << std::endl;
        std::cout << " sample count - " << bufs[ 0 ].Samples( );
        std::cout << std::endl << std::endl;
    }
    NDS_ASSERT( stream1.begin( ) == stream1.end( ) );
    // --------------------------------------------------------------------

    conn->parameters( ).set( "ITERATE_USE_GAP_HANDLERS", "false" );
    auto stream2 = conn->iterate( NDS::request_period( start, end ), cn );

    int count = 0;
    for ( auto cur : stream2 )
    {
        std::cout << "iteration: " << count << std::endl;
        bufs = *cur;
        // make sure that the state between iterate streams
        // does not leak, ie starting a new stream does
        // not 'restart' an old stream
        NDS_ASSERT( stream1.begin( ) == stream1.end( ) );
    }

    std::cout << "Retreived a buffer " << bufs[ 0 ].Start( ) << "-"
              << bufs[ 0 ].Stop( ) << std::endl;
    std::cout << " sample count - " << bufs[ 0 ].Samples( );
    std::cout << std::endl << std::endl;

    // --------------------------------------------------------------------

    conn->parameters( ).set( "ITERATE_USE_GAP_HANDLERS", "true" );
    conn->parameters( ).set( "GAP_HANDLER", "STATIC_HANDLER_ZERO" );
    auto stream3 =
        conn->iterate( NDS::request_period( start, end, stride ), cn );
    for ( auto bufs : stream3 )
    {
        std::cerr << ".................." << std::endl;
        NDS_ASSERT( bufs->size( ) == 1 );
        const NDS::buffer& cur_buf = bufs->at( 0 );
        std::cerr << "Got buffer at " << cur_buf.Start( ) << " with "
                  << cur_buf.Samples( ) << " samples." << std::endl;
        if ( last_start == 0 )
        {
            NDS_ASSERT( cur_buf.Start( ) == start );
        }
        else
        {
            NDS_ASSERT( cur_buf.Start( ) == ( last_start + stride ) );
        }
        NDS_ASSERT( cur_buf.Stop( ) == end ||
                    cur_buf.Samples( ) == samples_per_segment );
        last_start = cur_buf.Start( );
    }

    return 0;
}
