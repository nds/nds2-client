# -*- coding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=cmake:et:sw=4:ts=4:sts=4
add_library(test_environmental_support STATIC environmental_support.cc)
function(standalone_test name command)
    if( WIN32 )
        set(CMD_PATH ${EXECUTABLE_OUTPUT_PATH})
    else( WIN32 )
        set(CMD_PATH ${CMAKE_CURRENT_BINARY_DIR})
    endif( WIN32 )

    add_executable(${command} ${command}.cc)
    #target_include_directories(${command} ${test_includes})
    target_link_libraries(${command} ndscxx test_environmental_support)
    add_test(
        NAME ${name}
        COMMAND
            "${CMD_PATH}/${command}"
    )
    set_tests_properties( ${name}
        PROPERTIES
            SKIP_RETURN_CODE 77
            TIMEOUT 180
    )
    get_target_property(${command}_includes ${command} INCLUDE_DIRECTORIES)
    message("includes for ${command} ${${command}_includes}")
endfunction(standalone_test)
function(build_test command)
    add_executable(${command} ${command}.cc)
    #target_include_directories(${command} ${test_includes})
    target_link_libraries(${command} ndscxx test_environmental_support)
endfunction(build_test)
#------------------------------------------------------------------------
# Final variable substitution before use
#------------------------------------------------------------------------
string(CONFIGURE "${NDS_TESTER_FLAGS}" NDS_TESTER_FLAGS )
set(test_names)
set(test_includes
    "${CMAKE_CURRENT_BINARY_DIR}/../module"
    "${CMAKE_CURRENT_SOURCE_DIR}/../module"
    "${CMAKE_CURRENT_BINARY_DIR}/../../client"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../client"
)
#------------------------------------------------------------------------
# Set the test includes.  When we move off of cmake 2.8.x use
# target_include_directories instead
#------------------------------------------------------------------------
include_directories(${test_includes})
#------------------------------------------------------------------------
standalone_test(_cxx_is-trend-type_ test_is_trend_type_cxx)
standalone_test(_cxx_gap-composer_ test_gap_composer_cxx)
standalone_test(_cxx_gap-handlers_ test_gap_handlers_cxx)
standalone_test(_cxx_nds2_version_ test_nds2_version_cxx)
standalone_test(_cxx_buffer_iterator_ test_buffer_iterator_cxx)
standalone_test(_cxx_nano_second_support_ test_nano_cxx)
standalone_test(_cxx_trim_trend_from_channel_ test_trim_trend_from_channel_cxx)
#standalone_test(_cxx_bad_alloc_ test_for_bad_alloc_cxx)
#------------------------------------------------------------------------
if( PYTHON_EXECUTABLE )
    function(mock_server_test name command)
        add_executable(${command} ${command}.cc)
        #target_include_directories(${command} ${test_includes})
        target_link_libraries(${command} ndscxx test_environmental_support)
        add_test(
            NAME ${name}
            COMMAND
                "${PYTHON_EXECUTABLE}" "${PROG_NDS_TESTER}" ${NDS_TESTER_FLAGS}
                --nds1-mock-server
                "$<TARGET_FILE:${command}>" )
        set_tests_properties( ${name}
            PROPERTIES
                SKIP_RETURN_CODE 77
                TIMEOUT 180 )
        set( NDS_CUSTOM_MEMCHECK_IGNORE
            ${NDS_CUSTOM_MEMCHECK_IGNORE} ${name}
            CACHE INTERNAL "" )
    endfunction(mock_server_test)
    function(replay_server_test name command json)
        build_test( ${command} )
        replay_server_test_nobuild( ${name} ${command} ${json} ${ARGN} )
    endfunction(replay_server_test)
    function(replay_server_test_nobuild name command json)
        if( WIN32 )
            set(CMD_PATH ${EXECUTABLE_OUTPUT_PATH})
        else( WIN32 )
            set(CMD_PATH ${CMAKE_CURRENT_BINARY_DIR})
        endif( WIN32 )
        if ( REPLAY_BLOB_CACHE_DIR )
            set_nds_protocols(protocols ${name})
            foreach ( proto ${protocols} )
                set(test_name "${name}${proto}_")
                add_test(
                    NAME ${test_name}
                    COMMAND
                    	${PROG_ENV} "REPLAY_BLOB_REMOTE_CACHE=${REPLAY_BLOB_CACHE_DIR}"
                        "${PYTHON_EXECUTABLE}" "${PROG_NDS_TESTER}" ${NDS_TESTER_FLAGS}
                        --replay-server
                        --replay-json-filename ${json}
                        --replay-protocol ${proto}
                        "$<TARGET_FILE:${command}>" ${ARGN} )
                set_tests_properties( ${test_name}
                    PROPERTIES
                        SKIP_RETURN_CODE 77
                        TIMEOUT 180 )
                set( NDS_CUSTOM_MEMCHECK_IGNORE
                    ${NDS_CUSTOM_MEMCHECK_IGNORE} ${test_name}
                    CACHE INTERNAL "" )
            endforeach ( proto )
        endif ( REPLAY_BLOB_CACHE_DIR )
    endfunction(replay_server_test_nobuild)
    #====================================================================
    # These tests require a minimum of Python
    #====================================================================
    mock_server_test(_cxx_mockup_ test_mockup_cxx)
    #--------------------------------------------------------------------
    build_test(test_channel_selector_cxx)
    replay_server_test_nobuild(
            _cxx_nds2_test_channel_selector_
            test_channel_selector_cxx
            nds2-channel-selector.json)
    build_test(test_ambiguous_channel_cxx)
    replay_server_test_nobuild(
            _cxx_nds2_ambiguous_channel_
            test_ambiguous_channel_cxx
            nds2-ambiguous-channel.json
    )
    #--------------------------------------------------------------------
    build_test(test_iterate_with_full_gap_cxx)
    replay_server_test_nobuild(
            _cxx_nds2_test_iterate_with_full_gap_
            test_iterate_with_full_gap_cxx
            nds2-iterate-with-full-gap.json)
    #--------------------------------------------------------------------
    build_test(test_fetch_all_gap_cxx)
    replay_server_test_nobuild(
            _cxx_nds2_fetch_all_gap_
            test_fetch_all_gap_cxx
            nds2-fetch-all-gap.json)
    #--------------------------------------------------------------------
    build_test(test_fetch_all_gap_abort_test_cxx)
    replay_server_test_nobuild(
            _cxx_nds2_fetch_all_gap_abort_test_
            test_fetch_all_gap_abort_test_cxx
            nds2-fetch-all-gap-abort-test.json)

    #--------------------------------------------------------------------
    build_test(test_fetch_cxx)
    replay_server_test_nobuild(
            _cxx_nds2_fetch_
            test_fetch_cxx
            lho-prod.json)
    #--------------------------------------------------------------------
    build_test(test_fetch_standalone_cxx)
    replay_server_test_nobuild(
            _cxx_nds2_fetch_standalone_
            test_fetch_standalone_cxx
            nds1-test-fetch-standalone.json
            -explicit-args
    )
    replay_server_test_nobuild(
            _cxx_nds2_fetch_standalone_env_
            test_fetch_standalone_cxx
            nds1-test-fetch-standalone.json
    )
    #--------------------------------------------------------------------
    build_test(test_ticket74_bad_filler_cxx)
    replay_server_test_nobuild(
            _cxx_nds2_ticket74_bad_filler_
            test_ticket74_bad_filler_cxx
            nds2-test-ticket74-bad-filler.json)
    #--------------------------------------------------------------------
    build_test(test_timeout_cxx)
    replay_server_test_nobuild(
        _cxx_nds1_timeout_
        test_timeout_cxx
        nds1-timeout.json
        -proto-1)
    replay_server_test_nobuild(
        _cxx_nds2_timeout_
        test_timeout_cxx
        nds2-timeout.json
        -proto-2 )
    #--------------------------------------------------------------------
    replay_server_test(
        _cxx_nds2_availability_
        test_nds2_availability_cxx
        nds2-availability.json )
    #--------------------------------------------------------------------
    #replay_server_test(
    #    _cxx_nds1_basic-gap_
    #    test_nds1_basic_gap_cxx
    #    nds1-basic_gap_multi.json
    #    -multi )
    #--------------------------------------------------------------------
    replay_server_test(
        _cxx_nds1_get-epochs_
        test_get_epochs_cxx
        nds1-get-epoch.json -proto-1 )
    #--------------------------------------------------------------------
    replay_server_test(
            _cxx_nds2_one-second-channel-list_
            test_nds2_one-second-channel-list_cxx
            nds2-one_second_channel_list.json )
    #--------------------------------------------------------------------
    build_test( test_nds2_dissimilar_gaps_cxx )
    replay_server_test_nobuild(
        _cxx_nds2_dissimilar-gaps_
        test_nds2_dissimilar_gaps_cxx
        nds2-dissimilar_gaps.json )
    #--------------------------------------------------------------------
    replay_server_test(
            _cxx_nds2_iterate_no_data_
            test_nds2_iterate_no_data_cxx
            nds2-iterate_with_no_data.json
    )
    build_test( test_iterate_with_gaps_cxx )
    replay_server_test_nobuild(
        _cxx_nds1_iterate-with-gaps_
        test_iterate_with_gaps_cxx
        nds1-iterate_with_leading_gap.json
        -proto-1 )
    replay_server_test_nobuild(
        _cxx_nds2_iterate-with-gaps_
        test_iterate_with_gaps_cxx
        nds2-iterate_with_leading_gap.json
        -proto-2 )
    #--------------------------------------------------------------------
    build_test( test_gap_handling_no_data_cxx )
    replay_server_test_nobuild(
        _cxx_nds1_gap_handling_no_data_
        test_gap_handling_no_data_cxx
        nds1-gap_handling_no_data.json
    )
    #--------------------------------------------------------------------
    replay_server_test(
        _cxx_nds1_restart_failure_
        test_nds1_restart_failure_cxx
        nds1-restart-failure.json
    )
    #--------------------------------------------------------------------
    build_test( test_nds_count_channels_cxx )
    replay_server_test_nobuild(
        _cxx_nds1_count-channels_
        test_nds_count_channels_cxx
        nds1-count-channels-synthetic.json
        -proto-1 )
    replay_server_test_nobuild(
        _cxx_nds2_count-channels_
        test_nds_count_channels_cxx
        nds2-count-channels-synthetic.json
        -proto-2 )
    #--------------------------------------------------------------------
    replay_server_test(
        _cxx_nds2_ticket-169_
        test_ticket_169_cxx
        nds2-ticket-169.json )
    #--------------------------------------------------------------------
    replay_server_test(
        _cxx_nds2_ticket-246_
        test_ticket_246_cxx
        nds2-ticket-246.json )
    #--------------------------------------------------------------------
    replay_server_test(
            _cxx_nds2_test_ticket-260_
            test_ticket_260_cxx
            nds2-ticket-260.json
    )
    #--------------------------------------------------------------------
    build_test( test_nds_ticket_282_cxx )
    replay_server_test_nobuild(
            _cxx_nds1_ticket-282_cxx_
            test_nds_ticket_282_cxx
            nds1-ticket-282.json
            -proto-1
    )
    #--------------------------------------------------------------------
    replay_server_test(
        _cxx_nds2_test_ticket-288_
        test_ticket_288_cxx
        nds2-ticket-288.json
    )
    #--------------------------------------------------------------------
    replay_server_test(
            _cxx_nds2_ticket-289_
            test_nds2_ticket_289_cxx
            nds2-ticket-289.json
    )
    #--------------------------------------------------------------------
    build_test( test_nds_iterate_all_data_standalone_cxx )
    replay_server_test_nobuild(
            _cxx_nds2_iterate-all-data-standalone_
            test_nds_iterate_all_data_standalone_cxx
            nds2-iterate-all-data-standalone-synthetic.json
            -proto-2 -explicit-args
    )
    replay_server_test_nobuild(
            _cxx_nds2_iterate-all-data-standalone-nogaphandler_
            test_nds_iterate_all_data_standalone_cxx
            nds2-iterate-all-data-standalone-synthetic.json
            -proto-2 -no-gap -explicit-args
    )
    replay_server_test_nobuild(
            _cxx_nds2_iterate-all-data-standalone-env_
            test_nds_iterate_all_data_standalone_cxx
            nds2-iterate-all-data-standalone-synthetic.json
            -proto-2
    )
    replay_server_test_nobuild(
            _cxx_nds2_iterate-all-data-standalone-env-nogaphandler_
            test_nds_iterate_all_data_standalone_cxx
            nds2-iterate-all-data-standalone-synthetic.json
            -proto-2 -no-gap
    )
    #--------------------------------------------------------------------
    build_test( test_nds_iterate_all_data_cxx )
    replay_server_test_nobuild(
            _cxx_nds2_iterate-all-data_
            test_nds_iterate_all_data_cxx
            nds2-iterate-all-data-synthetic.json
            -proto-2
    )
    replay_server_test_nobuild(
            _cxx_nds2_iterate-all-data-nogaphandler_
            test_nds_iterate_all_data_cxx
            nds2-iterate-all-data-synthetic.json
            -proto-2 -no-gap
    )
    replay_server_test_nobuild(
            _cxx_nds1_iterate-all-data_
            test_nds_iterate_all_data_cxx
            nds1-iterate-all-data-synthetic.json
            -proto-1
    )
    replay_server_test_nobuild(
            _cxx_nds1_iterate-all-data-nogaphandler_
            test_nds_iterate_all_data_cxx
            nds1-iterate-all-data-synthetic.json
            -proto-1 -no-gap
    )
    #--------------------------------------------------------------------
    build_test( test_nds_iterate_with_synth_gaps_cxx )
    replay_server_test_nobuild(
            _cxx_nds1_iterate-with-synth-gaps_
            test_nds_iterate_with_synth_gaps_cxx
            nds1-iterate-with-gaps-synthetic.json
            -proto-1
    )
    replay_server_test_nobuild(
            _cxx_nds1_iterate-with-synth-gaps-nogaphandler_
            test_nds_iterate_with_synth_gaps_cxx
            nds1-iterate-with-gaps-synthetic.json
            -proto-1 -no-gap
    )
    replay_server_test_nobuild(
            _cxx_nds1_iterate-with-synth-gaps-defaultgaphandler_
            test_nds_iterate_with_synth_gaps_cxx
            nds1-iterate-with-gaps-synthetic.json
            -proto-1 -default-gap-handling
    )
    #--------------------------------------------------------------------
    build_test( test_nds_iterate_live_data_bounded_cxx )
    replay_server_test_nobuild(
            _cxx_nds2_iterate-live-data-bounded_
            test_nds_iterate_live_data_bounded_cxx
            nds2-iterate-live-data-bounded-synthetic.json
            -proto-2
    )
    replay_server_test_nobuild(
            _cxx_nds1_iterate-live-data-bounded_
            test_nds_iterate_live_data_bounded_cxx
            nds1-iterate-live-data-bounded-synthetic-multiconn.json
            -proto-1
    )
    #--------------------------------------------------------------------
    build_test( test_nds_iterate_live_data_cxx )
    replay_server_test_nobuild(
            _cxx_nds2_iterate-live-data_
            test_nds_iterate_live_data_cxx
            nds2-iterate-live-data-synthetic.json
            -proto-2
    )
    replay_server_test_nobuild(
            _cxx_nds1_iterate-live-data_
            test_nds_iterate_live_data_cxx
            nds1-iterate-live-data-synthetic.json
            -proto-1
    )
    #--------------------------------------------------------------------
    build_test( test_nds_get_parameters_cxx )
    replay_server_test_nobuild(
            _cxx_nds1_get_parameters_
            test_nds_get_parameters_cxx
            nds1-get-parameters.json
            -proto-1
    )
    replay_server_test_nobuild(
            _cxx_nds2_get_parameters_
            test_nds_get_parameters_cxx
            nds2-get-parameters.json
            -proto-2
    )

    #--------------------------------------------------------------------
    build_test(test_nds1_testpoint_cxx)
    replay_server_test_nobuild(
            _cxx_nds1_testpoint_
            test_nds1_testpoint_cxx
            nds1-iterate-testpoint-online-data-synthetic.json
    )
    #--------------------------------------------------------------------
    build_test(test_nds1_list_testpoint_cxx)
    replay_server_test_nobuild(
            _cxx_nds1_list_testpoint_
            test_nds1_list_testpoint_cxx
            nds1-list-testpoint.json
            -proto-1
    )
    #--------------------------------------------------------------------
    build_test( test_nds2_find_channels_synthetic_standalone_cxx )
    replay_server_test_nobuild(
            _cxx_nds2_find-channels-synthentic-standalone_
            test_nds2_find_channels_synthetic_standalone_cxx
            nds2-find-channels-synthetic-standalone.json
    )
    #--------------------------------------------------------------------
    build_test( test_nds2_find_channels_synthetic_cxx )
    replay_server_test_nobuild(
            _cxx_nds2_find-channels-synthentic_
            test_nds2_find_channels_synthetic_cxx
            nds2-find-channels-synthetic.json
    )
    #--------------------------------------------------------------------
    build_test(test_nds2_16hz_cxx)
    replay_server_test_nobuild(
            _cxx_nds2_16hz_data_
            test_nds2_16hz_cxx
            nds2-16hz_data.json
    )
    #--------------------------------------------------------------------
    build_test( test_nds_fast_data_cxx )
    replay_server_test_nobuild(
            _cxx_nds1_fast_data_
            test_nds_fast_data_cxx
            nds1-fast-data.json
            -proto-1 -option-1
    )
    #--------------------------------------------------------------------
    replay_server_test_nobuild(
            _cxx_nds2_fast_data_
            test_nds_fast_data_cxx
            nds2-fast-data.json
            -proto-2 -option-1
    )
    #--------------------------------------------------------------------
    replay_server_test_nobuild(
            _cxx_nds1_fast_data_bounded_
            test_nds_fast_data_cxx
            nds1-fast-data-bounded.json
            -proto-1 -option-2
    )
    #--------------------------------------------------------------------
    replay_server_test_nobuild(
            _cxx_nds2_fast_data_bounded_
            test_nds_fast_data_cxx
            nds2-fast-data-bounded.json
            -proto-2 -option-2
    )
    #--------------------------------------------------------------------
    build_test( test_cpp11_traits_cxx )
    #--------------------------------------------------------------------
    build_test( test_bad_hex_in_find_channels_cxx )
    replay_server_test_nobuild(
            _cxx_nds1_test_bad_hex_in_find_channels_
            test_bad_hex_in_find_channels_cxx
            nds1_bad_hex_value.json
    )
    #--------------------------------------------------------------------
    build_test( test_time_range_error )
    replay_server_test_nobuild(
            _cxx_nds2_time-range_
            test_time_range_error
            nds2-iterate-all-data-synthetic.json
            -proto-2
    )
    #--------------------------------------------------------------------
endif( PYTHON_EXECUTABLE )
