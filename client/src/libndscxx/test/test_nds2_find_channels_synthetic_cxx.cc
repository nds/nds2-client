#include "nds.hh"
#include "nds_testing.hh"
#include "test_macros.hh"

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using NDS::connection;

int
main( int argc, char** argv )
{
    std::vector< std::string > args = convert_args( argc, argv );

    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );
        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
        hostname = ::getenv( "NDS_TEST_HOST" );

    // This test only runs against NDS2
    connection::protocol_type proto = connection::PROTOCOL_TWO;

    std::cerr << "Connecting to " << hostname << ":" << port << " ndsv2"
              << std::endl;

    pointer< connection > conn(
        make_unique_ptr< connection >( hostname, port, proto ) );
    NDS_ASSERT( conn->parameters( ).port( ) == port );
    NDS_ASSERT( conn->parameters( ).protocol( ) == 2 );

    NDS::channels_type lst =
        conn->find_channels( NDS::channel_predicate( "*" ) );
    NDS_ASSERT( lst.size( ) == 5 );

    std::vector< std::string > ref_str1;
    ref_str1.emplace_back( "<X1:PEM-1 (256Hz, RAW, FLOAT32)>" );
    ref_str1.emplace_back( "<X1:PEM-1 (1Hz, STREND, FLOAT32)>" );
    ref_str1.emplace_back( "<X1:PEM-2 (256Hz, RAW, FLOAT32)>" );
    ref_str1.emplace_back( "<X1:PEM-2 (1Hz, STREND, FLOAT32)>" );
    ref_str1.emplace_back( "<X1:PEM-3 (256Hz, RAW, FLOAT32)>" );

    for ( int i = 0; i < lst.size( ); ++i )
    {
        std::cout << lst[ i ] << std::endl;
        NDS_ASSERT( lst[ i ].NameLong( ).compare( ref_str1[ i ] ) == 0 );
    }

    NDS::channels_type lst2 =
        conn->find_channels( NDS::channel_predicate( "X1*1" ) );
    NDS_ASSERT( lst2.size( ) == 2 );

    std::vector< std::string > ref_str2;
    ref_str2.emplace_back( "<X1:PEM-1 (256Hz, RAW, FLOAT32)>" );
    ref_str2.emplace_back( "<X1:PEM-1 (1Hz, STREND, FLOAT32)>" );

    for ( int i = 0; i < lst2.size( ); ++i )
    {
        std::cout << lst2[ i ] << std::endl;
        NDS_ASSERT( lst2[ i ].NameLong( ).compare( ref_str2[ i ] ) == 0 );
    }

    NDS::channels_type lst3 = conn->find_channels(
        NDS::channel_predicate( "X1*", NDS::channel::CHANNEL_TYPE_STREND ) );
    NDS_ASSERT( lst3.size( ) == 2 );

    std::vector< std::string > ref_str3;
    ref_str3.emplace_back( "<X1:PEM-1 (1Hz, STREND, FLOAT32)>" );
    ref_str3.emplace_back( "<X1:PEM-2 (1Hz, STREND, FLOAT32)>" );

    for ( int i = 0; i < lst3.size( ); ++i )
    {
        std::cout << lst3[ i ] << std::endl;
        NDS_ASSERT( lst3[ i ].NameLong( ).compare( ref_str3[ i ] ) == 0 );
    }

    conn->close( );
}
