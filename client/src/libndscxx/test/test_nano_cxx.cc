#include <iostream>
#include <iomanip>
#include <nds.hh>
#include <nds_testing.hh>

NDS::channel
create_slow_channel( const std::string& name, NDS::channel::channel_type ctype )
{
    using namespace NDS;
    NDS_ASSERT( ctype == channel::CHANNEL_TYPE_MTREND ||
                ctype == channel::CHANNEL_TYPE_STREND );

    channel::sample_rate_type rate =
        ( ctype == channel::CHANNEL_TYPE_MTREND ? 1 / 60. : 1. );
    return channel(
        name, ctype, channel::DATA_TYPE_INT32, rate, 1.f, 1.f, 0.f, "counts" );
}

NDS::channel
create_raw_channel( const std::string&             name,
                    NDS::channel::sample_rate_type rate )
{
    using namespace NDS;

    return channel( name,
                    channel::CHANNEL_TYPE_RAW,
                    channel::DATA_TYPE_INT32,
                    rate,
                    1.f,
                    1.f,
                    0.f,
                    "counts" );
}

void
test_low_rate_channels( )
{
    using namespace NDS;

    std::int32_t data = 0;

    std::vector< channel::channel_type > ctypes{ channel::CHANNEL_TYPE_MTREND,
                                                 channel::CHANNEL_TYPE_STREND };
    for ( auto ctype : ctypes )
    {
        buffer::gps_nanosecond_type start_nano = 6000000;

        buffer buf0( create_slow_channel( "chan0", ctype ),
                     1234567890,
                     0,
                     &data,
                     sizeof( data ) );
        buffer buf1( create_slow_channel( "chan1", ctype ),
                     1234567890,
                     start_nano,
                     &data,
                     sizeof( data ) );

        NDS_ASSERT( buf0.StartNano( ) == 0 );
        NDS_ASSERT( buf0.StopNano( ) == 0 );
        NDS_ASSERT( buf0.Start( ) != buf0.Stop( ) );

        NDS_ASSERT( buf1.StartNano( ) == start_nano );
        NDS_ASSERT( buf1.StartNano( ) == buf1.StopNano( ) );
        NDS_ASSERT( buf1.Start( ) != buf1.Stop( ) );

        for ( auto i : std::initializer_list< buffer::size_type >{
                  0, 1, 3, 5, 17, 259235 } )
        {
            // This should always be 0
            NDS_ASSERT( buf0.samples_to_trailing_nanoseconds( i ) == 0 );
        }
    }
}

void
test_fast_channels( )
{
    using namespace NDS;

    std::vector< std::uint32_t > _data( 64 * 1024 * 24 );
    auto data_size = _data.size( ) * sizeof( _data[ 0 ] );

    std::vector< channel::sample_rate_type > rates{
        16.,   32.,   64.,   128.,   256.,   512.,  1024.,
        2048., 4096., 8192., 16384., 32768., 65536.
    };
    for ( auto rate : rates )
    {
        buffer::gps_nanosecond_type start_nano = 6000000;

        void* data = reinterpret_cast< void* >( _data.data( ) );
        auto  small_sample_count = 3;
        auto  small_sample_size = 3 * sizeof( _data[ 0 ] );
        NDS_ASSERT( small_sample_size <= data_size );
        buffer buf0( create_raw_channel( "chan0", rate ),
                     1234567890,
                     0,
                     data,
                     data_size );
        buffer buf1( create_raw_channel( "chan1", rate ),
                     1234567890,
                     start_nano,
                     data,
                     data_size );
        buffer buf2( create_raw_channel( "chan2", rate ),
                     1234567890,
                     start_nano,
                     data,
                     small_sample_size );

        NDS_ASSERT( buf0.StartNano( ) == 0 );
        NDS_ASSERT( buf0.StopNano( ) == 0 );
        NDS_ASSERT( buf0.Start( ) != buf0.Stop( ) );

        NDS_ASSERT( buf1.StartNano( ) == start_nano );
        NDS_ASSERT( buf1.StartNano( ) == buf1.StopNano( ) );
        NDS_ASSERT( buf1.Start( ) != buf1.Stop( ) );

        NDS_ASSERT( buf2.StartNano( ) == start_nano );
        auto deltad =
            ( 1e+9 * static_cast< double >( small_sample_count ) / rate );
        auto delta = static_cast< buffer::gps_nanosecond_type >( deltad );
        std::cerr << "rate = " << rate << std::endl;
        std::cerr << "delta = " << delta << std::endl;
        std::cerr << "deltad = " << std::setprecision( 8 ) << deltad
                  << std::endl;
        std::cerr << "start = " << buf2.StartNano( ) << std::endl;
        std::cerr << "stop = " << buf2.StopNano( ) << std::endl;
        std::cerr << std::endl;
        NDS_ASSERT( buf2.StopNano( ) == buf2.StartNano( ) + delta );
        NDS_ASSERT( buf2.Start( ) == buf2.Stop( ) );
    }
}

int
main( int argc, char* argv[] )
{
    test_low_rate_channels( );
    test_fast_channels( );
}
