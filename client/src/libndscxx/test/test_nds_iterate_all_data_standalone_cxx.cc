#include "nds.hh"
#include "nds_helper.hh"

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sstream>

#include "nds_testing.hh"
#include "test_macros.hh"

template < typename T >
class SimpleMisMatchPredicate
{
    T val_;

public:
    SimpleMisMatchPredicate( T val ) : val_( val )
    {
    }
    bool
    operator( )( const T& other ) const
    {
        return val_ != other;
    }
};

// no gaps on this test
bool
is_gap_second( NDS::buffer::gps_second_type input_gps )
{
    return false;
}

template < typename T >
bool
check_data( NDS::buffer& buf, T val )
{
    bool                   result = true;
    NDS::buffer::size_type samples_per_sec = buf.seconds_to_samples( 1 );

    // T* data = const_cast< T* >( reinterpret_cast< const T* >( &buf[ 0 ] ) );
    auto data = buf.cbegin< T >( );
    for ( NDS::buffer::gps_second_type cur_gps = buf.Start( );
          cur_gps < buf.Stop( );
          ++cur_gps )
    {
        T expected =
            ( is_gap_second( cur_gps ) ? static_cast< T >( 0.0 ) : val );
        const T* end = data + samples_per_sec;
        const T* mismatch =
            std::find_if( data, end, SimpleMisMatchPredicate< T >( expected ) );
        if ( mismatch != end )
        {
            std::cerr << "Found mismatch for " << cur_gps
                      << " expected that it is a "
                      << ( is_gap_second( cur_gps ) ? "gap" : "data" )
                      << " segment" << std::endl;
            result = false;
        }
        data = end;
    }
    return result;
}

NDS::data_iterable
launch_iterate( const NDS::parameters                      params,
                NDS::request_period                        period,
                const NDS::connection::channel_names_type& chans,
                bool                                       explicit_args )
{
    if ( explicit_args )
    {
        return NDS::iterate( params, period, chans );
    }
    return NDS::iterate( period, chans );
}

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TRY;
    if ( find_opt( "-proto-1", args ) )
    {
        proto = connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = connection::PROTOCOL_TWO;
    }
    else if ( std::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps( std::getenv( "NDS_TEST_PROTO" ) );
        std::cerr << "NDS_TEST_PROTO = " << ps << std::endl;
        if ( ps.compare( "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }

    std::string gap_handler_setting =
        ( find_opt( "-no-gap", args ) ? "false" : "true" );
    bool explicit_args = find_opt( "-explicit-args", args );
    if ( !explicit_args )
    {
        std::ostringstream os;
        os << hostname << ":" << port;
        set_env_val( "NDSSERVER", os.str( ) );
        set_env_val( "NDS2_CLIENT_PROTOCOL_VERSION", "2" );

        set_env_val( "NDS2_CLIENT_ITERATE_USE_GAP_HANDLERS",
                     gap_handler_setting );
    }
    NDS::parameters params;
    if ( explicit_args )
    {
        params = NDS::parameters( hostname, port, proto );
        params.set( "ITERATE_USE_GAP_HANDLERS", gap_handler_setting );
    }

    NDS::connection::channel_names_type chans;
    chans.push_back( "X1:PEM-1" );
    chans.push_back( "X1:PEM-2" );

    NDS::buffer::gps_second_type cur_gps = 1770000000;

    auto stream = NDS::iterate(
        params, NDS::request_period( 1770000000, 1770000020, 10 ), chans );
    int i = 0;
    for ( auto bufs : stream )
    {
        NDS_ASSERT( bufs->at( 0 ).Samples( ) == 256 * 10 );
        NDS_ASSERT(
            check_data< float >( bufs->at( 0 ), ( i == 0 ? 1.5f : 3.5f ) ) );
        NDS_ASSERT( bufs->at( 1 ).Samples( ) == 512 * 10 );
        NDS_ASSERT(
            check_data< double >( bufs->at( 1 ), ( i == 0 ? 2.75 : 4.75 ) ) );

        NDS_ASSERT( bufs->at( 0 ).Start( ) == cur_gps );
        cur_gps = bufs->at( 0 ).Stop( );
        ++i;
    }
}
