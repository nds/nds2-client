#include "nds.hh"
#include "nds_testing.hh"

#include <iostream>
#include <sstream>
#include <cstdlib>

using namespace NDS;

/*
This python code would fail

The cause of it was the availability check changing the channel from a
s-trend to an m-trend

Test availability and the actual data transfer

c = nds.connection('nds.ligo-wa.caltech.edu', 31200)
dat = c.fetch(1107878416, 1107878420,
H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend)
*/
int
main( int argc, char* argv[] )
{
    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );
        ps >> port;
    }
    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::string(::getenv( "NDS_TEST_HOST" ) );
    }
    std::cerr << "connecting to " << hostname << ":" << port << std::endl;

    pointer< connection > conn( make_unique_ptr< connection >(
        hostname, port, connection::PROTOCOL_TRY ) );

    conn->parameters( ).set( "ALLOW_DATA_ON_TAPE", "true" );
    connection::channel_names_type cn;
    cn.push_back(
        "H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend" );

    buffer::gps_second_type start = 1107878416;
    buffer::gps_second_type end = 1107878420;

    availability_list_type avail =
        conn->get_availability( NDS::epoch( start, end ), cn );

    NDS_ASSERT( avail.size( ) == 1 );
    NDS_ASSERT( avail[ 0 ].name.compare( cn[ 0 ] ) == 0 );
    NDS_ASSERT( avail[ 0 ].data.size( ) == 1 );
    NDS_ASSERT( avail[ 0 ].data[ 0 ].frame_type.compare( "H-H1_T" ) == 0 );
    NDS_ASSERT( avail[ 0 ].data[ 0 ].gps_start == start );
    NDS_ASSERT( avail[ 0 ].data[ 0 ].gps_stop == end );

    buffers_type bufs = conn->fetch( start, end, cn );

    return 0;
}
