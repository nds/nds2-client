#include "nds.hh"

#include <memory>
#include <iostream>
#include <cstdlib>
#include <sstream>

#include "test_macros.hh"
#include "nds_testing.hh"

void
test_predicates( )
{
    // Test the default state of a predicate object
    {
        NDS::channel_predicate_object pred;
        NDS_ASSERT( pred.glob( ) == "*" );
        NDS_ASSERT( pred.sample_rates( ).maximum ==
                    NDS::channel::MAX_SAMPLE_RATE );
        NDS_ASSERT( pred.sample_rates( ).minimum ==
                    NDS::channel::MIN_SAMPLE_RATE );
        {
            int mask = 0;
            for ( auto entry : pred.data_types( ) )
            {
                mask |= (int)entry;
            }
            NDS_ASSERT( mask == (int)NDS::channel::DEFAULT_DATA_MASK );
        }
        {
            int mask = 0;
            for ( auto entry : pred.channel_types( ) )
            {
                mask |= (int)entry;
            }
            NDS_ASSERT( mask == (int)NDS::channel::DEFAULT_CHANNEL_MASK );
        }
    }
}

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    test_predicates( );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 50330;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TRY;
    if ( find_opt( "-proto-1", args ) )
    {
        proto = connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = connection::PROTOCOL_TWO;
    }
    else if (::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps(::getenv( "NDS_TEST_PROTO" ) );
        std::cerr << "NDS_TEST_PROTO = " << ps << std::endl;
        if ( ps.compare( "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }

    std::cerr << "Connecting to " << hostname << ":" << port << " " << proto
              << std::endl;
    pointer< connection > conn(
        make_unique_ptr< connection >( hostname, port, proto ) );

    // The test data sets are different from nds1 to nds2, the nds1 test set
    // has the full range of s-trend and m-trend channels included
    // the nds2 set has 1 s-trend & 1 m-trend per channel
    auto actual_proto = conn->parameters( ).protocol( );
    if ( actual_proto == connection::PROTOCOL_ONE )
    {
        NDS_ASSERT( conn->count_channels( NDS::channel_predicate( "*" ) ) ==
                    4 * 11 );
        NDS_ASSERT( conn->count_channels( NDS::channel_predicate( "*4*" ) ) ==
                    11 );
        NDS_ASSERT( conn->count_channels( NDS::channel_predicate(
                        "*4*",
                        NDS::channel::DEFAULT_CHANNEL_MASK,
                        NDS::channel::DEFAULT_DATA_MASK,
                        NDS::frequency_range( 0.9, 1.1 ) ) ) == 5 );
        std::cerr << "online = "
                  << conn->count_channels( NDS::channel_predicate(
                         "*", NDS::channel::CHANNEL_TYPE_ONLINE ) )
                  << std::endl;
        NDS_ASSERT( conn->count_channels( NDS::channel_predicate(
                        "*", NDS::channel::CHANNEL_TYPE_ONLINE ) ) == 4 );
        NDS_ASSERT( conn->count_channels( NDS::channel_predicate(
                        "*",
                        NDS::channel::CHANNEL_TYPE_ONLINE,
                        NDS::channel::DEFAULT_DATA_MASK,
                        NDS::frequency_range( 10.0, 300.0 ) ) ) == 4 );
    }
    else
    {
        NDS_ASSERT( conn->count_channels( NDS::channel_predicate( "*" ) ) ==
                    12 );
        NDS_ASSERT( conn->count_channels( NDS::channel_predicate( "*4*" ) ) ==
                    3 );
        NDS_ASSERT( conn->count_channels( NDS::channel_predicate(
                        "*4*",
                        NDS::channel::DEFAULT_CHANNEL_MASK,
                        NDS::channel::DEFAULT_DATA_MASK,
                        NDS::frequency_range( .9, 1.1 ) ) ) == 1 );
        NDS_ASSERT( conn->count_channels( NDS::channel_predicate(
                        "*", NDS::channel::CHANNEL_TYPE_RAW ) ) == 4 );
        NDS_ASSERT( conn->count_channels( NDS::channel_predicate(
                        "*",
                        NDS::channel::CHANNEL_TYPE_RAW,
                        NDS::channel::DEFAULT_DATA_MASK,
                        NDS::frequency_range( 10.0, 300.0 ) ) ) == 4 );
    }

    return 0;
}
