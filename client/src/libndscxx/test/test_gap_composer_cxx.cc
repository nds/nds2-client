#include "nds.hh"
#include "nds_channel.hh"
#include "nds_buffer.hh"
#include "nds_channel_selection.hh"
#include "nds_connection.hh"
#include "nds_connection_ptype.hh"
#include "nds_gap_handler.hh"
#include "nds_composer.hh"
#include "nds_memory.hh"
#include "nds_testing.hh"

#include <algorithm>
#include <exception>
#include <iterator>
#include <iostream>
#include <stdlib.h>

#ifndef __GNUC__
#define __attribute__( x ) /* Nothing */
#endif /* __GNUC__ */

using namespace NDS;

class signal_exception
{
};

class abort_channel_selector
{
public:
    channel
    operator( )(
        const std::string&                         name,
        detail::channel_selector::selection_method policy =
            detail::channel_selector::selection_method::UNIQUE_CHANNEL )
    {
        throw std::runtime_error(
            "abort_channel_selector was asked to select a channel" );
    }
};

class noop_gap_handler : public detail::gap_handler
{
public:
    noop_gap_handler( ) : gap_handler( ){};
    ~noop_gap_handler( ) override = default;
    ;

    std::unique_ptr< detail::delayed_gap_handler >
    fill_gap( buffer&           cur_buffer,
              buffer::size_type start_sample,
              buffer::size_type end ) const override
    {
        return nullptr;
    }

    std::unique_ptr< gap_handler >
    clone( ) const override
    {
        return std::unique_ptr< detail::gap_handler >(
            NDS::detail::make_unique< noop_gap_handler >( ) );
    }
};

class counting_gap_handler : public detail::gap_handler
{
protected:
    int* _dest;

public:
    counting_gap_handler( int* dest ) : _dest( dest ){};
    ~counting_gap_handler( ) override = default;
    ;

    std::unique_ptr< detail::delayed_gap_handler >
    fill_gap( buffer&           cur_buffer,
              buffer::size_type start_sample,
              buffer::size_type end ) const override
    {
        ++( *_dest );
        return nullptr;
    }

    std::unique_ptr< gap_handler >
    clone( ) const override
    {
        return std::unique_ptr< detail::gap_handler >(
            NDS::detail::make_unique< counting_gap_handler >( _dest ) );
    }
};

class test_delayed_gap_handler_impl : public detail::delayed_gap_handler
{
    int* _counter;

public:
    test_delayed_gap_handler_impl( int* counter ) : _counter( counter ){};
    ~test_delayed_gap_handler_impl( ) override = default;
    ;

    void
    operator( )( ) override
    {
        ++( *_counter );
    };
};

class counting_delayed_gap_handler : public counting_gap_handler
{
    int* _delayed_counter;

public:
    counting_delayed_gap_handler( int* dest, int* delayed_counter )
        : counting_gap_handler( dest ), _delayed_counter( delayed_counter ){};
    ~counting_delayed_gap_handler( ) override = default;
    ;

    std::unique_ptr< detail::delayed_gap_handler >
    fill_gap( buffer&           cur_buffer,
              buffer::size_type start_sample,
              buffer::size_type end ) const override
    {
        counting_gap_handler::fill_gap( cur_buffer, start_sample, end );
        return NDS::detail::make_unique< test_delayed_gap_handler_impl >(
            _delayed_counter );
    }

    std::unique_ptr< detail::gap_handler >
    clone( ) const override
    {
        return std::unique_ptr< detail::gap_handler >(
            NDS::detail::make_unique< counting_delayed_gap_handler >(
                _dest, _delayed_counter ) );
    }
};

class exclude_n_trend_gap_handler : public counting_gap_handler
{
public:
    exclude_n_trend_gap_handler( int* dest ) : counting_gap_handler( dest ){};
    ~exclude_n_trend_gap_handler( ) override = default;
    ;

    std::unique_ptr< detail::delayed_gap_handler >
    fill_gap( buffer&           cur_buffer,
              buffer::size_type start_sample,
              buffer::size_type end ) const override
    {
        static const std::string mtrend( ".n,m-trend" );
        static const std::string strend( ".n,s-trend" );

        const std::string& name = cur_buffer.Name( );
        if ( name.size( ) > mtrend.size( ) )
        {
            std::string endstr( name.substr( ) );
            if ( mtrend.compare( endstr ) == 0 ||
                 strend.compare( endstr ) == 0 )
            {
                std::cerr << "Found a .n trend at " << cur_buffer.Name( )
                          << std::endl;
                throw signal_exception( );
            }
        }
        return counting_gap_handler::fill_gap( cur_buffer, start_sample, end );
    }

    std::unique_ptr< gap_handler >
    clone( ) const override
    {
        return std::unique_ptr< detail::gap_handler >(
            NDS::detail::make_unique< exclude_n_trend_gap_handler >( _dest ) );
    }
};

detail::request_fragment::working_buffers
get_working_buffers( buffers_type& buffers )
{
    detail::request_fragment::working_buffers working_set;

    for ( buffers_type::const_iterator cur = buffers.begin( );
          cur != buffers.end( );
          ++cur )
    {
        working_set.push_back( const_cast< buffer* >( &( *cur ) ) );
    }
    return working_set;
}

connection::channel_names_type
extract_names( const buffers_type& input )
{
    connection::channel_names_type results;
    std::transform(
        input.begin( ),
        input.end( ),
        std::back_inserter( results ),
        []( const buffer& cur ) -> std::string { return cur.Name( ); } );
    return results;
}

buffers_type
setup_buffers_strend1( )
{
    buffers_type buffers;
    buffers.push_back( buffer( channel( std::string( "X:A.min,s-trend" ),
                                        channel::CHANNEL_TYPE_STREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:A.max,s-trend" ),
                                        channel::CHANNEL_TYPE_STREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    return buffers;
}

buffers_type
setup_buffers_raw1( )
{
    buffers_type buffers;
    buffers.push_back( buffer( channel( std::string( "X:A" ),
                                        channel::CHANNEL_TYPE_RAW,
                                        channel::DATA_TYPE_FLOAT64,
                                        1024.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:B" ),
                                        channel::CHANNEL_TYPE_RAW,
                                        channel::DATA_TYPE_FLOAT64,
                                        1024.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    return buffers;
}

buffers_type
setup_buffers_mtrend1( )
{
    buffers_type buffers;
    buffers.push_back( buffer( channel( std::string( "X:A.min,m-trend" ),
                                        channel::CHANNEL_TYPE_MTREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0 / 60.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:A.max,m-trend" ),
                                        channel::CHANNEL_TYPE_MTREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0 / 60.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    return buffers;
}

buffers_type
setup_buffers_strend2( )
{
    buffers_type buffers;
    buffers.push_back( buffer( channel( std::string( "X:A.min,s-trend" ),
                                        channel::CHANNEL_TYPE_STREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:A.max,s-trend" ),
                                        channel::CHANNEL_TYPE_STREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:A.mean,s-trend" ),
                                        channel::CHANNEL_TYPE_STREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:A.rms,s-trend" ),
                                        channel::CHANNEL_TYPE_STREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:A.n,s-trend" ),
                                        channel::CHANNEL_TYPE_STREND,
                                        channel::DATA_TYPE_INT32,
                                        1.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    return buffers;
}

buffers_type
setup_buffers_mtrend2( )
{
    buffers_type buffers;
    buffers.push_back( buffer( channel( std::string( "X:A.min,m-trend" ),
                                        channel::CHANNEL_TYPE_MTREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0 / 60.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:A.max,m-trend" ),
                                        channel::CHANNEL_TYPE_MTREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0 / 60.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:A.mean,m-trend" ),
                                        channel::CHANNEL_TYPE_MTREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0 / 60.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:A.rms,m-trend" ),
                                        channel::CHANNEL_TYPE_MTREND,
                                        channel::DATA_TYPE_FLOAT64,
                                        1.0 / 60.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    buffers.push_back( buffer( channel( std::string( "X:A.n,m-trend" ),
                                        channel::CHANNEL_TYPE_MTREND,
                                        channel::DATA_TYPE_INT32,
                                        1.0 / 60.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    return buffers;
}

buffers_type
setup_only_n_strend( )
{
    buffers_type buffers;
    buffers.push_back( buffer( channel( std::string( "X:A.n,s-trend" ),
                                        channel::CHANNEL_TYPE_STREND,
                                        channel::DATA_TYPE_INT32,
                                        1.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    return buffers;
}

buffers_type
setup_only_n_mtrend( )
{
    buffers_type buffers;
    buffers.push_back( buffer( channel( std::string( "X:A.n,m-trend" ),
                                        channel::CHANNEL_TYPE_MTREND,
                                        channel::DATA_TYPE_INT32,
                                        1.0 / 60.0,
                                        1.0,
                                        1.0,
                                        0.0,
                                        std::string( "counts" ) ),
                               0,
                               0,
                               nullptr,
                               0 ) );
    return buffers;
}

template < typename T >
buffer
create_compatible_segment( const channel&          match,
                           buffer::gps_second_type start,
                           buffer::size_type       samples,
                           T                       val )
{
    std::vector< T > data( samples, val );
    return buffer( match,
                   start,
                   0,
                   reinterpret_cast< void* >( &data[ 0 ] ),
                   samples * match.DataTypeSize( ) );
}

void test_for_gap_handler_invocation( buffers_type ( *gen )( ) )
{
    int gaps = 0;

    buffer::gps_second_type start = 1000000000, end = start + 6000;
    buffers_type            buffers = gen( );
    detail::request_fragment::working_buffers working =
        get_working_buffers( buffers );
    NDS::detail::buffer_initializer init( start, end );
    counting_gap_handler            handler( &gaps );

    auto                  channels = extract_names( buffers );
    NDS::detail::composer compose( working, channels, init, handler, false );
    buffers_type          segment;
    for ( buffers_type::const_iterator cur = buffers.begin( );
          cur != buffers.end( );
          ++cur )
    {
        buffer seg =
            create_compatible_segment< double >( *cur, start, 50, 1.0 );
        std::cerr << " seg " << seg.Start( ) << " - " << seg.Stop( ) << ": "
                  << seg.Samples( ) << std::endl;
        segment.push_back( seg );
    }
    compose.add_segment( segment );
    auto selector = abort_channel_selector( );
    compose.finish( selector );
    NDS_ASSERT( gaps > 0 );
}

template < typename T >
T
getSample( const buffer& cur_buffer, buffer::size_type sample )
{
    return cur_buffer.at< T >( sample );
}

// this is a bad name
// .n trends should never user a supplied gap handler
void test_zero_gap_handler_for_n_trend( buffers_type ( *gen )( ) )
{
    int gaps = 0;

    buffer::gps_second_type start = 1000000000, end = start + 6000;
    buffers_type            buffers = gen( );
    detail::request_fragment::working_buffers working =
        get_working_buffers( buffers );
    NDS::detail::buffer_initializer init( start, end );
    exclude_n_trend_gap_handler     handler( &gaps );

    auto                  channels = extract_names( buffers );
    NDS::detail::composer compose( working, channels, init, handler, false );
    buffers_type          segment;
    for ( buffers_type::const_iterator cur = buffers.begin( );
          cur != buffers.end( );
          ++cur )
    {
        if ( cur->DataType( ) == channel::DATA_TYPE_FLOAT64 )
            segment.push_back(
                create_compatible_segment< double >( *cur, start, 50, 1.0 ) );
        if ( cur->DataType( ) == channel::DATA_TYPE_INT32 )
            segment.push_back( create_compatible_segment< int32_t >(
                *cur,
                start,
                50,
                static_cast< int32_t >( cur->SampleRate( ) ) ) );
    }
    compose.add_segment( segment );
    auto selector = abort_channel_selector( );
    compose.finish( selector );
    // now check that data has zeros
    for ( buffers_type::const_iterator cur = buffers.begin( );
          cur != buffers.end( );
          ++cur )
    {
        for ( buffer::size_type i = 0; i < 50; ++i )
        {
            switch ( cur->DataType( ) )
            {
            case channel::DATA_TYPE_FLOAT64:
                NDS_ASSERT( getSample< double >( *cur, i ) == 1.0 );
                break;
            case channel::DATA_TYPE_INT32:
                NDS_ASSERT( getSample< int32_t >( *cur, i ) ==
                            static_cast< int32_t >( cur->SampleRate( ) ) );
                break;
            default:
                NDS_ASSERT( false );
            }
        }

        for ( buffer::size_type i = 50, max = cur->Samples( ); i < max; ++i )
        {
            switch ( cur->DataType( ) )
            {
            case channel::DATA_TYPE_FLOAT64:
                NDS_ASSERT( getSample< double >( *cur, i ) == 0.0 );
                break;
            case channel::DATA_TYPE_INT32:
                NDS_ASSERT( getSample< int32_t >( *cur, i ) == 0 );
                break;
            default:
                NDS_ASSERT( false );
            }
        }
        std::cerr << std::endl;
    }
}

void test_delayed_gap_handler_impl( buffers_type ( *gen )( ) )
{
    int gaps = 0;
    int delays = 0;

    buffer::gps_second_type start = 1000000000, end = start + 6000;
    buffers_type            buffers = gen( );
    detail::request_fragment::working_buffers working =
        get_working_buffers( buffers );
    NDS::detail::buffer_initializer init( start, end );
    counting_delayed_gap_handler    handler( &gaps, &delays );

    {
        auto                  channels = extract_names( buffers );
        NDS::detail::composer compose(
            working, channels, init, handler, false );
        buffers_type segment;
        for ( buffers_type::const_iterator cur = buffers.begin( );
              cur != buffers.end( );
              ++cur )
        {
            buffer seg =
                create_compatible_segment< double >( *cur, start, 50, 1.0 );
            std::cerr << " seg " << seg.Start( ) << " - " << seg.Stop( ) << ": "
                      << seg.Samples( ) << std::endl;
            segment.push_back( seg );
        }
        compose.add_segment( segment );
        auto selector = abort_channel_selector( );
        compose.finish( selector );
    }
    NDS_ASSERT( gaps > 0 );
    NDS_ASSERT( delays > 0 );
    NDS_ASSERT( gaps == delays );
}

void test_out_of_order_failure( buffers_type ( *gen )( ) )
{
    buffer::gps_second_type start = 1000000000, end = start + 6000;
    buffers_type            buffers = gen( );
    detail::request_fragment::working_buffers working =
        get_working_buffers( buffers );
    NDS::detail::buffer_initializer init( start, end );
    noop_gap_handler                handler;

    auto                  channels = extract_names( buffers );
    NDS::detail::composer compose( working, channels, init, handler, false );
    {
        buffers_type segment;
        for ( buffers_type::const_iterator cur = buffers.begin( );
              cur != buffers.end( );
              ++cur )
        {
            buffer seg =
                create_compatible_segment< double >( *cur, start, 50, 1.0 );
            std::cerr << " seg " << seg.Start( ) << " - " << seg.Stop( ) << ": "
                      << seg.Samples( ) << std::endl;
            segment.push_back( seg );
        }
        compose.add_segment( segment );
    }
    bool failed __attribute__( ( unused ) ) = false;
    try
    {
        buffers_type segment;
        // expect this to fail
        for ( buffers_type::const_iterator cur = buffers.begin( );
              cur != buffers.end( );
              ++cur )
        {
            buffer seg =
                create_compatible_segment< double >( *cur, start + 1, 50, 1.0 );
            std::cerr << " seg " << seg.Start( ) << " - " << seg.Stop( ) << ": "
                      << seg.Samples( ) << std::endl;
            segment.push_back( seg );
        }
        compose.add_segment( segment );
    }
    catch ( connection::unexpected_channels_received_error )
    {
        failed = true;
    }
    NDS_ASSERT( failed );
}

int
main( int argc, char** argv )
{
    // test that the gap handler is called
    test_for_gap_handler_invocation( &setup_buffers_strend1 );
    test_for_gap_handler_invocation( &setup_buffers_mtrend1 );
    test_for_gap_handler_invocation( &setup_buffers_raw1 );
    // Test that the regular handlers do not get called for
    // n-trends.
    test_zero_gap_handler_for_n_trend( &setup_buffers_strend2 );
    test_zero_gap_handler_for_n_trend( &setup_buffers_mtrend2 );
    test_zero_gap_handler_for_n_trend( &setup_buffers_raw1 );
    test_zero_gap_handler_for_n_trend( &setup_only_n_strend );
    test_zero_gap_handler_for_n_trend( &setup_only_n_mtrend );
    // test delayed handlers
    test_delayed_gap_handler_impl( &setup_buffers_strend1 );
    test_delayed_gap_handler_impl( &setup_buffers_mtrend1 );
    test_delayed_gap_handler_impl( &setup_buffers_raw1 );
    // add_segment should fail if we go backwards
    test_out_of_order_failure( &setup_buffers_strend1 );
    return 0;
}
