#include "nds.hh"
#include "nds_helper.hh"

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sstream>

#include "nds_testing.hh"
#include "test_macros.hh"

template < typename T >
class SimpleMisMatchPredicate
{
    T val_;

public:
    SimpleMisMatchPredicate( T val ) : val_( val )
    {
    }
    bool
    operator( )( const T& other ) const
    {
        return val_ != other;
    }
};

// no gaps on this test
bool
is_gap_second( NDS::buffer::gps_second_type input_gps )
{
    return false;
}

template < typename T >
bool
check_data( NDS::buffer& buf, T val )
{
    bool                   result = true;
    NDS::buffer::size_type samples_per_sec = buf.seconds_to_samples( 1 );

    T* data = const_cast< T* >( buf.cbegin< T >( ) );
    for ( NDS::buffer::gps_second_type cur_gps = buf.Start( );
          cur_gps < buf.Stop( );
          ++cur_gps )
    {
        T expected =
            ( is_gap_second( cur_gps ) ? static_cast< T >( 0.0 ) : val );
        T* end = data + samples_per_sec;
        T* mismatch =
            std::find_if( data, end, SimpleMisMatchPredicate< T >( expected ) );
        if ( mismatch != end )
        {
            std::cerr << "Found mismatch for " << cur_gps
                      << " expected that it is a "
                      << ( is_gap_second( cur_gps ) ? "gap" : "data" )
                      << " segment" << std::endl;
            result = false;
        }
        data = end;
    }
    return result;
}

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TRY;
    if ( find_opt( "-proto-1", args ) )
    {
        proto = connection::PROTOCOL_ONE;
    }
    else if ( find_opt( "-proto-2", args ) )
    {
        proto = connection::PROTOCOL_TWO;
    }
    else if ( std::getenv( "NDS_TEST_PROTO" ) )
    {
        std::string ps( std::getenv( "NDS_TEST_PROTO" ) );
        std::cerr << "NDS_TEST_PROTO = " << ps << std::endl;
        if ( ps.compare( "1" ) == 0 )
            proto = connection::PROTOCOL_ONE;
        else if ( ps.compare( "2" ) == 0 )
            proto = connection::PROTOCOL_TWO;
    }

    std::cerr << "Connecting to " << hostname << ":" << port << " " << proto
              << std::endl;
    pointer< connection > conn(
        make_unique_ptr< connection >( hostname, port, proto ) );

    NDS::connection::channel_names_type chans;
    if ( proto == connection::PROTOCOL_ONE )
    {
        chans.push_back( "X1:PEM-1" );
        chans.push_back( "X1:PEM-2" );
    }
    else
    {
        chans.push_back( "X1:PEM-1,online" );
        chans.push_back( "X1:PEM-2,online" );
    }

    int  i = 0;
    auto stream = conn->iterate( NDS::request_period( ), chans );
    /* On a really system this would continue indefinitly
    * however this test is set to only send 5 seconds of data
    */
    for ( auto bufs : stream )
    {
        NDS_ASSERT( bufs->at( 0 ).Samples( ) == 256 );
        NDS_ASSERT( check_data< float >( bufs->at( 0 ), 1.5 ) );
        NDS_ASSERT( bufs->at( 1 ).Samples( ) == 512 );
        NDS_ASSERT( check_data< double >( bufs->at( 1 ), 2.75 ) );
        ++i;
        if ( i == 5 )
            break;
    }
    NDS_ASSERT( i == 5 );
}
