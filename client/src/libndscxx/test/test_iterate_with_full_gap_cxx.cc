#include "nds.hh"

#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <memory>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

void
dump_summary( const NDS::buffer& buf )
{
    using namespace std;
    cout << "----------" << endl;
    cout << "Name = " << buf.Name( ) << endl;
    cout << "Gps = (" << buf.Start( ) << "-" << buf.Stop( ) << ")" << endl;
    cout << "Data Type = " << buf.DataType( ) << endl;
    cout << "Rate = " << buf.SampleRate( ) << endl;
    cout << "Sample = " << buf.at< std::int32_t >( 0 ) << endl;
}

int
main( int argc, char* argv[] )
{

    //---------------------------------------------------------------------
    // Obtain the host & port of the server
    //---------------------------------------------------------------------
    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }
    NDS::connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    NDS::connection conn( NDS::parameters( hostname, port ) );
    NDS_ASSERT( conn.parameters( ).set( "ITERATE_USE_GAP_HANDLERS", "True" ) );
    NDS_ASSERT(
        conn.parameters( ).set( "GAP_HANDLER", "STATIC_HANDLER_NEG_INF" ) );

    auto channel_list = NDS::connection::channel_names_type{ "X1:PEM-1" };

    auto period = NDS::request_period( 1218312574, 1218312578, 2 );
    auto stream = conn.iterate( period, channel_list );
    for ( const auto& bufs : stream )
    {
        NDS::buffer& buf = bufs->at( 0 );
        dump_summary( buf );
        std::int32_t expected_val =
            ( buf.Start( ) == 1218312574
                  ? std::numeric_limits< std::int32_t >::min( )
                  : 14 );
        if ( buf.Start( ) == 1218312574 )
        {
            std::for_each( buf.cbegin< std::int32_t >( ),
                           buf.cend< std::int32_t >( ),
                           [expected_val]( std::int32_t val ) -> void {
                               NDS_ASSERT( val == expected_val );
                           } );
        }
    }

    // change the time range to a spot where
    // find_channels returns ambiguous channels for this
    // channel name.  We expect to fail here.

    bool exception_raised = false;
    period = NDS::request_period( 1218312578, 1218312582, 2 );
    try
    {
        stream = conn.iterate( period, channel_list );
    }
    catch ( NDS::connection::daq_error& err )
    {
        exception_raised = true;
        std::string err_msg( err.what( ) );
        NDS_ASSERT( err_msg.find( "X1:PEM-1" ) != std::string::npos );
        NDS_ASSERT( err_msg.find( "Unable to map" ) != std::string::npos );
    }
    NDS_ASSERT( exception_raised );

    return 0;
}