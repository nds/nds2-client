//
// Created by jonathan.hanks on 8/30/18.
//

#include "nds.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    auto args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    NDS::connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    NDS::connection::protocol_type proto = NDS::connection::PROTOCOL_ONE;

    auto params = NDS::parameters( hostname, port, proto );

    pointer< NDS::connection > conn(
        make_unique_ptr< NDS::connection >( params ) );
    auto pred = NDS::channel_predicate( NDS::channel::CHANNEL_TYPE_TEST_POINT );
    auto chans = conn->find_channels( pred );
    NDS_ASSERT( chans.size( ) > 0 );

    pred.set( NDS::channel::DEFAULT_CHANNEL_MASK );
    pred.set( "X1:TEST_POINT_2*" );
    chans = conn->find_channels( pred );
    NDS_ASSERT( chans.size( ) == 1 );
    NDS_ASSERT( chans[ 0 ].Type( ) == NDS::channel::CHANNEL_TYPE_TEST_POINT );

    pred.set( "X1:TEST_POINT_1*" );
    chans = conn->find_channels( pred );
    int tp = 0;
    int mtrend = 0;
    int strend = 0;
    int online = 0;
    int other = 0;
    for ( const auto& chan : chans )
    {
        if ( chan.Name( ) == "X1:TEST_POINT_1" )
        {
            NDS_ASSERT( chan.Type( ) == NDS::channel::CHANNEL_TYPE_TEST_POINT );
        }
        else if ( chan.Name( ).find( "X1:TEST_POINT_1" ) == 0 )
        {
            NDS_ASSERT( chan.Type( ) != NDS::channel::CHANNEL_TYPE_TEST_POINT );
        }
        switch ( chan.Type( ) )
        {
        case NDS::channel::CHANNEL_TYPE_TEST_POINT:
            ++tp;
            break;
        case NDS::channel::CHANNEL_TYPE_MTREND:
            ++mtrend;
            break;
        case NDS::channel::CHANNEL_TYPE_STREND:
            ++strend;
            break;
        case NDS::channel::CHANNEL_TYPE_ONLINE:
            ++online;
            break;
        default:
            ++other;
            break;
        }
    }
    NDS_ASSERT( tp == 1 );
    NDS_ASSERT( online == 1 );
    NDS_ASSERT( mtrend == 5 );
    NDS_ASSERT( strend == 5 );
    NDS_ASSERT( other == 0 );

    auto channels = NDS::connection::channel_names_type{ "X1:TEST_POINT_1" };
    auto stream = conn->iterate( NDS::request_period( ), channels );
    for ( const auto& bufs : stream )
    {
        NDS_ASSERT( bufs->at( 0 ).Type( ) ==
                    NDS::channel::CHANNEL_TYPE_TEST_POINT );
        break;
    }
    stream.abort( );
    return 0;
}
