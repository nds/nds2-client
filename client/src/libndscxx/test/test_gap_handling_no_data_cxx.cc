#include "nds.hh"

#include <memory>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>

#include "test_macros.hh"
#include "nds_testing.hh"

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }

    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_ONE;

    NDS::parameters params( hostname, port, proto );
    NDS_ASSERT( params.set( "GAP_HANDLER", "STATIC_HANDLER_ZERO" ) );
    pointer< NDS::connection > conn =
        make_unique_ptr< NDS::connection >( params );

    /* request where no data is avaialble */
    NDS::buffer::gps_second_type start = 1230000000;
    NDS::buffer::gps_second_type end = start + 4;

    NDS::connection::channel_names_type channels{
        "X1:PEM-1.mean,s-trend",
    };

    auto buffers = conn->fetch( start, end, channels );
    std::cout << buffers[ 0 ].Name( ) << "\n";
    NDS_ASSERT( buffers[ 0 ].Name( ) == "X1:PEM-1.mean" );
    NDS_ASSERT( buffers[ 0 ].Type( ) == NDS::channel::CHANNEL_TYPE_STREND );

    /* request where data is available */
    start = 1240000000;
    end = start + 4;

    buffers = conn->fetch( start, end, channels );

    std::cout << buffers[ 0 ].Name( ) << "\n";
    NDS_ASSERT( buffers[ 0 ].Name( ) == "X1:PEM-1.mean" );
    NDS_ASSERT( buffers[ 0 ].Type( ) == NDS::channel::CHANNEL_TYPE_STREND );
    return 0;
}
