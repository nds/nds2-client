/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */

#ifndef SWIG__COMMON__NDS_HELPER_HH
#define SWIG__COMMON__NDS_HELPER_HH

#include <memory>

#include "daqc.h"

#include "nds_channel.hh"
#include "nds_availability.hh"

namespace NDS
{
    namespace detail
    {
        channel create_channel( const chan_req_t& Source );

        void simplify_availability_intl( const availability&       avail,
                                         simple_segment_list_type& output );

        void simplify_availability_list_intl(
            const availability_list_type&  avails,
            simple_availability_list_type& output );

        std::string strip_rate_from_channel_name( const std::string& ch );
    }
}

#endif /* SWIG__COMMON__NDS_HELPER_HH */
