//
// Created by jonathan.hanks on 5/9/18.
//
#include <algorithm>
#include <memory>
#include <utility>

extern "C" {
#include "daqc_internal.h"
}

#include "nds_bash_pattern.hh"

#include "nds_connection_ptype.hh"
#include "nds_channel_cache.hh"

#include "socket/socket.hh"
#include "socket/buffered_reader.hh"
#include "common/span_reader.hh"
#include "nds1/common/utils.hh"
#include "nds1/v12_2/channel_listing.hh"

namespace NDS
{
    namespace detail
    {
        //        struct nds1_channel_search_form
        //        {
        //            char* name;
        //            double                         rate;
        //            NDS::channel::channel_type      channel_type;
        //            NDS::channel::data_type         data_type;
        //            nds1_cached_channel* source;
        //        };

        class nds1_channel_iterator
        {
        public:
            typedef detail::nds1_cached_channel value_type;
            typedef value_type&                 reference;
            typedef value_type*                 pointer;
            typedef std::size_t                 difference_type;
            typedef std::input_iterator_tag     iterator_category;

            typedef channel_cache_nds1::cache_type cache_type;

            nds1_channel_iterator( cache_type&           cache,
                                   cache_type::size_type position )
                : cache_( cache ), cur_entry_( position ), cur_variation_( 0 ),
                  transformed_entry_( )
            {
            }

            nds1_channel_iterator( const nds1_channel_iterator& other ) =
                default;

            nds1_channel_iterator( nds1_channel_iterator&& other ) = default;

            bool
            operator==( const nds1_channel_iterator& other ) const
            {
                return &cache_ == &other.cache_ &&
                    cur_entry_ == other.cur_entry_ &&
                    cur_variation_ == other.cur_variation_;
            }

            bool
            operator!=( const nds1_channel_iterator& other ) const
            {
                return !this->operator==( other );
            }

            reference operator*( )
            {
                value_type& cur = cache_[ cur_entry_ ];

                if ( cur.channel_type == NDS::channel::CHANNEL_TYPE_TEST_POINT )
                {
                    transformed_entry_ = cur;
                }
                else
                {
                    transformed_entry_.channel_type =
                        channel_info_[ cur_variation_ ].channel_type_;
                    transformed_entry_.data_type =
                        channel_info_[ cur_variation_ ].data_type_;
                    if ( transformed_entry_.data_type ==
                         NDS::channel::DATA_TYPE_UNKNOWN )
                    {
                        transformed_entry_.data_type = cur.data_type;
                    }
                    transformed_entry_.rate =
                        channel_info_[ cur_variation_ ].data_rate_;
                    if ( transformed_entry_.rate == 0.0 )
                    {
                        transformed_entry_.rate = cur.rate;
                    }
                    transformed_entry_.tpnum = cur.tpnum;
                    transformed_entry_.gain = cur.gain;
                    transformed_entry_.slope = cur.slope;
                    transformed_entry_.offset = cur.offset;

                    // FIXME: how to do this w/o strcpy
                    std::strcpy( transformed_entry_.name.data( ),
                                 cur.name.data( ) );
                    std::strcat( transformed_entry_.name.data( ),
                                 channel_info_[ cur_variation_ ].suffix_ );
                    std::copy( cur.units.begin( ),
                               cur.units.end( ),
                               transformed_entry_.units.begin( ) );
                }
                return transformed_entry_;
            }

            nds1_channel_iterator& operator++( )
            {
                cur_variation_++;
                if ( cur_variation_ >= MAX_VARIATION_ ||
                     cache_[ cur_entry_ ].channel_type ==
                         NDS::channel::CHANNEL_TYPE_TEST_POINT )
                {
                    cur_variation_ = 0;
                    cur_entry_++;
                }
                return *this;
            }

            nds1_channel_iterator operator++( int )
            {
                auto result = nds1_channel_iterator( *this );
                this->operator++( );
                return result;
            }

        private:
            struct channel_info_type
            {
                const char*                    suffix_;
                NDS::channel::channel_type     channel_type_;
                NDS::channel::data_type        data_type_;
                NDS::channel::sample_rate_type data_rate_;
            };
            static const int MAX_VARIATION_ = 11;
            static const std::array< const channel_info_type, MAX_VARIATION_ >
                                  channel_info_;
            cache_type&           cache_;
            cache_type::size_type cur_entry_;
            int                   cur_variation_;

            value_type transformed_entry_;
        };

        const std::array< const nds1_channel_iterator::channel_info_type,
                          nds1_channel_iterator::MAX_VARIATION_ >
            nds1_channel_iterator::channel_info_ = {
                { { "",
                    NDS::channel::CHANNEL_TYPE_ONLINE,
                    NDS::channel::DATA_TYPE_UNKNOWN,
                    0. },
                  { ".max,m-trend",
                    NDS::channel::CHANNEL_TYPE_MTREND,
                    NDS::channel::DATA_TYPE_UNKNOWN,
                    1. / 60. },
                  { ".max,s-trend",
                    NDS::channel::CHANNEL_TYPE_STREND,
                    NDS::channel::DATA_TYPE_UNKNOWN,
                    1. },
                  { ".mean,m-trend",
                    NDS::channel::CHANNEL_TYPE_MTREND,
                    NDS::channel::DATA_TYPE_FLOAT64,
                    1. / 60. },
                  { ".mean,s-trend",
                    NDS::channel::CHANNEL_TYPE_STREND,
                    NDS::channel::DATA_TYPE_FLOAT64,
                    1. },
                  { ".min,m-trend",
                    NDS::channel::CHANNEL_TYPE_MTREND,
                    NDS::channel::DATA_TYPE_UNKNOWN,
                    1. / 60. },
                  { ".min,s-trend",
                    NDS::channel::CHANNEL_TYPE_STREND,
                    NDS::channel::DATA_TYPE_UNKNOWN,
                    1. },
                  { ".rms,m-trend",
                    NDS::channel::CHANNEL_TYPE_MTREND,
                    NDS::channel::DATA_TYPE_FLOAT64,
                    1. / 60. },
                  { ".rms,s-trend",
                    NDS::channel::CHANNEL_TYPE_STREND,
                    NDS::channel::DATA_TYPE_FLOAT64,
                    1. },
                  { ".n,m-trend",
                    NDS::channel::CHANNEL_TYPE_MTREND,
                    NDS::channel::DATA_TYPE_INT32,
                    1. / 60. },
                  { ".n,s-trend",
                    NDS::channel::CHANNEL_TYPE_STREND,
                    NDS::channel::DATA_TYPE_INT32,
                    1. } }
            };

        template < typename Container >
        class cached_channel_to_channel
        {
        public:
            typedef detail::nds1_cached_channel value_type;

            explicit cached_channel_to_channel( Container& c ) : c_( c )
            {
            }

            cached_channel_to_channel(
                const cached_channel_to_channel< Container >& other ) = default;

            cached_channel_to_channel( cached_channel_to_channel< Container >&&
                                           other ) noexcept = default;

            cached_channel_to_channel& operator=(
                const cached_channel_to_channel< Container >& other ) = default;

            cached_channel_to_channel& operator=(
                cached_channel_to_channel< Container >&& other ) = default;

            void
            push_back( const detail::nds1_cached_channel& input )
            {
                NDS::channel tmp( input.name.data( ),
                                  input.channel_type,
                                  input.data_type,
                                  input.rate,
                                  input.gain,
                                  input.slope,
                                  input.offset,
                                  input.units.data( ) );
                c_.emplace_back( std::move( tmp ) );
            }

        private:
            Container& c_;
        };

        class sc2_to_channel_cache_transform
        {
        public:
            sc2_to_channel_cache_transform( ) = default;

            detail::nds1_cached_channel
            operator( )( nds_impl::nds1::v12_2::raw_sc2_channel input )
            {
                detail::nds1_cached_channel dest;

                typedef nds_impl::common::Span< char > char_span;
                typedef nds_impl::common::SpanReader   span_reader_t;

                char_span name_span( input.data( ), 60 );
                char_span span( input.data( ) + 60, input.size( ) - 60 - 40 );
                span_reader_t                                     r{ span };
                nds_impl::nds1::common::Basic_IO< span_reader_t > io{ r };

                char_span text = nds_impl::nds1::common::identify_padded_string(
                    name_span.data( ), name_span.data( ) + name_span.size( ) );
                char* end = std::copy( text.data( ),
                                       text.data( ) + text.size( ),
                                       dest.name.data( ) );
                *end = '\0';

                dest.rate = static_cast< double >( io.read_uint32_hex( ) );
                dest.tpnum = io.read_uint32_hex( );
                std::uint32_t group_type = io.read_uint32_hex( );
                std::uint32_t type = ( group_type >> 16 ) & 0x0ffff;

                dest.channel_type = NDS::channel::CHANNEL_TYPE_ONLINE;
                if ( type == 1000 )
                {
                    dest.channel_type = NDS::channel::CHANNEL_TYPE_MTREND;
                }
                if ( dest.tpnum != 0 )
                {
                    dest.channel_type = NDS::channel::CHANNEL_TYPE_TEST_POINT;
                }
                dest.data_type =
                    NDS::channel::convert_daq_datatype( group_type & 0x0ffff );

                dest.gain = io.read_float32_hex( );
                dest.slope = io.read_float32_hex( );
                dest.offset = io.read_float32_hex( );

                char_span unit_span( input.data( ) + input.size( ) - 40, 40 );
                text = nds_impl::nds1::common::identify_padded_string(
                    unit_span.data( ), unit_span.data( ) + unit_span.size( ) );
                end = std::copy( text.data( ),
                                 text.data( ) + text.size( ),
                                 dest.units.data( ) );
                *end = '\0';

                // std::cout << dest.name << " " << dest.rate << " " <<
                // dest.s.signal_units << std::endl;

                return dest;
            }
        };

        class ChannelMatcher
        {
        public:
            ChannelMatcher( NDS::channel::channel_type     channel_type_mask,
                            NDS::channel::data_type        data_type_mask,
                            NDS::channel::sample_rate_type min_sample_rate,
                            NDS::channel::sample_rate_type max_sample_rate,
                            detail::bash_pattern&          pattern )
                : channel_type_mask_{ channel_type_mask },
                  data_type_mask_{ data_type_mask },
                  min_sample_rate_{ min_sample_rate },
                  max_sample_rate_{ max_sample_rate }, pattern_( pattern )
            {
            }

            ChannelMatcher( const ChannelMatcher& other ) = default;

            ChannelMatcher( ChannelMatcher&& other ) = default;

            ChannelMatcher& operator=( const ChannelMatcher& other ) = default;

            ChannelMatcher& operator=( ChannelMatcher&& other ) = default;

            bool
            operator( )( const detail::nds1_cached_channel& chan )
            {
                // match on the quick things first.
                if ( ( ( chan.data_type & data_type_mask_ ) == 0 ) ||
                     ( ( chan.channel_type & channel_type_mask_ ) == 0 ) ||
                     ( chan.rate < min_sample_rate_ ||
                       chan.rate > max_sample_rate_ ) )
                {
                    return false;
                }
                bool name_matches =
                    ( pattern_.matches( chan.name.data( ) ) == 1 );
                return name_matches;
            }

        private:
            NDS::channel::channel_type     channel_type_mask_;
            NDS::channel::data_type        data_type_mask_;
            NDS::channel::sample_rate_type min_sample_rate_;
            NDS::channel::sample_rate_type max_sample_rate_;
            detail::bash_pattern&          pattern_;
        };

        channel_cache_nds1::channel_cache_nds1(
            NDS::detail::daq_accessor& server )
            : server_( server )
        {
        }

        size_t
        channel_cache_nds1::count_channels(
            std::string               channel_glob,
            channel::channel_type     channel_type_mask,
            channel::data_type        data_type_mask,
            channel::sample_rate_type min_sample_rate,
            channel::sample_rate_type max_sample_rate )
        {
            update_cache( );

            detail::bash_pattern pattern( channel_glob );

            detail::nds1_channel_iterator begin( cache_, 0 );
            detail::nds1_channel_iterator end( cache_, cache_.size( ) );

            detail::ChannelMatcher pred( channel_type_mask,
                                         data_type_mask,
                                         min_sample_rate,
                                         max_sample_rate,
                                         pattern );

            size_t result = std::count_if( begin, end, pred );

            //        std::cerr << "count_channels = " << result << " for " <<
            //        channel_glob
            //                  << " " << channel_type_mask << " " <<
            //                  data_type_mask
            //                  << " "
            //                  << std::endl;

            return result;
        }

        channels_type
        channel_cache_nds1::find_channels(
            std::string               channel_glob,
            channel::channel_type     channel_type_mask,
            channel::data_type        data_type_mask,
            channel::sample_rate_type min_sample_rate,
            channel::sample_rate_type max_sample_rate )
        {
            channels_type results;

            update_cache( );
            detail::cached_channel_to_channel< channels_type > adapter(
                results );
            auto out = std::back_inserter( adapter );

            detail::bash_pattern pattern( channel_glob );

            detail::nds1_channel_iterator begin( cache_, 0 );
            detail::nds1_channel_iterator end( cache_, cache_.size( ) );

            detail::ChannelMatcher pred( channel_type_mask,
                                         data_type_mask,
                                         min_sample_rate,
                                         max_sample_rate,
                                         pattern );

            std::copy_if( begin, end, out, pred );

            //        std::cerr << "find_channels = " << results.size( ) << "
            //        for "
            //                  << channel_glob << " " << channel_type_mask << "
            //                  "
            //                  << data_type_mask << " " << std::endl;

            return results;
        }

        void
        channel_cache_nds1::update_cache( )
        {
            if ( !cache_.empty( ) )
            {
                return;
            }
            int  retry_count = 0;
            bool retry = false;
            do
            {
                try
                {
                    retry = false;
                    nds_impl::Socket::SocketHandle s{
                        server_( )->conceal->sockfd
                    };
                    nds_impl::Socket::BufferedReader<
                        nds_impl::Socket::SocketHandle >
                        r{ s };

                    daq_send( server_( ), "status channels 2;\n" );
                    nds_impl::nds1::v12_2::read_get_channels(
                        r,
                        std::back_inserter( cache_ ),
                        detail::sc2_to_channel_cache_transform( ) );
                }
                catch ( std::invalid_argument& err )
                {
                    if ( strstr( err.what( ), "converting a hex value" ) ==
                             NULL ||
                         retry_count > 1 )
                    {
                        throw;
                    }
                    int ec = _daq_cycle_conn( server_( ) );
                    if ( ec != DAQD_OK )
                    {
                        throw;
                    }
                    retry = true;
                    ++retry_count;
                }
            } while ( retry );
        }
    }
}
