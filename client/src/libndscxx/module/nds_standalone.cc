//
// Created by jonathan.hanks on 8/20/18.
//
#include "nds_standalone.hh"

namespace NDS
{
    inline namespace abi_0
    {

        NDS::buffers_type
        fetch( const NDS::parameters&                     params,
               buffer::gps_second_type                    gps_start,
               buffer::gps_second_type                    gps_stop,
               const NDS::connection::channel_names_type& channel_names )
        {
            NDS::connection conn( params );
            return conn.fetch( gps_start, gps_stop, channel_names );
        }

        NDS::buffers_type
        fetch( buffer::gps_second_type                    gps_start,
               buffer::gps_second_type                    gps_stop,
               const NDS::connection::channel_names_type& channel_names )
        {
            NDS::parameters params;
            return fetch( params, gps_start, gps_stop, channel_names );
        }

        NDS::data_iterable
        iterate( const NDS::parameters&                     params,
                 NDS::request_period                        period,
                 const NDS::connection::channel_names_type& channel_names )
        {
            NDS::connection conn( params );
            return conn.iterate( period, channel_names );
        }

        NDS::data_iterable
        iterate( NDS::request_period                        period,
                 const NDS::connection::channel_names_type& channel_names )
        {
            NDS::parameters params;
            return iterate( params, period, channel_names );
        }

        channels_type
        find_channels( const NDS::parameters&          params,
                       const channel_predicate_object& pred )
        {
            NDS::connection conn( params );
            return conn.find_channels( pred );
        }

        channels_type
        find_channels( const channel_predicate_object& pred )
        {
            NDS::parameters params;
            return find_channels( params, pred );
        }
    }
}
