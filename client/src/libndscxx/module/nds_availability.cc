#include "nds_buffer.hh"
#include "nds_availability.hh"
#include "nds_availability_helper.hh"
#include "nds_helper.hh"

#include <algorithm>
#include <map>

namespace NDS
{

    namespace detail
    {
        bool
        gaps_equivalent_pred( const simple_segment& e1,
                              const simple_segment& e2 )
        {
            return ( e1.gps_start == e2.gps_start &&
                     e1.gps_stop == e2.gps_stop );
        }

        bool
        gaps_equivalent( const simple_segment_list_type& l1,
                         const simple_segment_list_type& l2 )
        {
            if ( l1.size( ) != l2.size( ) )
            {
                return false;
            }
            return std::equal(
                l1.begin( ), l1.end( ), l2.begin( ), gaps_equivalent_pred );
        }
    }

    inline namespace abi_0
    {

        segment::segment( ) : frame_type( "" ), gps_start( 0 ), gps_stop( 0 )
        {
        }

        segment::segment( NDS::buffer::gps_second_type gps_start,
                          NDS::buffer::gps_second_type gps_stop )
            : frame_type( "" ), gps_start( gps_start ), gps_stop( gps_stop )
        {
        }

        segment::segment( std::string                  frame_type,
                          NDS::buffer::gps_second_type gps_start,
                          NDS::buffer::gps_second_type gps_stop )
            : frame_type( frame_type ), gps_start( gps_start ),
              gps_stop( gps_stop )
        {
        }

        simple_segment::simple_segment( ) : gps_start( 0 ), gps_stop( 0 )
        {
        }

        simple_segment::simple_segment( const simple_segment& other ) = default;

        simple_segment::simple_segment( gps_second_type start,
                                        gps_second_type stop )
            : gps_start( start ), gps_stop( stop )
        {
        }

        simple_segment& simple_segment::
        operator=( const simple_segment& other ) = default;

        std::ostream&
        operator<<( std::ostream& os, const segment& obj )
        {
            os << "<" << obj.frame_type << ":" << obj.gps_start << "-"
               << obj.gps_stop << ">";
            return os;
        }

        std::ostream&
        operator<<( std::ostream& os, const simple_segment& obj )
        {
            os << "<" << obj.gps_start << "-" << obj.gps_stop << ">";
            return os;
        }

        std::ostream&
        operator<<( std::ostream& os, const segment_list_type& obj )
        {
            os << "(";
            segment_list_type::const_iterator cur = obj.begin( );
            for ( ; cur != obj.end( ); ++cur )
            {
                os << " " << cur->frame_type << ":";
                os << cur->gps_start << "-" << cur->gps_stop;
            }
            os << " )";
            return os;
        }

        std::ostream&
        operator<<( std::ostream& os, const availability& obj )
        {
            os << "<" << obj.name << " " << obj.data << " >";
            return os;
        }

        std::ostream&
        operator<<( std::ostream& os, const availability_list_type& obj )
        {
            os << "( ";
            if ( obj.begin( ) != obj.end( ) )
            {
                for ( availability_list_type::const_iterator
                          cur = obj.begin( ),
                          last = obj.end( ) - 1;
                      cur != last;
                      ++cur )
                {
                    os << ( *cur ) << ", ";
                }
                os << ( obj.back( ) ) << " ";
            }
            os << ")";
            return os;
        }

        std::ostream&
        operator<<( std::ostream& os, const simple_availability_list_type& obj )
        {
            os << "( ";
            if ( obj.begin( ) != obj.end( ) )
            {
                for ( simple_availability_list_type::const_iterator
                          cur = obj.begin( ),
                          last = obj.end( ) - 1;
                      cur != last;
                      ++cur )
                {
                    os << *cur << ", ";
                }
                os << obj.back( );
            }
            os << " )";
            return os;
        }

        std::ostream&
        operator<<( std::ostream& os, const simple_segment_list_type& obj )
        {
            os << "( ";
            simple_segment_list_type::const_iterator cur = obj.begin( );
            if ( cur != obj.end( ) )
            {
                for ( simple_segment_list_type::const_iterator
                          cur = obj.begin( ),
                          last = obj.end( ) - 1;
                      cur != last;
                      ++cur )
                {
                    os << *cur << ", ";
                }
                os << obj.back( ) << " ";
            }
            os << ")";
            return os;
        }
    }
}
