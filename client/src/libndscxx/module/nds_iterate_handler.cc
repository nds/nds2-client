#include "nds_connection_ptype.hh"

#include "nds_iterate_handler.hh"

namespace NDS
{
    namespace detail
    {
        iterate_handler::~iterate_handler( ) = default;

        void
        iterate_handler::next( )
        {
            buffers_type tmp;
            if ( !has_next( ) )
            {
                invalidate( );
                return;
            }
            try
            {
                next( tmp );
                cache_ = std::make_shared< buffers_type >( std::move( tmp ) );
            }
            catch ( std::range_error& )
            {
                invalidate( );
            }
        }

        void
        iterate_handler::abort( )
        {
            if ( !conn_p_ )
            {
                return;
            }
            conn_p_->finalize_iterate(
                this,
                NDS::detail::conn_p_type::iterate_finalize_reason::ABORTED );
            conn_p_.reset( );
            cache_ = nullptr;
        }

        void
        iterate_handler::invalidate( )
        {
            if ( !conn_p_ )
            {
                return;
            }
            conn_p_->finalize_iterate(
                this,
                NDS::detail::conn_p_type::iterate_finalize_reason::FINISHED );
            conn_p_.reset( );
            cache_ = nullptr;
        }
    }
}
