#ifndef __NDS_COMPOSER_HH__
#define __NDS_COMPOSER_HH__

#include <memory>
#include "nds_export.hh"
#include "nds_channel_selection.hh"

namespace NDS
{
    namespace detail
    {
        class composer
        {

        public:
            composer( ) = delete;
            composer( const composer& other ) = delete;
            composer( composer&& other ) = delete;
            composer& operator=( const composer& other ) = delete;
            composer& operator=( composer other ) = delete;

            DLL_EXPORT
            composer( request_fragment::working_buffers&         buffers,
                      const NDS::connection::channel_names_type& channel_names,
                      const NDS::detail::buffer_initializer&     initializer,
                      const gap_handler&                         _gap_handler,
                      bool buffers_have_been_initialized );

            DLL_EXPORT void add_segment( const buffers_type& cur_bufs );

            /*
             * \brief Apply all the gaps and finish processing the buffers
             * \param selector A selection object with the interface of
             * NDS::detail::channel_selector
             * this is used to help with initialization when no data is found
             *
             * \notes this is implemented as a template to make testing easier.
             */
            template < typename T >
            void
            finish( T& selector )
            {
                if ( finished )
                {
                    return;
                }
                finished = true;
                if ( !initialized && !empty_is_safe )
                {
                    NDS::channels_type chans;
                    chans.reserve( buffers.size( ) );
                    std::transform(
                        _channel_names.begin( ),
                        _channel_names.end( ),
                        std::back_inserter( chans ),
                        [&selector]( const std::string& name ) -> NDS::channel {
                            return selector(
                                name,
                                channel_selector::selection_method::
                                    FIRST_CHANNEL );
                        } );
                    initialize_buffers( chans );
                    if ( !initialized )
                    {
                        throw connection::daq_error(
                            DAQD_ERROR,
                            "Internal client error, unable to "
                            "determine the channel meta data for a "
                            "buffer" );
                    }
                }
                do_finish_gap_handling( );
            }

        private:
            template < typename T >
            void
            initialize_buffers( const T& ref_set )
            {
                if ( initialized )
                {
                    return;
                }
                if ( ref_set.size( ) != buffers.size( ) )
                {
                    return;
                }
                int j = 0;
                for ( typename T::const_iterator cur_buf = ref_set.begin( );
                      cur_buf != ref_set.end( );
                      ++cur_buf, ++j )
                {
                    initializer.reset_buffer( buffers[ j ], *cur_buf );
                    NDS::detail::dout( )
                        << "Initializing buffer for " << buffers[ j ]->Name( )
                        << " with " << buffers[ j ]->Samples( ) << " entries"
                        << std::endl;
                }
                initialized = true;
            }

            void fill_gap( buffer&           cur_buffer,
                           buffer::size_type start_sample,
                           buffer::size_type end_sample );

            void bounds_check( const buffer&     cur_buffer,
                               buffer::size_type cur_fill,
                               buffer::size_type offset_start,
                               buffer::size_type offset_end );
            DLL_EXPORT
            void do_finish_gap_handling( );

            request_fragment::working_buffers&        buffers;
            const NDS::connection::channel_names_type _channel_names;
            const NDS::detail::buffer_initializer     initializer;
            const gap_handler&                        _gap_handler;
            std::vector< buffer::size_type >          cur_progress;
            std::vector< std::unique_ptr< delayed_gap_handler > >
                 delayed_handlers;
            bool empty_is_safe;
            bool initialized;
            bool finished;

            static fixed_point_gap_handler zero_gap_handler;
        };

        inline void
        composer::fill_gap( buffer&           cur_buffer,
                            buffer::size_type start_sample,
                            buffer::size_type end_sample )
        {
            static const std::string tname( ".n,s-trend" );
            static const std::string cmp( ".n," );

            // .n trends are always filled with 0s.  This is a flag that the
            // data
            // was gap filled
            // look for .n,s-trend or .n,m-trend, can probably optimize this to
            // look
            // for the '.n,'
            const std::string& name = cur_buffer.Name( );
            if ( name.size( ) > tname.size( ) &&
                 name.compare(
                     name.size( ) - tname.size( ), cmp.size( ), cmp ) == 0 )
            {
                zero_gap_handler.fill_gap(
                    cur_buffer, start_sample, end_sample );
            }
            else
            {
                std::unique_ptr< delayed_gap_handler > tmp(
                    _gap_handler.fill_gap(
                        cur_buffer, start_sample, end_sample ) );
                if ( tmp )
                {
                    delayed_handlers.push_back( std::move( tmp ) );
                }
            }
        }
    }

} // namespace NDS

#endif // __NDS_COMPOSER_H__
