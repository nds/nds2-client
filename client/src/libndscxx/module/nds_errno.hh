#ifndef NDS_ERRNO_HH
#define NDS_ERRNO_HH

namespace NDS
{

    enum errnum
    {
        EOS = 256, /* end of stream */
        CHANNEL_NAME_TOO_LONG, /* channel name is too long */
        UNEXPECTED_CHANNELS_RECEIVED, /* server sent more channels than expected
                                         */
        MINUTE_TRENDS, /* some minute trends were requested, but start and stop
                          times were not divisible by 60 */
        TRANSFER_BUSY, /* another transfer is already in progress */
        NO_SUCH_CHANNEL, /* no such channel */
        ALREADY_CLOSED, /* connection already closed */
        ERROR_LAST /* last error code */
    };
}

#endif // NDS_ERRNO_HH
