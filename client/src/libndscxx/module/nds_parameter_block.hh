/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */

#ifndef SWIG__COMMON__NDS_PARAMS_HH
#define SWIG__COMMON__NDS_PARAMS_HH

#include <string>
#include <vector>
#include <unordered_map>
#include <memory>

#include "nds_gap_handler.hh"
#include "nds_connection.hh"

namespace NDS
{
    namespace detail
    {
        /// \brief Given a std::string extract a boolean value
        /// \param input std::string input
        /// \param dest boolean value extracted from the string
        /// \return true if a value could be extracted, else false if
        /// dest is unchanged.
        bool str_to_bool( const std::string& input, bool& dest );

        struct param_gap_handler
        {
            std::string                    name;
            std::unique_ptr< gap_handler > handler;

            param_gap_handler( )
                : name( "ABORT_HANDLER" ),
                  handler(
                      NDS::detail::make_unique< detail::abort_gap_handler >( ) )
            {
            }
            param_gap_handler( std::string                    handler_name,
                               std::unique_ptr< gap_handler > handler_ptr )
                : name( std::move( handler_name ) ),
                  handler( std::move( handler_ptr ) )
            {
            }
            param_gap_handler( const param_gap_handler& other )
                : name( other.name ), handler( other.handler->clone( ) )
            {
            }
            param_gap_handler( param_gap_handler&& other ) = default;
            param_gap_handler&
            operator=( const param_gap_handler& other )
            {
                if ( this != &other )
                {
                    std::unique_ptr< gap_handler > tmp(
                        other.handler->clone( ) );
                    name = other.name;
                    handler = std::move( tmp );
                }
                return *this;
            }
            param_gap_handler& operator=( param_gap_handler&& other ) = default;
            ~param_gap_handler( ) = default;
        };

        struct param_net_conn_info
        {
            NDS::connection::host_type     hostname;
            NDS::connection::port_type     port;
            NDS::connection::protocol_type protocol;

            param_net_conn_info( )
                : hostname( "localhost" ),
                  port( NDS::connection::DEFAULT_PORT ),
                  protocol( NDS::connection::PROTOCOL_TRY )
            {
            }
            param_net_conn_info( NDS::connection::host_type     Hostname,
                                 NDS::connection::port_type     Port,
                                 NDS::connection::protocol_type Protocol )
                : hostname( std::move( Hostname ) ), port( Port ),
                  protocol( Protocol )
            {
            }
            param_net_conn_info( const param_net_conn_info& other ) = default;
            param_net_conn_info( param_net_conn_info&& other ) noexcept =
                default;

            param_net_conn_info&
            operator=( const param_net_conn_info& other ) = default;
            param_net_conn_info&
            operator=( param_net_conn_info&& other ) = delete;
        };

        //-----------------------------------------------------------------
        /// \brief A parameter block describes parameters affecting the
        /// behavior of the NDS::connection
        //-----------------------------------------------------------------

        class parameter_block
        {
        public:
            parameter_block( );
            explicit parameter_block( const param_net_conn_info& conn_info );

            parameter_block( const parameter_block& other ) = default;
            parameter_block( parameter_block&& other ) = default;

            parameter_block&
            operator=( const parameter_block& other ) = default;
            parameter_block&
            operator=( parameter_block&& other ) noexcept = delete;

            // Get the string representation of the given parameter, or ""
            // if the parameter does not exist
            std::string get( const std::string& parameter ) const;

            // Set the string representation of a parameter
            // @param[in] parameter - The name of the parameter to set
            // @param[in] value - The string representation of the value
            // @returns - true if the value could be set, else false
            bool set( const std::string& parameter, const std::string& value );

            // Return a sequence of strings listing all the parameters
            // that can be set
            //
            // @returns std::vector<std::string> contianing parameter names
            std::vector< std::string > parameters( ) const;

            // Return the standard prefix that should be applied to
            // the parameter name when reading it from the environment
            std::string env_prefix( ) const;

            // After how many commands should we cycle an nds1 connection
            int
            max_nds1_command_count( ) const
            {
                return _max_nds1_command_count;
            }

            // Are requests allowed to read data from tape?
            bool
            allow_data_on_tape( ) const
            {
                return _allow_data_on_tape;
            };

            // Retrieve gap handler
            std::unique_ptr< NDS::detail::gap_handler >
            gap_handler( ) const
            {
                return _gap_handler.handler->clone( );
            }

            // Should iterate use the gap handler
            bool
            iterate_uses_gap_handler( ) const
            {
                return _iterate_uses_gap_handler;
            }

            param_net_conn_info& conn_info( );

            const param_net_conn_info& conn_info( ) const;

        private:
            int                 _max_nds1_command_count;
            bool                _allow_data_on_tape;
            bool                _iterate_uses_gap_handler;
            param_gap_handler   _gap_handler;
            param_net_conn_info _net_conn_info;

            bool set_gap_handler( const std::string& handler_str );
        };

        /**
         * \brief A parameter_accessor allows internal code to access the
         * detail::parameter_block in a NDS::connection::parameters object
         */
        class parameter_accessor
        {
        public:
            explicit parameter_accessor( NDS::parameters& params )
                : params_( params )
            {
            }
            parameter_accessor( const parameter_accessor& other ) = default;
            parameter_accessor( parameter_accessor&& other ) = delete;

            parameter_block&
            operator( )( ) const
            {
                return *( params_.p_ );
            }

        private:
            NDS::parameters& params_;
        };
    }
}

#endif // SWIG__COMMON__NDS_PARAMS_HH
