#define NOMINMAX

#include "nds.hh"
#include "nds_channel.hh"
#include "nds_buffer.hh"
#include "nds_connection.hh"
#include "nds_connection_ptype.hh"
#include "nds_iterate_handlers.hh"
#include "nds_composer.hh"
#include "nds_helper.hh"
#include "nds_foreach.hh"
#include "nds_str_utility.hh"
#include "nds_channel_selection.hh"

#include "nds_errno.hh"

#include "debug_stream.hh"

#include "nds1.h"

#include "nds_os.h"

#ifdef HAVE_IO_H
#include <io.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <cassert>
#include <cctype>
#include <cmath>
#include <cstring>

#include <algorithm>
#include <iostream>
#include <sstream>
#include <utility>

static const int _NDS2_TYPICAL_BYTES_PER_FRAME = ( 1 << 26 );

namespace NDS
{

    typedef int daq_result_type;

    namespace
    {
        /// FIXME: duplicate code
        template < typename T >
        T
        safe_add( T val1, T val2 )
        {
            static const T max_val = std::numeric_limits< T >::max( );
            if ( val2 >= max_val - val1 )
            {
                return max_val;
            }
            return val1 + val2;
        }

        class fetch_planner
        {
        public:
            fetch_planner( detail::request_fragments_type& fragments )
                : _fragments( fragments )
            {
            }

            void
            operator( )( simple_segment_list_type& ssegment,
                         const std::string&        name,
                         buffer&                   buf )
            {
                bool placed = false;
                for ( detail::request_fragments_type::iterator cur_frag =
                          _fragments.begin( );
                      cur_frag != _fragments.end( ) && !placed;
                      ++cur_frag )
                {
                    if ( cur_frag->push_back_if( name, ssegment, &buf ) )
                    {
                        placed = true;
                    }
                }
                if ( !placed )
                {
                    detail::request_fragment fragment;
                    fragment.push_back_if( name, ssegment, &buf );
                    _fragments.push_back( fragment );
                }
            }

        private:
            detail::request_fragments_type& _fragments;
        };

        class segment_intersects_test
        {
            buffer::gps_second_type _gps_start;
            buffer::gps_second_type _gps_stop;

        public:
            segment_intersects_test( buffer::gps_second_type gps_start,
                                     buffer::gps_second_type gps_stop )
                : _gps_start( gps_start ), _gps_stop( gps_stop )
            {
            }

            template < typename T >
            bool
            operator( )( T segment )
            {
                return !( segment.gps_stop <= _gps_start ||
                          segment.gps_start >= _gps_stop );
            }
        };

        //
        bool
        default_channel_list_compare( const channel& ch1, const channel& ch2 )
        {
            bool result = true;
            int  name_state = ch1.Name( ).compare( ch2.Name( ) );
            if ( name_state < 0 )
            {
                result = true;
            }
            else if ( name_state > 0 )
            {
                result = false;
            }
            else
            {
                if ( ch1.Type( ) < ch2.Type( ) )
                {
                    result = true;
                }
                else if ( ch1.Type( ) > ch2.Type( ) )
                {
                    result = false;
                }
                else
                {
                    result = ( ch1.SampleRate( ) < ch2.SampleRate( ) );
                }
            }
            return result;
        };
    }

    namespace detail
    {
        simple_availability_list_type
        get_availabilty( conn_p_type&                         conn_p,
                         buffer::gps_second_type              gps_start,
                         buffer::gps_second_type              gps_stop,
                         const connection::channel_names_type channel_names )
        {
            auto get_avail = [&conn_p, gps_start, gps_stop](
                const connection::channel_names_type names )
                -> simple_availability_list_type {
                    try
                    {
                        auto avail = conn_p.get_availability(
                            gps_start, gps_stop, names );
                        return avail.simple_list( );
                    }
                    catch ( connection::daq_error& err )
                    {
                        if ( err.DAQCode( ) != DAQD_INVALID_CHANNEL_NAME )
                        {
                            throw;
                        }
                    }
                    return simple_availability_list_type( );
                };

            auto results = get_avail( channel_names );
            if ( !results.empty( ) )
            {
                return results;
            }
            // we must do this in a way to catch invalid channels
            for ( const auto& name : channel_names )
            {
                connection::channel_names_type cur_names;
                cur_names.push_back( name );

                auto short_list = get_avail( cur_names );
                if ( short_list.empty( ) )
                {
                    simple_segment_list_type tmp;
                    results.emplace_back( std::move( tmp ) );
                }
                else
                {
                    results.push_back( short_list.front( ) );
                }
            }
            return results;
        }

        void
        buffer_initializer::reset_buffer( buffer*        cur_buffer,
                                          const channel& channel_info ) const
        {
            if ( cur_buffer->Name( ) == channel_info.Name( ) &&
                 cur_buffer->Type( ) == channel_info.Type( ) &&
                 cur_buffer->DataType( ) == channel_info.DataType( ) &&
                 cur_buffer->SampleRate( ) == channel_info.SampleRate( ) &&
                 cur_buffer->Start( ) == gps_start &&
                 cur_buffer->Stop( ) == gps_stop )
            {
                NDS::detail::dout( )
                    << "Buffer already initialized, not resetting" << std::endl;
                return;
            }
            NDS::detail::dout( ) << "initializing buffer for "
                                 << channel_info.NameLong( );
            buffer::size_type N = static_cast< buffer::size_type >(
                ( gps_stop - gps_start ) * channel_info.SampleRate( ) );
            NDS::detail::dout( ) << " - N=" << N << " samples "
                                 << static_cast< void* >( cur_buffer )
                                 << std::endl;
            cur_buffer->reset_channel_info( channel_info, gps_start, 0 );
            cur_buffer->resize( N );
        }

        //---------------------------------------------------------------------
        // conn_p_type
        //---------------------------------------------------------------------

        bool conn_p_type::initialized = false;

        conn_p_type::conn_p_type( const NDS::parameters& params )
            : host( params.host( ) ), port( params.port( ) ),
              protocol( params.protocol( ) ), accesor_( this->handle ),
              connected( false ), request_start_time_( 0 ),
              request_end_time_( 0 ), request_in_progress_( false ),
              current_epoch_( "", 0, buffer::GPS_INF ),
              channel_cache_( accesor_ ), parameters_( params )
        {
            //-------------------------------------------------------------------
            // Make sure the underling libary is properly initialized
            //-------------------------------------------------------------------
            // Fixme: This should be a critical section
            if ( !initialized )
            {
                int result = daq_startup( );

                if ( result == DAQD_OK )
                {
                    initialized = true;
                }
                else
                {
                    throw connection::error( "daq_startup" );
                }
            }

            handle.err_num = DAQD_OK;
            connect( );
            sync_parameters( );
        }

        conn_p_type::~conn_p_type( )
        {
            shutdown( );
        }

        void
        conn_p_type::connect( )
        {
            daq_result_type result;

            if ( host.size( ) <= 0 )
            {
                return;
            }
            if ( protocol == connection::protocol_type::PROTOCOL_TRY )
            {
                NDS::detail::dout( ) << "Connect: Trying: PROTOCOL_TRY"
                                     << std::endl;
                result = daq_connect( &handle, host.c_str( ), port, nds_try );
            }
            else
            {
                NDS::detail::dout( ) << "Connect: Trying: PROTOCOL_??"
                                     << protocol << std::endl;
                result = daq_connect(
                    &handle, host.c_str( ), port, (nds_version)protocol );
            }
            NDS::detail::dout( )
                << "Connect: connecting using protocol: " << handle.nds_versn
                << std::endl;

            if ( result != DAQD_OK )
            {
                std::ostringstream msg;

                msg << "Failed to establish a connection";
                if ( handle.err_num != DAQD_OK )
                {
                    msg << "[INFO: " << daq_strerror( handle.err_num ) << "]";
                }
                else
                {
                    msg << "[INFO: " << daq_strerror( result ) << "]";
                }
                throw connection::error( msg.str( ) );
            }
            connected = true;
            switch ( handle.nds_versn )
            {
            case nds_v1:
                protocol = connection::protocol_type::PROTOCOL_ONE;
                NDS::detail::dout( ) << "Connect: PROTOCOL_ONE" << std::endl;
                break;
            case nds_v2:
                protocol = connection::protocol_type::PROTOCOL_TWO;
                NDS::detail::dout( ) << "Connect: PROTOCOL_TWO" << std::endl;
                break;
            default:
                throw connection::error( "Connected to server with unknown "
                                         "server protocol version" );
                break;
            }
            detail::parameter_accessor paccess( parameters_ );
            paccess( ).conn_info( ).protocol = protocol;
        }

        availability_list_type
        conn_p_type::get_availability(
            const epoch&                          time_span,
            const connection::channel_names_type& channel_names )
        {
            set_epoch_if_changed( time_span );
            return get_availability( 0, NDS::buffer::GPS_INF, channel_names );
        }

        availability_list_type
        conn_p_type::get_availability(
            buffer::gps_second_type               gps_start,
            buffer::gps_second_type               gps_stop,
            const connection::channel_names_type& channel_names )
        {

            availability_list_type retval;

            if ( request_in_progress( ) )
            {
                throw connection::transfer_busy_error( );
            }
            if ( protocol == connection::protocol_type::PROTOCOL_ONE )
            {
                for ( connection::channel_names_type::const_iterator cur =
                          channel_names.begin( );
                      cur != channel_names.end( );
                      cur++ )
                {
                    availability_list_type::value_type avail;
                    avail.name = *cur;
                    retval.push_back( avail );
                }
                // not really supported under NDS1
                return retval;
            }

            NDS::detail::dout( ) << "get_availability " << gps_start << "-"
                                 << gps_stop << std::endl;

            {
                std::ostringstream cmdBuf;
                cmdBuf << "get-source-data " << gps_start << " {";
                bool first = true;
                for ( connection::channel_names_type::const_iterator cur =
                          channel_names.begin( );
                      cur != channel_names.end( );
                      cur++ )
                {
                    if ( !first )
                    {
                        cmdBuf << " ";
                    }
                    cmdBuf << strip_rate_from_channel_name( *cur );
                    first = false;
                }
                cmdBuf << "};\n";
                validate_daq( daq_send( &( handle ), cmdBuf.str( ).c_str( ) ) );
                NDS::detail::dout( ) << "Sent command '" << cmdBuf.str( ) << "'"
                                     << std::endl;
            }
            socket_t   fd = handle.conceal->sockfd;
            uint4_type count = 0;
            if ( !_read_uint4( fd, &count ) )
            {
                throw connection::daq_error( DAQD_ERROR,
                                             "Error reading data from server" );
            }
            std::vector< char > list( count );
            _read_buffer( fd, &( list[ 0 ] ), count );
            std::string data( &list[ 0 ], count );

            // NDS::detail::dout() << "Availability data:" << std::endl << data
            // <<
            // std::endl
            // << std::endl;

            availability_list_type tmp;
            std::string::size_type cur_pos = 0;
            tmp.reserve( channel_names.size( ) );
            for ( connection::channel_names_type::const_iterator cur =
                      channel_names.begin( );
                  cur != channel_names.end( );
                  cur++ )
            {
                availability_list_type::value_type avail;
                avail.name = *cur;

                std::string expected_name( cur->substr( 0, cur->find( "," ) ) );
                // search for the name
                // all but the first name will have a preceeding ' '
                if ( cur_pos != 0 )
                {
                    if ( data[ cur_pos ] == ' ' )
                    {
                        cur_pos++;
                    }
                }

                std::string::size_type tmp_pos =
                    data.find( expected_name, cur_pos );
                if ( data.find( expected_name, cur_pos ) != cur_pos )
                {
                    throw connection::unexpected_channels_received_error( );
                }
                cur_pos += expected_name.size( );
                // get the ' {'
                if ( data.find( " {", cur_pos ) != cur_pos )
                {
                    NDS::detail::dout( ) << "could not find ' {'" << std::endl;
                    throw connection::daq_error(
                        DAQD_ERROR,
                        "Error parsing channel availability information." );
                }
                cur_pos += 2;
                // find the closing '}'
                std::string::size_type end_chan = data.find( "}", cur_pos );
                if ( end_chan == std::string::npos )
                {
                    NDS::detail::dout( ) << "could not find '}'" << std::endl;
                    throw connection::daq_error(
                        DAQD_ERROR,
                        "Error parsing channel availability information." );
                }

                // get the segments for this channel
                std::string segment_list_str =
                    data.substr( cur_pos, end_chan - cur_pos );

                // NDS::detail::dout() << "Spliting '" << segment_list_str <<
                // "'" <<
                // std::endl;

                std::vector< std::string > segment_list =
                    split( segment_list_str, ' ' );

                for ( std::vector< std::string >::iterator curs =
                          segment_list.begin( );
                      curs != segment_list.end( );
                      curs++ )
                {
                    if ( curs->size( ) == 0 )
                    {
                        continue;
                    }
                    // NDS::detail::dout() << "Iterating over segment '" <<
                    // *curs << "'"
                    // <<
                    // std::endl;
                    segment_list_type::value_type cur_segment;

                    std::string::size_type point1 = curs->find( ':' );
                    std::string::size_type point2 =
                        curs->find( '-', point1 + 1 );
                    if ( point1 == std::string::npos ||
                         point2 == std::string::npos )
                    {
                        NDS::detail::dout( ) << "could not find [:-]"
                                             << std::endl;
                        throw connection::daq_error(
                            DAQD_ERROR,
                            "Error parsing channel availability information." );
                    }
                    cur_segment.frame_type = curs->substr( 0, point1 );
                    {
                        ++point1;
                        std::istringstream tmpstr(
                            curs->substr( point1, point2 - point1 ) );
                        tmpstr >> cur_segment.gps_start;
                    }
                    {
                        std::istringstream tmpstr(
                            curs->substr( point2 + 1, std::string::npos ) );
                        tmpstr >> cur_segment.gps_stop;
                    }
                    /* trim segments to the time range we are looking for */
                    NDS::detail::dout( ) << "params (" << gps_start << ", "
                                         << gps_stop << ") ";
                    NDS::detail::dout( ) << "seg (" << cur_segment.gps_start
                                         << ", " << cur_segment.gps_stop << ")"
                                         << std::endl;
                    // reject out of hand
                    if ( cur_segment.gps_stop <= gps_start ||
                         cur_segment.gps_start >= gps_stop )
                    {
                        continue;
                    }
                    if ( cur_segment.gps_start < gps_start )
                    {
                        // overlaps the start
                        cur_segment.gps_start = gps_start;
                    }
                    if ( cur_segment.gps_stop > gps_stop )
                    {
                        // overlaps the end
                        cur_segment.gps_stop = gps_stop;
                    }
                    // sanity check
                    if ( cur_segment.gps_start >= cur_segment.gps_stop )
                    {
                        continue;
                    }
                    NDS::detail::dout( ) << cur_segment.frame_type << ":"
                                         << cur_segment.gps_start << "-"
                                         << cur_segment.gps_stop << std::endl;
                    avail.data.push_back( cur_segment );
                }
                tmp.push_back( avail );
                cur_pos = end_chan + 1;
            }
            retval.swap( tmp );
            return retval;
        }

        void
        conn_p_type::setup_daq_chanlist(
            buffer::gps_second_type               gps_start,
            const connection::channel_names_type& channel_names,
            bool&                                 have_minute_trends,
            double&                               bytes_per_sample,
            std::vector< NDS::channel >*          final_channel_list )
        {
            /* Fail if there is another transfer already in progress. */
            if ( request_in_progress( ) )
            {
                throw connection::transfer_busy_error( );
            }
            have_minute_trends = false;
            bytes_per_sample = 0.0;

            daq_clear_channel_list( &handle );

            NDS::detail::dout( ) << "looping through channel names - len "
                                 << channel_names.size( ) << std::endl;
            for ( connection::channel_names_type::const_iterator
                      cur = channel_names.begin( ),
                      last = channel_names.end( );
                  cur != last;
                  ++cur )
            {
                NDS::detail::dout( ) << "ch: " << *cur << std::endl;
                daq_channel_t channel;

                std::fill( reinterpret_cast< char* >( &channel ),
                           reinterpret_cast< char* >( &channel ) +
                               sizeof( channel ),
                           '\0' );

                infer_daq_channel_info( *cur, channel, gps_start );
                if ( channel.type == cMTrend )
                {
                    have_minute_trends = true;
                }

                /* For the purpose of computing the byte rate, use the
             * default channel bytes per sample (bytes_per_sample) and sample
             * rate.
             */
                bytes_per_sample += channel.bps * channel.rate;
                NDS::detail::dout( )
                    << "channel.bytes_per_sample(" << channel.bps
                    << ") * channel.rate(" << channel.rate
                    << ") = " << channel.bps * channel.rate << std::endl;
                NDS::detail::dout( )
                    << "bytes_per_sample = " << bytes_per_sample << std::endl;

                validate_daq(
                    daq_request_channel_from_chanlist( &handle, &channel ) );
                if ( final_channel_list )
                {
                    NDS::channel nds_chan(
                        channel.name,
                        NDS::channel::convert_daq_chantype( channel.type ),
                        NDS::channel::convert_daq_datatype( channel.data_type ),
                        channel.rate,
                        channel.s.signal_gain,
                        channel.s.signal_slope,
                        channel.s.signal_offset,
                        channel.s.signal_units );
                    final_channel_list->push_back( nds_chan );
                }
            }
        }

        void
        conn_p_type::process_check_data_result( int result, bool gaps_ok )
        {
            parameter_accessor paccess( parameters_ );

            if ( result == DAQD_OK || result == DAQD_COMMAND_SYNTAX ||
                 result == DAQD_NOT_SUPPORTED ||
                 ( result == DAQD_ONTAPE &&
                   paccess( ).allow_data_on_tape( ) ) ||
                 ( result == DAQD_NOT_FOUND && gaps_ok ) )
            {
                return;
            }
            validate_daq( result );
        }

        void
        conn_p_type::plan_fetches(
            buffer::gps_second_type               gps_start,
            buffer::gps_second_type               gps_stop,
            const connection::channel_names_type& channel_names,
            buffers_type&                         dest_buffers,
            request_fragments_type&               retval )
        {
            NDS::detail::dout( ) << "plan_fetches " << gps_start << "-"
                                 << gps_stop << std::endl;
            request_fragments_type fragments;

            // NDS1 or cases where data is completely available should
            // be fast tracked
            bool fast_track = false;
            try
            {
                check( gps_start, gps_stop, channel_names );
                fast_track = true;
            }
            catch ( connection::daq_error& err )
            {
                // in this case a not found error is not an error, it just means
                // we
                // need to figure out availability.  However anything else is an
                // error
                // so let that filter up.
                auto code = err.DAQCode( );
                if ( code != DAQD_NOT_FOUND &&
                     code != DAQD_INVALID_CHANNEL_NAME )
                {
                    throw;
                }
            }

            if ( fast_track )
            {
                request_fragment                  fragment;
                request_fragment::working_buffers buffers;
                for ( buffers_type::iterator cur = dest_buffers.begin( );
                      cur != dest_buffers.end( );
                      ++cur )
                {
                    buffers.push_back( &( *cur ) );
                }
                fragment.bulk_set(
                    channel_names, buffers, gps_start, gps_stop );
                fragments.push_back( fragment );
            }
            else
            {
                /* call check the data, this should check to see if it is
             * on disk or has gaps
             */
                // availability_list_type _avail =
                // get_availability( gps_start, gps_stop, channel_names );
                // simple_availability_list_type avail = _avail.simple_list( );
                simple_availability_list_type avail = get_availabilty(
                    *this, gps_start, gps_stop, channel_names );

                fetch_planner _planner( fragments );
                detail::for_each3(
                    avail.begin( ),
                    avail.end( ),
                    channel_names.begin( ),
                    const_cast< buffer* >( &( dest_buffers[ 0 ] ) ),
                    _planner );
            }

            retval.swap( fragments );
        }

        bool
        conn_p_type::check(
            buffer::gps_second_type               gps_start,
            buffer::gps_second_type               gps_stop,
            const connection::channel_names_type& channel_names )
        {
            if ( protocol == connection::PROTOCOL_ONE )
            {
                return true;
            }

            double dummy_bps = 0.0;
            bool   dummy_have_minute_trends = false;

            setup_daq_chanlist(
                gps_start, channel_names, dummy_have_minute_trends, dummy_bps );
            process_check_data_result( daq_request_check( &handle,
                                                          (time_t)gps_start,
                                                          (time_t)gps_stop ),
                                       false );
            return true;
        }

        buffers_type
        conn_p_type::fetch( buffer::gps_second_type               gps_start,
                            buffer::gps_second_type               gps_stop,
                            const connection::channel_names_type& channel_names,
                            channels_type* reference_channels )
        {
            buffers_type       retval;
            buffer_initializer initializer( gps_start, gps_stop );
            bool               buffers_initialized = false;

            retval.resize( channel_names.size( ) );
            if ( reference_channels &&
                 reference_channels->size( ) == channel_names.size( ) )
            {
                auto curChan = reference_channels->begin( );
                auto curBuf = retval.begin( );
                for ( ; curBuf != retval.end( ); ++curBuf, ++curChan )
                {
                    initializer.reset_buffer( &( *curBuf ), *curChan );
                }
                buffers_initialized = true;
            }
            if ( request_in_progress( ) )
            {
                throw connection::transfer_busy_error( );
            }
            request_fragments_type fragment_list;
            plan_fetches(
                gps_start, gps_stop, channel_names, retval, fragment_list );
            for ( auto& cur_fragment : fragment_list )
            {
                fetch_fragment(
                    cur_fragment, initializer, buffers_initialized );
            }

            return retval;
        }

        void
        conn_p_type::fill_gap( channel::data_type DataType,
                               channel::size_type DataSizeType,
                               unsigned char*     start,
                               unsigned char*     end )
        {
            std::fill( start, end, 0 );
        }

        void
        conn_p_type::infer_daq_channel_info( const std::string& channel_name,
                                             daq_channel_t&     channel,
                                             time_type          gps )
        {
            if ( protocol == connection::protocol_type::PROTOCOL_TWO )
            {
                chantype_t channel_type;
                double     sample_rate;

                /* For NDS2, infer channel type and sample rate from
             * name if possible (minute and second trends have
             * special suffixes), otherwise fill in defaults.
             */
                if ( channel::IsMinuteTrend( channel_name ) )
                {
                    channel_type = cMTrend;
                    sample_rate = 1.0 / 60;
                }
                if ( channel::IsSecondTrend( channel_name ) )
                {
                    channel_type = cSTrend;
                    sample_rate = 1.0;
                }
                else
                {
                    channel_type = cUnknown;
                    sample_rate = 16384;
                }

                daq_init_channel( &channel,
                                  channel_name.c_str( ),
                                  channel_type,
                                  sample_rate,
                                  _undefined );
            }
            else
            {
                /* For NDS1, query the server for the channel info. */
                int num_received;

                channel_mem_cache_type::iterator it =
                    channel_mem_cache_.find( channel_name );
                if ( it == channel_mem_cache_.end( ) )
                {
                    int result =
                        daq_recv_channels_by_pattern( &handle,
                                                      &channel,
                                                      1,
                                                      &num_received,
                                                      (time_t)gps,
                                                      cUnknown,
                                                      channel_name.c_str( ) );
                    if ( ( result != DAQD_OK ) || ( num_received != 1 ) )
                    {
                        throw connection::daq_error( result,
                                                     get_last_message( ) );
                    }
                    if ( channel.tpnum > 0 )
                    {
                        channel.type = cTestPoint;
                    }
                    channel_mem_cache_[ channel_name ] = channel;
                }
                else
                {
                    channel = it->second;
                }
            }
        }

        std::string
        conn_p_type::get_last_message( ) const throw( )
        {
            try
            {
                char buf[ 1024 ];
                int  len = 0;
                if ( daq_get_last_message( const_cast< daq_t* >( &handle ),
                                           buf,
                                           sizeof( buf ),
                                           &len ) < 0 )
                {
                    return std::string( "" );
                }
                buf[ sizeof( buf ) - 1 ] = '\0';
                return std::string( buf );
            }
            catch ( ... )
            {
                return std::string( "" );
            }
        }

        std::string
        conn_p_type::err_msg_unexpected_no_data_found(
            buffer::gps_second_type            gps_start,
            buffer::gps_second_type            gps_stop,
            const channel::channel_names_type& names )
        {
            std::ostringstream err_msg;
            err_msg << "No data was found for requested data segment ["
                    << gps_start << "," << gps_stop << ") on {";
            for ( channel::channel_names_type::const_iterator cur =
                      names.begin( );
                  cur != names.end( );
                  ++cur )
            {
                err_msg << " '" << *cur << "'";
            }
            err_msg << " }";
            return err_msg.str( );
        }

        void
        conn_p_type::fetch_fragment( request_fragment&         fragment,
                                     const buffer_initializer& initializer,
                                     bool buffers_initialized )
        {
            request_fragment::time_span_type   time_spans = fragment.time_spans;
            const channel::channel_names_type& channel_names = fragment.names;

            parameter_accessor             paccess( parameters_ );
            std::unique_ptr< gap_handler > _handler(
                paccess( ).gap_handler( ) );
            composer buffer_manager( fragment.buffers,
                                     channel_names,
                                     initializer,
                                     *_handler,
                                     buffers_initialized );

            int i = 0;
            for ( simple_segment_list_type::const_iterator
                      cur = time_spans.begin( );
                  cur != time_spans.end( );
                  cur++, i++ )
            {
                if ( cur->gps_start >= cur->gps_stop )
                {
                    NDS::detail::dout( ) << "skipping 0 length segment"
                                         << std::endl;
                    continue;
                }
                // make sure this is clear before we start any requests
                request_in_progress( false );
                NDS::detail::dout( ) << "iteration " << i << " "
                                     << cur->gps_start << "-" << cur->gps_stop
                                     << std::endl;
                std::vector< buffers_type > cur_set = fetch_available(
                    cur->gps_start, cur->gps_stop, channel_names );

                if ( cur_set.size( ) == 0 )
                {
                    NDS::detail::dout( ) << "fetch_available returned no data"
                                         << std::endl;
                    throw connection::daq_error(
                        DAQD_NOT_FOUND,
                        err_msg_unexpected_no_data_found(
                            cur->gps_start,
                            cur->gps_stop,
                            channel_names ) ); // CHECK: error on no data at
                    // all????
                }
                NDS::detail::dout( ) << "calling buffer_manager.add_segment"
                                     << std::endl;
                // now merge the output from fetch_available in with the main
                // result
                // set
                for ( std::vector< buffers_type >::const_iterator cur_bufs =
                          cur_set.begin( );
                      cur_bufs != cur_set.end( );
                      cur_bufs++ )
                {
                    NDS::detail::dout( ) << "add_segment( " << cur_bufs->size( )
                                         << " )" << std::endl;
                    buffer_manager.add_segment( *cur_bufs );
                }
            }
            NDS::detail::dout( ) << "calling buffer_manager.finish()"
                                 << std::endl;
            channel_selector selector( shared_from_this( ) );
            buffer_manager.finish( selector );
            request_in_progress( false );
            NDS::detail::dout( ) << "Leaving fetch_safe" << std::endl;
        }

        /**
     * internal implementation of the fetch command.
     * This should only be called by the fetch method.
     */
        std::vector< buffers_type >
        conn_p_type::fetch_available(
            buffer::gps_second_type               gps_start,
            buffer::gps_second_type               gps_stop,
            const connection::channel_names_type& channel_names )
        {
            std::vector< buffers_type > retval;

            NDS::detail::dout( ) << "Starting fetch_available " << gps_start
                                 << ", " << gps_stop << std::endl;
            try
            {
                validate( );

                NDS::detail::dout( ) << "iterate" << std::endl;
                /* Request data for indicated times and channels. */
                issue_iterate( gps_start, gps_stop, 0, channel_names );

                NDS::detail::dout( ) << "next" << std::endl;
                /* Retrieve the first set of buffers. */
                retval.push_back( buffers_type( ) );
                next_raw_buffer( retval.back( ) );

                /* Check that the correct number of buffers arrived. */
                if ( channel_names.size( ) != retval[ 0 ].size( ) )
                {
                    throw connection::unexpected_channels_received_error( );
                }

                /* Retrieve more buffers as they arrive. */
                while ( 1 )
                {
                    size_t             last_index = retval.size( ) - 1;
                    const NDS::buffer& last_buf = retval[ last_index ][ 0 ];
                    bool finished = ( last_buf.Stop( ) >= ( gps_stop ) );
                    NDS::detail::dout( ) << "data received until "
                                         << last_buf.Stop( ) << " waiting for "
                                         << gps_stop;
                    NDS::detail::dout( ) << " finished = " << finished
                                         << std::endl;
                    NDS::detail::dout( ) << last_buf.NameLong( );
                    NDS::detail::dout( )
                        << " samples = " << last_buf.Samples( )
                        << " SR = " << last_buf.SampleRate( ) << " mtrend = "
                        << channel::IsMinuteTrend( last_buf.Name( ) );
                    NDS::detail::dout( )
                        << " samples_to_seconds = "
                        << last_buf.samples_to_seconds( last_buf.Samples( ) )
                        << std::endl;
                    if ( finished )
                    {
                        break;
                    }

                    NDS::detail::dout( ) << "next" << std::endl;

                    retval.push_back( buffers_type( ) );
                    next_raw_buffer( retval.back( ) );
                }

                request_in_progress( false );

                /* Done! */
                return retval;
            }
            catch ( connection::daq_error& err )
            {
                NDS::detail::dout( )
                    << "fetch_available caught daq_error = " << err.DAQCode( )
                    << std::endl;
                /* Ending early is not a problem. Just return what we have,
             * which may be nothing
             */
                if ( err.DAQCode( ) == DAQD_NOT_FOUND ||
                     err.DAQCode( ) == DAQD_OK )
                {
                    return retval;
                }
                retval.resize( 0 );
                throw;
            }
            catch ( ... )
            {
                /* Uh-oh, something wrong happened. */
                retval.resize( 0 );
                throw;
            }
        }

        void
        conn_p_type::find_channels_nds1(
            channels_type&            output,
            std::string               channel_glob,
            channel::channel_type     channel_type_mask,
            channel::data_type        data_type_mask,
            channel::sample_rate_type min_sample_rate,
            channel::sample_rate_type max_sample_rate )
        {
            channels_type retval;

            if ( protocol == connection::protocol_type::PROTOCOL_TWO )
            {
                NDS::detail::dout( ) << "find_channels_nds1: NDS2: "
                                     << "Looking for channels: " << channel_glob
                                     << std::endl;
                return find_channels_nds2( output,
                                           channel_glob,
                                           channel_type_mask,
                                           data_type_mask,
                                           min_sample_rate,
                                           max_sample_rate );
            }

            validate( );

            NDS::detail::dout( ) << "find_channels_nds1: NDS1: "
                                 << "Looking for channels: " << channel_glob
                                 << std::endl;
            retval = channel_cache_.find_channels( channel_glob,
                                                   channel_type_mask,
                                                   data_type_mask,
                                                   min_sample_rate,
                                                   max_sample_rate );
            output.swap( retval );
        }

        channel
        conn_p_type::_parse_nds2_get_channel_line( char* buffer )
        {
            std::string        tmp( buffer );
            std::istringstream buf( tmp );

            std::string chname;
            std::string chtype_str;
            double      rate;
            std::string dtype_str;

            buf >> chname >> chtype_str >> rate >> dtype_str;
            if ( buf.fail( ) || buf.bad( ) )
            {
                throw connection::unexpected_channels_received_error( );
            }

            NDS::detail::dout( ) << "Processed buffer: " << chname << " "
                                 << chtype_str << " " << rate << " "
                                 << dtype_str << std::endl;

            enum chantype chtype = cvt_str_chantype( chtype_str.c_str( ) );
            daq_data_t    dtype = data_type_code( dtype_str.c_str( ) );

            // if (chtype == cUnknown || dtype == _undefined) throw
            // unexpected_channels_received_error();

            return channel( chname.c_str( ),
                            channel::convert_daq_chantype( chtype ),
                            channel::convert_daq_datatype( dtype ),
                            rate,
                            1.0,
                            1.0,
                            0.0,
                            "" );
        }

        bool
        conn_p_type::_read_buffer( socket_t fd, void* dest, size_t size )
        {
            unsigned char* buf = (unsigned char*)dest;
            size_t         remaining = size;
            ssize_t        count( 0 );
            while ( remaining > 0 )
            {
                count = read( fd, buf, remaining );
                if ( count < 0 )
                {
                    return false;
                }
                remaining -= count;
                buf += count;
            }
            return true;
        }

        bool
        conn_p_type::_read_uint4( socket_t fd, uint4_type* dest )
        {
            if ( _read_buffer( fd, (void*)dest, sizeof( *dest ) ) )
            {
                *dest = ntohl( *dest );
                return true;
            }
            return false;
        }

        /**
     * \brief Given a channel_type mask convert it into a vector of nds2 type
     * strings.
     * \param[in] channel_type_mask The type mask
     * \param[out] queryTypes The vector of strings to hold the list.  On
     * success its contents are replaced.
     */
        void
        conn_p_type::channel_mask_to_query_type_strings(
            channel::channel_type       channel_type_mask,
            std::vector< std::string >& queryTypes )
        {
            std::vector< std::string > results;

            if ( channel_type_mask == channel::DEFAULT_CHANNEL_MASK )
            {
                results.push_back( "unknown" );
            }
            else
            {
                channel::channel_type types[] = {
                    channel::CHANNEL_TYPE_ONLINE,
                    channel::CHANNEL_TYPE_RAW,
                    channel::CHANNEL_TYPE_RDS,
                    channel::CHANNEL_TYPE_STREND,
                    channel::CHANNEL_TYPE_MTREND,
                    channel::CHANNEL_TYPE_TEST_POINT,
                    channel::CHANNEL_TYPE_STATIC,
                    channel::CHANNEL_TYPE_UNKNOWN
                };
                for ( int i = 0; types[ i ] != channel::CHANNEL_TYPE_UNKNOWN;
                      ++i )
                {
                    if ( channel_type_mask & types[ i ] )
                    {
                        results.push_back(
                            channel_type_to_string( types[ i ] ) );
                    }
                }
            }
            results.swap( queryTypes );
        }

        connection::count_type
        conn_p_type::count_channels_nds1(
            std::string               channel_glob,
            channel::channel_type     channel_type_mask,
            channel::data_type        data_type_mask,
            channel::sample_rate_type min_sample_rate,
            channel::sample_rate_type max_sample_rate )
        {
            if ( protocol == connection::protocol_type::PROTOCOL_TWO )
            {
                NDS::detail::dout( ) << "count_channels_nds1: NDS2: "
                                     << "Looking for channels: " << channel_glob
                                     << std::endl;
                return count_channels_nds2( channel_glob,
                                            channel_type_mask,
                                            data_type_mask,
                                            min_sample_rate,
                                            max_sample_rate );
            }

            validate( );

            NDS::detail::dout( ) << "count_channels_nds1: NDS1: "
                                 << "Looking for channels: " << channel_glob
                                 << std::endl;
            return channel_cache_.count_channels( channel_glob,
                                                  channel_type_mask,
                                                  data_type_mask,
                                                  min_sample_rate,
                                                  max_sample_rate );
        }

        connection::count_type
        conn_p_type::count_channels_nds2(
            std::string               channel_glob,
            channel::channel_type     channel_type_mask,
            channel::data_type        data_type_mask,
            channel::sample_rate_type min_sample_rate,
            channel::sample_rate_type max_sample_rate )
        {
            if ( request_in_progress( ) )
            {
                throw connection::transfer_busy_error( );
            }
            request_in_progress( true );

            connection::count_type results = 0;

            std::vector< std::string > queryTypes;

            channel_mask_to_query_type_strings( channel_type_mask, queryTypes );

            if ( data_type_mask == channel::DEFAULT_DATA_MASK &&
                 min_sample_rate == channel::MIN_SAMPLE_RATE &&
                 max_sample_rate == channel::MAX_SAMPLE_RATE )
            {
                // simple counts
                for ( std::vector< std::string >::iterator cur =
                          queryTypes.begin( );
                      cur != queryTypes.end( );
                      ++cur )
                {
                    NDS::detail::dout( ) << "Getting channel count for type "
                                         << *cur << std::endl;
                    {
                        std::ostringstream cmdBuf;
                        cmdBuf << "count-channels 0 " << *cur;
                        if ( channel_glob.compare( "*" ) != 0 )
                        {
                            cmdBuf << " {" << channel_glob << "}";
                        }
                        cmdBuf << ";\n";
                        validate_daq(
                            daq_send( &( handle ), cmdBuf.str( ).c_str( ) ) );
                        NDS::detail::dout( ) << "Sent command '"
                                             << cmdBuf.str( ) << "'"
                                             << std::endl;
                    }
                    socket_t   fd = handle.conceal->sockfd;
                    uint4_type count = 0;
                    NDS::detail::dout( ) << "Reading channel count"
                                         << std::endl;
                    if ( !_read_uint4( fd, &count ) )
                    {
                        throw connection::unexpected_channels_received_error( );
                    }
                    NDS::detail::dout( ) << "Received count of " << count
                                         << " channels" << std::endl;

                    results += static_cast< connection::count_type >( count );
                }
            }
            else
            {
                // we must do manual filtering of the channel list
                detail::basic_channel_filter filter(
                    data_type_mask, min_sample_rate, max_sample_rate );
                detail::count_channels counter;

                retreive_channels_from_nds2(
                    queryTypes, channel_glob, filter, counter );
                results = counter.count( );
            }

            request_in_progress( false );
            return results;
        }

        void
        conn_p_type::find_channels( channels_type&                       output,
                                    const NDS::channel_predicate_object& pred )
        {
            int mask = 0;
            for ( auto entry : pred.data_types( ) )
            {
                mask |= (int)entry;
            }
            data_type data_type_mask = (data_type)mask;
            mask = 0;
            for ( auto entry : pred.channel_types( ) )
            {
                mask |= (int)entry;
            }
            channel_type channel_type_mask = (channel_type)mask;

            set_epoch_if_changed( pred.time_span( ) );
            if ( protocol == connection::protocol_type::PROTOCOL_ONE )
            {
                return find_channels_nds1( output,
                                           pred.glob( ),
                                           channel_type_mask,
                                           data_type_mask,
                                           pred.sample_rates( ).minimum,
                                           pred.sample_rates( ).maximum );
            }
            return find_channels_nds2( output,
                                       pred.glob( ),
                                       channel_type_mask,
                                       data_type_mask,
                                       pred.sample_rates( ).minimum,
                                       pred.sample_rates( ).maximum );
        }

        void
        conn_p_type::find_channels_nds2(
            channels_type&            output,
            std::string               channel_glob,
            channel::channel_type     channel_type_mask,
            channel::data_type        data_type_mask,
            channel::sample_rate_type min_sample_rate,
            channel::sample_rate_type max_sample_rate )
        {
            if ( protocol == connection::protocol_type::PROTOCOL_ONE )
            {
                return find_channels_nds1( output,
                                           channel_glob,
                                           channel_type_mask,
                                           data_type_mask,
                                           min_sample_rate,
                                           max_sample_rate );
            }

            if ( request_in_progress( ) )
            {
                throw connection::transfer_busy_error( );
            }
            request_in_progress( true );

            channels_type result;
            try
            {
                std::vector< std::string > queryTypes;

                channel_mask_to_query_type_strings( channel_type_mask,
                                                    queryTypes );

                detail::basic_channel_filter filter(
                    data_type_mask, min_sample_rate, max_sample_rate );
                detail::push_back_channel do_push_back( result );

                retreive_channels_from_nds2(
                    queryTypes, channel_glob, filter, do_push_back );

                request_in_progress( false );
            }
            catch ( ... )
            {
                result.clear( );
                throw;
            }
            NDS::detail::dout( ) << "Channel list retreived, sorting entries"
                                 << std::endl;
            std::sort(
                result.begin( ), result.end( ), default_channel_list_compare );
            output.swap( result );
        }

        void
        conn_p_type::load_epochs_to_cache( )
        {
            if ( epoch_cache_.size( ) > 0 )
            {
                return;
            }

            if ( protocol != connection::protocol_type::PROTOCOL_TWO )
            {
                epoch_cache_.emplace_back( epoch( "ALL", 0, buffer::GPS_INF ) );
                return;
            }
            NDS::detail::dout( ) << "in load_epochs_to_cache" << std::endl;

            epochs_type epochs;
            validate_daq( daq_send( &( handle ), "list-epochs;\n" ) );

            socket_t fd = handle.conceal->sockfd;
            // read the length
            uint4_type len = 0;
            validate_daq( ( _read_uint4( fd, &len ) ? DAQD_OK : DAQD_ERROR ) );
            std::vector< char >        buf( len );
            std::vector< std::string > segments;

            // read in the string, it will not be NULL terminated
            validate_daq( ( _read_buffer( fd, &( buf[ 0 ] ), len )
                                ? DAQD_OK
                                : DAQD_ERROR ) );

            NDS::detail::dout( ) << "list-epochs returned '"
                                 << std::string( buf.begin( ), buf.end( ) )
                                 << "'" << std::endl;

            split( buf.begin( ), buf.end( ), ' ', segments );

            // each segment should be of the format
            // NAME=start-stop
            for ( std::vector< std::string >::iterator cur = segments.begin( );
                  cur != segments.end( );
                  ++cur )
            {
                NDS::detail::dout( ) << "processing segment '" << *cur << "'"
                                     << std::endl;
                std::string::size_type point1 = cur->find( '=' );
                validate_daq( ( ( point1 != std::string::npos && point1 > 0 )
                                    ? DAQD_OK
                                    : DAQD_ERROR ) );
                std::string::size_type point2 = cur->find( '-', point1 );
                validate_daq(
                    ( point2 != std::string::npos ? DAQD_OK : DAQD_ERROR ) );

                std::string             name = cur->substr( 0, point1 );
                buffer::gps_second_type gps_start, gps_stop;
                {
                    ++point1;
                    std::istringstream tmpstr(
                        cur->substr( point1, point2 - point1 ) );
                    tmpstr >> gps_start;
                }
                {
                    std::istringstream tmpstr(
                        cur->substr( point2 + 1, std::string::npos ) );
                    tmpstr >> gps_stop;
                }
                epochs.push_back( epoch( name, gps_start, gps_stop ) );
            }
            epoch_cache_.swap( epochs );
        }

        epochs_type
        conn_p_type::get_epochs( )
        {
            // FIXME: this should do a deep copy
            NDS::detail::dout( ) << "in get_epochs" << std::endl;
            load_epochs_to_cache( );
            NDS::detail::dout( ) << "cache loaded " << epoch_cache_.size( )
                                 << " entries" << std::endl;
            epochs_type results( epoch_cache_.size( ) );
            std::copy(
                epoch_cache_.begin( ), epoch_cache_.end( ), results.begin( ) );
            NDS::detail::dout( ) << "copied to results" << std::endl;
            return results;
        }

        epoch
        conn_p_type::resolve_epoch( const std::string& epoch_name )
        {
            NDS::buffer::gps_second_type gps_start = 0;
            NDS::buffer::gps_second_type gps_stop = NDS::buffer::GPS_INF;

            if ( protocol == connection::protocol_type::PROTOCOL_TWO )
            {
                load_epochs_to_cache( );
                auto match =
                    std::find_if( epoch_cache_.begin( ),
                                  epoch_cache_.end( ),
                                  [&epoch_name]( const epoch& cur ) -> bool {
                                      return cur.name == epoch_name;
                                  } );
                if ( match != epoch_cache_.end( ) )
                {
                    gps_start = match->gps_start;
                    gps_stop = match->gps_stop;
                }
                else
                {
                    NDS::detail::dout( ) << "Could not find epoch '"
                                         << epoch_name << "'" << std::endl;
                    throw connection::daq_error(
                        DAQD_ERROR, "Could not find the request epoch" );
                }
            }
            return epoch( epoch_name, gps_start, gps_stop );
        }

        bool
        conn_p_type::set_epoch_if_changed( const NDS::epoch& time_span )
        {
            if ( protocol != connection::protocol_type::PROTOCOL_TWO )
            {
                return true;
            }
            epoch requested_epoch( ( time_span.name.empty( )
                                         ? time_span
                                         : resolve_epoch( time_span.name ) ) );
            if ( requested_epoch.gps_start != current_epoch_.gps_start ||
                 requested_epoch.gps_stop != current_epoch_.gps_stop )
            {
                return set_epoch( requested_epoch.gps_start,
                                  requested_epoch.gps_stop );
            }
            return true;
        }

        bool
        conn_p_type::set_epoch( const std::string& epoch_name )
        {
            if ( protocol != connection::protocol_type::PROTOCOL_TWO )
            {
                return true;
            }
            epoch tmp = resolve_epoch( epoch_name );

            std::ostringstream cmdBuf;
            cmdBuf << "set-epoch " << epoch_name << ";\n";
            validate_daq( daq_send( &( handle ), cmdBuf.str( ).c_str( ) ) );
            current_epoch_.gps_start = tmp.gps_start;
            current_epoch_.gps_stop = tmp.gps_stop;
            return true;
        }

        bool
        conn_p_type::set_epoch( buffer::gps_second_type gps_start,
                                buffer::gps_second_type gps_stop )
        {
            if ( protocol != connection::protocol_type::PROTOCOL_TWO )
            {
                return true;
            }
            if ( gps_start < 0 || gps_stop < 0 || gps_start > gps_stop ||
                 gps_start > buffer::GPS_INF )
            {
                return false;
            }
            gps_stop =
                ( gps_stop > buffer::GPS_INF ? buffer::GPS_INF : gps_stop );
            std::ostringstream cmdBuf;
            std::ostringstream epochStr;
            epochStr << gps_start << "-" << gps_stop;
            cmdBuf << "set-epoch " << epochStr.str( ) << ";\n";
            validate_daq( daq_send( &( handle ), cmdBuf.str( ).c_str( ) ) );
            current_epoch_.gps_start = gps_start;
            current_epoch_.gps_stop = gps_stop;
            return true;
        }

        epoch
        conn_p_type::current_epoch( ) const
        {
            return epoch( current_epoch_ );
        }

        const channel::hash_type&
        conn_p_type::hash( ) const
        {
            NDS::channel::hash_type& h =
                const_cast< channel::hash_type& >( hash_ );
            h.reset( );

            int received_hash_size = check_int_overflow< int >( h.size( ) );

            //-------------------------------------------------------------------
            // Get the hash from the server
            //-------------------------------------------------------------------
            int rc = daq_recv_channel_hash( const_cast< daq_t* >( &( handle ) ),
                                            &( h[ 0 ] ),
                                            &received_hash_size,
                                            (time_t)0,
                                            cUnknown );
            //-------------------------------------------------------------------
            // Shrink to the number of bytes that has actually been read
            //-------------------------------------------------------------------
            if ( rc == DAQD_OK )
            {
                h.resize( received_hash_size );
            }
            else
            {
                h.resize( 0 );
            }
            return hash_;
        }

        void
        conn_p_type::issue_iterate(
            buffer::gps_second_type               gps_start,
            buffer::gps_second_type               gps_stop,
            buffer::gps_second_type               stride,
            const connection::channel_names_type& channel_names,
            std::vector< NDS::channel >*          final_channel_list )
        {
            bool   have_minute_trends = false;
            double bps = 0;

            NDS::detail::dout( ) << "iterate " << gps_start << " " << gps_stop
                                 << " " << stride << std::endl;

            setup_daq_chanlist( gps_start,
                                channel_names,
                                have_minute_trends,
                                bps,
                                final_channel_list );

            /* NDS2 protocol does not support indefinite GPS stop time */
            if ( ( protocol == connection::protocol_type::PROTOCOL_TWO ) &&
                 ( !gps_stop ) )
            {
                gps_stop =
                    1893024016; /* A safely huge value of January 1, 2040. */
            }

            if ( stride == 0 )
            {
                if ( gps_start ) /* offline request */
                {
                    if ( have_minute_trends ) /* request includes minute trends
                                                 */
                    {
                        /* start with smallest stride that is a multiple of 60
                     * seconds and
                     * whose frame size in bytes is greater than or equal to
                     * _NDS2_TYPICAL_BYTES_PER_FRAME. */
                        stride =
                            (long)( 60 *
                                    std::ceil( _NDS2_TYPICAL_BYTES_PER_FRAME /
                                               ( 60 * bps ) ) );
                    }
                    else
                    {
                        /* request does not include minute trends */
                        /* start with smallest stride that is a multiple of 1
                     * second
                     * and
                     * whose frame size in bytes is greater than or equal to
                     * _NDS2_TYPICAL_BYTES_PER_FRAME. */
                        stride = (long)( std::ceil(
                            _NDS2_TYPICAL_BYTES_PER_FRAME / bps ) );
                        NDS::detail::dout( ) << "stride1 = " << stride
                                             << " bps = " << bps << std::endl;
                    }

                    if ( stride > ( gps_stop - gps_start ) ) /* stride is longer
                                                                than request
                                                                duration */
                    {
                        stride = gps_stop -
                            gps_start; /* set stride to request duration */
                    }
                }
                else
                {
                    /* online request */
                    if ( have_minute_trends )
                    {
                        /* request includes minute trends */
                        stride =
                            60; /* use shortest possible stride, 60 seconds */
                    }
                    else
                    {
                        stride = 1; /* use shortest possible stride, 1 second */
                    }
                }
            }

            if ( ( have_minute_trends ) &&
                 ( ( gps_start % 60 ) || ( gps_stop % 60 ) ||
                   ( stride % 60 ) ) )
            {
                /* Raise an error if any of the requested channels are
             * minute-trends and the start or stop times are indivisible
             * by 60.
             */
                throw connection::minute_trend_error( );
            }
            if ( protocol == connection::protocol_type::PROTOCOL_TWO &&
                 gps_start != 0 )
            {
                NDS::detail::dout( ) << "calling check data" << std::endl;
                process_check_data_result(
                    daq_request_check(
                        &handle, (time_t)gps_start, (time_t)gps_stop ),
                    true );
            }
            NDS::detail::dout( ) << "about to call daq request data "
                                 << gps_start << " " << gps_stop << " "
                                 << stride << std::endl;
            validate_daq( daq_request_data(
                &handle, (time_t)gps_start, (time_t)gps_stop, stride ) );
            request_start_time( gps_start );
            request_end_time( gps_stop );
            request_in_progress( true );
            NDS::detail::dout( ) << "finished with iterate" << std::endl;
        }

        void
        conn_p_type::finalize_iterate( detail::iterate_handler* handler,
                                       iterate_finalize_reason  reason )
        {
            if ( !handler )
            {
                return;
            }
            std::shared_ptr< NDS::detail::iterate_handler > my_handler{
                iterate_handler_.lock( )
            };
            if ( my_handler.get( ) != handler )
            {
                return;
            }
            iterate_handler_.reset( );
            request_in_progress( false );
            if ( reason == iterate_finalize_reason::ABORTED )
            {
                // aborting the iteration must do a 'hard' close
                // not a nice controlled close as it is being done
                // with a potentially endless stream of bytes coming
                // down the wire, there is no gap to put a close/quit command
                // into.
                daq_destroy( &handle );
                connected = false;
            }
        }

        std::shared_ptr< detail::iterate_handler >
        conn_p_type::iterate_simple_gaps(
            buffer::gps_second_type               gps_start,
            buffer::gps_second_type               gps_stop,
            buffer::gps_second_type               stride,
            const connection::channel_names_type& channel_names )
        {
            NDS::detail::dout( ) << "iterate_simple_gaps(" << gps_start << ", "
                                 << gps_stop << ", " << stride << ", ... "
                                 << channel_names.size( ) << std::endl;
            if ( request_in_progress( ) )
            {
                throw connection::transfer_busy_error( );
            }
            parameter_accessor             paccess( parameters_ );
            std::unique_ptr< gap_handler > ghandler(
                paccess( ).gap_handler( ) );
            if ( !ghandler )
            {
                throw std::runtime_error( "System error, a NULL gaphandler was "
                                          "found, cannot continue" );
            }
            auto handler = std::make_shared< iterate_handler_with_simple_gaps >(
                gps_start,
                gps_stop,
                stride,
                channel_names,
                shared_from_this( ),
                std::move( ghandler ) );
            iterate_handler_ = handler;
            return handler;
        }

        std::shared_ptr< detail::iterate_handler >
        conn_p_type::iterate_fast(
            buffer::gps_second_type               gps_start,
            buffer::gps_second_type               gps_stop,
            buffer::gps_second_type               stride,
            const connection::channel_names_type& channel_names )
        {
            NDS::detail::dout( ) << "iterate_fast(" << gps_start << ", "
                                 << gps_stop << ", " << stride << ", ... "
                                 << channel_names.size( ) << std::endl;
            if ( request_in_progress( ) )
            {
                throw connection::transfer_busy_error( );
            }

            std::shared_ptr< NDS::detail::iterate_handler > handler{
                std::make_shared< iterate_fast_handler >( gps_start,
                                                          gps_stop,
                                                          stride,
                                                          channel_names,
                                                          shared_from_this( ) )
            };
            iterate_handler_ = handler;
            return handler;
        }

        std::shared_ptr< detail::iterate_handler >
        conn_p_type::iterate_available(
            buffer::gps_second_type               gps_start,
            buffer::gps_second_type               gps_stop,
            buffer::gps_second_type               stride,
            const connection::channel_names_type& channel_names )
        {
            NDS::detail::dout( ) << "iterate_available(" << gps_start << ", "
                                 << gps_stop << ", " << stride << ", ... "
                                 << channel_names.size( ) << std::endl;
            if ( request_in_progress( ) )
            {
                throw connection::transfer_busy_error( );
            }

            iterate_handler_.reset( );

            auto handler = std::make_shared< iterate_available_handler >(
                gps_start,
                gps_stop,
                stride,
                channel_names,
                shared_from_this( ) );
            iterate_handler_ = handler;
            return handler;
        }

        std::shared_ptr< detail::iterate_handler >
        conn_p_type::iterate_full(
            buffer::gps_second_type               gps_start,
            buffer::gps_second_type               gps_stop,
            buffer::gps_second_type               stride,
            const connection::channel_names_type& channel_names )
        {
            NDS::detail::dout( ) << "iterate_full(" << gps_start << ", "
                                 << gps_stop << ", " << stride << ", ... "
                                 << channel_names.size( ) << std::endl;

            if ( request_in_progress( ) )
            {
                throw connection::transfer_busy_error( );
            }

            iterate_handler_.reset( );
            std::shared_ptr< detail::iterate_handler > handler;

            if ( gps_start == 0 )
            {
                NDS::detail::dout( ) << "Fast path" << std::endl;
                // online data does not get special treatement
                handler = std::make_shared< iterate_full_handler >(
                    0,
                    0,
                    0,
                    channel_names,
                    current_epoch_,
                    channels_type( ),
                    shared_from_this( ) );
                iterate_handler_ = handler;
                issue_iterate( gps_start, gps_stop, stride, channel_names );
                return handler;
            }
            if ( gps_start >= gps_stop || stride < 0 ||
                 gps_start > current_epoch_.gps_stop ||
                 gps_stop < current_epoch_.gps_start )
            {
                throw connection::daq_error( DAQD_NOT_FOUND,
                                             "The request times are invalid "
                                             "(start > stop) or are outside of "
                                             "the configured epoch" );
            }
            gps_start = std::max< NDS::buffer::gps_second_type >(
                gps_start, current_epoch_.gps_start );
            gps_stop = std::min< NDS::buffer::gps_second_type >(
                gps_stop, current_epoch_.gps_stop );

            NDS::detail::dout( ) << "Current EPOCH is "
                                 << current_epoch_.gps_start << "-"
                                 << current_epoch_.gps_stop << std::endl;
            epoch cur_epoch( current_epoch_ );
            set_epoch( gps_start, gps_stop );

            channels_type                  selected_channels;
            connection::channel_names_type selected_names;

            detail::channel_selector select_channel( shared_from_this( ) );
            for ( connection::channel_names_type::const_iterator cur =
                      channel_names.begin( );
                  cur != channel_names.end( );
                  ++cur )
            {
                channel curCh = select_channel(
                    *cur,
                    channel_selector::selection_method::UNIQUE_THEN_FIRST );
                selected_channels.push_back( curCh );

                if ( protocol == connection::protocol_type::PROTOCOL_TWO )
                {
                    std::ostringstream nameBuf;
                    nameBuf << curCh.Name( ) << ",";
                    nameBuf << channel_type_to_string( curCh.Type( ) );
                    if ( ( curCh.Type( ) & ( channel::CHANNEL_TYPE_MTREND |
                                             channel::CHANNEL_TYPE_STREND ) ) ==
                         0 )
                    {
                        // sample rate is intrinsic on STREND & MTREND specify
                        // it on
                        // everything else
                        nameBuf << "," << curCh.SampleRate( );
                    }
                    selected_names.push_back( nameBuf.str( ) );
                }
                else
                {
                    // for NDS1 we can just use the name that we are given, if
                    // we
                    // can
                    // get a channel back, then we know it is not ambiguous
                    selected_names.push_back( *cur );
                }
            }

            if ( stride == 0 )
            {
                stride =
                    calculate_stride( gps_start, gps_stop, selected_channels );
            }
            handler =
                std::make_shared< iterate_full_handler >( gps_start,
                                                          gps_stop,
                                                          stride,
                                                          selected_names,
                                                          cur_epoch,
                                                          selected_channels,
                                                          shared_from_this( ) );
            iterate_handler_ = handler;
            request_in_progress( true );
            return handler;
        }

        std::shared_ptr< detail::iterate_handler >
        conn_p_type::dispatch_iterate(
            NDS::buffer::gps_second_type               gps_start,
            NDS::buffer::gps_second_type               gps_stop,
            NDS::buffer::gps_second_type               stride,
            const NDS::connection::channel_names_type& channel_names )
        {
            std::shared_ptr< detail::iterate_handler > handler;
            parameter_accessor                         paccess( parameters_ );

            // all online requests take the fast path
            if ( gps_start == 0 )
            {
                try
                {
                    handler = iterate_fast(
                        gps_start, gps_stop, stride, channel_names );
                }
                catch ( connection::daq_error& err )
                {
                    // Older nds2 servers will reject FAST_STRIDE, we need
                    // AUTO_STRIDE for them.
                    // possibly change this to look at protocol revisions and
                    // skip this retry.
                    if ( err.DAQCode( ) == DAQD_COMMAND_SYNTAX &&
                         stride == NDS::request_period::FAST_STRIDE &&
                         protocol == NDS::connection::PROTOCOL_TWO )
                    {
                        stride = NDS::request_period::AUTO_STRIDE;
                        handler = iterate_fast(
                            gps_start, gps_stop, stride, channel_names );
                    }
                    else
                    {
                        throw err;
                    }
                }
            }
            else
            {
                if ( stride == NDS::request_period::FAST_STRIDE )
                {
                    stride = NDS::request_period::AUTO_STRIDE;
                }
                // below this line everything is an offline request
                if ( protocol == connection::protocol_type::PROTOCOL_TWO )
                {
                    // all nds2 offline queries w/o gaps take the fast path
                    if ( !has_gaps( gps_start, gps_stop, channel_names ) )
                    {
                        handler = iterate_fast(
                            gps_start, gps_stop, stride, channel_names );
                    }
                    else
                    {
                        // These are the 'hard' cases, offline nds2 with gaps.
                        if ( paccess( ).iterate_uses_gap_handler( ) )
                        {
                            handler = iterate_full(
                                gps_start, gps_stop, stride, channel_names );
                        }
                        else
                        {
                            handler = iterate_available(
                                gps_start, gps_stop, stride, channel_names );
                        }
                    }
                }
                else if ( protocol == connection::protocol_type::PROTOCOL_ONE )
                {
                    if ( paccess( ).iterate_uses_gap_handler( ) )
                    {
                        // fastish path for nds1 w/ gap handling turned on
                        handler = iterate_simple_gaps(
                            gps_start, gps_stop, stride, channel_names );
                    }
                    else
                    {
                        // return available data is a fast path
                        handler = iterate_fast(
                            gps_start, gps_stop, stride, channel_names );
                    }
                }
            }
            handler->next( );
            iterate_handler_ = handler;
            return handler;
        }

        bool
        conn_p_type::has_gaps(
            NDS::buffer::gps_second_type               gps_start,
            NDS::buffer::gps_second_type               gps_stop,
            const NDS::connection::channel_names_type& channel_names )
        {
            try
            {
                check( gps_start, gps_stop, channel_names );
                return false;
            }
            catch ( ... )
            {
            }
            return true;
        }

        NDS::buffer::gps_second_type
        conn_p_type::calculate_stride(
            NDS::buffer::gps_second_type gps_start,
            NDS::buffer::gps_second_type gps_stop,
            NDS::channels_type&          selected_channels ) const
        {
            NDS::buffer::gps_second_type stride = 1;
            double                       bytes_per_sample = 0.0;
            bool                         have_minute_trends = false;
            // calculate a stride.

            // First figure out the byte rate - taken from the C bindings
            // For the purpose of computing the byte rate, use the
            // default channel bytes per sample (bytes_per_sample) and sample
            // rate.
            for ( channels_type::iterator cur = selected_channels.begin( );
                  cur != selected_channels.end( );
                  ++cur )
            {
                bytes_per_sample += cur->DataTypeSize( ) * cur->SampleRate( );
                if ( cur->Type( ) == NDS::channel::CHANNEL_TYPE_MTREND )
                {
                    have_minute_trends = true;
                }
            }
            if ( have_minute_trends )
            {
                // start with smallest stride that is a multiple of 60 seconds
                // and
                // whose frame size in bytes is greater than or equal to
                //_NDS2_TYPICAL_BYTES_PER_FRAME.
                stride = ( NDS::buffer::gps_second_type )(
                    60 * ceil( _NDS2_TYPICAL_BYTES_PER_FRAME /
                               ( 60 * bytes_per_sample ) ) );
            }
            else
            {
                // start with smallest stride that is a multiple of 1 second and
                // whose frame size in bytes is greater than or equal to
                // _NDS2_TYPICAL_BYTES_PER_FRAME.
                stride = ( NDS::buffer::gps_second_type )(
                    ceil( _NDS2_TYPICAL_BYTES_PER_FRAME / bytes_per_sample ) );
            }
            if ( stride > ( gps_stop - gps_start ) )
            {
                stride = gps_stop - gps_start;
            }
            return stride;
        }

        //        bool
        //        conn_p_type::has_next() {
        //            if (!iterate_handler_.get()) {
        //                return false;
        //            }
        //            return iterate_handler_->has_next();
        //        }
        //
        //        buffers_type
        //        conn_p_type::next() {
        //            if (!iterate_handler_.get()) {
        //                throw std::out_of_range("No Next");
        //            }
        //            buffers_type output;
        //            iterate_handler_->next(output);
        //            if (!(iterate_handler_->has_next())) {
        //                iterate_handler_.reset();
        //                request_in_progress(false);
        //            }
        //            return output;
        //        }

        void
        conn_p_type::next_raw_buffer( buffers_type& output )
        {
            buffers_type retval;

            try
            {
                size_t      count_received_channels;
                chan_req_t* chan_reqs;

                if ( request_in_progress( ) == false )
                {
                    errno = NDS::EOS;
                    output.swap( retval );
                    return;
                }

                validate_daq( daq_recv_next( &handle ) );
                count_received_channels = handle.num_chan_request;

                retval.resize( count_received_channels );
                chan_reqs = handle.chan_req_list;

                for ( size_t i = 0; i < count_received_channels; ++i )
                {
                    int nbytes = chan_reqs[ i ].status;

                    if ( nbytes < 0 )
                    {
                        throw connection::daq_error( -nbytes,
                                                     get_last_message( ) );
                    }

                    NDS::detail::dout( ) << "block found "
                                         << daq_get_block_gps( &handle ) << ":"
                                         << daq_get_block_gpsn( &handle );

                    NDS::buffer b( create_channel( chan_reqs[ i ] ),
                                   daq_get_block_gps( &handle ),
                                   daq_get_block_gpsn( &handle ),
                                   daq_get_block_data( &handle ) +
                                       chan_reqs[ i ].offset,
                                   nbytes );

                    NDS::detail::dout( ) << " - "
                                         << daq_get_block_gps( &handle ) +
                            b.samples_to_seconds(
                                b.bytes_to_samples( nbytes ) );
                    NDS::detail::dout( )
                        << " (" << ( size_t )( nbytes / b.DataTypeSize( ) )
                        << " samples)" << std::endl;

                    const_cast< NDS::buffer& >( retval[ i ] ).swap( b );
                }

                if ( request_start_time( ) == 0 && request_end_time( ) != 0 &&
                     retval.size( ) > 0 )
                {
                    request_start_time( retval.front( ).Start( ) );
                    request_end_time( safe_add< time_type >(
                        request_start_time( ), request_end_time( ) ) );
                }

                if ( request_end_time( ) != 0 )
                {
                    bool has_next = false;
                    for ( buffers_type::const_iterator cur = retval.begin( ),
                                                       last = retval.end( );
                          cur != last;
                          ++cur )
                    {
                        if ( ( cur->Start( ) < request_end_time( ) ) &&
                             ( cur->Stop( ) < request_end_time( ) ) )
                        {
                            has_next = true;
                            break;
                        }
                    }
                    if ( has_next == false )
                    {
                        termination_block( );
                        request_in_progress( false );
                    }
                }
            }
            catch ( ... )
            {
                retval.resize( 0 );
                request_in_progress( false );
                throw;
            }
            output.swap( retval );
        }

        void
        conn_p_type::shutdown( )
        {
            if ( connected )
            {
                try
                {
                    daq_disconnect( &handle );
                }
                catch ( ... )
                {
                }
                connected = false;
            }
        }

        NDS::buffer::gps_second_type
        conn_p_type::cur_nds1_gpstime( )
        {
            NDS::buffer::gps_second_type gps;
            if ( !connected || protocol != NDS::connection::PROTOCOL_ONE )
            {
                throw connection::daq_error( DAQD_VERSION_MISMATCH );
            }
            validate_daq( daq_send( &handle, "gps;\n" ) );
            uint4_type dummy;
            /* We cannot use the regular reading code as
         * the block returned states 1s of data, but
         * sends no data.
         */
            daq_recv_id( &handle ); // dummy writer id
            _read_uint4( handle.conceal->sockfd,
                         &dummy ); // online/offline - will be online
            _read_uint4( handle.conceal->sockfd,
                         &dummy ); // block len - will be 4 * sizeof(uint4_type)
            _read_uint4( handle.conceal->sockfd, &dummy ); // secs
            _read_uint4( handle.conceal->sockfd, &dummy ); // gps
            gps = static_cast< NDS::buffer::gps_second_type >( dummy );
            _read_uint4( handle.conceal->sockfd, &dummy ); // nanosecs
            _read_uint4( handle.conceal->sockfd, &dummy ); // seq
            return gps;
        }

        inline void
        conn_p_type::validate( ) const
        {
            if ( !connected )
            {
                throw connection::already_closed_error( );
            }
        }

        inline void
        conn_p_type::validate_daq( int RetCode ) const
        {
            if ( RetCode != DAQD_OK )
            {
                NDS::detail::dout( ) << "validate_daq failed - " << RetCode
                                     << std::endl;
                throw connection::daq_error( RetCode, get_last_message( ) );
            }
        }

        void
        conn_p_type::cycle_nds1_connection( )
        {
            if ( protocol != connection::PROTOCOL_ONE )
            {
                return;
            }
            int resp = nds1_reconnect( &handle );
            if ( resp != DAQD_OK )
            {
                throw connection::daq_error( resp );
            }
        }
    }
}
