#include <stdint.h>
#include <algorithm>
#include <sstream>

#include "nds.hh"
#include "nds_channel.hh"
#include "nds_buffer.hh"
#include "nds_connection.hh"
#include "nds_connection_ptype.hh"
#include "nds_gap_handler.hh"
#include "nds_memory.hh"

namespace NDS
{
    namespace
    {
        void
        _load_sample( buffer&           cur_buffer,
                      buffer::size_type sample_offset,
                      detail::fixed_point_gap_handler::static_val& val )
        {
            buffer::size_type offset_bytes =
                cur_buffer.samples_to_bytes( sample_offset );
            unsigned char* base = const_cast< unsigned char* >(
                                      reinterpret_cast< const unsigned char* >(
                                          cur_buffer.cbegin< void >( ) ) ) +
                offset_bytes;
            switch ( cur_buffer.DataType( ) )
            {
            case channel::DATA_TYPE_INT16:
                val.int16val = *reinterpret_cast< ::uint16_t* >( base );
                break;
            case channel::DATA_TYPE_INT32:
                val.int32val = *reinterpret_cast< ::uint32_t* >( base );
                break;
            case channel::DATA_TYPE_INT64:
                val.int64val = *reinterpret_cast< ::uint64_t* >( base );
                break;
            case channel::DATA_TYPE_FLOAT32:
                val.float32val = *reinterpret_cast< float* >( base );
                break;
            case channel::DATA_TYPE_FLOAT64:
                val.float64val = *reinterpret_cast< double* >( base );
                break;
            case channel::DATA_TYPE_COMPLEX32:
                val.complexrval = *reinterpret_cast< float* >( base );
                val.complexival =
                    *reinterpret_cast< float* >( base + sizeof( float ) );
                break;
            case channel::DATA_TYPE_UINT32:
                val.uint32val = *reinterpret_cast< ::uint32_t* >( base );
                break;
            case channel::DATA_TYPE_UNKNOWN:
            default:
                break;
            }
        }

        void
        _do_fill( buffer&           cur_buffer,
                  buffer::size_type start_sample_offset,
                  buffer::size_type end_sample_offset,
                  const detail::fixed_point_gap_handler::static_val& val )
        {
            buffer::size_type offset_start =
                cur_buffer.samples_to_bytes( start_sample_offset );
            buffer::size_type offset_end =
                cur_buffer.samples_to_bytes( end_sample_offset );

            unsigned char* base = const_cast< unsigned char* >(
                reinterpret_cast< const unsigned char* >(
                    cur_buffer.cbegin< void >( ) ) );
            switch ( cur_buffer.DataType( ) )
            {
            case channel::DATA_TYPE_INT16:
                std::fill< ::int16_t*, ::int16_t >(
                    reinterpret_cast< ::int16_t* >( base + offset_start ),
                    reinterpret_cast< ::int16_t* >( base + offset_end ),
                    val.int16val );
                break;
            case channel::DATA_TYPE_INT32:
                std::fill< ::int32_t*, ::int32_t >(
                    reinterpret_cast< ::int32_t* >( base + offset_start ),
                    reinterpret_cast< ::int32_t* >( base + offset_end ),
                    val.int32val );
                break;
            case channel::DATA_TYPE_FLOAT32:
                std::fill< float*, float >(
                    reinterpret_cast< float* >( base + offset_start ),
                    reinterpret_cast< float* >( base + offset_end ),
                    val.float32val );
                break;
            case channel::DATA_TYPE_INT64:
                std::fill< ::int64_t*, ::int64_t >(
                    reinterpret_cast< ::int64_t* >( base + offset_start ),
                    reinterpret_cast< ::int64_t* >( base + offset_end ),
                    val.int64val );
                break;
            case channel::DATA_TYPE_FLOAT64:
                std::fill< double*, double >(
                    reinterpret_cast< double* >( base + offset_start ),
                    reinterpret_cast< double* >( base + offset_end ),
                    val.float64val );
                break;
            case channel::DATA_TYPE_COMPLEX32:
            {
                buffer::size_type start_offset = start_sample_offset;
                for ( float* cur =
                          reinterpret_cast< float* >( base + offset_start );
                      start_offset < end_sample_offset;
                      ++start_offset )
                {
                    *cur = val.complexrval;
                    ++cur;
                    *cur = val.complexival;
                    ++cur;
                }
            }
            break;
            case channel::DATA_TYPE_UINT32:
                std::fill< ::uint32_t*, ::uint32_t >(
                    reinterpret_cast< ::uint32_t* >( base + offset_start ),
                    reinterpret_cast< ::uint32_t* >( base + offset_end ),
                    val.uint32val );
                break;
            default:
                std::fill< unsigned char*, unsigned char >(
                    base + offset_start, base + offset_end, 0 );
                break;
            }
        }
    }

    namespace detail
    {

        class continue_delay_handler : public delayed_gap_handler
        {
        public:
            continue_delay_handler(
                buffer&                                    cur_buffer,
                buffer::size_type                          start_sample_offset,
                buffer::size_type                          end_sample_offset,
                const fixed_point_gap_handler::static_val& default_val )
                : cur_buffer( cur_buffer ),
                  start_sample_offset( start_sample_offset ),
                  end_sample_offset( end_sample_offset ),
                  default_val( default_val ){};

            ~continue_delay_handler( ) override = default;
            ;

            void operator( )( ) override;

        private:
            buffer&                             cur_buffer;
            buffer::size_type                   start_sample_offset;
            buffer::size_type                   end_sample_offset;
            fixed_point_gap_handler::static_val default_val;
        };

        std::unique_ptr< delayed_gap_handler >
        fixed_point_gap_handler::fill_gap(
            buffer&           cur_buffer,
            buffer::size_type start_sample_offset,
            buffer::size_type end_sample_offset ) const
        {
            _do_fill( cur_buffer, start_sample_offset, end_sample_offset, val );
            return nullptr;
        }

        std::unique_ptr< delayed_gap_handler >
        abort_gap_handler::fill_gap( buffer&           cur_buffer,
                                     buffer::size_type start_sample,
                                     buffer::size_type end_sample ) const
        {
            buffer::gps_second_type gap_start =
                cur_buffer.samples_to_seconds( start_sample ) +
                cur_buffer.Start( );
            buffer::gps_second_type gap_end =
                cur_buffer.samples_to_seconds( end_sample ) +
                cur_buffer.Start( );

            std::ostringstream os;
            os << "There is a gap in " << cur_buffer.Name( ) << " from gps "
               << gap_start << " to gps " << gap_end << ".\n";
            os << "Please note that there may be more gaps, this was the first "
                  "one identified."
               << std::endl;
            throw connection::daq_error( DAQD_NOT_FOUND, os.str( ) );
        }

        std::unique_ptr< delayed_gap_handler >
        continuation_gap_handler::fill_gap(
            buffer&           cur_buffer,
            buffer::size_type start_sample_offset,
            buffer::size_type end_sample_offset ) const
        {
            return NDS::detail::make_unique< continue_delay_handler >(
                cur_buffer, start_sample_offset, end_sample_offset, val );
        }

        void
        continue_delay_handler::operator( )( )
        {
            fixed_point_gap_handler::static_val ref_val( default_val );
            if ( start_sample_offset != 0 )
            {
                _load_sample( cur_buffer, start_sample_offset - 1, ref_val );
            }
            _do_fill(
                cur_buffer, start_sample_offset, end_sample_offset, ref_val );
        }
    }
}
