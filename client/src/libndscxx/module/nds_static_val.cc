#include "nds.hh"
#include "nds_channel.hh"
#include "nds_buffer.hh"
#include "nds_gap_handler.hh"

#include <limits>

namespace NDS
{

    namespace
    {
        template < typename T >
        T
        bound( double val )
        {
            if ( val > std::numeric_limits< T >::max( ) )
            {
                return std::numeric_limits< T >::max( );
            }
            else if ( val < std::numeric_limits< T >::min( ) )
            {
                return std::numeric_limits< T >::min( );
            }
            else
            {
                return static_cast< T >( val );
            }
        }
    }

    namespace detail
    {
        fixed_point_gap_handler::static_val::static_val(::int16_t  int16val,
                                                        ::int32_t  int32val,
                                                        ::int64_t  int64val,
                                                        float      float32val,
                                                        double     float64val,
                                                        float      complexrval,
                                                        float      complexival,
                                                        ::uint32_t uint32val )
            : int16val( int16val ), int32val( int32val ), int64val( int64val ),
              float32val( float32val ), float64val( float64val ),
              complexrval( complexrval ), complexival( complexival ),
              uint32val( uint32val )
        {
        }

        fixed_point_gap_handler::static_val::static_val( fixed_values spec )
        {
            switch ( spec )
            {
            case ZERO_VAL:
                int16val = 0;
                int32val = 0;
                int64val = 0;
                float32val = complexrval = complexival = 0.0;
                float64val = 0.0;
                uint32val = 0;
                break;
            case ONE_VAL:
                int16val = 1;
                int32val = 1;
                int64val = 1;
                float32val = complexrval = complexival = 1.0;
                float64val = 1.0;
                uint32val = 1;
                break;
            case NAN_VAL:
                int16val = 0;
                int32val = 0;
                int64val = 0;
                float32val = complexrval = complexival =
                    std::numeric_limits< float >::quiet_NaN( );
                float64val = std::numeric_limits< double >::quiet_NaN( );
                uint32val = 0;
                break;
            case POS_INF_VAL:
                int16val = std::numeric_limits< ::int16_t >::max( );
                int32val = std::numeric_limits< ::int32_t >::max( );
                int64val = std::numeric_limits< ::int64_t >::max( );
                float32val = complexrval = complexival =
                    std::numeric_limits< float >::infinity( );
                float64val = std::numeric_limits< double >::infinity( );
                uint32val = std::numeric_limits< ::uint32_t >::max( );
                break;
            case NEG_INF_VAL:
                int16val = std::numeric_limits< ::int16_t >::min( );
                int32val = std::numeric_limits< ::int32_t >::min( );
                int64val = std::numeric_limits< ::int64_t >::min( );
                float32val = complexrval = complexival =
                    -std::numeric_limits< float >::infinity( );
                float64val = -std::numeric_limits< double >::infinity( );
                uint32val = std::numeric_limits< ::uint32_t >::min( );
                break;
            }
        }

        fixed_point_gap_handler::static_val::static_val( double value )
            : int16val(
                  static_cast< ::int16_t >( bound< ::int16_t >( value ) ) ),
              int32val(
                  static_cast< ::int32_t >( bound< ::int32_t >( value ) ) ),
              int64val(
                  static_cast< ::int64_t >( bound< ::int64_t >( value ) ) ),
              float32val( static_cast< float >( value ) ),
              float64val( static_cast< double >( value ) ),
              complexrval( static_cast< float >( value ) ),
              complexival( static_cast< float >( value ) ),
              uint32val(
                  static_cast< ::uint32_t >( bound< ::uint32_t >( value ) ) )
        {
        }
    }
}