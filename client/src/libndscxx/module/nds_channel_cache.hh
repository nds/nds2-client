#ifndef NDS_INTERNAL_CHANNEL_CACHE_HH
#define NDS_INTERNAL_CHANNEL_CACHE_HH

#include <cstdint>
#include <vector>
#include <array>

#include "nds_connection.hh"

namespace NDS
{

    namespace detail
    {
        class daq_accessor;

        struct nds1_cached_channel
        {
            double                rate;
            uint32_t              tpnum;
            float                 gain;
            float                 slope;
            float                 offset;
            channel::channel_type channel_type;
            channel::data_type    data_type;
            std::array< char, 64 + 60 + 1 > name;
            std::array< char, 40 + 1 >      units;
        };

        class channel_cache_nds1
        {
        public:
            typedef detail::nds1_cached_channel     cache_entry_type;
            typedef std::vector< cache_entry_type > cache_type;

            channel_cache_nds1( detail::daq_accessor& server );

            size_t count_channels( std::string               channel_glob,
                                   channel::channel_type     channel_type_mask,
                                   channel::data_type        data_type_mask,
                                   channel::sample_rate_type min_sample_rate,
                                   channel::sample_rate_type max_sample_rate );

            channels_type
            find_channels( std::string               channel_glob,
                           channel::channel_type     channel_type_mask,
                           channel::data_type        data_type_mask,
                           channel::sample_rate_type min_sample_rate,
                           channel::sample_rate_type max_sample_rate );

        private:
            void update_cache( );

            detail::daq_accessor& server_;
            cache_type            cache_;
        };
    }
}

#endif // NDS_INTERNAL_CHANNEL_CACHE_HH