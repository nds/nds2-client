#include "daqc.h"
#include "nds_helper.hh"

//#include "debug_stream.hh"

#include <algorithm>
#include <sstream>

namespace NDS
{
    namespace detail
    {

        inline NDS::simple_segment_list_type
        simplify_availability_intl_cb( const NDS::availability& avail )
        {
            NDS::simple_segment_list_type result;
            NDS::detail::simplify_availability_intl( avail, result );
            return result;
        }

        channel
        create_channel( const chan_req_t& Source )
        {
            channel retval( Source.name,
                            channel::convert_daq_chantype( Source.type ),
                            channel::convert_daq_datatype( Source.data_type ),
                            Source.rate,
                            Source.s.signal_gain,
                            Source.s.signal_slope,
                            Source.s.signal_offset,
                            Source.s.signal_units );

            return retval;
        }

        bool
        availability_comp_intl( segment_list_type::value_type a,
                                segment_list_type::value_type b )
        {
            if ( a.gps_start < b.gps_start )
            {
                return true;
            }
            if ( a.gps_start == b.gps_start )
            {
                return ( a.gps_stop > b.gps_stop );
            }
            return false;
        }

        void
        simplify_availability_intl( const availability&       avail,
                                    simple_segment_list_type& output )
        {
            simple_segment_list_type tmp;
            availability             sortlist( avail );
            std::sort( sortlist.data.begin( ),
                       sortlist.data.end( ),
                       availability_comp_intl );

            buffer::gps_second_type gps_cur = 0;
            for ( segment_list_type::iterator cur = sortlist.data.begin( );
                  cur != sortlist.data.end( );
                  ++cur )
            {
                buffer::gps_second_type start = cur->gps_start,
                                        stop = cur->gps_stop;
                // NDS::dout << "ex: cur = " << gps_cur << " " <<
                // (*cur)->frame_type
                // << " " << start << " " << stop << std::endl;
                if ( gps_cur >= stop )
                {
                    continue;
                }

                if ( gps_cur > start )
                {
                    start = gps_cur;
                }

                if ( gps_cur == start && tmp.size( ) > 0 )
                {
                    tmp.back( ).gps_stop = stop;
                }
                else
                {
                    tmp.push_back( simple_segment_list_type::value_type(
                        simple_segment( start, stop ) ) );
                }
                gps_cur = stop;
            }
            output.swap( tmp );
        }

        void
        simplify_availability_list_intl( const availability_list_type&  avails,
                                         simple_availability_list_type& output )
        {
            simple_availability_list_type results;
            results.resize( avails.size( ) );
            std::transform( avails.begin( ),
                            avails.end( ),
                            results.begin( ),
                            simplify_availability_intl_cb );
            output.swap( results );
        }

        bool
        is_numeric_substring( const std::string&     ch,
                              std::string::size_type start,
                              std::string::size_type end )
        {
            for ( ; start < end; ++start )
            {
                char cur = ch[ start ];
                if ( cur < '0' || cur > '9' )
                {
                    return false;
                }
            }
            return true;
        }

        std::string
        strip_rate_from_channel_name( const std::string& ch )
        {
            std::ostringstream     dest;
            std::string::size_type prev = 0;
            std::string::size_type cur = 0;
            bool                   first = true;

            while ( ( cur = ch.find_first_of( "%,", cur ) ) !=
                    std::string::npos )
            {
                if ( !is_numeric_substring( ch, prev, cur ) )
                {
                    dest << ( first ? "" : "," )
                         << ch.substr( prev, cur - prev );
                    first = false;
                }
                ++cur;
                prev = cur;
            }
            if ( !is_numeric_substring( ch, prev, ch.size( ) ) )
            {
                dest << ( first ? "" : "," ) << ch.substr( prev );
            }
            return dest.str( );
        }
    }
}
