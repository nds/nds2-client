//
// Created by jonathan.hanks on 2/6/20.
//
#include "daq_bsd_string.h"
#include <string.h>

#include "catch.hpp"

TEST_CASE(
    "daq_strlcpy copies strings safely when buffers are sufficiently large" )
{
    const char* src = "Hello World!";
    char        dst1[ 100 ] = "";
    char        dst2[ 13 ] = "";

    REQUIRE( _daq_strlcpy( dst1, src, sizeof( dst1 ) ) == strlen( src ) );
    REQUIRE( strcmp( src, dst1 ) == 0 );

    REQUIRE( _daq_strlcpy( dst2, src, sizeof( dst2 ) ) == strlen( src ) );
    REQUIRE( strcmp( src, dst2 ) == 0 );
}

TEST_CASE(
    "daq_strlcpy copies strings and truncates when the dst is too small" )
{
    const char* src = "Hello World!";
    char        dst1[ 6 ] = "";
    char        dst2[ 1 ] = "";

    REQUIRE( _daq_strlcpy( dst1, src, sizeof( dst1 ) ) == strlen( src ) );
    REQUIRE( strcmp( "Hello", dst1 ) == 0 );

    REQUIRE( _daq_strlcpy( dst2, src, sizeof( dst2 ) ) == strlen( src ) );
    REQUIRE( strcmp( "", dst2 ) == 0 );
}

TEST_CASE( "daq_strlcpy does nothing when arguments are bad" )
{
    const char* src = "Hello World!";
    char        dst[ 100 ];

    REQUIRE( _daq_strlcpy( NULL, src, 5 ) == 0 );
    REQUIRE( _daq_strlcpy( dst, NULL, sizeof( dst ) ) == 0 );
    REQUIRE( _daq_strlcpy( dst, src, 0 ) == 0 );
    REQUIRE( _daq_strlcpy( NULL, NULL, 0 ) == 0 );
}

TEST_CASE(
    "daq_strlcat appends strings safely when buffers are sufficiently large" )
{
    const char* src1 = "Hello World!";
    const char* src3 = "Hello ";
    const char* src4 = "World!";
    char        dst1[ 100 ] = "";
    char        dst2[ 13 ] = "";
    char        dst3[ 100 ] = "";
    char        dst4[ 13 ] = "";

    REQUIRE( _daq_strlcat( dst1, src1, sizeof( dst1 ) ) == strlen( src1 ) );
    REQUIRE( strcmp( src1, dst1 ) == 0 );

    REQUIRE( _daq_strlcat( dst2, src1, sizeof( dst2 ) ) == strlen( src1 ) );
    REQUIRE( strcmp( src1, dst2 ) == 0 );

    REQUIRE( _daq_strlcat( dst3, src3, sizeof( dst3 ) ) == strlen( src3 ) );
    REQUIRE( _daq_strlcat( dst3, src4, sizeof( dst3 ) ) ==
             strlen( src3 ) + strlen( src4 ) );
    REQUIRE( strcmp( src1, dst3 ) == 0 );

    REQUIRE( _daq_strlcat( dst4, src3, sizeof( dst4 ) ) == strlen( src3 ) );
    REQUIRE( _daq_strlcat( dst4, src4, sizeof( dst4 ) ) ==
             strlen( src3 ) + strlen( src4 ) );
    REQUIRE( strcmp( src1, dst4 ) == 0 );
}

TEST_CASE( "daq_strlcat appends strings safely and truncates when needed" )
{
    const char* src1a = "Hello World!";
    const char* src1 = "Hello ";
    const char* src2 = "World!";
    char        dst1[ 10 ] = "";
    char        dst2[ 12 ] = "";
    char        dst3[ 7 ] = "";

    REQUIRE( _daq_strlcat( dst1, src1, sizeof( dst1 ) ) == strlen( src1 ) );
    REQUIRE( strcmp( src1, dst1 ) == 0 );
    REQUIRE( _daq_strlcat( dst1, src2, sizeof( dst1 ) ) ==
             strlen( src1 ) + strlen( src2 ) );
    REQUIRE( strcmp( "Hello Wor", dst1 ) == 0 );

    REQUIRE( _daq_strlcat( dst2, src1, sizeof( dst2 ) ) == strlen( src1 ) );
    REQUIRE( strcmp( src1, dst2 ) == 0 );
    REQUIRE( _daq_strlcat( dst2, src2, sizeof( dst2 ) ) ==
             strlen( src1 ) + strlen( src2 ) );
    REQUIRE( strcmp( "Hello World", dst2 ) == 0 );

    REQUIRE( _daq_strlcat( dst3, src1, sizeof( dst3 ) ) == strlen( src1 ) );
    REQUIRE( strcmp( src1, dst3 ) == 0 );
    REQUIRE( _daq_strlcat( dst3, src2, sizeof( dst3 ) ) ==
             strlen( src1 ) + strlen( src2 ) );
    REQUIRE( strcmp( src1, dst3 ) == 0 );
}

TEST_CASE( "If given a non-null terminated dest, strlcat will truncate the "
           "destination and not overflow dest." )
{
    char dst[ 20 ];

    for ( int i = 0; i < 20; ++i )
    {
        dst[ i ] = '1';
    }
    dst[ 20 - 1 ] = 0;

    REQUIRE( strcmp( dst, "1111111111111111111" ) == 0 );
    REQUIRE( _daq_strlcat( dst, "Hello", 15 ) == 15 + strlen( "Hello" ) - 1 );
    REQUIRE( strcmp( dst, "11111111111111" ) == 0 );
}

TEST_CASE( "daq_strlcat does nothing when arguments are bad" )
{
    const char* src = "Hello World!";
    char        dst[ 100 ];

    REQUIRE( _daq_strlcat( NULL, src, 5 ) == 0 );
    REQUIRE( _daq_strlcat( dst, NULL, sizeof( dst ) ) == 0 );
    REQUIRE( _daq_strlcat( dst, src, 0 ) == 0 );
    REQUIRE( _daq_strlcat( NULL, NULL, 0 ) == 0 );
}

TEST_CASE( "An empty or NULL string has zero bytes remaining" )
{
    REQUIRE( _daq_strlremainder( NULL, 100 ) == 0 );
    REQUIRE( _daq_strlremainder( "", 0 ) == 0 );
}

TEST_CASE( "A string that is full will have zero bytes remaining" )
{
    REQUIRE( _daq_strlremainder( "Hello", 6 ) == 0 );
}

TEST_CASE(
    "A string that is not null terminated will have zero bytes remaining" )
{
    REQUIRE(
        _daq_strlremainder( "string longer than length, ie not null terminated",
                            2 ) == 0 );
}

TEST_CASE(
    "A string who's buffer is not full will give the proper length back" )
{
    char buf1[ 50 ] = "abc";
    char buf2[ 10 ] = "world";
    char buf3[ 10 ] = "";

    REQUIRE( _daq_strlremainder( buf1, sizeof( buf1 ) ) == 46 );
    REQUIRE( _daq_strlremainder( buf2, sizeof( buf2 ) ) == 4 );
    REQUIRE( _daq_strlremainder( buf3, sizeof( buf2 ) ) == 9 );
}

TEST_CASE( "You can slprintf into a string whos buffer is big enough" )
{
    const char* hello = "Hello World!";
    char        buf1[ 50 ] = "";
    char        buf2[ 10 ] = "";

    REQUIRE( _daq_slprintf( buf1, sizeof( buf1 ), "%s", hello ) ==
             strlen( hello ) );
    REQUIRE( strcmp( "Hello World!", buf1 ) == 0 );
    REQUIRE( _daq_slprintf( buf2, sizeof( buf2 ), "%s", "123456789" ) ==
             strlen( "123456789" ) );
    REQUIRE( strcmp( "123456789", buf2 ) == 0 );
}

TEST_CASE(
    "You can slprintf into a string whos buffer is big enough to concatenate" )
{
    const char* hello = "Hello World!";
    const char* abc_hello = "abcHello World!";
    const char* abc_123 = "abc123456";
    char        buf1[ 50 ] = "abc";
    char        buf2[ 10 ] = "abc";

    REQUIRE( _daq_slprintf( buf1, sizeof( buf1 ), "%s", hello ) ==
             strlen( abc_hello ) );
    REQUIRE( strcmp( abc_hello, buf1 ) == 0 );
    REQUIRE( _daq_slprintf( buf2, sizeof( buf2 ), "%s", "123456789" ) ==
             strlen( "abc" ) + strlen( "123456789" ) );
    REQUIRE( strcmp( abc_123, buf2 ) == 0 );
}

TEST_CASE( "You can slprintf into a non-null terminated string and it will "
           "truncate the buffer" )
{
    char dst[ 20 ];

    for ( int i = 0; i < 20; ++i )
    {
        dst[ i ] = '1';
    }
    dst[ 20 - 1 ] = 0;

    REQUIRE( strcmp( dst, "1111111111111111111" ) == 0 );
    REQUIRE( _daq_slprintf( dst, 5, "abc" ) == 5 + strlen( "abc" ) - 1 );
    REQUIRE( strcmp( dst, "1111" ) == 0 );
}

TEST_CASE( "You can call slprintf with bad arguments" )
{
    char buf[ 100 ];

    REQUIRE( _daq_slprintf( NULL, 50, "abc" ) == 0 );
    REQUIRE( _daq_slprintf( buf, 0, "abc" ) == 0 );
    REQUIRE( _daq_slprintf( buf, 50, NULL ) == 0 );
    REQUIRE( _daq_slprintf( NULL, 0, NULL ) == 0 );
}