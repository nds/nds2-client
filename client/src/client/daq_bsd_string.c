//
// Created by jonathan.hanks on 2/6/20.
//
#include "daq_bsd_string.h"
#include <stdio.h>
#include <string.h>

size_t
_daq_strlcpy( char* dst, const char* src, size_t size )
{
    size_t bytes_copied = 0;

    if ( !dst || !src || size < 1 )
    {
        return 0;
    }

    while ( *src && size > 1 )
    {
        *dst++ = *src++;
        ++bytes_copied;
        --size;
    }
    *dst = 0;
    return strlen( src ) + bytes_copied;
}

size_t
_daq_strlcat( char* dst, const char* src, size_t size )
{
    size_t bytes_traversed = 0;

    if ( !dst || !src || size < 1 )
    {
        return 0;
    }
    while ( *dst && size > 1 )
    {
        ++dst;
        ++bytes_traversed;
        --size;
    }
    *dst = 0;
    return _daq_strlcpy( dst, src, size ) + bytes_traversed;
}

size_t
_daq_strlremainder( const char* src, size_t size )
{
    size_t remaining = 0;

    if ( !src )
    {
        return 0;
    }
    remaining = strlen( src ) + 1;
    if ( remaining <= size )
    {
        return size - remaining;
    }
    return 0;
}

size_t
_daq_slprintf( char* dest, size_t size, const char* fmt, ... )
{
    va_list args;
    size_t  remaining = _daq_strlremainder( dest, size ) + 1;
    size_t  used = size - remaining;
    size_t  printed = 0;

    if ( !dest || size < 1 || !fmt )
    {
        return 0;
    }

    va_start( args, fmt );
    printed = vsnprintf( dest + used, remaining, fmt, args );
    va_end( args );
    /* remove the NULL count */
    return used + printed;
}