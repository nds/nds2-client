//
// Created by jonathan.hanks on 2/6/20.
//

#ifndef NDS2_CLIENT_DAQ_BSD_STRING_H
#define NDS2_CLIENT_DAQ_BSD_STRING_H

/*!
 * @file The daq_string.h file implements some string manipulation functions
 * patterned after bsd strl... functions.  In addition some helpers for using
 * a safe sprintf are provided.
 *
 * This is re-implemented here instead of linking to libbsd to keep this
 * simplier for the windows port and to allow for expansions in interface.
 */

#include <stdarg.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * @brief Copy from src to dest.
 * This copies from src to dest.  It will always null terminate
 * dest.
 *
 * @param dst Pointer to the destination which is at least size bytes long
 * @param src Source buffer, which must be a NULL terminated string
 * @param size the length of dst (including space for a NULL byte)
 * @returns The size of the string that was requested, ie strlen(src).  This is
 * done to match the bsd interface, which uses this feature to detect truncation
 * when the return value is >= size.
 *
 * @note if src == NULL or dst == NULL or size == 0 this will do
 * nothing and will return 0
 */
size_t _daq_strlcpy( char* dst, const char* src, size_t size );

/*!
 * @brief Append src to dest.
 * This copies from src to the end of dest.  It will always null terminate
 * dest.
 *
 * @param dst Pointer to the destination which is at least size bytes long (this
 * is the total size, including existing content, not just the remaining
 * buffer space available).
 * @param src Source buffer, which must be a NULL terminated string
 * @param size the length of dst (including space for a NULL byte)
 * @returns The size of the string that was requested, ie strlen(src) +
 * strlen(dst).  This is done to match the bsd interface, which uses this
 * feature to detect truncation when the return value is >= size.
 *
 * @note if src == NULL or dst == NULL or size == 0 this will do
 * nothing
 * @note In a difference from the bsd version, strlcat *ALWAYS* null terminates
 * if size > 0.
 */
size_t _daq_strlcat( char* dst, const char* src, size_t size );

/*!
 * @brief Return the number of usable bytes left at the end of the given string
 * @param src A NULL terminated string
 * @param size The maximum size of the string (counting the NULL terminator)
 * @return The number of bytes that can be appended to the string while staying
 * withing a max size of size bytes (counting the NULL terminator).
 */
size_t _daq_strlremainder( const char* src, size_t size );

/*!
 * @brief a length aware printf that appends to dst.
 * @param dst The destination character array to append data to
 * @param size The total size of the destination buffer
 * @param fmt The format to apply, interpreted by printf
 * @param ... The arguments to format
 * @returns The size of the string after printing, if it is larger than size
 * then the string is truncated.
 * @notes Use this as snprintf, except that the size is the total buffer size,
 * not just the remaining amount.
 */
size_t _daq_slprintf( char* dst, size_t size, const char* fmt, ... );
#ifdef __cplusplus
};
#endif

#endif // NDS2_CLIENT_DAQ_BSD_STRING_H
