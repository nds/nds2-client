#========================================================================
# This script is used to find the multiple versions of Matlab
#========================================================================
#========================================================================
# For versions earlier than 2.6
#========================================================================
CMAKE_POLICY(PUSH)
CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
IF ( NOT DEFINED WITHOUT_MATLAB
     AND ( ( DEFINED WITH_MATLAB
	     AND ( "${WITH_MATLAB}" MATCHES "no" ) )
	   OR NOT DEFINED WITH_MATLAB ) )
#========================================================================
# 
#========================================================================
SET( MATLAB_VERSIONS_BAD
     14_sp3
)
SET( MATLAB_PREFIX matlab_r )
SET(MATLAB_SUFFIX "_BLANK_")
IF(APPLE)
  LIST(INSERT MATLAB_SUFFIX 0 .app )
  SET( MATLAB_PREFIX MATLAB_R )
  SET( MATLAB_PATH 
       /Applications # Mac OS
  )
ELSEIF(WIN32)
  SET( MATLAB_PREFIX R )
  SET( MATLAB_PATH
       # "/Program Files/MATLAB" # Windows
       "/Progra~1/MATLAB" # Windows
  )
ELSE(APPLE)
  SET( MATLAB_PATH 
       /ldcg         # Linux
       /usr
  )
ENDIF(APPLE)

#========================================================================
# Internal functions and macros
#========================================================================
MACRO(MATLAB_CHECK DIR VERSION)
  
ENDMACRO(MATLAB_CHECK)

FUNCTION(GET_MATLAB_VERSION DIR VERSION_VAR)
  SET( PATTERN "^.*/${MATLAB_PREFIX}(.*)\$" )
  STRING(TOLOWER ${PATTERN} PATTERN)
  # MESSAGE( "PATTERN is now set to: ${PATTERN}" )
  STRING(TOLOWER ${DIR} DIR)
  STRING(REGEX REPLACE ${PATTERN} "\\1" DIR "${DIR}")
  SET(DONE 0)
  FOREACH ( S ${MATLAB_SUFFIX} )
    IF (DONE EQUAL 0)
      IF (${S} MATCHES "_BLANK_")
      ENDIF (${S} MATCHES "_BLANK_")
      STRING(REGEX REPLACE "^(.*)${S}$" "\\1" DIR_TEMP "${DIR}")
      IF (NOT ${DIR_TEMP} STREQUAL ${DIR})
        SET(DONE 1)
	SET(DIR ${DIR_TEMP})
      ENDIF (NOT ${DIR_TEMP} STREQUAL ${DIR})
    ENDIF (DONE EQUAL 0)
  ENDFOREACH ( S ${MATLAB_SUFFIX} )
  SET(${VERSION_VAR} ${DIR} PARENT_SCOPE)
ENDFUNCTION(GET_MATLAB_VERSION)

#========================================================================
# Locate directory canidates
#========================================================================

FOREACH( PATH ${MATLAB_PATH} )
  FOREACH(SUFFIX ${MATLAB_SUFFIX})
    IF(${SUFFIX} MATCHES "_BLANK_")
      SET(SUFFIX "")
    ENDIF(${SUFFIX} MATCHES "_BLANK_")
    FILE( GLOB MATLAB_DIRS_TMP
          "${PATH}/${MATLAB_PREFIX}*${SUFFIX}" )
    IF(MATLAB_DIRS_TMP)
      LIST(APPEND MATLAB_DIRS ${MATLAB_DIRS_TMP})
    ENDIF(MATLAB_DIRS_TMP)
  ENDFOREACH(SUFFIX ${MATLAB_SUFFIX})
ENDFOREACH( PATH ${MATLAB_PATH} )
LIST(SORT MATLAB_DIRS)
LIST(REMOVE_DUPLICATES MATLAB_DIRS)

#========================================================================
# Generate what can be done for each canidate
#========================================================================

FOREACH(MATLAB_DIR ${MATLAB_DIRS})
  GET_MATLAB_VERSION("${MATLAB_DIR}" MATLAB_VERSION)
  set(MATLAB_SKIP_VERSION 0)
  #----------------------------------------------------------------------
  # Version specific compiler
  #----------------------------------------------------------------------
  IF (WIN32)
    SET(MEX${MATLAB_VERSION} "${MATLAB_DIR}/bin/mex.bat")
  ELSE (WIN32)
    SET(MEX${MATLAB_VERSION} "${MATLAB_DIR}/bin/mex")
  ENDIF (WIN32)
  # MESSAGE("Just set MEX${MATLAB_VERSION} to ${MEX${MATLAB_VERSION}}")
  #----------------------------------------------------------------------
  # Collect MATLAB configuration information
  #----------------------------------------------------------------------
  IF(WIN32)
    IF(CMAKE_CL_64)
      SET(MATLAB_CONFIGURATION "ARCH = win64")
    ELSE(CMAKE_CL_64)
      SET(MATLAB_CONFIGURATION "ARCH = win32")
    ENDIF(CMAKE_CL_64)
  ELSE (WIN32)
    EXECUTE_PROCESS( COMMAND ${MATLAB_DIR}/bin/matlab -nosplash -nodesktop -n
	             OUTPUT_VARIABLE MATLAB_CONFIGURATION )
  ENDIF (WIN32)
  # MESSAGE( "Configuration: ${MATLAB_CONFIGURATION}" )
  #----------------------------------------------------------------------
  # ARCH
  #----------------------------------------------------------------------
  STRING(REGEX MATCH "ARCH[^=]*=[^=\n]+"
         MATLAB_${MATLAB_VERSION}_ARCH
	 "${MATLAB_CONFIGURATION}")
  STRING(REGEX REPLACE "^[^=]*="
         "" MATLAB_${MATLAB_VERSION}_ARCH
	 "${MATLAB_${MATLAB_VERSION}_ARCH}")
  STRING(STRIP "${MATLAB_${MATLAB_VERSION}_ARCH}" MATLAB_${MATLAB_VERSION}_ARCH)
  IF (APPLE)
    SET(IS_ARCH_64BIT 0)
    STRING(REGEX MATCH "64$"
           IS_ARCH_64BIT
           "${MATLAB_${MATLAB_VERSION}_ARCH}")
	   # MESSAGE("DEBUG: IS_ARCH_64BIT: ${IS_ARCH_64BIT}")
      
    IF (DEFINED ENABLE_64BIT)
      # MESSAGE("Working on apple by forcing 64 bit: ${MATLAB_${MATLAB_VERSION}_ARCH}")
      IF ( NOT ( "${IS_ARCH_64BIT}" STREQUAL "64" ) )
        # MESSAGE( "Removed: ${MATLAB_DIR}")
        LIST(REMOVE_ITEM MATLAB_DIRS ${MATLAB_DIR})
	set(MATLAB_SKIP_VERSION 1)
      ENDIF ( NOT ( "${IS_ARCH_64BIT}" STREQUAL "64" ) )
    ELSE (DEFINED ENABLE_64BIT)
      # MESSAGE("Working on apple by forcing 32 bit")
      IF ( "${IS_ARCH_64BIT}" STREQUAL "64" )
        LIST(REMOVE_ITEM MATLAB_DIRS ${MATLAB_DIR})
        # MESSAGE( "Removed: ${MATLAB_DIR}")
	set(MATLAB_SKIP_VERSION 1)
      ENDIF ( "${IS_ARCH_64BIT}" STREQUAL "64" )
    ENDIF (DEFINED ENABLE_64BIT)
  ENDIF (APPLE)
  FILE(GLOB MATLAB_BIN_ARCH_DIR "${MATLAB_DIR}/bin/${MATLAB_${MATLAB_VERSION}_ARCH}")
  if ((NOT DEFINED MATLAB_INCLUDE_DIR) AND
      (NOT MATLAB_SKIP_VERSION) )
    # message(STATUS "Still searching for MATLAB_INCLUDE_DIR as ${MATLAB_DIR}/external/include/mex.h")
    if (EXISTS "${MATLAB_DIR}/extern/include/mex.h")
      set(MATLAB_INCLUDE_DIR "${MATLAB_DIR}/extern/include"
	  CACHE INTERNAL "")
      # message(STATUS "Setting MATLAB_INCLUDE_DIR: ${MATLAB_INCLUDE_DIR}")
    endif ()
  endif ()
  
  IF(MATLAB_BIN_ARCH_DIR)
    # MESSAGE( STATUS "MATLAB_${MATLAB_VERSION}_ARCH: ${MATLAB_${MATLAB_VERSION}_ARCH}" )
    #--------------------------------------------------------------------
    # OVER RIDES
    #--------------------------------------------------------------------
    IF(${MATLAB_VERSION} STREQUAL "13" )
      IF (${MATLAB_${MATLAB_VERSION}_ARCH} MATCHES "^.*64$" )
        SET(MEX${MATLAB_VERSION}_OVERRIDES "'LDFLAGS=-m64 \$LDFLAGS' 'CFLAGS=\$CFLAGS -m64'")
      ELSE (${MATLAB_${MATLAB_VERSION}_ARCH} MATCHES "^.*64$" )
        SET(MEX${MATLAB_VERSION}_OVERRIDES "'LDFLAGS=-m32 \$LDFLAGS' 'CFLAGS=\$CFLAGS -m32'")
      ENDIF (${MATLAB_${MATLAB_VERSION}_ARCH} MATCHES "^.*64$" )
    ELSE(${MATLAB_VERSION} EQUAL 13 )
      SET(MEX${MATLAB_VERSION}_OVERRIDES "")
    ENDIF(${MATLAB_VERSION} STREQUAL "13" )
    # MESSAGE("Just set MEX${MATLAB_VERSION}_OVERRIDES to ${MEX${MATLAB_VERSION}_OVERRIDES}")
  ELSE(MATLAB_BIN_ARCH_DIR)
    LIST(REMOVE_ITEM MATLAB_DIRS ${MATLAB_DIR})
  ENDIF(MATLAB_BIN_ARCH_DIR)
  #----------------------------------------------------------------------
  # Extension
  #----------------------------------------------------------------------
  SET(MATLAB_${MATLAB_VERSION}_EXT mex${MATLAB_${MATLAB_VERSION}_ARCH})
  IF(WIN32)
    STRING(REPLACE "win" "w" MATLAB_${MATLAB_VERSION}_EXT "${MATLAB_${MATLAB_VERSION}_EXT}")
  ENDIF(WIN32)
  #======================================================================
  # HAVE_MEX_OPTION_xxx
  #======================================================================
  EXECUTE_PROCESS(COMMAND ${MEX${MATLAB_VERSION}} -h
                  OUTPUT_VARIABLE MEX_HELP_MESSAGE)
  #----------------------------------------------------------------------
  # HAVE_MEX_OPTION_ARCH
  #----------------------------------------------------------------------
  STRING(REGEX MATCH "-<arch>"
         HAVE_MEX_${MATLAB_VERSION}_OPTION_ARCH
         "${MEX_HELP_MESSAGE}")
  STRING(REGEX REPLACE "<arch>" "${MATLAB_${MATLAB_VERSION}_ARCH}"
         HAVE_MEX_${MATLAB_VERSION}_OPTION_ARCH
	 "${HAVE_MEX_${MATLAB_VERSION}_OPTION_ARCH}")
  # MESSAGE("HAVE_MEX_${MATLAB_VERSION}_OPTION_ARCH: ${HAVE_MEX_${MATLAB_VERSION}_OPTION_ARCH}")
  #----------------------------------------------------------------------
  # HAVE_MEX_OPTION_OUTPUT
  #----------------------------------------------------------------------
  STRING(REGEX MATCH "-output"
         HAVE_MEX_${MATLAB_VERSION}_OPTION_OUTPUT
         "${MEX_HELP_MESSAGE}")
  IF(HAVE_MEX_${MATLAB_VERSION}_OPTION_OUTPUT)
    SET(HAVE_MEX_${MATLAB_VERSION}_OPTION_OUTPUT
        "${HAVE_MEX_${MATLAB_VERSION}_OPTION_OUTPUT} ${MATLAB_VERSION}")
  ENDIF(HAVE_MEX_${MATLAB_VERSION}_OPTION_OUTPUT)

  # MESSAGE("HAVE_MEX_${MATLAB_VERSION}_OPTION_OUTPUT: ${HAVE_MEX_${MATLAB_VERSION}_OPTION_OUTPUT}")
ENDFOREACH(MATLAB_DIR ${MATLAB_DIRS})
# MESSAGE(STATUS "DIRS: ${MATLAB_DIRS}")

set(pCMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES})
set(CMAKE_REQUIRED_INCLUDES ${MATLAB_INCLUDE_DIR})
set(pCMAKE_EXTRA_INCLUDE_FILES ${CMAKE_EXTRA_INCLUDE_FILES})
set(CMAKE_EXTRA_INCLUDE_FILES "mex.h")
check_type_size(mwSize SIZEOF_MWSIZE)
# message("Sizeof wmSize: ${SIZEOF_MWSIZE}")
set(CMAKE_EXTRA_INCLUDE_FILES ${pCMAKE_EXTRA_INCLUDE_FILES})
set(CMAKE_REQUIRED_INCLUDES ${pCMAKE_REQUIRED_INCLUDES})

ENDIF ( NOT DEFINED WITHOUT_MATLAB
        AND ( ( DEFINED WITH_MATLAB
	        AND ( "${WITH_MATLAB}" MATCHES "no" ) )
	        OR NOT DEFINED WITH_MATLAB ) )
#========================================================================
# For versions earlier than 2.6
#========================================================================
CMAKE_POLICY(POP)
