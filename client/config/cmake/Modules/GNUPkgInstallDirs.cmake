# -*- coding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=cmake:et:sw=4:ts=4:sts=4
include(GNUInstallDirs)

if ( NOT CMAKE_INSTALL_PKGDATADIR)
    set(CMAKE_INSTALL_PKGDATADIR "" CACHE PATH "package specific read-only architecture-independent data (DATADIR/PROJECT_NAME)")
    set(CMAKE_INSTALL_PKGDATADIR "${CMAKE_INSTALL_DATADIR}/${PROJECT_NAME}")
endif ( NOT CMAKE_INSTALL_PKGDATADIR)

mark_as_advanced(
    CMAKE_INSTALL_PKGDATADIR
    )
