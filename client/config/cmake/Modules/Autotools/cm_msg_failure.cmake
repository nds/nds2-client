#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_msg_error( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

include ( Autotools/cm_msg_notice )

function( cm_msg_error txt )
  set( prefix "FAILURE" )

  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )

  cmake_parse_arguments( ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cm_msg_notice( "${prefix}: ${txt}" )
  message( FATAL_ERROR "" )

endfunction( )
