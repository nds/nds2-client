#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( Autotools/Internal/ci_cache )
include( Autotools/Internal/ci_join )

include( cm_msg_debug_variable )

#------------------------------------------------------------------------
# ch_top( text )
#
#   Place all text at the top of the configuration file
#
# NOTE:
#   This is part of the autoheader look and feel
#------------------------------------------------------------------------
function( ch_top )
  unset( CH_TOP_TEXT CACHE )
  ci_join( CH_TOP_TEXT "\n" ${ARGN} )
  cm_cache( CH_TOP_TEXT VALUE "${CH_TOP_TEXT}" )
endfunction()

