#
# TODO: This still needs:
#    1) OPTION()
#    2) Recording in cache
# 
INCLUDE( CheckIncludeFiles )
include(FindPackageHandleStandardArgs)

UNSET(SASL_INCLUDE_DIR)
UNSET(SASL_LIBRARY)
IF ( DEFINED WITHOUT_SASL OR WITH_SASL MATCHES "^[Nn][Oo]$" )
  SET( WITH_SASL_NO 1 )
  SET( WITH_SASL_LENGTH 0 )
ELSEIF ( WITH_SASL MATCHES "^[Yy][Ee][Ss]$")
  SET( WITH_SASL_LENGTH 0 )
ELSE ( DEFINED WITHOUT_SASL OR WITH_SASL MATCHES "^[Nn][Oo]$" )
  STRING( TOLOWER "${WITH_SASL}" WITH_SASL_LOWER)
  STRING( LENGTH "${WITH_SASL}" WITH_SASL_LENGTH )
ENDIF ( DEFINED WITHOUT_SASL OR WITH_SASL MATCHES "^[Nn][Oo]$" )


IF( WITH_SASL_NO )
  #======================================================================
  # Simply do nothing about detecting SASL
  #======================================================================
  MESSAGE(STATUS "SASL: disabled")
ELSEIF( NOT WITH_SASL_LENGTH EQUAL 0 )
  MESSAGE(STATUS "SASL: User supplied")
  #======================================================================
  # User is suplying hints of where the SASL library is located.
  # Give them the first chance at the libraries and header files
  #======================================================================
  FIND_PATH(SASL_INCLUDE_DIR sasl/sasl.h
            HINTS ${WITH_SASL}/include
	    PATHS /usr/include )
  FIND_LIBRARY(SASL_LIBRARY NAMES sasl2 sasl
               HINTS ${WITH_SASL}/lib ${WITH_SASL}/bin
               PATHS /usr/lib )
ELSE( WITH_SASL_NO )
  #======================================================================
  # Search only the system supplied places
  #======================================================================
  MESSAGE(STATUS "SASL: ELSE")
  FIND_PATH(SASL_INCLUDE_DIR sasl/sasl.h
	    PATHS /usr/include )
  FIND_LIBRARY(SASL_LIBRARY NAMES sasl2 sasl
               PATHS /usr/lib )
ENDIF( WITH_SASL_NO )
#========================================================================
# Handle the QUIETLY and REQUIRED arguments and set SASL_FOUND to TRUE
# if all listed variables are TRUE
#========================================================================
find_package_handle_standard_args(SASL DEFAULT_MSG
			          SASL_LIBRARY
				  SASL_INCLUDE_DIR )
mark_as_advanced(SASL_LIBRARY SASL_INCLUDE_DIR)
set(SASL_FOUND ${SASL_FOUND} CACHE INTERNAL "" FORCE)
