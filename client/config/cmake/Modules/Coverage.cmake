set (COVERAGE_LCOV "NO")
if (${ENABLE_COVERAGE})
    set(_ENABLE_COVERAGE_LINK_FLAGS_TMP ${CMAKE_REQUIRED_LIBRARIES})

    # test gcov/lcov
    set(CMAKE_REQUIRED_LIBRARIES "gcov")
    check_cxx_compiler_flag("-p -fprofile-arcs -ftest-coverage" ENABLE_COVERAGE_FLAGS_AVAILABLE)
    find_program(ENABLE_COVERAGE_LCOV_PROG NAMES lcov
            PATH /bin /usr/bin /usr/local/bin /opt/local/bin )
    find_program(ENABLE_COVERAGE_GENHTML_PROG NAMES genhtml
            PATH /bin /usr/bin /usr/local/bin /opt/local/bin )

    if (${ENABLE_COVERAGE_FLAGS_AVAILABLE})
        if (NOT ENABLE_COVERAGE_LCOV_PROG)
            message(FATAL_ERROR "Code coverage requested, but the lcov program could not be found.")
        endif (NOT ENABLE_COVERAGE_LCOV_PROG)

        if (NOT ENABLE_COVERAGE_GENHTML_PROG)
            message(FATAL_ERROR "Code coverage requested, but the genhtml program could not be found.")
        endif (NOT ENABLE_COVERAGE_GENHTML_PROG)

        list(APPEND CMAKE_C_FLAGS "-p -fprofile-arcs -ftest-coverage")
        list(APPEND CMAKE_CXX_FLAGS "-p -fprofile-arcs -ftest-coverage")
        list(APPEND CMAKE_EXE_LINKER_FLAGS "-fprofile-arcs -ftest-coverage")

        set(COVERAGE_LCOV "YES")

    else (${ENABLE_COVERAGE_FLAGS_AVAILABLE})
        message(FATAL_ERROR "Code coverage instrumentation requested but not supported")
    endif(${ENABLE_COVERAGE_FLAGS_AVAILABLE})

    set(CMAKE_REQUIRED_LIBRARIES ${_ENABLE_COVERAGE_LINK_FLAGS_TMP})
endif (${ENABLE_COVERAGE})