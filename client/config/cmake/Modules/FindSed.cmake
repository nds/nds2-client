include(FindCygwin)

find_program(SED
	     sed
	     ${CYGWIN_INSTALL_PATH}/bin
	     /bin
	     /usr/bin
	     /usr/local/bin
	     /sbin/bin
)
mark_as_advanced(SED)
