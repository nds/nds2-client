include( CMakeParseArguments )

find_program( PROG_RPM rpm )
find_program( PROG_DNF dnf )
find_program( PROG_YUM yum )

function( echo )
  execute_process( COMMAND ${CMAKE_COMMAND} -E echo ${ARGN})
endfunction( )

function( exclude DEPENDENCIES )
  if ( DEFINED EXCLUDE_PATTERN )
    if ( NOT "${EXCLUDE_PATTERN}" MATCHES "^[\\^]")
      set(EXCLUDE_PATTERN "^.*${EXCLUDE_PATTERN}")
    endif( )
    if ( NOT "${EXCLUDE_PATTERN}" MATCHES "[\\$]$")
      set(EXCLUDE_PATTERN "${EXCLUDE_PATTERN}.*$")
    endif( )
    foreach( dep ${${DEPENDENCIES}} )
      message( INFO "Comparing ${dep} to ${EXCLUDE_PATTERN}" )
      if ( NOT "${dep}" MATCHES "${EXCLUDE_PATTERN}" )
        list(APPEND new_depends "${dep}" )
      endif( )
    endforeach( )
    message( INFO "new_depends:  ${new_depends}" )
    set( ${DEPENDENCIES} ${new_depends} PARENT_SCOPE )
  endif( )
endfunction( )

function( DebInstallDependencies )
  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs DEPENDENCIES
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  message( "DebInstallDependencies: Installing: ${ARG_DEPENDENCIES}")
  foreach( _dependency ${ARG_DEPENDENCIES} )
    string(REPLACE "|" ";"  _dependency ${_dependency} )
    set( installed FALSE )
    foreach( dep ${_dependency} )
      message( "DebInstallDependencies: Installing: ${PACKAGE_MANAGER} install -y ${dep}" )
      execute_process( COMMAND ${PACKAGE_MANAGER} install -y ${dep}
        RESULT_VARIABLE exit_status
        OUTPUT_VARIABLE output_msg
        ERROR_VARIABLE error_msg
        )
      if ( exit_status EQUAL 0 )
        set( installed TRUE )
        break( )
      endif( )
    endforeach( )
    if ( NOT installed )
      message( FATAL_ERROR "Unable to install any of: ${_dependency}" )
    endif( )
  endforeach( )
endfunction( )

function( DebBuildDependencies VAR )
  file( GLOB_RECURSE files "${TOP_DIR}/control" "${TOP_DIR}/control.in" )
  set( keepers )
  foreach( file ${files})
    if ( NOT file MATCHES /legacy/ )
      list( APPEND keepers ${file} )
    endif( )
  endforeach( )
  set( files ${keepers} )
  # message( "DEBUG: files: ${files}" )
  unset(deps)
  foreach( SPEC_FILE ${files} )
    file( READ ${SPEC_FILE} dependencies )
    string( CONFIGURE "${dependencies}" dependencies @ONLY )
    # Remove any undefined substitution variables
    string( REGEX REPLACE '@[A-Za-z0-9_]*@' "_junk_" dependencies "${dependencies}" )
    string( REGEX REPLACE "\n" ";" dependencies "${dependencies}" )
    set( stage 1 )
    foreach(line ${dependencies})
      string(REGEX REPLACE "[ \t\n]+$" "" line "${line}" )
      string(REGEX REPLACE "^[ \t\n]+" "" line "${line}" )
      if ( stage EQUAL 1 )
        if( "${line}" MATCHES "^[Bb]uild-[Dd]epends:" )
          string( REGEX REPLACE "^[Bb]uild-[Dd]epends:" "" line "${line}" )
          string( REGEX REPLACE "[,]" ";" line "${line}" )
          string( REGEX REPLACE "[ \t]+" "" line "${line}" )
          list( APPEND deps ${line} )
          set( stage 2 )
        endif ( )
      elseif( stage EQUAL 2 )
        if ( NOT "${line}" MATCHES ",[ \t]*$" )
          set( stage 1 )
        endif( )
        string( REGEX REPLACE "[,]" ";" line "${line}" )
        string( REGEX REPLACE "[ \t]+" "" line "${line}" )
        list( APPEND deps ${line} )
      endif( )
    endforeach( )
    string( REGEX REPLACE "[(][^)]*[)]" "" deps "${deps}")
    #list( REMOVE_ITEM deps debhelper )
    # message( STATUS "DEB dependencies: ${deps}")
  endforeach( )
  message( "INSTALL: ${INSTALL} debian packages" )
  exclude( deps )
  string( REGEX REPLACE "[|]" ";" deps "${deps}" )
  list( SORT deps )
  list( REMOVE_DUPLICATES deps )
  if ( INSTALL )
    foreach ( pkg devscripts equivs )
      execute_process(
        COMMAND ${pkg_mgr}
        -o Debug::pkgProblemResolver=yes
        --no-install-recommends
        --yes
        install ${pkg}
        OUTPUT_VARIABLE cout
        ERROR_VARIABLE cerr
        RESULT_VARIABLE EXIT_STATUS
        )
      # message( "DEBUG: installing ${pkg}" )
      # message( "DEBUG: exit status: ${EXIT_STATUS}" )
      # message( "DEBUG: output: ${cout}" )
      # message( "DEBUG: error output: ${cerr}" )
    endforeach( )
    find_program( PROG_MK_BUILD_DEPS mk-build-deps )
    # message( "DEBUG: PROG_MK_BUILD_DEPS: ${PROG_MK_BUILD_DEPS}" )
    # message( "DEBUG: control files: ${files}" )
    make_directory( "debian" )
    foreach( SPEC_FILE ${files} )
      # message( "DEBUG: Installing biuld dependencies from dependencies" )
      file( READ "${SPEC_FILE}" spec_file_source )
      string( CONFIGURE "${spec_file_source}" spec_file_source @ONLY )
      string( REPLACE "\n" ";" spec_file_source "${spec_file_source}" )
      string( REPLACE ";;" ";# blank_line;" spec_file_source "${spec_file_source}" )
      # message( "DEBUG: psec_file_source as list: ${spec_file_source}" )
      foreach( trailer "," "" )
        set( tmp_spec "" )
        set( pattern "^([ \\t]+).*${EXCLUDE_PATTERN}.*${trailer}$" )
        # message( "DEBUG: exclude pattern: ${pattern}" )
        foreach( line ${spec_file_source} )
          string( REGEX REPLACE  ${pattern} "\tcoreutils${trailer}" line "${line}" )
          list( APPEND tmp_spec "${line}" )
        endforeach( )
        set( spec_file_source ${tmp_spec} )
      endforeach( )
      string( REPLACE ";" "\n" spec_file_source "${spec_file_source}" )
      string( REPLACE "# blank_line" "" spec_file_source "${spec_file_source}" )
      file( WRITE debian/control "${spec_file_source}" )
      file( READ debian/control dependencies_CONTENT )
      # message( "DEBUG: contents:\n${dependencies_CONTENT}" )
      execute_process(
        COMMAND ${PROG_MK_BUILD_DEPS}
        debian/control
        OUTPUT_VARIABLE cout
        ERROR_VARIABLE cerr
        RESULT_VARIABLE EXIT_STATUS )
      # message( "DEBUG: exit status: ${EXIT_STATUS}" )
      # message( "DEBUG: output: ${cout}" )
      # message( "DEBUG: error output: ${cerr}" )
      file( GLOB_RECURSE files "*-build-deps_*.deb" )
      # message( "DEBUG: glob .deb files: ${files}" )
      # message( "DEBUG: installing build dependencies" )
      execute_process(
        COMMAND ${pkg_mgr}
          install -y ${files}
        OUTPUT_VARIABLE cout
        ERROR_VARIABLE cerr
        RESULT_VARIABLE EXIT_STATUS )
      # message( "DEBUG: exit status: ${EXIT_STATUS}" )
      # message( "DEBUG: output: ${cout}" )
      # message( "DEBUG: error output: ${cerr}" )
    endforeach( )
  endif( )
  set( ${VAR} ${deps} PARENT_SCOPE )
endfunction( )

function( RPMInstallPackage PACKAGE )
  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( PROG_DNF )
    execute_process(
      COMMAND ${PROG_DNF} install --assumeyes ${PACKAGE}
      OUTPUT_VARIABLE local_output
      ERROR_VARIABLE local_error
      )
  elseif( PROG_YUM )
    execute_process(
      COMMAND ${PROG_YUM} install --assumeyes ${PACKAGE}
      OUTPUT_VARIABLE local_output
      ERROR_VARIABLE local_error
      )
  endif( )
  unset( local_output )
  unset( local_error )

endfunction( )

##
# @fn RPMListDependencies( SPEC_FILE VAR )
#
# @brief List build dependencies from given SPEC_FILE
#
# @details
# This function will list BuildRequires dependencies
# specified in the SPEC_FILE.
#
# @param[in] SPEC_FILE
#   RPM spec file; must have the extension .spec
# @param[out] VAR
#   name of variable to substitute with result
#
# @see @ref RPMInstallDependencies
#
function( RPMListDependencies SPEC_FILE VAR )
  RPMInstallPackage( rpm-build )
  find_program( PROG_RPMSPEC rpmspec )

  message( INFO "PROG_RPMSPEC: ${PROG_RPMSPEC}")
  message( INFO "PROG_RPM: ${PROG_RPM}")
  set( dependencies "" )
  if ( PROG_RPMSPEC )
    execute_process(
      COMMAND ${PROG_RPMSPEC} --buildrequires --query "${SPEC_FILE}"
      OUTPUT_VARIABLE dependencies
      ERROR_VARIABLE dependencies_error
      )
  elseif ( PROG_RPM )
    file( READ ${SPEC_FILE} dependencies )
    execute_process(
      COMMAND ${PROG_RPM} --eval "${dependencies}"
      OUTPUT_VARIABLE raw_dependencies
      ERROR_VARIABLE dependencies_error
      )
    set( deps "" )
    foreach(line ${raw_dependencies})
      if( "${line}" MATCHES "^[Bb]uild[Rr]equires:" )
        string(REGEX REPLACE "^[Bb]uild[Rr]equires:[ \t]*" "" line ${line})
        list(APPEND dependencies ${line})
      endif( )
    endforeach( )
  endif( )
  message(WARNING "dependencies ${dependencies}")
  message(WARNING "dependencies_error ${dependencies_error}")
  string( REGEX REPLACE "\n" ";" dependencies "${dependencies}")
  foreach(line ${dependencies})
    string(REGEX REPLACE ">*=[ \t]*((([0-9]+)[.])*[0-9]+)*[ \t]*," "," line ${line})
    string(REGEX REPLACE ">*=[ \t]*[@][^@]*[@][ \t]*," "," line ${line})
    string(REGEX REPLACE ">*=[ \t]*((([0-9]+)[.])*[0-9]+)*$" "" line ${line})
    string(REGEX REPLACE ">*=[ \t]*[@][^@]*[@]$" "" line ${line})
    string(REGEX REPLACE "," ";" line ${line})
    foreach(pkg_ ${line})
      string(STRIP "${pkg_}" pkg_)
      list(APPEND deps ${pkg_})
    endforeach()
  endforeach( )
  set( ${VAR} ${deps} PARENT_SCOPE )
endfunction( )

##
## @fn RPMInstallDependencies( SPEC_FILE VAR )
##
## @brief Installs build dependencies from given SPEC_FILE
##
## @details
## This function will attempt to install BuildRequires dependencies
## specified in the SPEC_FILE.
##
## @param[in] SPEC_FILE
##   RPM spec file; must have the extension .spec
## @param[out] VAR
##   name of variable to substitute with result
##
## @note
##   If there is no known way to install the dependencies,
##   a list of packages to be installed will be returned
##
## @see @ref RPMListDependencies
##
function( RPMInstallDependencies SPEC_FILE VAR )
  if( INSTALL )
    message( INFO "INFO: DEP: PROG_DNF: ${PROG_DNF}")
    message( INFO "INFO: DEP: PROG_YUM: ${PROG_YUM}")
    execute_process(
      COMMAND rpmspec --buildrequires -q "${SPEC_FILE}"
      OUTPUT_VARIABLE dependencies
      ERROR_VARIABLE dependencies_error
      )
    message(WARNING "INFO: DEP: Dependencies to install: ${dependencies}" )
    if ( PROG_DNF )
      execute_process(
        COMMAND dnf builddep -y "${SPEC_FILE}"
        OUTPUT_VARIABLE dependencies
        ERROR_VARIABLE dependencies_error
        )
    elseif( PROG_YUM )
      RPMInstallPackage( yum-utils )
      find_program( PROG_YUM_BUILDDEP yum-builddep )
      if ( PROG_YUM_BUILDDEP )
        message( INFO " COMMAND: ${PROG_YUM_BUILDDEP} --assumeyes --verbose --debuglevel=10 \"${SPEC_FILE}\"" )
        execute_process(
          COMMAND ${PROG_YUM_BUILDDEP} --assumeyes --verbose --debuglevel=10 "${SPEC_FILE}"
          OUTPUT_VARIABLE dependencies
          ERROR_VARIABLE dependencies_error
          )
      endif( )
    else( )
      RPMListDependencies( ${SPEC_FILE} dependencies )
    endif( )
    set( ${VAR} ${dependencies} PARENT_SCOPE )
  endif( )
endfunction( )

function( RPMBuildDependencies VAR )
  file( GLOB_RECURSE files "${TOP_DIR}/*.spec" "${TOP_DIR}/*.spec.in" )
  set( keepers )
  foreach( file ${files})
    if ( NOT file MATCHES /legacy/
        AND NOT file MATCHES "[/][.][^/]*$" )
      list( APPEND keepers ${file} )
    endif( )
  endforeach( )
  set( files ${keepers} )
  message( WARNING "files: ${files}" )
  unset(deps)
  foreach( SPEC_FILE ${files} )
    file( READ ${SPEC_FILE} dependencies )
    string( CONFIGURE "${dependencies}" dependencies @ONLY )
    # Remove any undefined substitution variables
    string( REGEX REPLACE '@[A-Za-z0-9_]*@' "_junk_" dependencies "${dependencies}" )
    message(WARNING "configured dependencies ${dependencies}")
    get_filename_component(SPEC_FILE_EXPANDED "${SPEC_FILE}" NAME)
    set( SPEC_FILE_EXPANDED "/tmp/${SPEC_FILE_EXPANDED}" )
    string(REGEX REPLACE "[.]in$" "" SPEC_FILE_EXPANDED "${SPEC_FILE_EXPANDED}" )
    file( WRITE ${SPEC_FILE_EXPANDED} "${dependencies}" )
    file( READ ${SPEC_FILE_EXPANDED} dependencies )
    message(WARNING "INFO: DEP: Contents of ${SPEC_FILE_EXPANDED}: ${dependencies}")
    message(WARNING "INFO: DEP: INSTALL: ${INSTALL}")
    if ( INSTALL )
      RPMInstallDependencies( ${SPEC_FILE_EXPANDED} deps )
    else( )
      RPMListDependencies( ${SPEC_FILE_EXPANDED} deps )
    endif( )
    file( REMOVE ${SPEC_FILE_EXPANDED} )
  endforeach( )
  if ( deps )
    list( SORT deps )
    list( REMOVE_DUPLICATES deps )
  endif( )
  message( WARNING "deps ${deps}" )
  set( ${VAR} ${deps} PARENT_SCOPE )
endfunction( )

set( VERBOSE True )
if( NOT PYTHON3_VERSION_NODOTS )
  if ( PYTHON3_VERSION )
    set( PYTHON3_VERSION_NODOTS ${PYTHON3_VERSION} )
    string( REPLACE "." "" PYTHON3_VERSION_NODOTS "${PYTHON3_VERSION_NODOTS}" )
    set( PYTHON3_VERSION_NODOTS "${PYTHON3_VERSION_NODOTS}" CACHE INTERNAL "" )
  endif( )
endif( )
if( NOT PYTHON2_VERSION_NODOTS )
  if ( PYTHON2_VERSION )
    set( PYTHON2_VERSION_NODOTS ${PYTHON2_VERSION} )
    string( REPLACE "." "" PYTHON2_VERSION_NODOTS "${PYTHON2_VERSION_NODOTS}" )
    set( PYTHON2_VERSION_NODOTS ${PYTHON2_VERSION_NODOTS} CACHE INTERNAL "" )
  endif( )
endif( )
if ( PACKAGER STREQUAL "rpm" )
  find_program( pkg_mgr NAMES yum )
  set( PACKAGE_MANAGER ${pkg_mgr} CACHE STRING "Package Manager used to install packages" )
  RPMBuildDependencies( depends )
elseif ( PACKAGER STREQUAL "deb" )
  find_program( pkg_mgr NAMES apt-get )
  set( PACKAGE_MANAGER ${pkg_mgr} CACHE STRING "Package Manager used to install packages" )
  DebBuildDependencies( depends )
else( )
  set( VERBOSE False )
endif( )
if ( VERBOSE )
  if ( DEFINED INCLUDE_PATTERN )
    if ( NOT "${INCLUDE_PATTERN}" MATCHES "^[\\^]")
      set(INCLUDE_PATTERN "^.*${INCLUDE_PATTERN}")
    endif( )
    if ( NOT "${INCLUDE_PATTERN}" MATCHES "[\\$]$")
      set(INCLUDE_PATTERN "${INCLUDE_PATTERN}.*$")
    endif( )
    foreach( dep ${depends} )
      if ( "${dep}" MATCHES "${INCLUDE_PATTERN}" )
        list(APPEND new_depends "${dep}" )
      endif( )
    endforeach( )
    set(depends ${new_depends})
  endif( )
  if ( NOT DEFINED INSTALL )
    set(INSTALL FALSE )
  endif( )
  exclude( depends )
  foreach( dep ${depends} )
    echo( ${dep} )
  endforeach( )
endif( )
