#include "socket/socket.hh"

#include <sys/select.h>

#include <array>
#include <atomic>
#include <algorithm>
#include <ctime>
#include <deque>
#include <list>
#include <mutex>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <thread>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include "checksum_crc32.hh"
#include "json_local.hpp"

using json = nlohmann::json;
namespace filesystem = boost::filesystem;

namespace detail
{
    int get_index()
    {
        static std::atomic<int> counter{0};
        return counter.fetch_add(1);
    }

    int get_uuid()
    {
        static std::atomic<int> counter{0};
        return counter.fetch_add(1);
    }
}

// std::make_unique didn't make it into C++11, so
// to allow this to work in a pre C++14 world, we
// provide a simple replacement.
//
// A make_unique<> for C++11.  Taken from
// "Effective Modern C++ by Scott Meyers (O'Reilly).
// Copyright 2015 Scott Meyers, 978-1-491-90399-5"
//
// Permission given in the book to reuse code segments.
//
// @tparam T The type of the object to be managed by the unique_ptr
// @tparam Ts The type of the arguments to T's constructor
// @param params The arguments to forward to the constructor
// @return a std::unique_ptr<T>
template < typename T, typename... Ts >
std::unique_ptr< T >
make_unique( Ts&&... params )
{
    return std::unique_ptr< T >( new T( std::forward< Ts >( params )... ) );
}

template < typename T >
typename T::value_type remove_front(T& t)
{
    typename T::value_type tmp{std::move(t.front())};
    t.pop_front();
    return tmp;
}

struct DataBlob {
    static const int MAX_SIZE = 4096;
    std::array<char, MAX_SIZE> data = {};
    int used = 0;
};

using CommandType = std::string;
using BlobType = std::unique_ptr<DataBlob>;
using BlobList = std::vector<BlobType>;
using BlobPair = std::pair<BlobType, BlobType>;

BlobType clone(DataBlob* input)
{
    BlobType result = make_unique<DataBlob>(*input);
    return result;
}

using DataScript = std::pair<CommandType, std::vector<char>>;

bool is_binary(const DataScript& script)
{
    auto result = std::find_if(script.second.begin(), script.second.end(), [](char ch)
    {
       return ch == 0 || ch > 127;
    });
    return result != script.second.end();
}

char to_hex_nibble(int val)
{
    val &= 0x0f;
    return static_cast<char>(val < 10 ? '0' + val : 'a' + (val-10));
}

std::string to_hex(const DataScript& script)
{
    std::vector<char> results(script.second.size()*2);
    char* cur = results.data();
    for (const auto& entry:script.second)
    {
        *cur = to_hex_nibble((static_cast<int>(entry)&0x0f0) >> 4);
        ++cur;
        *cur = to_hex_nibble(static_cast<int>(entry)&0x0f);
        ++cur;
    }
    return std::string(results.data(), results.size());
}

struct Transaction
{
    CommandType command;
    BlobList blobs;

    bool empty() const
    {
        return command.empty() && blobs.empty();
    }

    void clear()
    {
        command.clear();
        blobs.clear();
    }

    size_t blob_size() const
    {
        return std::accumulate(blobs.begin(),
                blobs.end(),
                static_cast<size_t>(0),
                [](size_t cur, const BlobType& blob) -> size_t {
            return cur + static_cast<size_t>(blob->used);
        });
    }

    DataScript to_script() const
    {
        size_t data_size = blob_size();
        std::vector<char> buffer(data_size);
        char* cur = buffer.data();
        for (const auto& entry:blobs)
        {
            auto begin = entry->data.data();
            auto end = begin + entry->used;
            std::copy(begin, end, cur);
            cur += entry->used;
        }
        return std::make_pair(command, std::move(buffer));
    }
};

json to_json(const Transaction& t)
{
    const int max_string = 162;
    json result{
        {"command", t.command },
        {"type", "script"}
    };
    DataScript ds { t.to_script() };
    if (ds.second.size() <= max_string)
    {
        if (is_binary(ds))
        {
            result["response_hex"] = to_hex(ds);
        }
        else
        {
            result["response"] = std::string(ds.second.data(), ds.second.size());
        }
    }
    else
    {
        std::time_t now = std::time(nullptr);
        std::tm* now_tm = std::gmtime(&now);

        checksum_crc32 crc;
        crc.add(ds.second.data(), static_cast<std::uint32_t>(ds.second.size()));
        std::ostringstream os;
        os << (now_tm->tm_year + 1900) << "-" << (now_tm->tm_mon + 1) << "-" << now_tm->tm_mday << ".id" << detail::get_uuid() << "_c" << crc.result();

        std::string blob_name = os.str();
        if (!filesystem::exists("blobs"))
        {
            filesystem::create_directory("blobs");
        }
        filesystem::path blob_path = filesystem::path{"blobs"} / blob_name;
        std::cout << "Writing blob to " << blob_path.string() << "\n";
        filesystem::ofstream blob_stream(blob_path, std::ios_base::out | std::ios_base::binary);
        blob_stream.write(ds.second.data(), ds.second.size());

        result["response_blob"] = blob_name;
    }
    return result;
}

using TransactionList = std::vector<Transaction>;

class ClientRecorder {
public:
    ClientRecorder() = default;
    ~ClientRecorder() = default;

    void client_read(BlobType blob)
    {
        set_command(std::string(blob->data.data(), static_cast<std::string::size_type>(blob->used)));
    }

    void client_write(BlobType blob)
    {
        receive_response(std::move(blob));
    }

    void server_read(BlobType blob) {}
    void server_write(BlobType blob) {}

    void save_session()
    {
        std::vector<json> scripts;
        scripts.reserve(transactions_.size());

        std::transform(transactions_.begin(), transactions_.end(), std::back_inserter(scripts), to_json);

        const std::string base_path = "proxy-output";
        filesystem::ofstream out;
        auto path = open_next(out, base_path);
        std::cout << "Saving session as " << path.string() << "\n";
        json j {
            {"recorded_script", {
                { "type", "compound" },
                { "scripts", scripts }
            }}
        };

        out << std::setw(4) << j;
    }
private:
    std::string permute_path(const std::string& base, int count)
    {
        std::ostringstream os;
        os << base << "-" << count << ".json";
        return os.str();
    }

    filesystem::path open_next(filesystem::ofstream& out, const std::string& base_path)
    {
        filesystem::path output_path;
        while (!out.is_open())
        {
            output_path = permute_path(base_path, detail::get_index());
            // avoid the race between existance checks and opening the file
            std::lock_guard<std::mutex> l_(file_lock_);
            if (!filesystem::exists(output_path))
            {
                out.open(output_path, std::ios_base::out);
            }
        }
        return output_path;
    }

    void set_command(const CommandType& cmd)
    {
        if (!cmd.empty() && cmd.find_first_not_of('\n') == std::string::npos)
        {
            cur_transaction_.command.append(cmd);
            std::cout << "Expanding command " << cur_transaction_.command << "\n";
            return;
        }
        push_back();
        cur_transaction_.command = cmd;
    }

    void push_back()
    {
        if (!cur_transaction_.empty())
        {
            Transaction tmp;
            std::swap(tmp, cur_transaction_);
            transactions_.emplace_back(std::move(tmp));
        }
        cur_transaction_.clear();
    }

    void receive_response(BlobType blob)
    {
        cur_transaction_.blobs.emplace_back(std::move(blob));
    }

    Transaction cur_transaction_;
    TransactionList transactions_;
    std::mutex file_lock_{};
};

BlobType read_block(nds_impl::Socket::FullSocket& s)
{
    BlobType block = make_unique<DataBlob>();
    auto begin = block->data.data();
    auto end = s.read_available(begin, begin + block->data.size());
    block->used = static_cast<int>(end - begin);
    if (block->used == 0)
    {
        block.reset(nullptr);
    }
    return block;
}

BlobPair write_block(nds_impl::Socket::FullSocket& s, BlobType block)
{
    auto begin = block->data.data();
    auto end = begin + block->used;
    s.write_all(begin, end);
    return std::make_pair<BlobType, BlobType>(std::move(block), nullptr);
}

void run_proxy(nds_impl::Socket::FullSocket client, std::atomic<bool>* finish_flag)
try
{
    class MarkDone {
    public:
        explicit MarkDone(std::atomic<bool>* f): flag_{f} {}
        MarkDone(const MarkDone& other) = delete;
        MarkDone(MarkDone&& other) = delete;
        MarkDone& operator=(const MarkDone& other) = delete;
        MarkDone operator=(MarkDone&& other) = delete;
        ~MarkDone()
        {
            flag_->store(true);
        }
    private:
        std::atomic<bool>* flag_;
    };
    MarkDone done_{finish_flag};
    ClientRecorder recorder;

    try {
        using BlobDeque = std::deque<BlobType>;

        nds_impl::Socket::FullSocket server;
        server.connect("localhost:8088");
        std::cerr << "Connected to " << "localhost:8088\n";

        fd_set read_set;
        fd_set write_set;
        int nfds = 0;

        auto client_handle = client.get();
        auto server_handle = server.get();

        auto set_proper_queue = [&read_set, &write_set, &nfds](nds_impl::Socket::FullSocket::socket_type handle,
                                                               const BlobDeque &buffers) {
            if (buffers.empty()) {
                FD_SET(handle, &read_set);
            } else {
                FD_SET(handle, &write_set);
            }
            if (handle > nfds) {
                nfds = handle;
            }
        };

        BlobDeque cbuf;
        BlobDeque sbuf;

        bool closed = false;
        while (!closed) {
            FD_ZERO(&read_set);
            FD_ZERO(&write_set);

            nfds = 0;
            set_proper_queue(client_handle, cbuf);
            set_proper_queue(server_handle, sbuf);

            auto count = select(nfds + 1, &read_set, &write_set, nullptr, nullptr);

            if (count < 0) {
                if (errno == EINTR) {
                    continue;
                }
                std::cerr << "aborting connection due to error on select\n";
                break;
            }

            if (FD_ISSET(client_handle, &read_set)) {
                auto tmp = read_block(client);
                if (tmp) {
                    std::cout << "Received " << tmp->used << " bytes from the client '" << tmp->data.data() << "'\n";
                    auto tmp_copy = clone(tmp.get());
                    recorder.client_read(std::move(tmp));
                    sbuf.emplace_back(std::move(tmp_copy));
                } else {
                    std::cout << "Client closed\n";
                    closed = true;
                }
            }
            if (FD_ISSET(server_handle, &read_set)) {
                auto tmp = read_block(server);
                if (tmp) {
                    std::cout << ". ";
                    auto tmp_copy = clone(tmp.get());
                    recorder.server_read(std::move(tmp));
                    cbuf.emplace_back(std::move(tmp_copy));
                } else {
                    std::cout << "Server closed\n";
                    closed = true;
                }

            }
            if (FD_ISSET(client_handle, &write_set) && !cbuf.empty()) {
                auto blocks = write_block(client, remove_front(cbuf));
                if (blocks.second) {
                    cbuf.emplace_front(std::move(blocks.second));
                }
                recorder.client_write(std::move(blocks.first));
            }
            if (FD_ISSET(server_handle, &write_set) && !sbuf.empty()) {
                auto blocks = write_block(server, remove_front(sbuf));
                if (blocks.second) {
                    sbuf.emplace_front(std::move(blocks.second));
                }
                recorder.server_write(std::move(blocks.first));
            }
        }
    }
    catch (std::runtime_error& err)
    {
        std::cout << "Closing connections: " << err.what() << "\n";
    }
    recorder.save_session();
}
catch(...)
{
}

int main(int argc, char** argv)
{
    struct thread_info {
        thread_info(): thread{}, finished{false} {}
        ~thread_info()
        {
            if (finished && thread.joinable())
            {
                thread.join();
            }
        }

        std::thread thread;
        std::atomic<bool> finished;
    };
    std::list<std::unique_ptr<thread_info>> threads;

    nds_impl::Socket::FullSocket s;
    s.bind("localhost:39199");
    s.listen();
    while (true)
    {
        fd_set read_set;
        FD_ZERO(&read_set);
        FD_SET(s.get(), &read_set);
        struct timeval tv{};
        tv.tv_sec = 0;
        tv.tv_usec = 250000;
        if (select(s.get() + 1, &read_set, nullptr, nullptr, &tv) > 0)
        {
            threads.emplace_back(make_unique<thread_info>());
            threads.back()->thread = std::thread(run_proxy, s.accept(), &threads.back()->finished);
        }
        else
        {
            auto end = std::remove_if(threads.begin(), threads.end(),
                                      [](const std::unique_ptr<thread_info> &tinfo) -> bool {
                                          return tinfo->finished.load();
                                      });
            threads.erase(end, threads.end());
        }
    }

    return 0;
}