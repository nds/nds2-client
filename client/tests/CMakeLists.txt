# -*- mode: cmake; coding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=cmake:et:sw=4:ts=4:sts=4
#------------------------------------------------------------------------
#------------------------------------------------------------------------
cmake_minimum_required( VERSION 3.0 FATAL_ERROR )

include(FindThreads)

set( TEST_SCRIPTS
  mockup_server.py
  nds2_proxy.py
  nds_tester.py
  null_server.py
  record_proxy.py
  record_wrapper.py
  replay.py
  replay_wrapper.py
  sock_test.py
  unit_test.py
  )

configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/nds-client-tester.pc.in
                ${CMAKE_CURRENT_BINARY_DIR}/nds-client-tester.pc @ONLY)

if(NOT WIN32)
    find_package(Boost COMPONENTS filesystem system)
    if (${Boost_FOUND})
        add_executable(simple_proxy
                simple_proxy.cc
                checksum_crc32.cc
                checksum_crc32.hh)
        target_include_directories(simple_proxy PRIVATE ${Boost_INCLUDE_DIRS})
        # this is horribly wrong, but at least on Debian Jessie
        # the Boost_LIBRARIES variable is empty
        # and the Boost_<component>_LIBRARY variable is empty as well
        # so this works, at least on Linux.
        target_link_libraries(simple_proxy PRIVATE
                nds::impl
                ${Boost_FILESYSTEM_LIBRARY}
                ${Boost_SYSTEM_LIBRARY}
                ${CMAKE_THREAD_LIBS_INIT})
        target_requires_cpp11( simple_proxy PUBLIC )
    endif(${Boost_FOUND})
endif()

include( CMakePackageConfigHelpers )

configure_package_config_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/NDSTesterConfig.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/NDSTesterConfig.cmake
    INSTALL_DESTINATION ${CMAKE_INSTALL_BINDIR}
    PATH_VARS
      CMAKE_INSTALL_INCLUDEDIR
      CMAKE_INSTALL_LIBDIR
      CMAKE_INSTALL_LIBEXECDIR
      )
write_basic_package_version_file(
    ${CMAKE_CURRENT_BINARY_DIR}/NDSTesterConfigVersion.cmake
    VERSION ${PROJECT_VERSION}
    COMPATIBILITY AnyNewerVersion
    )
install(
    FILES
      ${CMAKE_CURRENT_BINARY_DIR}/NDSTesterConfig.cmake
      ${CMAKE_CURRENT_BINARY_DIR}/NDSTesterConfigVersion.cmake
    DESTINATION ${_nds_client_install_cmake_dir}
    COMPONENT Development
    )

install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/nds-client-tester.pc
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig
  COMPONENT Development
)

install(
  PROGRAMS ${TEST_SCRIPTS}
  DESTINATION ${CMAKE_INSTALL_LIBEXECDIR}/nds2-client
  COMPONENT Development
  )

install(
  DIRECTORY
    ${CMAKE_CURRENT_SOURCE_DIR}/modules
    ${CMAKE_CURRENT_SOURCE_DIR}/sasl
    ${CMAKE_CURRENT_SOURCE_DIR}/test-json
    ${CMAKE_CURRENT_SOURCE_DIR}/test-scripts
  DESTINATION ${CMAKE_INSTALL_LIBEXECDIR}/nds2-client
  COMPONENT Development
  )

