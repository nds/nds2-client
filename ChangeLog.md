nds2-client-suite NEXT
-----------------------------
  * Removed automatic running of CI/CD pipelines for Debian Stretch
  * Added CI pipeline for Rocky Linux 9
  * Removed dependencies on python's distutils module which has been depricated
