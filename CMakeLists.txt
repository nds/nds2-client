#========================================================================
# This is the master build script controlling the building of the
#   NDS Client
#
# Development define:
#  Define COMBINED_BUILD in a development setting to add the C++ client and SWIG
#  as sub-directories and not external projects.  This DOES NOT allow building packages,
#  but can speed up testing and building during development.  It also allows some
#  cmake based tooling to get a better view of the combined project.
#
#  cmake -DCOMBINED_BUILD=1 <SourceDir>
#
# DEFINES:
#   CMAKE_INSTALL_PREFIX  - Installation prefix
#   ENABLE_SWIG_OCTAVE    - Conditionally build Octave bindings
#   MATLAB_NO_NODISPLAY   - Run the matlab tests w/o using the -nodisplay option
#   PROG_MATLAB           - Full path to MATLAB executable
#   PYTHON2_VERSION       - Version of python 2 interpreter to use
#   PYTHON3_VERSION       - Version of python 3 interpreter to use
#   REPLAY_BLOB_CACHE_DIR - Directory containing replay blobs for testing
#   TEST_PACKAGER          - <rpm|macports|deb> test packaging of system
#
# ** C O N F I G U R A T I O N **
# > cmake -DPYTHON2_VERSION=2.7 -DPYTHON3_VERSION=3.4 <SourceDir>
#
# ** B U I L D **
# > cmake --build . -- VERBOSE=0
#========================================================================

cmake_minimum_required(VERSION 3.2)
project(nds2-client_superbuild)

if (COMBINED_BUILD)
  message("COMBINED_BUILD enabled, this is a development build mode, not to be used to build production releases")
  include( CTest )

  add_subdirectory(client)
  add_subdirectory(swig)
else()

include(ExternalProject)
set(installDir ${CMAKE_CURRENT_BINARY_DIR}/install)

set( ENV{PKG_CONFIG_PATH} ${installDir}/lib/pkgconfig:$ENV{PKG_CONFIG_PATH} )
set( TEST_PACKAGER IGNORE CACHE STRING "Specify the OS native packager (ex: rpm, macports, deb)" )

if ( TEST_PACKAGER )
  add_custom_target( ${TEST_PACKAGER}
    COMMAND ${CMAKE_COMMAND} -E echo "Completed: ${TEST_PACKAGER}" )
endif( )

function( BuildCMakePackage )
  set( options )
  set( oneValueArgs PACKAGE_NAME )
  set( multiValueArgs DEPENDS )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_PACKAGE_DIR )
    set( ARG_PACKAGE_DIR ${ARG_PACKAGE_NAME} )
  endif( )

  set(_PackageName ${ARG_PACKAGE_NAME} )
  set(_PackageDir ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_DIR})

  set( USER_DEFINES "-DCMAKE_INSTALL_PREFIX:PATH=${installDir}" "-DCMAKE_PREFIX_PATH:PATH=${installDir}" )
  if ( PYTHON2_VERSION )
    list( APPEND USER_DEFINES "-DPYTHON2_VERSION:STRING=${PYTHON2_VERSION}" )
  endif( )
  if ( PYTHON3_VERSION )
    list( APPEND USER_DEFINES "-DPYTHON3_VERSION:STRING=${PYTHON3_VERSION}" )
  endif( )
  if ( PROG_MATLAB )
    list( APPEND USER_DEFINES -DPROG_MATLAB:FILEPATH=${PROG_MATLAB} )
  endif ( )
  if ( DEFINED SWIG_EXECUTABLE )
    list( APPEND USER_DEFINES -DSWIG_EXECUTABLE:FILEPATH=${SWIG_EXECUTABLE} )
  endif( )
  foreach(SWIG_LANGUAGE JAVA MATLAB OCTAVE PYTHON2 PYTHON3)
    if ( DEFINED ENABLE_SWIG_${SWIG_LANGUAGE} )
      list( APPEND USER_DEFINES -DENABLE_SWIG_${SWIG_LANGUAGE}:BOOL=${ENABLE_SWIG_${SWIG_LANGUAGE}} )
    endif( )
  endforeach()
  if ( REPLAY_BLOB_CACHE_DIR )
    list( APPEND USER_DEFINES -DREPLAY_BLOB_CACHE_DIR:PATH=${REPLAY_BLOB_CACHE_DIR} )
  endif( )

  if ( ARG_DEPENDS )
    list( INSERT ARG_DEPENDS 0 DEPENDS)
  endif( )
  message(STATUS "USER_DEFINES: ${USER_DEFINES}" )
  ExternalProject_Add( ${_PackageName}
    SOURCE_DIR ${_PackageDir}
    TEST_BEFORE_INSTALL TRUE
    INSTALL_DIR ${installDir}
    CMAKE_ARGS ${USER_DEFINES}
    ${ARG_DEPENDS}
    )
  #----------------------------------------------------------------------
  TestPackaging( PACKAGE_NAME ${_PackageName} ${ARG_DEPENDS} )
endfunction( )

function( TestPackaging )
  if ( TEST_PACKAGER )
    set( options )
    set( oneValueArgs PACKAGE_NAME )
    set( multiValueArgs DEPENDS CONFIGURE_OPTIONS )
    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    set( pkgr ${TEST_PACKAGER} )
    unset( pkg_depends )
    foreach( d ${ARG_DEPENDS} )
      list( APPEND pkg_depends ${d}-${pkgr} )
    endforeach( )
    unset( pkg_cmd )
    unset( post_pkg_cmd )
    if ( pkgr STREQUAL rpm )
      # -----------------------------------------------------------------
      #   RedHat
      # -----------------------------------------------------------------
      RPMBuildDependencies(
        deps
        TOP_DIR "${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_NAME}"
        )
      set( pkg_cmd yum install -y )
      set( post_pkg_cmd /bin/sh -c "yum --nogpgcheck localinstall -y /root/rpmbuild/RPMS/*/*ldas-tools-*.rpm" )
      set( setup ${pkg_cmd} ${deps} )
    elseif ( pkgr STREQUAL deb )
      # -----------------------------------------------------------------
      #   Debian
      # -----------------------------------------------------------------
      file( GLOB_RECURSE SPEC_FILE ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_NAME}/config/control* )
      if ( NOT SPEC_FILE )
        file( GLOB_RECURSE SPEC_FILE  ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_NAME}/debian/control )
      endif( )
      DebBuildDependencies( deps ${SPEC_FILE} )
      string( REGEX REPLACE ";[^;]*ldas-tools-[^;]*;" ";" deps "${deps}" )
      string( REGEX REPLACE "^[^;]*ldas-tools-[^;]*;" "" deps "${deps}" )
      string( REGEX REPLACE ";[^;]*ldas-tools-[^;]*$" "" deps "${deps}" )
      set( pkg_cmd_wait_on_lock /bin/bash ${CMAKE_SOURCE_DIR}/apt-get-lock-guard  )
      set( pkg_cmd_update apt-get update)
      set( pkg_cmd_fix_broken apt-get -y --fix-broken install)
      set( pkg_cmd_install apt-get -y install)
      set( post_pkg_cmd
        ${pkg_cmd_wait_on_lock}
        COMMAND /bin/sh -c "dpkg -i `find ${CMAKE_BINARY_DIR}/${ARG_PACKAGE_NAME}-prefix/src/${ARG_PACKAGE_NAME}-build -name \\*.deb` || true"
        COMMAND ${pkg_cmd_wait_on_lock}
        COMMAND ${pkg_cmd_fix_broken} )
      set( setup ${pkg_cmd_wait_on_lock}
        COMMAND ${pkg_cmd_update}
        COMMAND ${pkg_cmd_wait_on_lock}
        COMMAND ${pkg_cmd_install} ${deps}
        COMMAND rm -rf ${installDir}
        )
    elseif ( pkgr STREQUAL macports )
      # -----------------------------------------------------------------
      #   MacPorts
      # -----------------------------------------------------------------
      # file( GLOB_RECURSE SPEC_FILE  ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_NAME}/*/Portfile.in )
      #if ( NOT SPEC_FILE )
      #  file( GLOB_RECURSE SPEC_FILE  ${CMAKE_SOURCE_DIR}/${ARG_PACKAGE_NAME}/debian/control )
      #endif( )
      #DebBuildDependencies( deps ${SPEC_FILE} )
      set( post_pkg_cmd ${CMAKE_COMMAND} -E echo "Installation done as part of building step" )
      set( setup ${CMAKE_COMMAND} -E echo "No setup for macports" )
    endif ( )
    ExternalProject_Add_Step(
      ${ARG_PACKAGE_NAME} ${pkgr}-setup
      COMMENT "Installing project dependencies"
      COMMAND ${setup}
      ALWAYS 0
      DEPENDEES patch
      DEPENDERS configure
      )
    ExternalProject_Add_Step(
      ${ARG_PACKAGE_NAME} ${pkgr}
      COMMENT "Building project packages"
      COMMAND make ${MAKE_PARALLEL} ${pkgr}
      WORKING_DIRECTORY <BINARY_DIR>
      ALWAYS 0
      DEPENDEES install
      )
    ExternalProject_Add_Step(
      ${ARG_PACKAGE_NAME} ${pkgr}-post
      COMMENT "Installing project packages"
      COMMAND ${post_pkg_cmd}
      WORKING_DIRECTORY <BINARY_DIR>
      ALWAYS 0
      DEPENDEES ${pkgr}
      )
    add_dependencies(
      ${pkgr}
      ${ARG_PACKAGE_NAME}-${pkgr}-setup
      ${ARG_PACKAGE_NAME}-${pkgr}
      ${ARG_PACKAGE_NAME}-${pkgr}-post
      )
    ExternalProject_Add_StepTargets( ${ARG_PACKAGE_NAME} ${pkgr} ${pkgr}-setup ${pkgr}-post )
    if ( pkg_depends )
      add_dependencies( ${ARG_PACKAGE_NAME}-${pkgr} ${pkg_depends} )
    endif( )
  endif( )
endfunction( )

BuildCMakePackage( PACKAGE_NAME client )
BuildCMakePackage( PACKAGE_NAME swig DEPENDS client )
endif()