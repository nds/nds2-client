/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */

%{

#include "nds_channel.hh"
#include "nds_swig.hh"

  struct chan_req_t;

  using NDS::channel;
  using NDS_swig::channels_type;
%}

%shared_ptr( channel );
%shared_ptr( std::vector< std::shared_ptr< epoch > > );

%nds_doc_class_seperator( )

%nds_doc_class_begin(channel)
%nds_doc_brief("Represents a LIGO data channel")
%nds_doc_details("A channel encasulates the meta data of LIGO data.  It has a name and parameters.")
%nds_doc_class_end()
class channel
{
public:
  %feature("autodoc","channel types") channel_type;
  typedef enum {
    CHANNEL_TYPE_UNKNOWN,
    CHANNEL_TYPE_ONLINE,
    CHANNEL_TYPE_RAW,
    CHANNEL_TYPE_RDS,
    CHANNEL_TYPE_STREND,
    CHANNEL_TYPE_MTREND,
    CHANNEL_TYPE_TEST_POINT,
    CHANNEL_TYPE_STATIC
  } channel_type;

  %feature("autodoc","data types") data_type;
  typedef enum {
    DATA_TYPE_UNKNOWN,
    DATA_TYPE_INT16,
    DATA_TYPE_INT32,
    DATA_TYPE_INT64,
    DATA_TYPE_FLOAT32,
    DATA_TYPE_FLOAT64,
    DATA_TYPE_COMPLEX32,
    DATA_TYPE_UINT32
  } data_type;
  
  typedef double sample_rate_type;
  typedef float signal_gain_type;
  typedef float signal_slope_type;
  typedef float signal_offset_type;
  typedef size_t      size_type;
  
  typedef std::vector< std::string > channel_names_type;

  static const channel_type DEFAULT_CHANNEL_MASK;
  static const data_type DEFAULT_DATA_MASK;

  static const sample_rate_type MIN_SAMPLE_RATE;
  static const sample_rate_type MAX_SAMPLE_RATE;

  %nds_doc_method_begin(channel,Name,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The name of the channel")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  channel( const channel& Source );

  const std::string& Name( ) const;

  %nds_doc_method_begin(channel,Name,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The name of the channel formated with rate and type information")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  std::string NameLong( );

  %nds_doc_method_begin(channel,Type,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The type of the channel")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  channel_type Type( ) const;

  %nds_doc_method_begin(channel,DataType,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The data type of the channel")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  data_type DataType( ) const;
  
  %nds_doc_method_begin(channel,DataTypeSize,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The number of bytes taken up by a single sample point of the channel")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  size_type DataTypeSize( ) const;

  %nds_doc_method_begin(channel,SampleRate,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The sample rate of the channel.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  sample_rate_type SampleRate( ) const;

  %nds_doc_method_begin(channel,Gain,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The gain of the channel.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  signal_gain_type Gain( ) const;

  %nds_doc_method_begin(channel,Slope,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The slope of the channel.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  signal_slope_type Slope( ) const;

  %nds_doc_method_begin(channel,Offset,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The offset of the channel.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  signal_offset_type Offset( ) const;

  %nds_doc_method_begin(channel,Units,)
  %nds_doc_body_begin( )
  %nds_doc_returns("The units of the channel.")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  const std::string& Units( ) const;

  %nds_doc_method_begin(channel,IsSecondTrend,)
  %nds_doc_body_begin( )
  %nds_doc_brief("Check the channel name to see if it is a second trend.")
  // %nds_doc_param(Name,%nds_type_string,"Name A channel name, as a string.")
  %nds_doc_returns("true if Name ends in 's-trend' else false")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  static bool IsSecondTrend( const std::string& Name );

  %nds_doc_method_begin(channel,IsMinuteTrend,)
  %nds_doc_body_begin( )
  %nds_doc_brief("Check the channel name to see if it is a minute trend.")
  //%nds_doc_param(Name,%nds_type_string,"Name A channel name, as a string.")
  %nds_doc_returns("true if Name ends in 'm-trend' else false")
  %nds_doc_body_end( )
  %nds_doc_method_end( )
  static bool IsMinuteTrend( const std::string& Name );


#if !defined(SWIGJAVA)
  // gwpy needs the channel_type_to_string
  %extend {
    static const std::string& channel_type_to_string(channel_type ctype)
    {
      return channel_type_to_string( ctype );
    }

    static const std::string& data_type_to_string(data_type dtype)
    {
      return data_type_to_string( dtype );
    }
  }
#endif /* ! defined(SWIGJAVA) */

};

%nds_doc_class_seperator( )

#if ! defined( %nds_channels_type )
%define %nds_channels_type( )
%shared_ptr( channels_type );
%shared_ptr( std::vector< std::shared_ptr< channel > > );
%template(vectorChannels) std::vector< std::shared_ptr< channel > >;
class channels_type
  : public std::vector< std::shared_ptr< channel > >
{
public:
};
%enddef
#endif /* ! defined( %nds_channels_type ) */

%nds_channels_type( );

%template(channel_names_type) std::vector< std::string >;
