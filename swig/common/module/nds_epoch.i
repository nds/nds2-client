/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */

%include "std_string.i"
%include "std_vector.i"

%{
#include "nds_epoch.hh"
#include "nds_swig.hh"

using NDS::epoch;
using NDS_swig::epochs_type;
%}

%shared_ptr( epoch );
%shared_ptr( std::vector< std::shared_ptr< epoch > > );
%shared_ptr( epochs_type );
%template(vectorEpochs) std::vector< std::shared_ptr< epoch > >;

%nds_doc_class_seperator( )

%nds_doc_class_begin(epoch)
%nds_doc_brief("A timespan, either named or explicity delimited")
%nds_doc_details("An epoch is used to limit the scope of requests against an NDS2 server."
"The epoch may be named or have explicit [start, stop) ranges.")
%nds_doc_class_end( )
class epoch
{
public:
    std::string name;
    buffer::gps_second_type gps_start;
    buffer::gps_second_type gps_stop;

    epoch();
    epoch(const epoch &other);
    epoch (const std::string &name, buffer::gps_second_type start, buffer::gps_second_type stop);
};

%nds_doc_class_seperator( )

class epochs_type
  : public std::vector<std::shared_ptr<epoch> >
{
public:
};
