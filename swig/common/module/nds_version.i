/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */

%{
#include "nds_swig_version.hh"

//using namespace NDS;
using NDS_swig::version;
%}

std::string version();