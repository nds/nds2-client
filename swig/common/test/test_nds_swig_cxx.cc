//
// Created by jonathan.hanks on 4/23/18.
//

#include "nds.hh"
#include "nds_swig.hh"

int
main( int argc, char** argv )
{
    NDS_swig::connection< NDS::connection, NDS::connection > swig_conn( "" );
    auto ver = swig_conn.get_port( );
    return 0;
}