#include <stdlib.h>

#include <string>
#include <vector>
#include <map>

#include "nds_parameter_block.hh"
#include "nds_param_str.h"
#include "nds_connection.hh"

#include "nds_testing.hh"

#ifdef _WIN32

int
setenv( const char* name, const char* value, int overwrite )
{
    std::ostringstream os;
    os << name << "=" << value;
    std::string env = os.str( );
    _putenv( env.c_str( ) );
    return 0;
}

int
unsetenv( const char* name )
{
    return setenv( name, "", 1 );
}

#endif

void
clear_env( )
{
    std::string                prefix = NDS_PARAM_ENV_PREFIX;
    std::vector< std::string > var_list{
        NDS_PARAM_ALLOW_DATA_ON_TAPE, NDS_PARAM_GAP_HANDLER,
        NDS_PARAM_ITERATE_GAPS,       NDS_CYCLE_NDS1_AFTER_N_COMMANDS,
        NDS_PARAM_HOSTNAME,           NDS_PARAM_PORT,
        NDS_PARAM_PROTOCOL_VERSION,
    };
    for ( auto& entry : var_list )
    {
        auto full_name = prefix + entry;
        NDS_ASSERT( unsetenv( full_name.c_str( ) ) == 0 );
    }
    NDS_ASSERT( unsetenv( "NDSSERVER" ) == 0 );
}

void
setup_env( const std::map< std::string, std::pair< std::string, std::string > >&
               settings )
{
    const std::string prefix{ NDS_PARAM_ENV_PREFIX };

    clear_env( );
    for ( const auto& entry : settings )
    {
        std::string key( std::string( prefix ) + std::string( entry.first ) );
        if ( entry.first == "NDSSERVER" )
        {
            key = "NDSSERVER";
        }
        setenv( key.c_str( ), entry.second.first.c_str( ), 1 );
    }
}

void
test_parameters( )
{

    {
        // just do defaults
        clear_env( );
        NDS::parameters params;
        NDS_ASSERT( params.host( ) == "localhost" );
        NDS_ASSERT( params.port( ) == NDS::connection::DEFAULT_PORT );
        NDS_ASSERT( params.protocol( ) == NDS::connection::PROTOCOL_TRY );
        NDS_ASSERT( params.get( NDS_PARAM_ALLOW_DATA_ON_TAPE ) == "false" );
    }

    // use a non-default host/port/proto
    {
        std::map< std::string, std::pair< std::string, std::string > > vals{
            { NDS_PARAM_HOSTNAME,
              { "nds-server.example", "nds-server.example" } },
            { NDS_PARAM_PORT, { "8088", "8088" } },
            { NDS_PARAM_PROTOCOL_VERSION, { "Two", "2" } },
            { NDS_PARAM_ALLOW_DATA_ON_TAPE, { "1", "true" } },
            { NDS_PARAM_GAP_HANDLER,
              { "STATIC_HANDLER_ZERO", "STATIC_HANDLER_ZERO" } },
            { NDS_PARAM_GAP_HANDLER, { "True", "true" } },
            { NDS_CYCLE_NDS1_AFTER_N_COMMANDS, { "15", "15" } }
        };
        setup_env( vals );
        // setup from the enviornment
        NDS::parameters params;
        for ( const auto& entry : vals )
        {
            std::string value = params.get( entry.first );
            NDS_ASSERT( value == entry.second.second );
        }
    }

    // mix in the NDSSERVER variable
    {
        std::map< std::string, std::pair< std::string, std::string > > vals{
            { NDS_PARAM_ALLOW_DATA_ON_TAPE, { "1", "true" } },
            { NDS_PARAM_GAP_HANDLER,
              { "STATIC_HANDLER_ZERO", "STATIC_HANDLER_ZERO" } },
            { NDS_PARAM_GAP_HANDLER, { "True", "true" } },
            { NDS_CYCLE_NDS1_AFTER_N_COMMANDS, { "15", "15" } }
        };
        setup_env( vals );
        ::setenv( "NDSSERVER", "nds-server.example:8088", 1 );
        // setup from the enviornment
        NDS::parameters params;
        for ( const auto& entry : vals )
        {
            std::string value = params.get( entry.first );
            NDS_ASSERT( value == entry.second.second );
        }
        NDS_ASSERT( params.get( std::string{ NDS_PARAM_HOSTNAME } ) ==
                    "nds-server.example" );
        NDS_ASSERT( params.get( std::string{ NDS_PARAM_PORT } ) == "8088" );
        NDS_ASSERT( params.get( std::string{ NDS_PARAM_PROTOCOL_VERSION } ) ==
                    "try" );
    }

    // test NDS2_CLIENT + NDSSERVER for overrides, the NDS2_client should
    // override
    {
        std::map< std::string, std::pair< std::string, std::string > > vals{
            { NDS_PARAM_HOSTNAME,
              { "nds-server1.example", "nds-server1.example" } },
            { NDS_PARAM_PORT, { "8088", "8088" } },
            { NDS_PARAM_PROTOCOL_VERSION, { "Two", "2" } },
            { NDS_PARAM_ALLOW_DATA_ON_TAPE, { "1", "true" } },
            { NDS_PARAM_GAP_HANDLER,
              { "STATIC_HANDLER_ZERO", "STATIC_HANDLER_ZERO" } },
            { NDS_PARAM_GAP_HANDLER, { "True", "true" } },
            { NDS_CYCLE_NDS1_AFTER_N_COMMANDS, { "15", "15" } }
        };
        setup_env( vals );
        ::setenv( "NDSSERVER", "nds-server2.example:31201", 1 );
        // setup from the enviornment
        NDS::parameters params;
        for ( const auto& entry : vals )
        {
            std::string value = params.get( entry.first );
            NDS_ASSERT( value == entry.second.second );
        }
    }

    // test NDS2_CLIENT + NDSSERVER for overrides, the NDS2_client should
    // provide defaults
    {
        std::map< std::string, std::pair< std::string, std::string > > vals{
            { NDS_PARAM_PORT, { "8088", "31201" } }, // because hostname is not
            // present this defaults
            // not overrides
            { NDS_PARAM_PROTOCOL_VERSION, { "Two", "2" } },
            { NDS_PARAM_ALLOW_DATA_ON_TAPE, { "1", "true" } },
            { NDS_PARAM_GAP_HANDLER,
              { "STATIC_HANDLER_ZERO", "STATIC_HANDLER_ZERO" } },
            { NDS_PARAM_GAP_HANDLER, { "True", "true" } },
            { NDS_CYCLE_NDS1_AFTER_N_COMMANDS, { "15", "15" } }
        };
        setup_env( vals );
        ::setenv( "NDSSERVER", "nds-server2.example:31201", 1 );
        // setup from the enviornment
        NDS::parameters params;
        for ( const auto& entry : vals )
        {
            std::string value = params.get( entry.first );
            NDS_ASSERT( value == entry.second.second );
        }
        NDS_ASSERT( params.get( NDS_PARAM_HOSTNAME ) == "nds-server2.example" );
    }

    // test NDS2_CLIENT + NDSSERVER for overrides, the NDS2_client should
    // provide defaults
    {
        std::map< std::string, std::pair< std::string, std::string > > vals{
            { NDS_PARAM_PORT, { "8088", "8088" } },
            { NDS_PARAM_PROTOCOL_VERSION, { "Two", "2" } },
            { NDS_PARAM_ALLOW_DATA_ON_TAPE, { "1", "true" } },
            { NDS_PARAM_GAP_HANDLER,
              { "STATIC_HANDLER_ZERO", "STATIC_HANDLER_ZERO" } },
            { NDS_PARAM_GAP_HANDLER, { "True", "true" } },
            { NDS_CYCLE_NDS1_AFTER_N_COMMANDS, { "15", "15" } }
        };
        setup_env( vals );
        ::setenv( "NDSSERVER", "nds-server2.example", 1 );
        // setup from the enviornment
        NDS::parameters params;
        for ( const auto& entry : vals )
        {
            std::string value = params.get( entry.first );
            NDS_ASSERT( value == entry.second.second );
        }
        NDS_ASSERT( params.get( NDS_PARAM_HOSTNAME ) == "nds-server2.example" );
    }

    // test local defines NDS2_CLIENT + NDSSERVER for overrides, the local
    // params should override
    {
        std::map< std::string, std::pair< std::string, std::string > > vals{
            { NDS_PARAM_HOSTNAME,
              { "nds-server1.example", "nds-server3.example" } },
            { NDS_PARAM_PORT, { "8088", "8089" } },
            { NDS_PARAM_PROTOCOL_VERSION, { "Two", "1" } },
            { NDS_PARAM_ALLOW_DATA_ON_TAPE, { "1", "true" } },
            { NDS_PARAM_GAP_HANDLER,
              { "STATIC_HANDLER_ZERO", "STATIC_HANDLER_ZERO" } },
            { NDS_PARAM_GAP_HANDLER, { "True", "true" } },
            { NDS_CYCLE_NDS1_AFTER_N_COMMANDS, { "15", "15" } }
        };
        setup_env( vals );
        ::setenv( "NDSSERVER", "nds-server2.example", 1 );
        // setup from the enviornment
        NDS::parameters params(
            "nds-server3.example", 8089, NDS::connection::PROTOCOL_ONE );
        for ( const auto& entry : vals )
        {
            std::string value = params.get( entry.first );
            NDS_ASSERT( value == entry.second.second );
        }
    }

    // test local defines/defaults NDS2_CLIENT + NDSSERVER for overrides, the
    // local params should override
    {
        std::map< std::string, std::pair< std::string, std::string > > vals{
            { NDS_PARAM_HOSTNAME,
              { "nds-server1.example", "nds-server3.example" } },
            { NDS_PARAM_PORT, { "8088", "31200" } },
            { NDS_PARAM_PROTOCOL_VERSION, { "Two", "try" } },
            { NDS_PARAM_ALLOW_DATA_ON_TAPE, { "1", "true" } },
            { NDS_PARAM_GAP_HANDLER,
              { "STATIC_HANDLER_ZERO", "STATIC_HANDLER_ZERO" } },
            { NDS_PARAM_GAP_HANDLER, { "True", "true" } },
            { NDS_CYCLE_NDS1_AFTER_N_COMMANDS, { "15", "15" } }
        };
        setup_env( vals );
        ::setenv( "NDSSERVER", "nds-server2.example", 1 );
        // setup from the enviornment
        NDS::parameters params( "nds-server3.example" );
        for ( const auto& entry : vals )
        {
            std::string value = params.get( entry.first );
            NDS_ASSERT( value == entry.second.second );
        }
    }
}

int
main( int argc, char* argv[] )
{
    test_parameters( );
    return 0;
}