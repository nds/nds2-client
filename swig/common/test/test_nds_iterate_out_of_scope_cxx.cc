#include "nds.hh"
#include "nds_helper.hh"

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sstream>

#include "nds_testing.hh"
#include "test_macros.hh"

#ifndef __GNUC__
#define __attribute__( x ) /* Nothing */
#endif /* __GNUC__ */

template < typename T >
class SimpleMisMatchPredicate
{
    T val_;

public:
    SimpleMisMatchPredicate( T val ) : val_( val )
    {
    }
    bool
    operator( )( const T& other ) const
    {
        return val_ != other;
    }
};

// no gaps on this test
bool
is_gap_second( NDS::buffer::gps_second_type input_gps )
{
    return false;
}

template < typename T >
bool
check_data( NDS::buffer& buf, T val )
{
    bool                   result = true;
    NDS::buffer::size_type samples_per_sec = buf.seconds_to_samples( 1 );

    // T* data = const_cast< T* >( reinterpret_cast< const T* >( &buf[ 0 ] ) );
    auto data = buf.cbegin< T >( );
    for ( NDS::buffer::gps_second_type cur_gps = buf.Start( );
          cur_gps < buf.Stop( );
          ++cur_gps )
    {
        T expected =
            ( is_gap_second( cur_gps ) ? static_cast< T >( 0.0 ) : val );
        const T* end = data + samples_per_sec;
        const T* mismatch =
            std::find_if( data, end, SimpleMisMatchPredicate< T >( expected ) );
        if ( mismatch != end )
        {
            std::cerr << "Found mismatch for " << cur_gps
                      << " expected that it is a "
                      << ( is_gap_second( cur_gps ) ? "gap" : "data" )
                      << " segment" << std::endl;
            result = false;
        }
        data = end;
    }
    return result;
}

NDS::data_iterable
start_iteration( std::string                    hostname,
                 NDS::connection::port_type     port,
                 NDS::connection::protocol_type proto,
                 NDS::buffer::gps_second_type   gps_start )
{
    using NDS::connection;

    std::cerr << "Connecting to " << hostname << ":" << port << " " << proto
              << std::endl;
    pointer< connection > conn( new connection( hostname, port, proto ) );
    conn->parameters( ).set( "ITERATE_USE_GAP_HANDLERS", "false" );

    NDS::connection::channel_names_type chans;
    chans.push_back( "X1:PEM-1" );
    chans.push_back( "X1:PEM-2" );

    return conn->iterate( NDS::request_period( gps_start, gps_start + 20, 10 ),
                          chans );
}

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TWO;

    NDS::buffer::gps_second_type cur_gps __attribute__( ( unused ) ) =
        1770000000;

    NDS::data_iterable stream =
        start_iteration( hostname, port, proto, cur_gps );

    int i = 0;
    for ( auto bufs : stream )
    {
        NDS_ASSERT( bufs->at( 0 ).Samples( ) == 256 * 10 );
        NDS_ASSERT(
            check_data< float >( bufs->at( 0 ), ( i == 0 ? 1.5f : 3.5f ) ) );
        NDS_ASSERT( bufs->at( 1 ).Samples( ) == 512 * 10 );
        NDS_ASSERT(
            check_data< double >( bufs->at( 1 ), ( i == 0 ? 2.75 : 4.75 ) ) );

        NDS_ASSERT( bufs->at( 0 ).Start( ) == cur_gps );
        cur_gps = bufs->at( 0 ).Stop( );
        ++i;
    }
}
