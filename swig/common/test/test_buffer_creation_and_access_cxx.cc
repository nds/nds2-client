#include "nds.hh"

#include <string>
#include <vector>

#include "nds_testing.hh"

template < typename T >
class Range
{
public:
    explicit Range( NDS::buffer& buf ) : buf_( buf )
    {
    }

    const T*
    begin( )
    {
        return buf_.cbegin< T >( );
    }
    const T*
    end( )
    {
        return buf_.cend< T >( );
    }

private:
    NDS::buffer& buf_;
};

int
main( int, char** )
{
    NDS::channel chan_info( "X1:TEST-CHAN",
                            NDS::channel::CHANNEL_TYPE_RAW,
                            NDS::channel::DATA_TYPE_FLOAT32,
                            16.0,
                            1.0,
                            1.0,
                            0.0,
                            "counts" );

    std::vector< unsigned char > data( 16 * sizeof( float ) );
    {
        auto cur = reinterpret_cast< float* >( data.data( ) );
        for ( auto i = 0; i < 16; ++i )
        {
            *cur++ = 3.1415f;
        }
    }
    NDS::buffer test_buffer( chan_info, 1000000, 0, std::move( data ) );
    NDS_ASSERT( test_buffer.Samples( ) == 16 );

    for ( auto cur = test_buffer.cbegin< float >( );
          cur < test_buffer.cend< float >( );
          ++cur )
    {
        NDS_ASSERT( *cur == 3.1415f );
    }

    for ( auto& cur : Range< float >( test_buffer ) )
    {
        NDS_ASSERT( cur == 3.1415f );
    }

    for ( auto i = 0; i < 16; ++i )
    {
        NDS_ASSERT( test_buffer.at< float >( i ) == 3.1415f );

        bool type_check = false;
        try
        {
            test_buffer.at< double >( i );
        }
        catch ( std::runtime_error& )
        {
            type_check = true;
        }
        NDS_ASSERT( type_check );
    }

    bool bounds_check = false;
    try
    {
        test_buffer.at< float >( -1 );
    }
    catch ( std::out_of_range& )
    {
        bounds_check = true;
    }
    NDS_ASSERT( bounds_check );

    bounds_check = false;
    try
    {
        test_buffer.at< float >( 17 );
    }
    catch ( std::out_of_range& )
    {
        bounds_check = true;
    }
    NDS_ASSERT( bounds_check );
    return 0;
}