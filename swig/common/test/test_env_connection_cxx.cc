#include "nds.hh"

#include <memory>
#include <iostream>
#include <cstdlib>
#include <sstream>

#include "nds_param_str.h"

#include "test_macros.hh"
#include "nds_testing.hh"

#ifdef _WIN32

int
setenv( const char* name, const char* value, int overwrite )
{
    std::ostringstream os;
    os << name << "=" << value;
    std::string env = os.str( );
    _putenv( env.c_str( ) );
    return 0;
}

int
unsetenv( const char* name )
{
    return setenv( name, "", 1 );
}

#endif

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if (::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if (::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = ::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TRY;

    std::vector< std::string > keys = {
        "NDS_TEST_PORT",
        "NDS_TEST_HOST",
        std::string( NDS_PARAM_ENV_PREFIX ) + std::string( NDS_PARAM_HOSTNAME ),
        std::string( NDS_PARAM_ENV_PREFIX ) + std::string( NDS_PARAM_PORT ),
        std::string( NDS_PARAM_ENV_PREFIX ) +
            std::string( NDS_PARAM_PROTOCOL_VERSION ),
        "NDSSERVER"
    };
    for ( const auto& entry : keys )
    {
        ::unsetenv( entry.c_str( ) );
    }

    if ( find_opt( "-option1", args ) )
    {
        std::ostringstream os;
        os << hostname << ":" << port;
        std::string s = os.str( );
        auto        val = ::setenv( "NDSSERVER", s.c_str( ), 1 );
        std::cout << "env set " << val << std::endl;
    }
    else if ( find_opt( "-option2", args ) )
    {
        std::string port_str = std::to_string( port );

        std::string key( std::string( NDS_PARAM_ENV_PREFIX ) +
                         std::string( NDS_PARAM_HOSTNAME ) );
        ::setenv( key.c_str( ), hostname.c_str( ), 1 );

        key = std::string( std::string( NDS_PARAM_ENV_PREFIX ) +
                           std::string( NDS_PARAM_PORT ) );
        ::setenv( key.c_str( ), port_str.c_str( ), 1 );
    }
    else
    {
        return 1;
    }

    std::cerr << "Connecting to " << hostname << ":" << port
              << " via environment" << std::endl;
    pointer< connection > conn( new connection( NDS::parameters( ) ) );

    NDS_ASSERT( conn->parameters( ).host( ) == hostname );
    NDS_ASSERT( conn->parameters( ).port( ) == port );
    NDS_ASSERT( conn->parameters( ).protocol( ) ==
                NDS::connection::PROTOCOL_TWO );

    return 0;
}
