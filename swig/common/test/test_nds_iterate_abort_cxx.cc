#include "nds.hh"
#include "nds_helper.hh"

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sstream>

#include "nds_testing.hh"
#include "test_macros.hh"

#ifndef __GNUC__
#define __attribute__( x ) /* Nothing */
#endif /* __GNUC__ */

enum class TestType
{
    live_test
};

int
main( int argc, char* argv[] )
{
    using NDS::connection;

    TestType type = TestType::live_test;

    std::vector< std::string > args = convert_args( argc, argv );

    //---------------------------------------------------------------------
    // Obtain the port of the server
    //---------------------------------------------------------------------
    connection::port_type port = 31200;
    if ( std::getenv( "NDS_TEST_PORT" ) )
    {
        std::istringstream ps(::getenv( "NDS_TEST_PORT" ) );

        ps >> port;
    }
    std::string hostname( "localhost" );
    if ( std::getenv( "NDS_TEST_HOST" ) )
    {
        hostname = std::getenv( "NDS_TEST_HOST" );
    }

    connection::protocol_type proto = connection::PROTOCOL_TWO;
    if ( find_opt( "-live", args ) )
    {
        type = TestType::live_test;
    }
    NDS::buffer::gps_second_type start = 0;
    NDS::buffer::gps_second_type stop = 0;
    NDS::buffer::gps_second_type stride = 0;

    std::cerr << "Connecting to " << hostname << ":" << port << " " << proto
              << std::endl;
    pointer< connection > conn( new connection( hostname, port, proto ) );

    conn->parameters( ).set( "ITERATE_USE_GAP_HANDLERS", "false" );

    NDS::connection::channel_names_type chans;
    chans.push_back( "X1:PEM-1" );
    chans.push_back( "X1:PEM-2" );

    auto stream =
        conn->iterate( NDS::request_period( start, stop, stride ), chans );
    for ( auto bufs : stream )
    {
        stream.abort( );
        break;
    }

    NDS_ASSERT( stream.begin( ) == stream.end( ) );

    bool really_done = false;
    // make sure we cannot do other operations
    try
    {
        conn->find_channels( NDS::channel_predicate( ) );
    }
    catch ( ... )
    {
        really_done = true;
    }
    NDS_ASSERT( really_done );
    return 0;
}
