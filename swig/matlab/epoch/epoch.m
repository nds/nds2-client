function ep = epoch( base ) % -*- mode: octave; -*-
  switch( nargin )
    case 0
      ep = ndsm.epoch( );
    case 1
      switch( class( base ) )
	case 'nds.epoch'
	  ep = ndsm.epoch( base );
      end
    otherwise
      ep = ndsm.epoch( );
  end
end
