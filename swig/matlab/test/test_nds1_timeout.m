% -*- mode: octave -*-
host = getenv( 'NDS_TEST_HOST' )
if ( isempty( host ) );
  host = 'localhost'
end
port = getenv( 'NDS_TEST_PORT' )
if ( isempty( port ) )
  port = 31200
else
  port = str2num( port )
end
c = ndsm.Connection( host, port, ndsm.Connection.PROTOCOL_ONE )

try;
  lst = c.findchannels('*')
catch except;
  disp( 'Caught Runtime error, should be from a timeout, but we currently cannot get a good error message' );
  exit(0);
end;
disp( 'Did not test timeouts!!!!' )
exit(1);
