% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

global unit_test;
global START;
global STOP;
global STRIDE;
global GPS_START;
global GPS_STOP;
global CHANNELS;

unit_test = UnitTest( argv );

START = 0;
STOP = START + 20;
STRIDE = 10;
GPS_START = 1770000000;
GPS_STOP = GPS_START + 20;

MULTIPLIER = 10;
EXPECTED_I = 2;
TIP_POINT = 0;      % when does the data do a switch
                    % this is just part of the test

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = 'unknown';
use_gap_handler = 'true';
NO_GAPS=true;

%------------------------------------------------------------------------
% Need to adjust according to parameter
%------------------------------------------------------------------------
if ( unit_test.hasoption( '-proto-1' ) )
  protocol = ndsm.Connection.PROTOCOL_ONE;
  MULTIPLIER = 1;
  EXPECTED_I = 20;
  TIP_POINT = 10;
  CHANNELS = { 'X1:PEM-1', ...
	       'X1:PEM-2', ...
	      };
end
if ( unit_test.hasoption( '-proto-2' ) )
  protocol = ndsm.Connection.PROTOCOL_TWO;
  MULTIPLIER = 10;
  EXPECTED_I = 2;
  TIP_POINT = 0;
  CHANNELS = { 'X1:PEM-1,online', ...
	       'X1:PEM-2,online', ...
	    };
end
if ( unit_test.hasoption( '-no-gap' ) )
  use_gap_handler = 'false';
  NO_GAPS = false;
end

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = ndsm.Connection(hostname, port, protocol );
conn.setparameter( 'ITERATE_USE_GAP_HANDLERS', use_gap_handler );
conn.setparameter( 'GAP_HANDLER', 'STATIC_HANDLER_ZERO' );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------

expected = { 1.5, 2.75 };

i = 0;
iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
while( iter.hasnext( ) )
  bufs = iter.next( );
  %----------------------------------------------------------------------
  unit_test.check( bufs(1).samples( ), ...
		   256 * MULTIPLIER, ...
		   'Verifying samples of buffer 1' );
  unit_test.check( [ checkdata_ildb( bufs(1), expected{1} ) ], ...
		   true, ...
		   'Verifying data of buffer 1' );
  %----------------------------------------------------------------------
  unit_test.check( bufs(2).samples( ), ...
		   512 * MULTIPLIER, ...
		   'Verifying samples of buffer 0' );
  unit_test.check( [ checkdata_ildb( bufs(2), expected{2} ) ], ...
		   true, ...
		   'Verifying data of buffer 1' );
  %----------------------------------------------------------------------
  i = i + 1;
  if (i == TIP_POINT)
    expected = { 3.5, 4.75 };
  end
end
unit_test.check( i, ...
		 EXPECTED_I, ...
		 'Verifying number of iterations: ' );

%------------------------------------------------------------------------
% Make sure we can do other operations
%------------------------------------------------------------------------

conn.setepoch( GPS_START, GPS_STOP );
conn.findchannels( 'X1:PEM-1,online' );
conn.findchannels( 'X1:PEM-2,online' );

expected = { 1.5, 2.75 };

i = 0;
iter = conn.iterate(0, 2, CHANNELS);
while( iter.hasnext( ) )
  bufs = iter.next( );
  unit_test.check( bufs(1).samples( ), ...
		   256, ...
		   'Verifying samples of buffer 1 after setepoch' );
  unit_test.check( true, ...
		   [ checkdata_ildb( bufs(1), 1.5 ) ], ...
		   'Verifying data of buffer 1 after setepoch' );
  unit_test.check( bufs(2).samples( ), ...
		   512, ...
		   'Verifying samples of buffer 2 after setepoch' );
  unit_test.check( true, ...
		   [ checkdata_ildb( bufs(2), 2.75 ) ], ...
		   'Verifying data of buffer 2 after setepoch' );
  i = i + 1;
end

unit_test.check( i, ...
		 2, ...
		 'Verifying second number of iterations: ' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------

conn.close( );
unit_test.exit( );
 

