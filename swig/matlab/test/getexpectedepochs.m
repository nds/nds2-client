function EPOCHS = getexpectedepochs( Proto )
  EPOCHS = struct( 'name', {}, 'start', {}, 'stop', {} );
  if ( Proto == ndsm.Connection.PROTOCOL_ONE )
    EPOCHS( 1 ) = struct( 'name', 'ALL', 'start', 0, 'stop', 1999999999 );
  else
    EPOCHS( 1 ) = struct( 'name', 'ALL', 'start', 0, 'stop', 1999999999 );
    EPOCHS( 2 ) = struct( 'name', 'ER2', 'start', 1025636416, 'stop', 1028563232 );
    EPOCHS( 3 ) = struct( 'name', 'ER3', 'start', 1042934416, 'stop', 1045353616 );
    EPOCHS( 4 ) = struct( 'name', 'ER4', 'start', 1057881616, 'stop', 1061856016 );
    EPOCHS( 5 ) = struct( 'name', 'ER5', 'start', 1073606416, 'stop', 1078790416 );
    EPOCHS( 6 ) = struct( 'name', 'ER6', 'start', 1102089216, 'stop', 1102863616 );
    EPOCHS( 7 ) = struct( 'name', 'ER7', 'start', 1116700672, 'stop', 1118330880 );
    EPOCHS( 8 ) = struct( 'name', 'ER8', 'start', 1123856384, 'stop', 1126621184 );
    EPOCHS( 9 ) = struct( 'name', 'NONE', 'start', 0, 'stop', 0 );
    EPOCHS( 10 ) = struct( 'name', 'O1', 'start', 1126621184, 'stop', 1999999999 );
    EPOCHS( 11 ) = struct( 'name', 'S5', 'start', 815153408, 'stop', 880920032 );
    EPOCHS( 12 ) = struct( 'name', 'S6', 'start', 930960015, 'stop', 971654415 );
    EPOCHS( 13 ) = struct( 'name', 'S6a', 'start', 930960015, 'stop', 935798415 );
    EPOCHS( 14 ) = struct( 'name', 'S6b', 'start', 937785615, 'stop', 947203215 );
    EPOCHS( 15 ) = struct( 'name', 'S6c', 'start', 947635215, 'stop', 961545615 );
    EPOCHS( 16 ) = struct( 'name', 'S6d', 'start', 961545615, 'stop', 971654415 );
  end
end % function - getexpectedepochs
