% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

START = 1107878416;
STOP = 1107878420;
CHANNELS =  { 'H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend'
    };

unit_test = UnitTest;

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = ndsm.Connection.PROTOCOL_TWO;

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = ndsm.Connection(hostname, port, protocol );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------
conn.setparameter('ALLOW_DATA_ON_TAPE', 'true');

conn.setepoch( START, STOP );
avail = conn.getavailability( CHANNELS );

unit_test.check( length( avail ), ...
		 1, ...
		 'Availability list size' );
unit_test.check( avail( 1 ).getname( ), ...
		 char( CHANNELS( 1 ) ), ...
		 'Availability name' );
unit_test.check( length( avail( 1 ).getdata( ) ), ...
		 1, ...
		 'Availability data size' );
data = avail( 1 ).getdata( )
unit_test.check( data( 1 ).getframetype( ), ...
		 'H-H1_T', ...
		 'Availability data frame type' );
unit_test.check( data( 1 ).getgpsstart( ),  ...
		 START,  ...
		 'Availability data GPS start' );
unit_test.check( data( 1 ).getgpsstop( ),  ...
		 STOP,  ...
		 'Availability data GPS stop' );

bufs = conn.fetch( START, STOP, CHANNELS );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );
