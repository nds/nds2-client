% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

global unit_test;
global CHANNELS;
global EXPECTED_I;

unit_test = UnitTest( argv );

EXPECTED_I = 4;

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = 'unknown';

%------------------------------------------------------------------------
% Need to adjust according to parameter
%------------------------------------------------------------------------
if ( unit_test.hasoption( '-proto-1' ) )
  protocol = ndsm.Connection.PROTOCOL_ONE;
  CHANNELS = { 'X1:PEM-1', ...
	       'X1:PEM-2', ...
	      };
end
if ( unit_test.hasoption( '-proto-2' ) )
  protocol = ndsm.Connection.PROTOCOL_TWO;
  CHANNELS = { 'X1:PEM-1,online', ...
	       'X1:PEM-2,online', ...
	    };
end

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = ndsm.Connection(hostname, port, protocol );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------

expected = { 1.5, 2.75 };

i = 0;
iter = conn.iterate(CHANNELS);
while( iter.hasnext( ) )
  bufs = iter.next( );
  %----------------------------------------------------------------------
  unit_test.check( bufs(1).samples( ), ...
		   256, ...
		   'Verifying samples of buffer 1' );
  unit_test.check( [ checkdata_ild( bufs(1), expected{1} ) ], ...
		   true, ...
		   'Verifying data of buffer 1' );
  %----------------------------------------------------------------------
  unit_test.check( bufs(2).samples( ), ...
		   512, ...
		   'Verifying samples of buffer 0' );
  unit_test.check( [ checkdata_ild( bufs(2), expected{2} ) ], ...
		   true, ...
		   'Verifying data of buffer 1' );
  %----------------------------------------------------------------------
  i = i + 1;
  if ( i == EXPECTED_I )
    % On a real system this would continue indefinitly
    % however, this test is set to only send 5 seconds
    % of data.
    break;
  end
end
unit_test.check( i, ...
		 EXPECTED_I, ...
		 'Verifying number of iterations: ' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------

conn.close( );
unit_test.exit( );
 
