% -*- mode: octave -*-
% -----------------------------------------------------------------------
%   Simple test to verify version function
% -----------------------------------------------------------------------
unit_test = UnitTest;

path( )

% -----------------------------------------------------------------------
%   Minute Trend
% -----------------------------------------------------------------------
trends = { 'A,m-trend',
	   'X1:ABC,m-trend',
	   'X1:ABC.mean,m-trend',
	   'A:,m-trend'
	   };
  
for i = trends
  unit_test.check( ndsm.Channel.isminutetrend( i{1} ), ...
		   true, ...
		   'Verify mTrends are seen as minute trends' );
  unit_test.check( ndsm.Channel.issecondtrend( i{1} ), ...
		   false, ...
		   'Verify mTrends are not seen as second trends' );
end

% -----------------------------------------------------------------------
%   Second Trend
% -----------------------------------------------------------------------
trends = { 'A,s-trend',
	   'X1:ABC,s-trend',
	   'X1:ABC.mean,s-trend',
	   'A:,s-trend'
	   };
  
for i = trends
  unit_test.check( ndsm.Channel.isminutetrend( i{1} ), ...
		   false, ...
		   'Verify sTrends are not seen as minute trends' );
  unit_test.check( ndsm.Channel.issecondtrend( i{1} ), ...
		   true, ...
		  'Verify sTrends are seen as second trends' );
end

% -----------------------------------------------------------------------
%   Junk Trend
% -----------------------------------------------------------------------
trends = { '',
	   's-trend',
	   ',s-trend',
	   'm-trend',
	   ',m-trend',
	   'S-TREND',
	   ',S-TREND',
	   'M-TREND',
	   ',M-TREND',
	   ',mtrend',
	   'STREND',
	   ',strend',
	   'MTREND',
	   'X1:ABCCDEFGs-trend',
	   'X1:ABCDEFGS-TREND',
	   'X1:ABCCDEFGm-trend',
	   'X1:ABCDEFGM-TREND',
	   'X1:ABCDEFGM'
	   };
  
for i = trends
  unit_test.check( ndsm.Channel.isminutetrend( i{1} ), ...
		   false, ...
		   'Verify junk is not seen as minute trends' );
  unit_test.check( ndsm.Channel.issecondtrend( i{1} ), ...
		   false, ...
		   'Verify junk is not seen as second trends' );
end
unit_test.exit( );

% -----------------------------------------------------------------------

