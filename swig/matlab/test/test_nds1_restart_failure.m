% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

global unit_test;
global START;
global STOP;
global STRIDE;
global CHANNELS;

unit_test = UnitTest;

START = 0;
STOP = START + 1;
STRIDE = 1;
CHANNELS = { 'X1:PEM-1', ...
      };

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = ndsm.Connection.PROTOCOL_ONE;

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------

conn = ndsm.Connection(hostname, port, protocol );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------


i = 0;
EXPECTED_I = 1;
iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
while( iter.hasnext( ) )
  bufs = iter.next( );
  i = i + 1;
end
unit_test.check( i, ...
		 EXPECTED_I, ...
		 'Verifying number of iterations: ' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------

conn.close( );
unit_test.exit( );


