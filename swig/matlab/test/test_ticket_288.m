% -*- mode: octave -*-
%------------------------------------------------------------------------
global unit_test

unit_test = UnitTest( );
hostname = unit_test.hostname( );
port = unit_test.port( );

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = ndsm.Connection(hostname, port );
conn.setparameter('ALLOW_DATA_ON_TAPE', 'true');
conn.setparameter('ITERATE_USE_GAP_HANDLERS', 'false');

%------------------------------------------------------------------------
% Ticket 288
%------------------------------------------------------------------------

blockCount = 0;
lastGPS = 0;
expected = [ ...
	    66.7626, ...
	    0.0, ...
	    67.4257, ...
	    0.0, ...
	    0.0, ...
	    0.0 ...
	    ];
actual = [ ...
	    -1.0, ...
	    -1.0, ...
	    -1.0, ...
	    -1.0, ...
	    -1.0, ...
	    -1.0 ...
	    ];
start = 1167530460;
finish =  1178294460;
channels = { ...
	    'H1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean,m-trend' ...
	};

iter = conn.iterate( start, finish, channels );
while( iter.hasnext( ) )
  blockCount = blockCount + 1;
  bufs = iter.next( );
  lastGPS = bufs( 1 ).stop( );
  data = bufs( 1 ).getdata( );
  actual( blockCount ) = data( 121 );
end
unit_test.check( blockCount, 6, ...
		'Correct number of blocks were returned' );
unit_test.check( lastGPS, finish, ...
		'Correct last GPS valued reported' );
for i = 1:numel( expected )
  unit_test.check( actual( i ), ...
		  expected( i ), ...
		  'Verifying channel contents' );
end

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );

exit( unit_test.exitcode( ) );
