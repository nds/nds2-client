% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

unit_test = UnitTest;

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = ndsm.Connection.PROTOCOL_TWO;

if ( strcmp( argv, '-proto-1' ) )
    protocol = ndsm.Connection.PROTOCOL_ONE;
elseif ( strcmp( argv, '-proto-2' ) )
	protocol = ndsm.Connection.PROTOCOL_TWO;
end

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
conn = ndsm.Connection(hostname, port, protocol );
	   
cur = conn.currentepoch( );

%------------------------------------------------------------------------
% Verify default as returning ALL epocs
%------------------------------------------------------------------------
unit_test.check( cur.getgpsstart( ), ...
		 0, ...
		 'Check GPS start of current epoch: ' );
unit_test.check( cur.getgpsstop( ), ...
		 1999999999, ...
		 'Check GPS start of current epoch: ' );

%------------------------------------------------------------------------
% Verify list of epoch data
%------------------------------------------------------------------------
available_epochs = conn.getepochs( );
expected_epochs = getexpectedepochs( conn.getprotocol( ) );
unit_test.check( length( expected_epochs ), ...
		 length( available_epochs ), ...
		 'Verify size of epoch lists' );
%------------------------------------------------------------------------
% Protocol dependent behaviors
%------------------------------------------------------------------------
if ( protocol == ndsm.Connection.PROTOCOL_TWO )
  %----------------------------------------------------------------------
  % Test all epochs (except 'NONE') by name
  %----------------------------------------------------------------------
  for  i = 1:length(expected_epochs)
    epoch = available_epochs( i );
    if ( strcmp( epoch.getname( ), 'NONE' ) )
      continue;
    end
    conn.setepoch( epoch.getname( ) );

    cur = conn.currentepoch( );
    unit_test.check( epoch.getgpsstart( ), ...
		     cur.getgpsstart( ), ...
		     'Epics by name GPS start' );
    unit_test.check( epoch.getgpsstop( ), ...
		     cur.getgpsstop( ), ...
		     'Epics by name GPS stop' );
  end % for - name
  %----------------------------------------------------------------------
  % Test all epochs (except 0-0) by time range
  %----------------------------------------------------------------------
  for i = 1:length(expected_epochs)
    epoch = available_epochs( i );
    if ( epoch.getgpsstop( ) == 0 )
      continue;
    end
    conn.setepoch( epoch.getgpsstart( ), ...
		   epoch.getgpsstop( ) );

    cur = conn.currentepoch( );
    unit_test.check( epoch.getgpsstart( ), ...
		     cur.getgpsstart( ), ...
		     'Epics by range GPS start' );
    unit_test.check( epoch.getgpsstop( ), ...
		     cur.getgpsstop( ), ...
		     'Epics by range GPS stop' );
  end % for - time range
end % PROTOCOL_TWO
%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );

unit_test.exit( );

