function Depricated( Old, New ) % -*- mode: octave; -*-
  % Used by other classes and methods within the ndsm package
  % to educate users and developers of routines that will
  % be removed in the near future.
  %
  %   ndsm.Depricated( Old )
  %
  %   ndsm.Depricated( Old, New )
  %
  %   INPUTS:
  %     Old  : String name of old function or method that is being depricated
  %     New  : String name of the function or method to use in place of Old
  switch( nargin )
    case 1
      warning( 'WARNING: %s has been depricated', Old );
    case 2
      warning( 'WARNING: %s has been depricated. Please use %s instead', Old, New );
  end
end
