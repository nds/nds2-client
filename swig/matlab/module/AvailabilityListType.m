classdef AvailabilityListType < handle % -*- mode: octave; -*-
    properties (Access = private)
        % property attributes: http://www.mathworks.com/help/matlab/matlab_oop/property-attributes.html
        handle = 0;
    end
    
    methods (Hidden, Access = protected)
        function HANDLE = gethandle(this)
            if isa(this.handle,'numeric')
                throw(MException('nds.connection:notopen','No valid channel for this operation'));
            end
            HANDLE = this.handle;
        end
    end
    
    methods (Access = public)
        
       function OBJ = AvailabilityListType( varargin )
         % OBJ constructor for the ndsm.AvailabilityListType class
         %
         %   OUTPUTS:
         %     OBJ : ndsm.AvailabilityListType instance
	 switch( nargin )
	   case 0
	     h = nds2.availability_list_type( );
	   case 1
	     h = varargin{1};
	 end
	 OBJ.handle = h;
       end

       function pushback( self, container )
	 % Add elements
	 %
	 %   INPUTS:
	 %     self       : ndsm.AvailabilityListType instance
	 %     container  : element to add
	 %
	 class( container )
	 if ( isa( container, 'nds2.availability' ) )
	   self.gethandle( ).pushBack( container );
	 elseif ( isa( container, 'ndsm.Availability' ) )
	   self.gethandle( ).pushBack( container.gethandle( ) );
	 end
       end

       function RETVAL = simplelist( self )
	 % Retrieve simplified list of available segments
	 %
	 %   INPUTS:
	 %     self    : ndsm.AvailabilityListType
	 %
	 %   OUTPUTS:
	 %     RETVAL  : Simplified list of available segments
	 RETVAL = ndsm.SimpleAvailabilityListType( self.gethandle( ).simpleList( ) );
       end
    end
end
