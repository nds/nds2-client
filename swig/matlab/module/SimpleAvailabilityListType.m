classdef SimpleAvailabilityListType < handle % -*- mode: octave; -*-
    properties (Access = private)
        % property attributes: http://www.mathworks.com/help/matlab/matlab_oop/property-attributes.html
        handle = 0;
	array_handle = 0;
    end
    
    methods (Hidden, Access = protected)
        function HANDLE = gethandle(this)
            if isa(this.handle,'numeric')
                throw(MException('nds.connection:notopen','No valid channel for this operation'));
            end
            HANDLE = this.handle;
        end
    end
    
    methods (Access = public)
        
       function OBJ = SimpleAvailabilityListType( varargin )
         % OBJ constructor for the ndsm.AvailabilityListType class
	 %
	 % OBJ = ndsm.SimpleAvailabilityListType( )
	 %
	 % OBJ = ndsm.SimpleAvailabilityListType( INSTANCE )
         %
	 %   INPUTS:
	 %     INSTANCE : Either an instance of ndsm.SimpleAvailabilityListType
	 %                or an instance of nds2.simple_availability_list_type
         %   OUTPUTS:
         %     OBJ : ndsm.SimpleAvailabilityListType instance
	 switch( nargin )
	   case 0
	     h = nds2.simple_availability_list_type( );
	   case 1
	     if( isa(varargin{1}, 'ndsm.SimpleAvailabilityListType' ) )
	       h = varargin{1}.gethandle( );
	     elseif( isa(varargin{1}, 'nds2.simple_availability_list_type' ) )
	       h = varargin{1};
	     else
	       h = varargin{1};
	     end
	 end
	 OBJ.handle = h;
       end

       function RETVAL = get( self, offset )
	 % Retrieve the SimpleAvailability at the offset
	 %
	 % OBJ = self.get( offset )
	 %
	 %   INPUTS:
	 %     self   : ndsm.SimpleAvailabilityListType instance
	 %     offset : offset postion of element starting at 0
	 %
	 %   OUTPUTS:
	 %     RETVAL : The SimpleAvailability at the specified offset
	 RETVAL = self.gethandle( ).get( offset )
       end

       function RETVAL = size( self )
	 % Retrieve the number of SimpleAvailability instances in the list
	 %
	 % s = self.size( )
	 %
	 %   INPUTS:
	 %     self   : ndsm.SimpleAvailabilityListType instance
	 %
	 %   OUTPUTS:
	 %     RETVAL : The number of Segments in the list
	 RETVAL = self.gethandle( ).size( );
       end

       function RETVAL = tostring( self )
	 % Retrieve the string representation of the instance
	 %
	 % s = self.tostring( )
	 %
	 %   INPUTS:
	 %     self   : ndsm.SimpleAvailabilityListType instance
	 %
	 %   OUTPUTS:
	 %     RETVAL : The string representation of the instance
	 RETVAL = char(self.gethandle( ).toString( ));
       end
    end
end
