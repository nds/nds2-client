classdef Connection < handle % -*- mode: matlab; -*-
%Connection Primary NDS class for communicating with a server
%   See the detailed docs on the web, I'm sure it's there somewhere
    
    properties (Access = private)
        % property attributes: http://www.mathworks.com/help/matlab/matlab_oop/property-attributes.html
        mycon = 0;
    end
    
    properties (Constant, GetAccess = public)
        PROTOCOL_ONE = nds2.connection.PROTOCOL_ONE % Connect only to NDS1 servers
        PROTOCOL_TWO = nds2.connection.PROTOCOL_TWO % Connect only to NDS2 servers
        PROTOCOL_TRY = nds2.connection.PROTOCOL_TRY % Determiine server type automatically
    end
    
    
    methods (Hidden, Access = private)
        function con = getCon(this)
            if isa(this.mycon,'numeric')
                throw(MException('nds.connection:notopen','No open connection for this operation'));
            end
            con = this.mycon;
        end
    end
    
    methods (Access = public)
        
        function CONN = Connection( server, varargin )
        %CONNECTION constructor for the ndsm.Connection class
        %
        %   CONN = Connection( SERVER )
        %     Creates a new instance which is suitable for communitating with an NDS server.
        %
        %   CONN = Connection( SERVER, PORT )
        %     Creates a new instance which is suitable for communitating with an NDS server.
        %
        %   CONN = Connection( SERVER, PORT, PROTOCOL )
        %     Creates a new instance which is suitable for communitating with an NDS server.
        %
        %  INPUTS:
        %    SERVER            : string
        %    PORT              : int
        %    PROTOCOL          : int
        %
        %  OUTPUTS:
        %    CONN              : ndsm.Connection instance
            
            port = 31200;
            protocol = ndsm.Connection.PROTOCOL_TRY ;
            if nargin > 1
                port = varargin{1};
            end
            if nargin > 2
                protocol = varargin{2};
            end

            % --------------------------------------------------------------
            %   NDS_MODEL nds nds2
            % --------------------------------------------------------------
            mod = getenv('NDS_MODEL');
            if isempty(mod)
                model = 1;
            elseif strcmp(mod,'nds')
                model = 1;
            elseif strcmp(mod, 'nds2')
                model =2;
            else
                throw(MException('ndsm.Connection','Bad NDS_MODEL value in Environment'));
            end
            
            switch model % which lib nds or nds2?
              case 1
                mycon = nds2.connection(server, port, protocol);
              case 2
                switch protocol
                  case ndsm.Connection.PROTOCOL_TRY 
                    protocol = nds2.connection.PROTOCOL_TRY
                  case ndsm.Connection.PROTOCOL_TRY 
                    protocol = nds2.connection.PROTOCOL_TRY
                  case ndsm.Connection.PROTOCOL_TRY 
                    protocol = nds2.connection.PROTOCOL_TRY
                end
                mycon = nds2.connection(server, port, protocol);
            end
            CONN.mycon = mycon;
        end
        
        function BOOL = check( this, Start, Stop, Channels )
        % Given a start/stop time and a channel list check to see if a fetch request would suceed.
        % This will return failure if there is a gap in the data, or
        % if the data is on tape.
        %
        % INPUTS
        %     Start    : Start time of request
        %     Stop     : Stop time of request
        %     Channels : List of channel names
        %
        % OUTPUTS
        %     BOOL     : True if the data is avialble and not on tape
        %
            BOOL = this.check( Start, Stop, Channels );
        end % check

        function close(this)
        % CONN.close()
        %  Closes a previously opened connection to an NDS server.
            con = this.getCon();
            con.close()
            this.mycon = 0;
        end % close
        
        function COUNT = countchannels( this, ChannelGlob, ChannelTypeMask, DataTypeMask, MinSampleRate, MaxSampleRate ) 
        % Return the count of channels matching the given parameters.
        % detail Return a count of channels that are available form this connection within the given contstraints.
        %
        % INTPUTS:
        %   channelGlob     A string to match channels to.  Bash style globs are allowed (* to mach anything, ? matches 1 character).  "*" matches all channels.
        %   channelTypeMask Set of channel types to search.  Defaults to all.
        %   dataTypeMask    A set of data types to search.  Defaults to all.
        %   minSampleRate   The lowest sample rate to return.
        %   maxSampleRate   The highest sample rate to return.
        %
        % OUTPUTS:
        %   A count of channels matching the input parameters.
        %
        % NOTES:
        %   For NDS1 a local cache of the channel list is searched.  For NDS2 this call may result in one or
        %     requests to the server, no local cache is used.
        %
            con = this.getCon();
            switch( nargin )
              case 1
                COUNT = con.countChannels( )
              case 2
                COUNT = con.countChannels( ChannelGlob )
              case 3
                COUNT = con.countChannels( ChannelGlob, ChannelTypeMask )
              case 4
                COUNT = con.countChannels( ChannelGlob, ChannelTypeMask, DataTypeMask )
              case 5
                COUNT = con.countChannels( ChannelGlob, ChannelTypeMask, DataTypeMask, MinSampleRate )
              case 6
                COUNT = con.countChannels( ChannelGlob, ChannelTypeMask, DataTypeMask, MinSampleRate, MaxSampleRate )
              otherwise
                msgID = 'connection.check:invalid_argument'
                msg = 'An incorrect number of arguments were passed'
                baseException = MException( msgID, msg );
                throw( baseException )
            end
        end % countChannels
        
        function EPOCH = currentepoch(this)
        % Return the currently set epoch.
        %
        % OUTPUTS
        %     EPOCH A epoch value representing the current epoch.
        %
        % NOTES
        %     This command only works for NDS2
        %
            EPOCH = ndsm.Epoch( this.getCon( ).currentEpoch( ) );
        end % currentEpoch

        function BUFFERS = fetch( this, Start, Stop, ChannelNames )
        % Retrieve data from the server
        % 
        % INPUTS:
        %   Start        : Start time of request
        %   Stop         : Stop time of request
        %   ChannelNames : List of channels
        % 
        % OUTPUTS:
        %   BUFFERS      : array of buffers
        %
            BUFFERS = ndsm.Buffer( this.getCon( ).fetch( Start, Stop, ChannelNames ) );
        end % fetch

        function CHANNELS = findchannels(this, varargin)
        % Retrieve the set of channels that match the pattern.
        % Pattern matching uses standard glob syntax.
        % 
        % INTPUTS:
        %   PATTERN      : string - pattern specifying the channels to retrieve.
        %   CHANNEL_TYPE : 
        %
        % OUTPUTS:
        %   CHANNELS  : array of channels
            switch( length( varargin ) )
              case 2
                pattern = varargin{1};
                channel_type = varargin{2};
                CHANNELS = ndsm.Channel( this.getCon( ).findChannels( pattern, ...
                                                                  channel_type ) );
              case 1
                pattern = varargin{1};
                CHANNELS = ndsm.Channel( this.getCon( ).findChannels( pattern ) );
            end
        end % findchannels

        function AVAILABILITY = getavailability(this, ChannelNames)
        % Given a list of channels return their availability over the current epoch.
        %
        % INPUTS
        %     ChannelNames : A list of channels to return availability for.
        %
        % OUTPUTS
        %     AVAILABILITY : A list a channel availabilities.  There is one set of aviailabilities returned per channel requested.
        %
            AVAILABILITY = ndsm.Availability( this.getCon( ).getAvailability( ChannelNames ) );
        end % getavailability

        function epochs = getepochs(this)
        % Return a list of epochs
        %
        % NDS2 has the concept of epochs.  A epoch may be set on a connection to constrain all data and channel requests
        % to a given time frame.  This call returns the currently available epochs.
        %
        % OUTPUTS
        %     A list of epochs
        %
        % NOTES
        %     Only available on NDS2 connections.
            epochs = ndsm.Epoch( this.getCon( ).getEpochs( ) );
        end % getepochs

        function VALUE = getparameter(this, parameter)
        % Retreive the current parameter setting on a connection.
        %
        % Retreive the current value set for parameter.  See #set_parameter for documentation
        % on the available parameters.
        %
        % INPUTS
        %     parameter : Parameter name, as a string.
        %
        % OUTPUTS
        %     VALUE : The parameter value, or "" if an invalid parameter is found.
        %
            VALUE = char(this.getCon( ).getParameter(parameter));
        end % getparameter

        function VALUE_PAIRS = getparameters(this)
        % Return a list of supported parameters.
        %
        % OUTPUTS
        %     VALUE_PAIRS : returns a list of parameter names that may be used with setParameter or getParameter
        %
            VALUE_PAIRS = this.getCon( ).getParameters();
        end % getparameters

        function HOSTNAME = gethost(this)
        % Retrieve the hostname used by the connection to
        % NDS server was established.
        % 
        % OUTPUTS:
        %     HOSTNAME      : int - Port number of connected server
            HOSTNAME = char(this.getCon().getHost( ));
        end % gethost

        function port = getport(this)
        % Retrieve the port number on which the connection to
        % NDS server was established.
        % 
        % OUTPUTS:
        %  PORT      : int - Port number of connected server
            port = this.getCon().getPort( );
        end % getPort

        function PROTOCOL = getprotocol(this)
        % Retrieve the protocol number of the running
        % NDS server.
        % 
        % OUTPUTS:
        %  PROTOCOL      : int - Protocol version number of connected server
            PROTOCOL = this.getCon().getProtocol( );
        end % getprotocol

        function BOOL = hasnext(this)
        % Check for more data in the current iterate request
        % Returns true if the current iterate request has more data to be read.  Data should be read using the #next() method.
        %
        % OUTPUTS
        %     BOOL : True if there is more data, else false
        %
            BOOL = this.getCon( ).hasNext( );
        end % hasnext

        function HASH = hash( this )
        % Return the NDS2 channel hash.
        %
        % OUTPUTS:
        %     HASH : A hash value.
        %
        % NOTES:
        %     NDS version 2 only.
        %
            HASH = this.getCon( ).hash( );
        end % hash

        function ITERATOR = iterate( this, varargin )
        % Retreive data in segments.
        % Setup an iterative data retrieval process.  This function initiates the request for data in [gps_start, gps_stop), but does
        % not actually retreive the data.  Once iterate is called, then the data is retreived via the #next method.
        %
        % CONN.iterate( CHANNEL_NAMES )
        % CONN.iterate( STRIDE, CHANNEL_NAMES )
        % CONN.iterate( START, STOP, CHANNEL_NAMES )
        % CONN.iterate( START, STOP, STRIDE, CHANNEL_NAMES )
        %
        % INPUTS:
        %     START         : The start time of the request.
        %     STOP          : The end time of the request [gps_start, gps_stop).
        %     STRIDE        : The number of seconds of data to return in each chunk.
        %     CHANNEL_NAMES : The list of channels to retrieve data for.
        %
        % OUTPUTS:
        %     CONNECTION : A reference to the connection.
        %
            nVarargs = length(varargin);

            switch( nVarargs )
              case 1
                retval = this.getCon( ).iterate( varargin{1} );
              case 2
                retval = this.getCon( ).iterate( varargin{1}, varargin{2} );
              case 3
                retval = this.getCon( ).iterate( varargin{1}, varargin{2}, varargin{3} );
              case 4
                retval = this.getCon( ).iterate( varargin{1}, varargin{2}, varargin{3}, varargin{4} );
              otherwise
                msgID = 'connection.iterate:invalid_argument';
                msg = 'An incorrect number of arguments were passed';
                baseException = MException( msgID, msg );
                throw( baseException );
            end
            ITERATOR = ndsm.ConnectionIterator( retval );
        end % iterate

        function BUFFERS = next(this)
        % Retrieve the next data block from an iterate request
        % 
        % OUTPUTS:
        %   BUFFERS  : array of buffers
            jbuffers = this.getCon( ).next( );
            js = size( jbuffers, 1);
            if js ~= 0
                for n = 1:js
                    BUFFERS(n,1) = ndsm.Buffer( jbuffers( n, 1 ) );
                end
            end
        end % next

        function BOOL = requestinprogress(this)
        % Query the connection to see if a request is in progress.
        %
        % OUTPUTS
        %     BOOL : True if a request is in progress, else false.
        %
        % NOTES
        %     A connection can only do on request at a time.
        %
            BOOL = this.getCon( ).requestInProgress( );
        end % requestinprogress

        function BOOL = setepoch(this, varargin)
        % Set the epoch for the connection
        %
        % Given a epoch value described as a string set the current epoch.
        % All requests for data and channel lists will be constrained to this epoch.
        %
        %
        % INTPUTS
        %     DESC  : The epoch value as a string.
        %     START : The start time in seconds from the gps epoch.
        %     STOP  : The one second after the last second of the requeste epoch, in seconds fromt the gps epoch.
        %
        % OUTPUTS
        %     True if the epoch can be set
        %
        % NOTES
        %     * The epoch may be a named epoch, ie "O1" or a string with start/stop gps times, ie "1126621184-1137258496".
        %     * This command only works for NDS2, NDS1 servers will silently ignore it.
            nVarargs = length(varargin);

            switch( nVarargs )
              case 1
                BOOL = this.getCon( ).setEpoch( varargin{1} );
              case 2
                BOOL = this.getCon( ).setEpoch( varargin{1}, varargin{2} );
              otherwise
                msgID = 'connection.setEpoc:invalid_argument'
                msg = 'An incorrect number of arguments were passed'
                baseException = MException( msgID, msg );
                throw( baseException )
            end
        end % setepoch

        function BOOL = setparameter(this, parameter, value)
        % Change the default behavior of the connection.
        %
        % The connection object has a series of parameters that can be set.  Currently the parameters
        % that can be set are "ALLOW_DATA_ON_TAPE", "GAP_HANDLER", and "ITERATE_USE_GAP_HANDLERS"
        %
        % Parameters
        % ALLOW_DATA_ON_TAPE
        % NDS2 only.  The NDS2 server may serve data that resides on a high latency storage layer, such as a tape system.  This may lead
        % to data requests taking minutes or hours to complete, depending on the load on the storage system.  As of version 0.12 of the client
        % the default is to raise an error when accessing data that is on a high latency storage layer.  This allows the
        % application to provide feedback (if needed) to a users regarding amount of time that a request may take.  If this parameter is set to a 
        % true value ("True", "1", "yes") then an error will not be raised when requesting data on a high latency storage.
        %
        % GAP_HANDLER
        % For a given request there may not be be data available to fill the request completely.  This happens due to issues upstream of the
        % NDS server.  How this is handled is application specific.  Setting the "GAP_HANDLER" parameter allows the application to
        % specify what to do.  This includes options such as abort, zero fill the data, ...
        %
        % Available GAP_HANDLERS
        %  * "ABORT_HANDLER" This aborts the request when a gap is found in the data.
        %  * "STATIC_HANDLER_ZERO" This zero fills any missing data.
        %  * "STATIC_HANDLER_ONE" This fills any missing data with ones.
        %  * "STATIC_HANDLER_NAN" This fills any missing data with NaN values (or zero for integer channels).
        %  * "STATIC_HANDLER_POS_INF" This fills any missing data with +infinity (or the maximum integer value for integer channels).
        %  * "STATIC_HANDLER_NEG_INF" This fills any missing data with -infinity (or the minimum integer value for integer channels).
        %
        % ITERATE_USE_GAP_HANDLERS
        % The iterate methods have a special case.  Unlike fetch operations which work on a single block, the iterate methods retrieve chunks of data that
        % may not need to be contigous.  Setting ITERATE_USE_GAP_HANDLERS to "false" configures the connection to simply skip any gaps in the data and only return
        % the data that is available.
        %
        % Please note that if you are asking for multiple channels that do not have identical gaps the NDS servers will return a data not found error if ITERATE_USE_GAP_HANDLERS is set to false.
        %
        % INPUTS
        %     parameter : A parameter name as a string.
        %     value     : The value to set parameter to, as a string.
        %
        % OUTPUTS
        %     BOOL : True if the value could be set, else false.
        %
            BOOL = this.getCon( ).setParameter(parameter, value);
        end % setparameter

        function shutdown(this)
        % Manually close the connection
            this.getCon( ).shutdown();
        end % shutdown

    end
end

