classdef SimpleSegment < handle % -*- mode: octave; -*-
    % Contains minimal segment information
    properties (Access = private)
        % property attributes: http://www.mathworks.com/help/matlab/matlab_oop/property-attributes.html
        handle = 0;
        array_handle = 0;
    end
    
    methods (Hidden, Access = public)
        function HANDLE = gethandle(this)
            if isa(this.handle,'numeric')
                throw(MException('nds.connection:notopen','No valid channel for this operation'));
            end
            HANDLE = this.handle;
        end
    end
    
    methods (Access = public)
        
       function OBJ = SimpleSegment( varargin )
         % OBJ constructor for the ndsm.Availability class
         %
	 % OBJ = ndsm.SimpleSegment( )
	 %
	 % OBJ = ndsm.SimpleSegment( Instance )
	 %
	 % OBJ = ndsm.SimpleSegment( InstanceList )
	 %
	 %   INPUTS:
	 %     Instance     : Either an instance of ndsm.SimpleSegment or
	 %                    an instance of nds2.simple_segment
	 %     InstanceList : 
         %   OUTPUTS:
         %     OBJ : ndsm.Availability instance
	 switch( nargin )
	   case 0
	     h = nds2.segment( );
	   case 1
	     if ( isa( varargin{1}, 'nds2.simple_segment' ) )
	       h = varargin{1};
	     elseif ( isa( varargin{1}, 'ndsm.SimpleSegment' ) )
	       h = varargin{1}.gethandle( );
	     elseif ( isa( varargin{1}, 'nds2.simple_segment_list_type' ) )
	       for x = 1:size( varargin{1} )
		 OBJ(x) = ndsm.SimpleSegment( varargin{1}.get(x-1), varargin{1} );
	       end
	       return;
	     end
	   case 2
	     if ( isa( varargin{1}, 'nds2.simple_segment' ) && ...
		  isa( varargin{2}, 'nds2.simple_segment_list_type' ) )
	       h = varargin{1};
	       OBJ.array_handle = varargin{2};
	     else
	       h = nds2.simpleSegment( varargin{1}, varargin{2} );
	     end
	 end
	 OBJ.handle = h;
       end

       function RETVAL = getgpsstart( self )
         % Retrieves the GPS start time of the segment
	 %
	 %   RETVAL = seg.getgpsstart( );
         %
         %   INPUTS:
	 %     self   : Segment object
	 %
         %   OUTPUTS:
         %     RETVAL : The GPS start time
	 RETVAL = self.gethandle( ).getGpsStart( );
       end % getgpsstart

       function RETVAL = getgpsstop( self )
         % Retrieves the GPS stop time of the segment
	 %
	 %   RETVAL = seg.getgpsstop( );
         %
         %   INPUTS:
	 %     self   : Segment object
	 %
         %   OUTPUTS:
         %     RETVAL : The GPS stop time
	 RETVAL = self.gethandle( ).getGpsStop( );
       end % getgpsstop

       function RETVAL = tostring( self )
         % Name representation of the segment
	 %
	 %   RETVAL = seg.tostring( );
         %
         %   INPUTS:
	 %     self   : Segment object
	 %
         %   OUTPUTS:
         %     RETVAL : String representation of the segment
	 RETVAL = char( self.gethandle( ).toString( ) );
       end % function - tostring
    end
end
