# -*- mode: cmake; coding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=cmake:et:sw=4:ts=4:sts=4
if ( ENABLE_SWIG_JAVA AND ENABLE_SWIG_MATLAB )
  include(UseCopyIfDifferent)

  set(MATLAB_PACKAGE ndsm)

  set( MATLAB_SOURCES
      Depricated.m
      Availability.m
      Buffer.m
      Channel.m
      Connection.m
      ConnectionIterator.m
      Epoch.m
      Segment.m
      SimpleAvailabilityListType.m
      SimpleSegment.m
      version.m )
  copy_if_different(
      ${CMAKE_CURRENT_SOURCE_DIR}
      ${CMAKE_CURRENT_BINARY_DIR}/+${MATLAB_PACKAGE}
      "${MATLAB_SOURCES}"
      IncludeTargets "Includes" )
  add_custom_target(CopyIncludes ALL DEPENDS ${IncludeTargets})

  if( NOT WIN32 )
      configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/nds2-client-matlab.cfg.in
          ${CMAKE_CURRENT_BINARY_DIR}/nds2-client-matlab.cfg
          @ONLY )
      install(
          FILES ${CMAKE_CURRENT_BINARY_DIR}/nds2-client-matlab.cfg
          DESTINATION ${CMAKE_INSTALL_FULL_SYSCONFDIR}/${PROJECT_BASENAME}
          COMPONENT MATLAB
          )
  endif( )
  install(
      FILES ${MATLAB_SOURCES}
      DESTINATION "${CMAKE_INSTALL_DATAROOTDIR}/${PROJECT_BASENAME}/matlab/+${MATLAB_PACKAGE}"
      COMPONENT MATLAB
      )
endif ( )
