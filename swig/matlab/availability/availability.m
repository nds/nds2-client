function ep = availability( base ) % -*- mode: octave; -*-
  switch( nargin )
    case 0
      ep = ndsm.availability( );
    case 1
      switch( class( base ) )
	case 'nds.availability'
	  ep = ndsm.availability( base );
      end
    otherwise
      ep = ndsm.availability( );
  end
end
