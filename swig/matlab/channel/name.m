function value = name( self ) % -*- mode: octave; -*-
% NAME retrieve the name of the channel
%
%   Retrieves the name of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.name( )
end
