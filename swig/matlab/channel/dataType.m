function value = dataType( self ) % -*- mode: octave; -*-
% DATATYPE retrieve the dataType of the channel
%
%   Retrieves the dataType of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.dataType( )
end
