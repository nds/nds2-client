function value = units( self ) % -*- mode: octave; -*-
% DATATYPE retrieve the units of the channel
%
%   Retrieves the units of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.units( )
end
