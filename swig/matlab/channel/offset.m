function value = offset( self ) % -*- mode: octave; -*-
% DATATYPE retrieve the offset of the channel
%
%   Retrieves the offset of the channel
%
%   INPUTS:
%     SELF   : ndsm.channel
%
%   OUTPUTS:
%     VALUE
  value = self.offset( )
end
