#------------------------------------------------------------------------
# -*- mode: cmake -*-
#
# NOTE:
#   Must be called after including CPack
#------------------------------------------------------------------------
include( CMakeParseArguments )

function(_rpm_set_value VAR VALUE)
  if ( NOT ${VAR} )
    set( ${VAR} ${VALUE} CACHE INTERNAL "" FORCE )
  endif ( NOT ${VAR} )
endfunction(_rpm_set_value)

function(cx_packaging_rpm_spec_file)
  set(options)
  set(oneValueArgs INPUT OUTPUT)
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if(NOT ARG_INPUT)
    set(ARG_INPUT "${CMAKE_SOURCE_DIR}/config/cmake/${PROJECT_NAME}.spec.in")
  endif(NOT ARG_INPUT)
  if(NOT ARG_OUTPUT)
    set(ARG_OUTPUT "${CMAKE_BINARY_DIR}/config/cmake/${PROJECT_NAME}.spec")
  endif(NOT ARG_OUTPUT)
  string( REPLACE ";" "\n" PROJECT_DESCRIPTION_LONG "${PROJECT_DESCRIPTION_LONG}")
  configure_file( ${ARG_INPUT} ${ARG_OUTPUT} @ONLY )
endfunction(cx_packaging_rpm_spec_file)

#------------------------------------------------------------------------

function(ax_packaging_rpm)
  set(options)
  set(oneValueArgs RPM_SPEC_FILE_TEMPLATE)
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_RPM_SPEC_FILE_TEMPLATE )
    set( ARG_RPM_SPEC_FILE_TEMPLATE ${CMAKE_SOURCE_DIR}/config/cmake/${PROJECT_NAME}.spec.in )
  endif ( NOT ARG_RPM_SPEC_FILE_TEMPLATE )

  #======================================================================
  # Define CPack variables
  #----------------------------------------------------------------------
  _rpm_set_value(CPACK_RPM_PACKAGE_NAME ${PROJECT_NAME})
  _rpm_set_value(CPACK_RPM_PACKAGE_VERSION ${PROJECT_VERSION})
  _rpm_set_value(CPACK_RPM_PACKAGE_RELEASE 1)
  #----------------------------------------------------------------------
  # RPM Spec file
  #----------------------------------------------------------------------
  cx_packaging_rpm_spec_file( INPUT ${ARG_RPM_SPEC_FILE_TEMPLATE}
    		  	      OUTPUT ${PROJECT_SPEC_FILENAME}
  )
  if( "${CMAKE_GENERATOR}" MATCHES "Make" )
    add_custom_target( dist_body_rpm
      COMMAND ${CMAKE_COMMAND} -E copy ${PROJECT_SPEC_FILENAME} ${STAGING_DIR}/${TAR_DIR_NAME}/config/cmake/.
      DEPENDS dist_body_begin
      )
    add_dependencies( dist_body dist_body_rpm )
  endif (  )
  find_program(RPMBUILD rpmbuild)
  if (RPMBUILD)
    set(TAR_DIR_NAME "${PROJECT_NAME}-${${PROJECT_NAME}_VERSION}")
    add_custom_target(rpm
		      COMMAND ${CMAKE_COMMAND} -E echo "Building RPM package..."
		      COMMAND ${RPMBUILD} -v -ta --clean "${CMAKE_BINARY_DIR}/${CPACK_SOURCE_PACKAGE_FILE_NAME_FULL}")
    add_dependencies(rpm dist)
  else (RPMBUILD)
    add_custom_target(rpm
    		      COMMAND ${CMAKE_COMMAND} -E echo "This platform does not support building of RPM packages")
  endif(RPMBUILD)
endfunction(ax_packaging_rpm)
