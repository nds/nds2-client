#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_msg_debug( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/cm_msg_notice )

function(cm_msg_debug txt)
  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cm_msg_notice( "+++ DEBUG +++ ${txt}" )
endfunction(cm_msg_debug)
