#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_swig_python_module( ) - Create python module using SWIG
#
# INTERFACE_FILE - 
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/cm_msg_error )

function(cx_swig_python_module)
  #----------------------------------------------------------------------
  # Parse arguments
  #----------------------------------------------------------------------
  set(options
    )
  set(oneValueArgs
    INTERFACE_FILE
    MODULE
    MODULE_DIR
    PREFIX
    )
  set(multiValueArgs
    LINK_LIBRARIES
    SWIG_FLAGS
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_PREFIX )
    set( ARG_PREFIX PYTHON )
  endif( )
  if ( ARG_MODULE_DIR )
    set( ARG_MODULE_DIR "/${ARG_MODULE_DIR}" )
  endif ( ARG_MODULE_DIR )
  if ( NOT ARG_SWIG_FLAGS )
    set( ARG_SWIG_FLAGS "-shadow" "-importall" "-Wall" "-classic" "-threads" "-O" )
  endif ( NOT ARG_SWIG_FLAGS )

  #----------------------------------------------------------------------
  # Establish compiled characteristics
  #----------------------------------------------------------------------

  set(PYC_FILE_EXT ".pyc" )
  set(PYO_FILE_EXT ".pyo" )
  if (${${ARG_PREFIX}_VERSION_MAJOR} EQUAL 2)
    set(PYC_DIR "")
    set(PYC_FILE "${ARG_MODULE}${PYC_FILE_EXT}")
    set(PYO_FILE "${ARG_MODULE}${PYO_FILE_EXT}")
  elseif (${${ARG_PREFIX}_VERSION_MAJOR} EQUAL 3)
    if ( ${${ARG_PREFIX}_VERSION_MINOR} GREATER 4 )
      set( PYO_FILE_EXT ".opt-1.pyc" )
    endif ( ${${ARG_PREFIX}_VERSION_MINOR} GREATER 4 )
    set(PYC_DIR "/__pycache__")
    set(PYC_FILE "__pycache__/${ARG_MODULE}.cpython-${${ARG_PREFIX}_VERSION_MAJOR}${${ARG_PREFIX}_VERSION_MINOR}${PYC_FILE_EXT}")
    set(PYO_FILE "__pycache__/${ARG_MODULE}.cpython-${${ARG_PREFIX}_VERSION_MAJOR}${${ARG_PREFIX}_VERSION_MINOR}${PYO_FILE_EXT}")
  else (${${ARG_PREFIX}_VERSION_MAJOR} EQUAL 2)
    cm_msg_error( "This version of python is not yet supported for use in builds of ${ARG_MODULE}" )
  endif (${${ARG_PREFIX}_VERSION_MAJOR} EQUAL 2)

  #----------------------------------------------------------------------
  # 
  #----------------------------------------------------------------------

  set(CMAKE_SWIG_FLAGS ${ARG_SWIG_FLAGS})
  set( target_name "${ARG_MODULE}${${ARG_PREFIX}_VERSION_MAJOR}" )
  set_source_files_properties( ${ARG_INTERFACE_FILE}
    PROPERTIES
      CPLUSPLUS on )
  set_source_files_properties( ${ARG_MODULE}.cc PROPERTIES GENERATED true )

  if ( COMMAND SWIG_ADD_LIBRARY )
    swig_add_library( ${target_name}
      LANGUAGE python
      SOURCES ${ARG_INTERFACE_FILE} )
  else ( COMMAND SWIG_ADD_LIBRARY )
    swig_add_module( ${target_name} python ${ARG_INTERFACE_FILE} )
  endif ( COMMAND SWIG_ADD_LIBRARY )
  set_target_properties(${SWIG_MODULE_${target_name}_REAL_NAME}
    PROPERTIES
      OUTPUT_NAME _${ARG_MODULE})
  if ( APPLE )
    set_target_properties(${SWIG_MODULE_${target_name}_REAL_NAME}
      PROPERTIES
        INSTALL_RPATH "${CMAKE_INSTALL_FULL_LIBDIR}"
        BUILD_WITH_INSTALL_RPATH TRUE )
  endif( )
  # cm_msg_debug_variable(${ARG_PREFIX}_LIBRARIES)
  swig_link_libraries( ${target_name} ${${ARG_PREFIX}_LIBRARIES} ${ARG_LINK_LIBRARIES} )

  #----------------------------------------------------------------------
  # Compile .py files
  #----------------------------------------------------------------------

  add_custom_command(
    #--------------------------------------------------------------------
    # Compile python standard
    #--------------------------------------------------------------------
    OUTPUT
      ${CMAKE_CURRENT_BINARY_DIR}/${PYC_FILE}
    COMMAND
      ${${ARG_PREFIX}_EXECUTABLE} -m py_compile
        "${CMAKE_CURRENT_BINARY_DIR}/${ARG_MODULE}.py"
    DEPENDS ${SWIG_MODULE_${target_name}_REAL_NAME}
    )
  add_custom_command(
    #--------------------------------------------------------------------
    # Compile python with optimization
    #--------------------------------------------------------------------
    OUTPUT
      ${CMAKE_CURRENT_BINARY_DIR}/${PYO_FILE}
    COMMAND
      ${${ARG_PREFIX}_EXECUTABLE} -O -m py_compile
        "${CMAKE_CURRENT_BINARY_DIR}/${ARG_MODULE}.py"
    DEPENDS ${SWIG_MODULE_${ARG_MODULE}_REAL_NAME}
    )
  add_custom_target(
    ${target_name}_python_compile
    ALL
    DEPENDS
      ${CMAKE_CURRENT_BINARY_DIR}/${PYC_FILE}
      ${CMAKE_CURRENT_BINARY_DIR}/${PYO_FILE}
      )

  #----------------------------------------------------------------------
  # Installation rules
  #----------------------------------------------------------------------

  set(MODULE_REAL_NAME ${SWIG_MODULE_${target_name}_REAL_NAME})
  get_target_property( MODULE_OUTPUT_NAME ${MODULE_REAL_NAME} OUTPUT_NAME )
  get_target_property( MODULE_SUFFIX ${MODULE_REAL_NAME} SUFFIX )
  if ( NOT MODULE_SUFFIX )
    get_target_property( MODULE_SUFFIX ${MODULE_REAL_NAME} IMPORT_SUFFIX )
    if ( NOT MODULE_SUFFIX )
      set( MODULE_SUFFIX ${CMAKE_SHARED_MODULE_SUFFIX} )
    endif ( NOT MODULE_SUFFIX )
  endif ( NOT MODULE_SUFFIX )

  install(
    FILES
      ${CMAKE_CURRENT_BINARY_DIR}/${ARG_MODULE}.py
      ${CMAKE_CURRENT_BINARY_DIR}/${MODULE_OUTPUT_NAME}${MODULE_SUFFIX}
    DESTINATION
      "${PYTHON${${ARG_PREFIX}_VERSION_MAJOR}${${ARG_PREFIX}_VERSION_MINOR}_MODULE_INSTALL_DIR}${ARG_MODULE_DIR}"
    COMPONENT Python
    )
  install(
    FILES
      ${CMAKE_CURRENT_BINARY_DIR}/${PYC_FILE}
      ${CMAKE_CURRENT_BINARY_DIR}/${PYO_FILE}
    DESTINATION
      "${PYTHON${${ARG_PREFIX}_VERSION_MAJOR}${${ARG_PREFIX}_VERSION_MINOR}_MODULE_INSTALL_DIR}${ARG_MODULE_DIR}/${PYC_DIR}"
    COMPONENT Python
    )
endfunction(cx_swig_python_module)
