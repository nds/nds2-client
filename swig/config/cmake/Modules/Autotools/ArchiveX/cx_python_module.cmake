#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_python_module( )
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( Autotools/ArchiveX/cx_variable_strip_nl )

include( Autotools/cm_msg_checking )
include( Autotools/cm_msg_result )

function(cx_python_module)
  set(options
    REQUIRED
    )
  set(oneValueArgs
    MODULE
    PYTHON_EXECUTABLE
    PREFIX
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
  if ( NOT ARG_PREFIX )
    set( ARG_PREFIX PYTHON )
  endif ( )
  if ( NOT ARG_PYTHON_EXECUTABLE )
    set( ARG_PYTHON_EXECUTABLE ${${ARG_PREFIX}_EXECUTABLE} )
  endif ( )

  
  if ( ARG_PYTHON_EXECUTABLE )
    #--------------------------------------------------------------------
    # Checking for Python modules
    #--------------------------------------------------------------------
    cm_msg_checking( "for python ${${ARG_PREFIX}_VERSION_STRING} module: ${ARG_MODULE}" )
    execute_process( COMMAND
      ${ARG_PYTHON_EXECUTABLE} -c "import ${ARG_MODULE}"
      RESULT_VARIABLE var
      OUTPUT_VARIABLE var_stdout
      ERROR_VARIABLE var_stderr )
    if ( "${var_stdout}" STREQUAL "" AND var_stderr STREQUAL "")
      set(_ans "yes")
    else ( "${var_stdout}" STREQUAL "" AND var_stderr STREQUAL "")
      set(_ans "no")
    endif ( "${var_stdout}" STREQUAL ""  AND var_stderr STREQUAL "")
    cm_msg_result( "${_ans}" )
    if (NOT _ans AND ARG_REQUIRED )
      message( FATAL_ERROR "  failed to find required python module '${ARG_MODULE}'")
    endif (NOT _ans AND ARG_REQUIRED )
    if ( "${var_stdout}" STREQUAL "" )
      #------------------------------------------------------------------
      # Module found and is loadable.
      #------------------------------------------------------------------
      #------------------------------------------------------------------
      # Get additional information for some modules
      #------------------------------------------------------------------
      if ( ${ARG_MODULE} STREQUAL "numpy" )
	      #----------------------------------------------------------------
	      # NumPy
	      #----------------------------------------------------------------
	      cm_msg_debug_variable( ARG_PYTHON_EXECUTABLE )
	      execute_process( COMMAND
	        ${ARG_PYTHON_EXECUTABLE} -c "import ${ARG_MODULE}; print( numpy.get_include() );"
	        RESULT_VARIABLE var
	        OUTPUT_VARIABLE PYTHON_NUMPY_INCLUDE_PATH
	        ERROR_VARIABLE var_stderr )
	      cx_variable_strip_nl( PYTHON_NUMPY_INCLUDE_PATH )
	      # message( STATUS "PYTHON_NUMPY_INCLUDE_PATH: ${PYTHON_NUMPY_INCLUDE_PATH}" )
	      set(${ARG_PREFIX}_NUMPY_INCLUDE_PATH ${PYTHON_NUMPY_INCLUDE_PATH} PARENT_SCOPE)
	      if ( EXISTS "${PYTHON_NUMPY_INCLUDE_PATH}/numpy/arrayobject.h" )
	        set( _result true )
	      else ( EXISTS "${PYTHON_NUMPY_INCLUDE_PATH}/numpy/arrayobject.h" )
	        set( _result false )
	      endif ( EXISTS "${PYTHON_NUMPY_INCLUDE_PATH}/numpy/arrayobject.h" )
	      _cx_python_have_include( "numpy/arrayobject.h" _result )
      endif ( ${ARG_MODULE} STREQUAL "numpy" )
    endif ( "${var_stdout}" STREQUAL "" )
  endif ( )
endfunction(cx_python_module)
