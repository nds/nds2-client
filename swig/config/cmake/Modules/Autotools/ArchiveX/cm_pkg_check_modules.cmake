#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_pkg_check_modules
#
#------------------------------------------------------------------------
include( CMakeParseArguments )

find_package(PkgConfig)

function( private_pkg_check_modules )
  #----------------------------------------------------------------------
  # Parse arguments
  #----------------------------------------------------------------------
  set(options
    QUIET
    REQUIRED
    )
  set(oneValueArgs
    PREFIX
    )
  set(multiValueArgs
    MODULES
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( ARG_REQUIRED )
    set(ARG_REQUIRED REQUIRED)
  else ( ARG_REQUIRED )
    unset(ARG_REQUIRED)
  endif ( ARG_REQUIRED )
  if ( ARG_QUIET )
    set(ARG_QUIET QUIET)
  else ( ARG_QUIET )
    unset(ARG_QUIET)
  endif ( ARG_QUIET )


endfunction( )

function(cm_pkg_check_modules)
  #----------------------------------------------------------------------
  # Parse arguments
  #----------------------------------------------------------------------
  set(options
    QUIET
    REQUIRED
    )
  set(oneValueArgs
    PREFIX
    )
  set(multiValueArgs
    MODULES
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( ARG_REQUIRED )
    set(ARG_REQUIRED REQUIRED)
  else ( ARG_REQUIRED )
    unset(ARG_REQUIRED)
  endif ( ARG_REQUIRED )
  if ( ARG_QUIET )
    set(ARG_QUIET QUIET)
  else ( ARG_QUIET )
    unset(ARG_QUIET)
  endif ( ARG_QUIET )

  pkg_check_modules( ${ARG_PREFIX}
    ${ARG_REQUIRED} ${ARG_QUIET}
    ${ARG_MODULES}
    )
  if ( ${ARG_PREFIX}_FOUND )
    foreach(lib ${${ARG_PREFIX}_LIBRARIES})
      find_library(__JUNK__ NAMES ${lib}
	HINTS ${${ARG_PREFIX}_LIBRARY_DIRS} )
      if ( __JUNK__)
	list(APPEND ${ARG_PREFIX}_LIBRARIES_FULL_PATH ${__JUNK__})
      endif ( __JUNK__)
      unset(__JUNK__ CACHE)
    endforeach(lib)
    set( ${ARG_PREFIX}_FOUND ${${ARG_PREFIX}_FOUND} PARENT_SCOPE )
    set( ${ARG_PREFIX}_LIBRARIES ${${ARG_PREFIX}_LIBRARIES} PARENT_SCOPE )
    set( ${ARG_PREFIX}_LIBRARIES_FULL_PATH ${${ARG_PREFIX}_LIBRARIES_FULL_PATH} PARENT_SCOPE )
    set( ${ARG_PREFIX}_LIBRARY_DIRS ${${ARG_PREFIX}_LIBRARY_DIRS} PARENT_SCOPE )
    set( ${ARG_PREFIX}_LDFLAGS ${${ARG_PREFIX}_LDFLAGS} PARENT_SCOPE )
    set( ${ARG_PREFIX}_LDFLAGS_OTHER ${${ARG_PREFIX}_LDFLAGS_OTHER} PARENT_SCOPE )
    set( ${ARG_PREFIX}_INCLUDE_DIRS ${${ARG_PREFIX}_INCLUDE_DIRS} PARENT_SCOPE )
    set( ${ARG_PREFIX}_CFLAGS ${${ARG_PREFIX}_CFLAGS} PARENT_SCOPE )
    set( ${ARG_PREFIX}_CFLAGS_OTHER ${${ARG_PREFIX}_CFLAGS_OTHER} PARENT_SCOPE )

    set( CMAKE_FIND_LIBRARY_PREFIXES "lib" )
    set( CMAKE_FIND_LIBRARY_SUFFIXES ".a" )
    foreach(lib ${${ARG_PREFIX}_STATIC_LIBRARIES})
      find_library(__JUNK__ NAMES ${lib}
	HINTS ${${ARG_PREFIX}_STATIC_LIBRARY_DIRS}
	)
      if ( __JUNK__)
	list(APPEND ${ARG_PREFIX}_STATIC_LIBRARIES_FULL_PATH ${__JUNK__})
      endif ( __JUNK__)
      unset(__JUNK__ CACHE)
    endforeach(lib)
    unset( CMAKE_FIND_LIBRARY_SUFFIXES )
    unset( CMAKE_FIND_LIBRARY_PREFIXES )
    set( ${ARG_PREFIX}_STATIC_FOUND ${${ARG_PREFIX}_STATIC_FOUND} PARENT_SCOPE )
    set( ${ARG_PREFIX}_STATIC_LIBRARIES ${${ARG_PREFIX}_STATIC_LIBRARIES} PARENT_SCOPE )
    set( ${ARG_PREFIX}_STATIC_LIBRARIES_FULL_PATH ${${ARG_PREFIX}_STATIC_LIBRARIES_FULL_PATH} PARENT_SCOPE )
    set( ${ARG_PREFIX}_STATIC_LIBRARY_DIRS ${${ARG_PREFIX}_STATIC_LIBRARY_DIRS} PARENT_SCOPE )
    set( ${ARG_PREFIX}_STATIC_LDFLAGS ${${ARG_PREFIX}_STATIC_LDFLAGS} PARENT_SCOPE )
    set( ${ARG_PREFIX}_STATIC_LDFLAGS_OTHER ${${ARG_PREFIX}_STATIC_LDFLAGS_OTHER} PARENT_SCOPE )
    set( ${ARG_PREFIX}_STATIC_INCLUDE_DIRS ${${ARG_PREFIX}_STATIC_INCLUDE_DIRS} PARENT_SCOPE )
    set( ${ARG_PREFIX}_STATIC_CFLAGS ${${ARG_PREFIX}_STATIC_CFLAGS} PARENT_SCOPE )
    set( ${ARG_PREFIX}_STATIC_CFLAGS_OTHER ${${ARG_PREFIX}_STATIC_CFLAGS_OTHER} PARENT_SCOPE )
  endif ( ${ARG_PREFIX}_FOUND )

endfunction(cm_pkg_check_modules)
