#!/bin/bash

echo "Inserting CI information from " `pwd`

DT=`date -u +%Y%m%d%H%M`

dch -p -c debian/changelog -v "$1.$DT-0" "CI Build"
dch -p -c debian/changelog -r "CI Build"

sed -i "s/##CI_code_version_update/set( PACKAGE_VERSION \"\$\{PACKAGE_VERSION\}.$DT\")/g" CMakeLists.txt
