% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

nds2;
UnitTest;

try
  unit_test_ARGV = argv;

  hostname = unit_test_hostname( );
  port = unit_test_port( );
  if ( unit_test_hasoption( '-proto-1' ) )
    protocol = nds2.connection.PROTOCOL_ONE;
  elseif ( unit_test_hasoption( '-proto-2' ) )
    protocol = nds2.connection.PROTOCOL_TWO;
  else
    protocol = nds2.connection.PROTOCOL_TWO;
  end


  %----------------------------------------------------------------------
  % Establish the connection
  %----------------------------------------------------------------------
	   
  conn = nds2.connection(hostname, port, protocol );

  %----------------------------------------------------------------------
  % Run the test
  %----------------------------------------------------------------------

  last_start = 0;

  if( protocol == nds2.connection.PROTOCOL_ONE )
    %--------------------------------------------------------------------
    % connection.PROTOCOL_ONE
    %--------------------------------------------------------------------
    start = 1130797740;
    stride = 3000;
    finish = start + stride * 3;
    samples_per_segment = stride / 60;
    channels = { 'X1:CDS-DACTEST_COSINE_OUT_DQ.n,m-trend'
		};
  else
    %--------------------------------------------------------------------
    % Default is to use connection.PROTOCOL_TWO
    %--------------------------------------------------------------------
    start = 1116286100;
    finish = start + 200;
    stride = 50;
    data_start = 1116286200;
    samples_per_segment = stride;
    channels = { 'H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend'
		};
  end

  conn.set_parameter('ITERATE_USE_GAP_HANDLERS', 'false');

  iter = conn.iterate(start, finish, channels)
  try
    while( true )
      % Read off buffers
      bufs = iter.next( );
    end
  catch err
    if ( ! strcmp( "No Next (SWIG_IndexError)", err.message ) )
      rethrow( lasterror );
    end
  end

  count = 0;
  iter = conn.iterate(start, finish, channels)
  try
    while( true )
      % Read off buffers
      bufs = iter.next( );
      count = count + 1;
    end
  catch err
    if ( ! strcmp( "No Next (SWIG_IndexError)", err.message ) )
      rethrow( lasterror );
    end
  end

  conn.set_parameter('ITERATE_USE_GAP_HANDLERS', 'true');
  conn.set_parameter('GAP_HANDLER', 'STATIC_HANDLER_ZERO');

  iter = conn.iterate(start, finish, stride, channels);
  try
    while( true )
      bufs = iter.next( );
      unit_test_check( length( bufs ), ...
		       1, ... 
		       'Stride returns 1 buffer' );
      if( last_start == 0 )
	unit_test_check( bufs{1}.start( ), ...
			 start, ...
			 'Validate initial GPS start' );
      else
	unit_test_check( bufs{1}.start( ), ...
			 last_start + stride, ...
			 'Validate offset GPS start' );
      end
      unit_test_check( bufs{1}.stop( ) == finish || ...
		       bufs{1}.samples( ) == samples_per_segment, ...
		       true, ...
		       'Verify lenth of buffer' );
      last_start = bufs{1}.start( );
    end
  catch err
    if ( ! strcmp( "No Next (SWIG_IndexError)", err.message ) )
      rethrow( lasterror );
    end
  end

  %----------------------------------------------------------------------
  % Finish
  %----------------------------------------------------------------------
  conn.close( );
catch err
  s = unit_test_dump_stack( err.stack );
  unit_test_msginfo( s );
  unit_test_check( false, ...
		   true, ...
		   sprintf( "Caught unexpected excption: %s\n%s", ...
			   err.message, ...
			   s ) );
end

unit_test_exit( );
