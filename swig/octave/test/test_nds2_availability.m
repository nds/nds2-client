% -*- mode: octave -*-
% -------------------------------------------------------------------
%   Simple test to verify availability function
% -------------------------------------------------------------------

function test_nds2_availability_matlab( varargin )
  unit_test = UnitTestClass;

  unit_test = unit_test.initialize( varargin );

  hostname = unit_test.hostname( );
  port = unit_test.port( );
  protocol = nds2.connection.PROTOCOL_TWO;

  conn = nds2.connection(hostname, port, protocol );

  gps_start = 1116733655;
  gps_stop = 1116733675;

  cn = {
	'H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ',
	'H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ',
	'H1:SUS-ETMX_M0_MASTER_OUT_F3_DQ',
	'H1:SUS-ETMX_M0_MASTER_OUT_LF_DQ',
	'H1:SUS-ETMX_M0_MASTER_OUT_RT_DQ',
	'H1:SUS-ETMX_M0_MASTER_OUT_SD_DQ',
	'H1:SUS-ETMX_L1_MASTER_OUT_UL_DQ',
	'H1:SUS-ETMX_L1_MASTER_OUT_UR_DQ',
	'H1:SUS-ETMX_L1_MASTER_OUT_LL_DQ',
	'H1:SUS-ETMX_L1_MASTER_OUT_LR_DQ',
	'H1:SUS-ETMX_L2_MASTER_OUT_UL_DQ',
	'H1:SUS-ETMX_L2_MASTER_OUT_UR_DQ',
	'H1:SUS-ETMX_L2_MASTER_OUT_LL_DQ',
	'H1:SUS-ETMX_L2_MASTER_OUT_LR_DQ',
	'H1:SUS-ETMX_L3_MASTER_OUT_UL_DQ',
	'H1:SUS-ETMX_L3_MASTER_OUT_UR_DQ'
	};

  conn.setEpoch(gps_start, gps_stop);

  avail = conn.getAvailability( cn );

  unit_test.msg_info( '%s', avail.toString( ) );
  unit_test = unit_test.check( length( cn ) == avail.size( ), ...
			       'number of epochs( %d) =?= number of channel names (%d)', ...
                               length( cn ), avail.size( ) );

	    
  for i = 1:length( cn )
    entry = avail.get( i - 1 );
%    unit_test = unit_test.check( strcmp( entry.getName( ), cn( i ) ), ...
%				 'entry.getName( %s ) =?= cn( %s)', ...
%                                 entry.getName( ), cn( i ) );
    unit_test = unit_test.check( entry.getData( ).size( ) == 2, ...
				 'entry.getData( ).size(%d ) =?= %d', ...
                                 entry.getData( ).size( ), 2 );
    expected = 'H-H1_C';
    actual = entry.getData( ).get( 0 ).getFrameType( );
    unit_test = unit_test.check( strcmp( actual, expected ), ...
				 'frameType(0): actual(%s) =?= expected(%s)', ...
                                 actual, expected );
    expected = 'H-H1_R';
    actual = entry.getData( ).get( 1 ).getFrameType( );
    unit_test = unit_test.check( strcmp( actual, expected ), ...
				 'frameType(1): actual(%s) =?= expected(%s)', ...
                                 actual, expected );
    % -------------------------------------------------------------------
    unit_test = unit_test.check( entry.getData( ).get( 0 ).getGpsStart( ) == ...
				 entry.getData( ).get( 1 ).getGpsStart( ), ...
				 'start time equal: offset-0(%d) =?= offset-1(%d)', ...
                                 entry.getData( ).get( 0 ).getGpsStart( ), ...
				 entry.getData( ).get( 1 ).getGpsStart( ) );
    unit_test = unit_test.check( entry.getData( ).get( 0 ).getGpsStart( ) == ...
				 gps_start, ...
				 'start time correct: actual(%d) =?= expected(%d)', ...
                                 entry.getData( ).get( 0 ).getGpsStart( ), ...
				 gps_start );
    % -------------------------------------------------------------------
    unit_test = unit_test.check( entry.getData( ).get( 0 ).getGpsStop( ) == ...
				 entry.getData( ).get( 1 ).getGpsStop( ), ...
				 'stop time equal: offset-0(%d) =?= offset-1(%d)', ...
                                 entry.getData( ).get( 0 ).getGpsStop( ), ...
				 entry.getData( ).get( 1 ).getGpsStop( ) );
    unit_test = unit_test.check( entry.getData( ).get( 0 ).getGpsStop( ) == ...
				 gps_stop, ...
				 'stop time correct: actual(%d) =?= expected(%d)', ...
                                 entry.getData( ).get( 0 ).getGpsStop( ), ...
				 gps_stop );
  end

  if ( true )
    expected = '( <H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ ( H-H1_C:1116733655-1116733675 H-H1_R:1116733655-1116733675 ) >';
    actual = avail.toString( ).substring( 0, length( expected ) );

    unit_test = unit_test.check( strcmp( actual,  expected ), ...
				 'actual(%s) =?= expected(%s)', ...
			         actual, expected );
  end

  if ( true )
    expected = '<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ ( H-H1_C:1116733655-1116733675 H-H1_R:1116733655-1116733675 ) >';
    actual = avail.get( 0 ).toString( );

    unit_test.msg_info( 'actual: %s expected: %s', actual, expected );

    assert( actual.equals( expected ) );
  end

  if ( true )
    test_avail = nds2.availability_list_type( );
    entry = nds2.availability( );

    entry.setName( 'X1:TEST_1' );
    entry.getData( ).pushBack( nds2.segment( 'X-X1_C',  gps_start, gps_start + 1024) );
    entry.getData( ).pushBack( nds2.segment( 'X-X1_R',  gps_start, gps_start + 100) );
    entry.getData( ).pushBack( nds2.segment( 'X-X1_R',  gps_start + 1024, gps_start + 1024+100) );
    test_avail.pushBack( entry );
	    
    simple_test = test_avail.simpleList( );
	    
    if ( true )
      expected = '( ( 1116733655-1116734779 ) )';
      actual = simple_test.toString( );

      unit_test = unit_test.check( strcmp( actual, expected ), ...
				   'simple_list range: actual(%s) =?= expected(%s)', ...
				   actual, expected );
    end

    expected = 1;
    actual = simple_test.size( );
    unit_test = unit_test.check( actual ==  expected, ...
				 'simple_list size: actual(%d) =?= expected(%d)', ...
				 actual, expected );
    expected = 1;
    actual = simple_test.get( 0 ).size( );
    unit_test = unit_test.check( actual ==  expected, ...
				 'simple_list[ 0 ] size: actual(%d) =?= expected(%d)', ...
				 actual, expected );
    expected = gps_start;
    actual = simple_test.get( 0 ).get( 0 ).getGpsStart( );
    unit_test = unit_test.check( actual ==  expected, ...
				 'simple_list[ 0 ]  GPS Start: actual(%d) =?= expected(%d)', ...
				 actual, expected );
    expected = gps_start + 1024 + 100;
    actual = simple_test.get( 0 ).get( 0 ).getGpsStop( );
    unit_test = unit_test.check( actual ==  expected, ...
				 'simple_list[ 0 ]  GPS Stop: actual(%d) =?= expected(%d)', ...
				 actual, expected );
  end

  % ---------------------------------------------------------------------
  % test the implicit availability call in fetch
  % ---------------------------------------------------------------------
  bufs = conn.fetch( gps_start, gps_stop, cn );

  unit_test.msg_info( bufs.toString( ) );
  unit_test.msg_info( bufs( 1 ).toString( ) );

  if ( false ) % DISABLED because bufs.toString() is not returning the correct value
    expected = '(''<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ (GPS time 1116733655, 10240 samples)>'',''<H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ (GPS time 1116733655, 10240 samples)>'',''';
    actual = bufs.toString( ).substring( 0, length( expected ) );

    unit_test = unit_test.check( strcmp( actual,  expected ), ...
				 'actual(%s) =?= expected(%s)', ...
			         actual, expected );
  end

  if ( true )
    expected = '<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ (512Hz, RAW, FLOAT32)>';
    actual = bufs( 1 ).toString( );

    unit_test = unit_test.check( strcmp( actual,  expected ), ...
				 'actual(%s) =?= expected(%s)', ...
			          actual, expected );
  end

  %------------------------------------------------------------------------
  % Test a larger availability.  This particular one will be > 128k
  %------------------------------------------------------------------------
  conn.setEpoch(0, 1999999999);
  cn = { 'H1:GDS-CALIB_STRAIN,reduced'};
  avail = conn.getavailability( cn );


  conn.close( );
  unit_test.exit( );
end
