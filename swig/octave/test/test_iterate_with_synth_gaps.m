% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------
nds2;
UnitTest;

%------------------------------------------------------------------------
% Utility functions
%------------------------------------------------------------------------

function RETVAL = isgapsecond( GPS )
  global START;

  gps = GPS - START;
  if ( (gps < 1) ...
      || (gps == 9 ) ...
      || (gps == 18 ) ...
      || (gps >= 32 && gps < 40) ...
      || (gps >= 50 && gps < 61) ...
      || (gps > 67) )
    RETVAL = true;
  else
    RETVAL = false;
  end
end

function RETVAL = checkdata( Buf, Value )
  global  unit_test;

  samples_per_sec = Buf.sample_rate( );
  CHANNEL_TYPE = Buf.data_type( );

  unit_test_msginfo( 'samples_per_sec: %d', ...
		     int32(samples_per_sec) );
  offset = 1;
  %======================================================================
  % MATLAB loops are inclusive so need to trim
  % the tail by 1 sec
  %======================================================================
  rs = Buf.start( );
  re = Buf.stop( ) - 1;
  for cur_gps = rs:re
    expected = Value;
    if ( isgapsecond( cur_gps ) )
      expected = 0.0;
    end
    d = Buf.data( );
    for i = 1:samples_per_sec
      if ( d( offset ) ~= expected )
	RETVAL = false;
	break
      end
      offset = offset + 1;
    end
  end
  RETVAL = true;
end

%------------------------------------------------------------------------
% Main program
%------------------------------------------------------------------------
try
  unit_test_ARGV = argv;

  hostname = unit_test_hostname( );
  port = unit_test_port( );
  protocol = 'unknown';
  use_gap_handler = 'true';
  NO_GAPS=true;
  set_handlers = true;

  %----------------------------------------------------------------------
  % Need to adjust according to parameter
  %----------------------------------------------------------------------
  if ( unit_test_hasoption( '-proto-1' ) )
    protocol = nds2.connection.PROTOCOL_ONE;
  end
  if ( unit_test_hasoption( '-proto-2' ) )
    protocol = nds2.connection.PROTOCOL_TWO;
  end
  if ( unit_test_hasoption( '-default-gap-handling' ) )
    set_handlers = false;
    NO_GAPS = false;
  elseif ( unit_test_hasoption( '-no-gap' ) )
    use_gap_handler = 'false';
    NO_GAPS = false;
  end

  %----------------------------------------------------------------------
  % Establish the connection
  %----------------------------------------------------------------------
	   
  conn = nds2.connection(hostname, port, protocol );
  if ( set_handlers )
    conn.set_parameter( 'ITERATE_USE_GAP_HANDLERS', use_gap_handler );
    conn.set_parameter( 'GAP_HANDLER', 'STATIC_HANDLER_ZERO' );
  end

  %----------------------------------------------------------------------
  % Run the test
  %----------------------------------------------------------------------
  START = 1770000000;
  STOP = START + 70;
  STRIDE = 10;
  CHANNELS = { 'X1:PEM-1',
	      'X1:PEM-2'
	    };
  EXPECTED_I = 6;

  last_start = 0;
  cur_gps = START;

  i = 1;
  iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
  try
    while( true )
      bufs = iter.next( );
      comp = bufs{1}.start( ) >= cur_gps;
      unit_test_check( true, comp, ...
 		       [ sprintf( 'Verifying GPS second of buffer 1: %d >?= %d', ...
 			          bufs{1}.start( ), ...
 			          cur_gps ) ] );
      if( NO_GAPS )
	unit_test_check( bufs{1}.gps_seconds( ), ...
 			 cur_gps, ...
			 'Verifying GPS second of buffer 1 when no gaps' );
      end
      delta = bufs{1}.stop( ) - bufs{1}.start( );
      %------------------------------------------------------------------
      unit_test_check( bufs{1}.samples( ), ...
 		       256 * delta, ...
 		       'Verifying samples of buffer 1: ' );
      unit_test_check( true, ...
		       [ checkdata( bufs{1}, 1.5 ) ], ...
  		       'Verifying data of buffer 1: ' );
      %------------------------------------------------------------------
      unit_test_check( bufs{2}.samples( ), ...
 		       512 * delta, ...
 		       'Verifying samples of buffer 2: ' );
      unit_test_check( true, ...
		       [checkdata( bufs{2}, 2.75 ) ], ...
 		       'Verifying data of buffer 2: ' );
      %------------------------------------------------------------------
      cur_gps = bufs{1}.stop( );
      i = i + 1;
    end
  catch err
    if ( ! strcmp( "No Next (SWIG_IndexError)", err.message ) )
      rethrow( lasterror );
    end
  end
  unit_test_check( true, [ i >= EXPECTED_I ], ...
 		   'Verifying number of iterations' );
  if ( NO_GAPS )
    unit_test_check( cur_gps, ...
 		     STOP, ...
 		     'Verifying final GPS time' );
  end
 
  %----------------------------------------------------------------------
  % Make sure we can do other operations
  %----------------------------------------------------------------------

  conn.set_epoch( START, STOP );
  conn.find_channels( 'X1:PEM-1' );
  conn.find_channels( 'X1:PEM-2' );
 
  %----------------------------------------------------------------------
  % Finish
  %----------------------------------------------------------------------

  conn.close( );
catch err
  s = unit_test_dump_stack( err.stack );
  unit_test_msginfo( s );
  unit_test_check( false, ...
		   true, ...
		   sprintf( "Caught unexpected excption: %s\n%s", ...
			   err.message, ...
			   s ) );
end

unit_test_exit( );
