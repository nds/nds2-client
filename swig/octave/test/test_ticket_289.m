% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

try
  nds2;
  UnitTest;
catch err
  fdisp( stderr, ...
        sprintf( "Caught unexpected exception: %s\n", ...
		 err.message ) );
end

try
  unit_test_ARGV = argv;

  hostname = unit_test_hostname( );
  port = unit_test_port( );

  %----------------------------------------------------------------------
  % Establish the connection
  %----------------------------------------------------------------------

  conn = nds2.connection(hostname, port );
  conn.set_parameter("ALLOW_DATA_ON_TAPE", "true");
  conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "false");

  %----------------------------------------------------------------------
  % Ticket 289
  %----------------------------------------------------------------------

  chans = conn.find_channels( "*", nds2.channel.CHANNEL_TYPE_RDS );
  unit_test_check( length( chans ), 3, ...
		  "Correct number of channels" );
  unit_test_check( chans{ 1 }.name( ), "X1:PEM-1", ...
		  "Validate channel zero name" );
%chans{ 1 }.data_type( )
nds2.channel.DATA_TYPE_UNKNOWN
%  unit_test_check( chans{ 1 }.data_type, nds2.channel.DATA_TYPE_UNKNOWN, ...
%		  "Validate channel zero data type" );
  unit_test_check( chans{ 3 }.name( ), "X1:PEM-3", ...
		  "Validate channel two name" );
%  unit_test_check( chans{ 3 }.data_type( ), nds2.channel.DATA_TYPE_FLOAT32, ...
%		  "Validate channel two data type" );

  %----------------------------------------------------------------------
  % Finish
  %----------------------------------------------------------------------
  conn.close( );
catch err
  s = unit_test_dump_stack( );
  unit_test_check( false, ...
		  true, ...
		  sprintf( "Caught unexpected exception: %s\n%s", ...
			  err.message, ...
			  s ) );
end
% exit( unit_test_exitcode( ) );
