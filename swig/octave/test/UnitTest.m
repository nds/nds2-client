% -*- mode: octave -*-
1;

global unit_test_VERBOSE_LEVEL = 0;
global unit_test_STATUS = 0;
global unit_test_ARGV = argv;

function VALUE = unit_test__logicaltostring( l )
  if ( l )
    v1 = '1';
  else
    v1 = '0';
  end
  VALUE = v1;
endfunction

function RETVAL = unit_test_dump_stack(  )
  RETVAL = "";
  err = lasterror( )
  for i = err.stack
    RETVAL = sprintf( "%s  FILE: %s NAME: %s LINE: %d\n", RETVAL, i.file, i.name, i.line );
  end
endfunction

function RETVAL = unit_test_hasoption( Option )
  global unit_test_ARGV;

  v = unit_test_ARGV;
  try
    RETVAL = validatestring( Option, v );
    if ( ( ischar( RETVAL ) ) && ( strcmp( RETVAL, Option ) ) )
      RETVAL = true;
    else
      RETVAL = false;
    end
  catch ME
    RETVAL = false;
  end
end

function VALUE = unit_test_hostname( )
  VALUE = getenv( 'NDS_TEST_HOST' );
  if ( isempty( VALUE ) );
    VALUE = 'localhost';
  end
endfunction

function VALUE = unit_test_port( )
  VALUE = getenv( 'NDS_TEST_PORT' );
  if ( isempty( VALUE ) )
    VALUE = 31200;
  else
    VALUE = str2num( VALUE );
  end
endfunction

function unit_test_check( varargin )
  global unit_test_STATUS;

  condition = true;
  frmt = '';
  switch( length( varargin ) )
    case 3
      s1 = '';
      s2 = '';
      if ( ( unit_test_checkstring( varargin{1} )
	    || ischar( varargin{1} ) )
	  &&
	  ( unit_test_checkstring( varargin{2} )
	   || ischar( varargin{2} ) ) )
	condition = strcmp( varargin{1}, varargin{2} );
	s1 = char( varargin{1} );
	s2 = char( varargin{2} );
      elseif ( ( islogical( varargin{1} ) && ( islogical( varargin{2} ) ) ) )
	condition = ( varargin{1} == varargin{2} );
	s1 = unit_test__logicaltostring( varargin{1} );
	s2 = unit_test__logicaltostring( varargin{2} );
      elseif ( ( isnumeric( varargin{1} ) && ( isnumeric( varargin{2} ) ) ) )
	epsilon = 0.0001;
	condition = ( ( varargin{1} == varargin{2} )
		     || ( [ abs( varargin{1} - varargin{2} ) ] < epsilon ) );
	s1 = num2str( varargin{1} );
	s2 = num2str( varargin{2} );
      end
      frmt = sprintf( '%s [%s =?= %s]',
		     varargin{3}, s1, s2 );
  endswitch
  if ( condition )
    lead = '-- PASS: ';
  else
    lead = '-- FAIL: ';
    unit_test_STATUS = 1
  end
  frmt = sprintf( '%s%s',
		 lead, frmt  );
  unit_test_message( frmt );
endfunction

function VALUE = unit_test_checkstring( s )
  if ( exist('isstring'))
    VALUE = isstring(s);
  else
    VALUE = ischar(s);
  end
endfunction

function unit_test_exit( )
  global unit_test_STATUS;

  % ---------------------------------------------------------------------
  % Leave according to how testing completed
  % NOTE: Currently, octave will sig abort if exit is called from script
  % ---------------------------------------------------------------------
  % exit( unit_test_STATUS );
endfunction% function - exit

function VALUE = unit_test_exitcode( )
  global unit_test_STATUS;

  % ---------------------------------------------------------------------
  % Leave according to how testing completed
  % NOTE: Currently, octave will sig abort if exit is called from script
  % ---------------------------------------------------------------------
  VALUE = unit_test_STATUS
endfunction% function - exit

function unit_test_message( text )
  fdisp( stderr, text );
endfunction

function unit_test_msgdebug( level, frmt, varargin )
  global unit_test_VERBOSE_LEVEL

  if ( unit_test_VERBOSE_LEVEL >= level )
    if( length( varargin ) > 0 )
      frmt = sprintf( frmt, varargin{:} );
    end
    frmt = sprintf( "-- DBUG: %s", frmt );
    unit_test_message( frmt );
  end % unit_test_VERBOSE_LEVEL >= level
endfunction

function unit_test_msginfo( frmt, varargin )
  if( length( varargin ) > 0 )
    frmt = sprintf( frmt, varargin{:} );
  end
  frmt = sprintf( "-- INFO: %s", frmt );
  unit_test_message( frmt );
endfunction % unit_test_msginfo
