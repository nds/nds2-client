/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 * This file allows for SWIG specialization for Octave
 */
#if defined(SWIGOCTAVE)

/* Make all functions and non-constant fields camel-case. */
/* Follow MATLAB naming convention for compatability */
%rename("%(undercase)s", %$isclass, %$ismember, %$isvariable, %$not %$hasconsttype) "";
%rename("%(undercase)s", %$isfunction) "";

#-------

%define %nds_doc_brief(TEXT)
%enddef

%define %nds_doc_details(TEXT)
%enddef

%define %nds_doc_remark(TEXT)
%enddef

%define %nds_doc_returns(TEXT)
%enddef

%define %nds_type_gps_seconds()
%enddef

%define %nds_type_sample_rate()
%enddef

%define %nds_doc_param(CALL,TYPE,DESC)
%enddef

%define %nds_doc_brief(TEXT)
%enddef

%define %nds_doc_details(TEXT)
%enddef

%define %nds_doc_remark(TEXT)
%enddef
%define %nds_doc_returns(TEXT)
%enddef

#-------

%{
  /* Turn off warnings for which nothing can be done */
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wc++1z-compat-mangling"
%}

%include "nds_octave_macros.i"
%include "nds_octave_buffer.i"
%include "nds_octave_channel.i"
%include "nds_octave_connection.i"

%include "nds.i"

#endif /* defined(SWIGOCTAVE) */
