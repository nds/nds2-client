#ifndef NDS_OCTAVE_VECTOR_TO_ARRAY_I
#define NDS_OCTAVE_VECTOR_TO_ARRAY_I

// ======================================================================
//   Present a std::vector as an array
// ======================================================================
%define %nds_vector_to_array(Vector, ValueType)
#if 1
  %typemap(out) Vector ( Cell temp ) {
    //-------------------------------------------------------------------
    // Convert Vector to ValueType[]
    //-------------------------------------------------------------------
    temp = Cell( $1.size( ), 1 );
    int	i = 0;
    for ( $1_basetype::iterator
            cur = $1.begin( ),
            last = $1.end( );
          cur != last;
          ++cur,
	  ++i )
    {
      std::shared_ptr<  ValueType > *smartcur = new std::shared_ptr<  ValueType >( *cur );
      _outv = SWIG_NewPointerObj(SWIG_as_voidptr(smartcur), $descriptor(std::shared_ptr< ValueType >*), 1 | SWIG_POINTER_OWN);
      temp( i, 0 ) = _outv;
    }
    $result = temp;
  }

  typedef std::vector< std::shared_ptr< ValueType > > Vector;

#else /* 0 */
  %nds_vect_typemap(Vector, ValueType);
  %nds_vect_p_typemap(Vector, ValueType);
#endif /* 0 */
%enddef /* %nds_vector_to_array(Vector, ValueType) */

#endif /* NDS_OCTAVE_VECTOR_TO_ARRAY_I */

