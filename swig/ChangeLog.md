nds2-client-swig-NEXT
-----------------------------
  * Added JAVA compatibility detection code to configuration process (clossed #110)
  * Removed dependencies on python's distutils module which has been depricated

nds2-client-swig-0.16.12
-----------------------------
  * Fixed pythoon shebang lines for EL8

nds2-client-swig-0.16.11
-----------------------------
  * Fixed a memory corruption issue when using the python client in a
    multi-threaded context  #130.
  * Updated the python standalone functions (nds2.fetch, nds2.iterate) to take 
    gap handler parameters as strings as well as enum values #123.

nds2-client-swig-0.16.10
-----------------------------
  * Modified build process to distribute Octave bindings in standard location
    for Debian systems (closes #116)
  * Updated Debian description for python package to only include Python major
    version number (closes #119)

nds2-client-swig-0.16.9
-----------------------------
  * Updated test infrastructure for online data retrieval at subsecond strides.
  * Misc updates to the test code

nds2-client-swig-0.16.8
-----------------------------
  * Allow developer control over building of SWIG extensions via SWIG_ENABLE_<LANGUAGE>
  * Modification of documentation macros for SWIG 4.x
  * Have MacPorts build use swig3

nds2-client-swig-0.16.7
-----------------------------
  * Updated packaging rules based on lessons learned from LDAS
  * Updated home page url (closes #98)
  * Updated project source url
  * Replaced hard coded nds2-client version with variable NDS_CLIENT_VERSION (closes #77)

nds2-client-swig-0.16.6
-----------------------------
  * Bump the minimum java capatability target to 1.7 so that jdk 12 can be used
  * Updated Python flags to take advantage of more advanced features.
  * Added passing of SWIG_EXECUTABLE to allow user override.
  * Added additional logic to disable generation of OCTAVE bindings even
    when system can find pieces neeeded to generate the bindings.

nds2-client-swig-0.16.5
-----------------------------
  * Correction to Octave unit tests

nds2-client-swig-0.16.4
-----------------------------
  * Fix for #82, added range checking on intervals to ensure start < end

nds2-client-swig-0.16.3
-----------------------------
  * Added definition for CMAKE_INSTALL_PKGDATADIR used by MATLAB
  * Corrected some documentation for MATLAB

nds2-client-swig-0.16.2
-----------------------------
  * Added a "connectionless" interface for Python, Java, Java/MATLAB (ticket #64).
  * Clarifying documentation in some python docstrings.
  * Updated the user documenation to discuss the connectionless interface.
  * Reimplemented nds2.connection_iterator to hold a java pointer to the underlying connection (ticket #63).
  * nds2.connection & nds2.connection_iterator now implement java.io.Closeable (ticket #65).
  * updated documentation and tests to reflect changes in the libndscxx gap handling.
  * Work on ticket #71, making the ndsm interface handle java.lang.String inputs.

nds2-client-swig-0.16.1 (August 3, 2018)
-----------------------------
  * Modified RPM rules to generically support python3 variables
  * Updated print statements to function calls for python3

nds2-client-swig-0.16.0 (August 2, 2018)
-----------------------------
  * Corrected population of py35 variables (ticket #43)
  * Added a __version__ field to the python module (ticket #56)
