#!/usr/bin/env python
import os
import sys

import nds2


host = os.getenv('NDS_TEST_HOST', 'localhost')
port = int(os.getenv('NDS_TEST_PORT', 31200))

c = nds2.connection(host, port)

chans = c.find_channels( "*", nds2.channel.CHANNEL_TYPE_RDS )
assert( len(chans) == 3 )
assert( chans[0].name == "X1:PEM-1" )
assert( chans[0].data_type == nds2.channel.DATA_TYPE_UNKNOWN )
assert( chans[2].name == "X1:PEM-3" )
assert( chans[2].data_type == nds2.channel.DATA_TYPE_FLOAT32 )


c.close()
sys.exit(0)