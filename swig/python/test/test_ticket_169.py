import nds2
import os
import sys

# This python code would fail
#
# The cause of it was the availability check changing the channel from a
# s-trend to an m-trend
#
# Test availability and the actual data transfer
#
# c = nds.connection('nds.ligo-wa.caltech.edu', 31200)
# dat = c.fetch(1107878416, 1107878420, H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend)

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])


proto = nds2.connection.PROTOCOL_TWO

conn = nds2.connection(host, port, proto)
conn.set_parameter("ALLOW_DATA_ON_TAPE", "true")

start = 1107878416
stop = 1107878420
channels = [
    "H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend",
]

conn.set_epoch(start, stop)

avail = conn.get_availability(channels)

assert( len(avail) == 1 )
assert( avail[0].name == channels[0] )
assert( len(avail[0].data) == 1 )
assert( avail[0].data[0].frame_type == "H-H1_T")
assert( avail[0].data[0].gps_start == start )
assert( avail[0].data[0].gps_stop == stop )

bufs = conn.fetch( start, stop, channels )