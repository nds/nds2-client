from __future__ import print_function
import nds2
import os
import sys

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_TWO

abort_at = 17
expected_nano = 0
delta = 1000000000/16

for bufs in nds2.iterate(hostname=host, port=port, channels=['X1:FEC-117_CPU_METER,online'], stride=nds2.connection.FAST_STRIDE):
    print(bufs[0])
    assert( len(bufs[0].data) == 1 )
    assert( bufs[0].gps_nanoseconds == expected_nano )
    abort_at -= 1
    if abort_at == 0:
        break
    expected_nano += delta
    if expected_nano == 1000000000:
        expected_nano = 0