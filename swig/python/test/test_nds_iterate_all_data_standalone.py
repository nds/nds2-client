import nds2
import os
import sys


def is_gap_second(input_gps):
    # no gaps in this test
    return False


def check_data(buf, val):
    samples_per_sec = int(buf.sample_rate)

    offset = 0
    for cur_gps in range(buf.Start(), buf.Stop()):
        expected = val
        if is_gap_second(cur_gps):
            expected = 0.0
        for i in range(samples_per_sec):
            assert( buf.data[offset] == expected )
            offset += 1

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

use_strings = '--strings' in sys.argv

proto = nds2.connection.PROTOCOL_TWO
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO

gap_handler_val = "true"
handler = nds2.ABORT_HANDLER
if use_strings:
    handler = "ABORT_HANDLER"
if "-no-gap" in sys.argv:
    gap_handler_val = "false"
    handler = nds2.SKIP_GAPS_IN_ITERATE
    if use_strings:
        handler = "SKIP_GAPS_IN_ITERATE"
kwargs = {}
params = None
if '-explicit-args' in sys.argv:
    params = nds2.parameters(host, port, proto)
    params.set("ITERATE_USE_GAP_HANDLERS", gap_handler_val)
    kwargs['params'] = params
elif '-named-params' in sys.argv:
    kwargs['hostname'] = host
    kwargs['port'] = port
    kwargs['protocol'] = proto
    kwargs['gap_handler'] = handler
else:
    os.environ['NDSSERVER'] = "{0}:{1}".format(host, port)
    os.environ['NDS2_CLIENT_PROTOCOL_VERSION'] = '{0}'.format(proto)
    os.environ['NDS2_CLIENT_ITERATE_USE_GAP_HANDLERS'] = gap_handler_val

chans = ["X1:PEM-1", "X1:PEM-2"]

i = 0
cur_gps = 1770000000
for bufs in nds2.iterate(chans, 1770000000, 1770000020, 10, **kwargs):
    expected = [1.5, 2.75]
    if i > 0:
        expected = [3.5, 4.75]

    assert( bufs[0].gps_seconds == cur_gps )
    assert( bufs[0].Samples() == 256*10 )
    check_data( bufs[0], expected[0] )
    assert( bufs[1].Samples() == 512*10 )
    check_data( bufs[1], expected[1] )
    cur_gps = bufs[0].Stop()
    i += 1