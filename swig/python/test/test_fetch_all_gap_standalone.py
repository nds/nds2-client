from __future__ import print_function

import nds2
import os
import sys

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

channels = ['X1:PEM-1',]

handler = nds2.STATIC_HANDLER_NEG_INF
for entry in sys.argv:
    if entry  == "--strings":
        handler = "STATIC_HANDLER_NEG_INF"

buffers = nds2.fetch(channels, 1219524930, 1219524930 + 4, hostname=host, port=port, protocol=nds2.connection.PROTOCOL_TWO, gap_handler=handler)

buf = buffers[0]

for entry in buf.data:
    assert(entry == -2147483648)