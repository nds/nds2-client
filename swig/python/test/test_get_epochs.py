from __future__ import print_function

import nds2
import os
import sys


def get_expected_epochs(proto):
    if proto == nds2.connection.PROTOCOL_ONE:
        return (("ALL", 0, 1999999999),)
    return (
        ("ALL", 0, 1999999999),
        ("ER2", 1025636416, 1028563232),
        ("ER3", 1042934416, 1045353616),
        ("ER4", 1057881616, 1061856016),
        ("ER5", 1073606416, 1078790416),
        ("ER6", 1102089216, 1102863616),
        ("ER7", 1116700672, 1118330880),
        ("ER8", 1123856384, 1126621184),
        ("NONE", 0, 0),
        ("O1", 1126621184, 1999999999),
        ("S5", 815153408, 880920032),
        ("S6", 930960015, 971654415),
        ("S6a", 930960015, 935798415),
        ("S6b", 937785615, 947203215),
        ("S6c", 947635215, 961545615),
        ("S6d", 961545615, 971654415),
    )

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_ONE
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO




conn = nds2.connection(host, port, proto)

cur = conn.current_epoch()
assert( cur.gps_start == 0 )
assert( cur.gps_stop == 1999999999 )

available_epochs = conn.get_epochs()
expected_epochs = get_expected_epochs( conn.get_protocol() )

assert( len(expected_epochs) == len(available_epochs) )
for i in range(len(expected_epochs)):
    assert(available_epochs[i].name == expected_epochs[i][0])
    assert(available_epochs[i].gps_start == expected_epochs[i][1])
    assert(available_epochs[i].gps_stop == expected_epochs[i][2])


# This only works on nds2 servers
# verify that you can set all avialable epochs by name and time range
if proto == nds2.connection.PROTOCOL_TWO:

    # test all epochs (except "NONE") by name
    for epoch in available_epochs:
        if epoch.name == "NONE":
            continue
        conn.set_epoch(epoch.name)
        cur = conn.current_epoch()
        assert( epoch.gps_start == cur.gps_start )
        assert( epoch.gps_stop == cur.gps_stop )

    # test all epochs (except 0-0) by time range
    for epoch in available_epochs:
        if epoch.gps_stop == 0:
            continue
        conn.set_epoch(epoch.gps_start, epoch.gps_stop)
        cur = conn.current_epoch()
        assert( epoch.gps_start == cur.gps_start )
        assert( epoch.gps_stop == cur.gps_stop )
