import nds2
import os
import sys

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_TWO


conn = nds2.connection(host, port, proto)

start = 1112749800
stop = 1112762400
channels = [
    "H1:ASC-INP2_Y_INMON.mean,m-trend",
    "L1:ASC-INP2_Y_INMON.mean,m-trend",
    "H1:ASC-INP2_Y_INMON.n,m-trend",
    "L1:ASC-INP2_Y_INMON.n,m-trend",
]

conn.set_parameter("GAP_HANDLER", "STATIC_HANDLER_ZERO")

bufs = conn.fetch(start, stop, channels)

assert( bufs[0].DataType() == nds2.channel.DATA_TYPE_FLOAT64 )
assert( bufs[0].Samples() == (stop - start)//60 )

# look for a zero filled gap at [1112752800-1112756400) on the first channel
gap_start = 1112752800
gap_stop = 1112756400
start_index = (gap_start - start)//60
stop_index = (gap_stop - start)//60
for i in range(start_index, stop_index):
    assert( bufs[0].data[i] == 0.0 )

# look for  ~0.110272226 at time 1112752740 (sample prior to gap)
index = (1112752740 - start)//60
print("\n\n\n\n\n%s\n\n\n\n" % str(bufs[0].data[index]))
delta = abs(bufs[0].data[index] - 0.110272226)
assert( delta <= 0.0000000001 )