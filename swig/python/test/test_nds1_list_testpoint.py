from __future__ import print_function

import nds2
import os
import sys

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_ONE

params = nds2.parameters(host, port, proto)
conn = nds2.connection(params)

chans = conn.find_channels("*", nds2.channel.CHANNEL_TYPE_TEST_POINT)
assert( len(chans) > 0 )

chans = conn.find_channels("X1:TEST_POINT_2*")
assert( len(chans) == 1 )
assert( chans[0].channel_type == nds2.channel.CHANNEL_TYPE_TEST_POINT)

chans = conn.find_channels("X1:TEST_POINT_1*")
tp = 0
mtrend = 0
strend = 0
online = 0
other = 0

for chan in chans:
    if chan.name == "X1:TEST_POINT_1":
        assert( chan.channel_type == nds2.channel.CHANNEL_TYPE_TEST_POINT )
    elif chan.name.startswith("X1:TEST_POINT_1"):
        assert( chan.channel_type != nds2.channel.CHANNEL_TYPE_TEST_POINT )
    if chan.channel_type == nds2.channel.CHANNEL_TYPE_TEST_POINT:
        tp += 1
    elif chan.channel_type == nds2.channel.CHANNEL_TYPE_MTREND:
        mtrend += 1
    elif chan.channel_type == nds2.channel.CHANNEL_TYPE_STREND:
        strend += 1
    elif chan.channel_type == nds2.channel.CHANNEL_TYPE_ONLINE:
        online += 1
    else:
        other += 1

assert( tp == 1 )
assert( online == 1 )
assert( mtrend == 5 )
assert( strend == 5 )
assert( other == 0 )

for bufs in conn.iterate(["X1:TEST_POINT_1",]):
    assert( bufs[0].channel_type == nds2.channel.CHANNEL_TYPE_TEST_POINT )
    break

conn.close()