import nds2
import os
import sys


def is_gap_second(input_gps):
    gps = input_gps - 1770000000
    if (gps < 1) or (gps == 9 ) or (gps == 18 ) or (gps >= 32 and gps < 40) or (gps >= 50 and gps < 61) or (gps > 67):
        return True;
    return False

def check_data(buf, val):
    samples_per_sec = int(buf.sample_rate)

    offset = 0
    for cur_gps in range(buf.Start(), buf.Stop()):
        expected = val
        if is_gap_second(cur_gps):
            expected = 0.0
        for i in range(samples_per_sec):
            assert( buf.data[offset] == expected )
            offset += 1


host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_TWO
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO

conn = nds2.connection(host, port, proto)

if "-default-gap-handling" in sys.argv:
    # use the defaults for gap handling
    # which should be ITERATE_USE_GAP_HANDLERS = false and
    # the abort handler
    no_gaps = False
else:
    if "-no-gap" in sys.argv:
        conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "false")
        no_gaps = False
    else:
        conn.set_parameter("ITERATE_USE_GAP_HANDLERS", "true")
        no_gaps = True
    conn.set_parameter("GAP_HANDLER", "STATIC_HANDLER_ZERO")


chans = ["X1:PEM-1", "X1:PEM-2"]

i = 0
cur_gps = 1770000000

for bufs in conn.iterate(1770000000, 1770000070, 10, chans):
    assert( bufs[0].gps_seconds >= cur_gps )
    if no_gaps:
        assert( bufs[0].gps_seconds == cur_gps )
    delta = bufs[0].Stop() - bufs[0].Start()

    assert( bufs[0].Samples() == 256*delta )
    check_data( bufs[0], 1.5 )
    assert( bufs[1].Samples() == 512*delta )
    check_data( bufs[1], 2.75 )
    cur_gps = bufs[0].Stop()
    i += 1
assert( i >= 5 )
if no_gaps:
    assert( cur_gps == 1770000070 )

# make sure we can do other operations
conn.set_epoch(1770000000, 1770000000+20)
conn.find_channels("X1:PEM-1")
conn.find_channels("X1:PEM-2")