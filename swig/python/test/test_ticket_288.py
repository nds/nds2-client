#!/usr/bin/env python

from __future__ import print_function

import os
import sys

import nds2


host = os.getenv('NDS_TEST_HOST', 'localhost')
port = int(os.getenv('NDS_TEST_PORT', 31200))

c = nds2.connection(host, port)
c.set_parameter('ALLOW_DATA_ON_TAPE', 'true')
c.set_parameter('ITERATE_USE_GAP_HANDLERS', 'false')

block_count = 0
last_gps = 0

expected = [
    "66.7626",
    "0.0",
    "67.4257",
    "0.0",
    "0.0",
    "0.0",
]

actual = []
for bufs in c.iterate(1167530460, 1178294460, ["H1:DMT-SNSH_EFFECTIVE_RANGE_MPC.mean,m-trend",]):
    block_count += 1
    last_gps = bufs[0].Stop()
    actual.append("{0}".format(bufs[0].data[120]))

assert( block_count == 6 )
assert( last_gps == 1178294460 )
for i in range(len(expected)):
    print( expected[i], actual[i] )
    assert( expected[i] == actual[i][0:7] )

c.close()
sys.exit(0)