#!/usr/bin/env python
from __future__ import print_function
import os
import sys

import nds2

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

params = nds2.parameters(host, port)
lst = nds2.find_channels('*', params=params, timespan=(1200000000, 1210000000))
assert len(lst) == 5

for item in lst:
    print( item )

chans = ["<X1:PEM-1 (256Hz, RAW, FLOAT32)>",
         "<X1:PEM-1 (1Hz, STREND, FLOAT32)>",
         "<X1:PEM-2 (256Hz, RAW, FLOAT32)>",
         "<X1:PEM-2 (1Hz, STREND, FLOAT32)>",
         "<X1:PEM-3 (256Hz, RAW, FLOAT32)>",
         ]

for i in range(len(chans)):
    print("testing %s == %s" % (chans[i], str(lst[i])))
    assert chans[i] == str(lst[i])
