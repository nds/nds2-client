from __future__ import print_function

import nds2
import os

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

c = nds2.connection(host, port, nds2.connection.PROTOCOL_TWO)

assert( c.set_parameter("ITERATE_USE_GAP_HANDLERS", "TRUE"))
assert( c.set_parameter("GAP_HANDLER", "STATIC_HANDLER_NEG_INF") )

channels = ['X1:PEM-1',]

# This request has a leading gap, it should be filled in
for bufs in c.iterate(1218312574, 1218312578, 2, channels):
    buf = bufs[0]

    expected = -2147483648
    if buf.gps_seconds != 1218312574:
        expected = 14
    for entry in buf.data:
        assert(entry == expected)

# change the request to a fictious period where X1:PEM-1 cannot
# be resolved to a single unique channel, we expect this to fail
try:
    for bufs in c.iterate(1218312578, 1218312582, 2, channels):
        # make sure we don't get into the loop
        assert(False)
    # make sure we don't do 0 iterations
    assert(False)
except Exception as e:
    assert( 'Unable to map' in str(e))
    assert( 'X1:PEM-1' in str(e))
