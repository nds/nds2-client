import nds2
import os
import sys


def is_gap_second(input_gps):
    # no gaps in this test
    return False


def check_data(buf, val):
    samples_per_sec = int(buf.sample_rate)

    offset = 0
    for cur_gps in range(buf.Start(), buf.Stop()):
        expected = val
        if is_gap_second(cur_gps):
            expected = 0.0
        for i in range(samples_per_sec):
            assert( buf.data[offset] == expected )
            offset += 1


host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_TWO
chans = ["X1:PEM-1", "X1:PEM-2"]
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO
    chans = ["X1:PEM-1,online", "X1:PEM-2,online"]

conn = nds2.connection(host, port, proto)

i = 0
for bufs in conn.iterate(chans):
    assert( bufs[0].Samples() == 256 )
    check_data( bufs[0], 1.5 )
    assert( bufs[1].Samples() == 512 )
    check_data( bufs[1], 2.75 )
    i += 1
    # On a real system this would continue indefinitly
    # however, this test is set to only send 5 seconds of data
    if i == 5:
        break

assert( i == 5 )