#!/usr/bin/env python
from __future__ import print_function

import nds2
import os
import sys

def count_channels(c):
    print("counting channels")
    return c.count_channels("*")

def find_channels(c):
    print("find channels")
    return c.find_channels()

host = 'localhost'
if 'NDS_TEST_HOST' in os.environ:
    host = os.environ['NDS_TEST_HOST']
port = 31200
if 'NDS_TEST_PORT' in os.environ:
    port = int(os.environ['NDS_TEST_PORT'])

proto = nds2.connection.PROTOCOL_ONE
if '-proto-1' in sys.argv:
    proto = nds2.connection.PROTOCOL_ONE
    test_func = find_channels
elif '-proto-2' in sys.argv:
    proto = nds2.connection.PROTOCOL_TWO
    test_func = count_channels

print("test_timeout startup args = {0}".format(sys.argv))

c = nds2.connection(host, port, proto)
try:
    test_func(c)
except Exception as err:
    print("timed out as expected")
    print(err)
    sys.exit(0)
sys.exit(1)
