from __future__ import print_function
import nds2

test_cases = {
    nds2.ABORT_HANDLER: nds2.ABORT_HANDLER,
    nds2.STATIC_HANDLER_ZERO: nds2.STATIC_HANDLER_ZERO,
    nds2.STATIC_HANDLER_ONE: nds2.STATIC_HANDLER_ONE,
    nds2.STATIC_HANDLER_NAN: nds2.STATIC_HANDLER_NAN,
    nds2.STATIC_HANDLER_POS_INF: nds2.STATIC_HANDLER_POS_INF,
    nds2.STATIC_HANDLER_NEG_INF: nds2.STATIC_HANDLER_NEG_INF,
    nds2.SKIP_GAPS_IN_ITERATE: nds2.SKIP_GAPS_IN_ITERATE,
    "ABORT_HANDLER": nds2.ABORT_HANDLER,
    "STATIC_HANDLER_ZERO": nds2.STATIC_HANDLER_ZERO,
    "STATIC_HANDLER_ONE": nds2.STATIC_HANDLER_ONE,
    "STATIC_HANDLER_NAN": nds2.STATIC_HANDLER_NAN,
    "STATIC_HANDLER_POS_INF": nds2.STATIC_HANDLER_POS_INF,
    "STATIC_HANDLER_NEG_INF": nds2.STATIC_HANDLER_NEG_INF,
    "SKIP_GAPS_IN_ITERATE": nds2.SKIP_GAPS_IN_ITERATE,
    u"ABORT_HANDLER": nds2.ABORT_HANDLER,
    u"STATIC_HANDLER_ZERO": nds2.STATIC_HANDLER_ZERO,
    u"STATIC_HANDLER_ONE": nds2.STATIC_HANDLER_ONE,
    u"STATIC_HANDLER_NAN": nds2.STATIC_HANDLER_NAN,
    u"STATIC_HANDLER_POS_INF": nds2.STATIC_HANDLER_POS_INF,
    u"STATIC_HANDLER_NEG_INF": nds2.STATIC_HANDLER_NEG_INF,
    u"SKIP_GAPS_IN_ITERATE": nds2.SKIP_GAPS_IN_ITERATE,
    b"ABORT_HANDLER": nds2.ABORT_HANDLER,
    b"STATIC_HANDLER_ZERO": nds2.STATIC_HANDLER_ZERO,
    b"STATIC_HANDLER_ONE": nds2.STATIC_HANDLER_ONE,
    b"STATIC_HANDLER_NAN": nds2.STATIC_HANDLER_NAN,
    b"STATIC_HANDLER_POS_INF": nds2.STATIC_HANDLER_POS_INF,
    b"STATIC_HANDLER_NEG_INF": nds2.STATIC_HANDLER_NEG_INF,
    b"SKIP_GAPS_IN_ITERATE": nds2.SKIP_GAPS_IN_ITERATE,
    None: None,
    'abc': 'abc',
}

for key in test_cases:
    print(key)
    assert( nds2.__resolve_gap_handler(key) == test_cases[key])