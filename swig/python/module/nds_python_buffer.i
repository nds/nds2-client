%include "nds_buffer.i"

%include <attribute.i>

%begin %{
#include <cstring>

#include "nds_buffer.hh"
%}

%extend buffer {

  //---------------------------------------------------------------------
  // NOTE: This is to allow backwards compatability
  //---------------------------------------------------------------------
  %define DOC_NDS_BUFFER_CHANNEL
  "Return the channel characteristics associated with the buffer"
  %enddef
  %feature("autodoc", DOC_NDS_BUFFER_CHANNEL) Channel;
  channel& Channel( )
  {
    return *($self);
  }

  %define DOC_NDS_BUFFER_DATA
  "Return the data associated with the buffer as a NumPy array"
  %enddef
  %feature("autodoc", DOC_NDS_BUFFER_DATA) Data;
  %nothread data_pvt;
  PyObject*
  data_pvt( )
  {
    PyObject*                   retval( (PyObject*)NULL );
    const void*                 dp( ($self)->cbegin<void>() );

    int typenum;

    switch ( $self->DataType( ) )
    {
    case NDS::channel::DATA_TYPE_INT16:
      typenum = NPY_INT16;
      break;
    case NDS::channel::DATA_TYPE_INT32:
      typenum = NPY_INT32;
      break;
    case NDS::channel::DATA_TYPE_INT64:
      typenum = NPY_INT64;
      break;
    case NDS::channel::DATA_TYPE_FLOAT32:
      typenum = NPY_FLOAT32;
      break;
    case NDS::channel::DATA_TYPE_FLOAT64:
      typenum = NPY_FLOAT64;
      break;
    case NDS::channel::DATA_TYPE_COMPLEX32:
      typenum = NPY_COMPLEX64;
      break;
    case NDS::channel::DATA_TYPE_UINT32:
      typenum = NPY_UINT32;
      break;
    default:
      // SWIG_exception(SWIG_TypeError, "Unknown NDS data type");
      break;
    }

    {
      npy_intp dims[ 1 ];
      int nd = sizeof( dims ) / sizeof( *dims );

      dims[ 0 ] = $self->Samples( );

      void* buffer = calloc( $self->Samples( ),
      	                     $self->DataTypeSize( ) );
      std::memcpy( buffer, dp, $self->Samples( ) * $self->DataTypeSize( ) );

      retval = PyArray_SimpleNewFromData( nd, dims, typenum, buffer );
      PyArrayObject* retarray = reinterpret_cast< PyArrayObject* >( retval );
#if NPY_API_VERSION < NPY_1_7_API_VERSION
      /* numpy 1.7 introduces PyArray_ENABLEFLAGS and changes NPY_OWNDATA and friends
       * to NPY_ARRAY_OWNDATA.  But do it this way for older numpy.
       */
      retarray->flags |= NPY_OWNDATA | NPY_ALIGNED;
#else
      int flags = PyArray_FLAGS( retarray );
      PyArray_ENABLEFLAGS( retarray, flags | NPY_ARRAY_OWNDATA | NPY_ARRAY_ALIGNED );
#endif
    }

    return retval;
  }

  %pythoncode %{
     def Data( self ):
       try:
         return self.pydata
       except AttributeError:
         self.pydata = self.data_pvt( )
       return self.pydata

     try:
       __swig_getmethods__
     except NameError:
       __swig_getmethods__ = {}

     __swig_getmethods__['channel'] = Channel
     __swig_getmethods__['data'] = Data
     __swig_getmethods__['gps_seconds'] = Start
     __swig_getmethods__['gps_nanoseconds'] = StartNano
     __swig_getmethods__['length'] = Samples

     try:
       object
       channel = property(Channel)
       data = property(Data)
       gps_seconds = property(Start)
       gps_nanoseconds = property(StartNano)
       length = property(Samples)
     except AttributeError:
       pass
  %}
}
