/* -*- mode: C++ ; c-basic-offset: 2; indent-tabs-mode: nil -*- */
/*
 * This file allows for SWIG specialization for Python
 */
#if defined(SWIGPYTHON)

%{

#include <sstream>

%}

%feature("autodoc","1");

#-------

%define %nds_type_gps_seconds() "long"
%enddef

%define %nds_type_sample_rate() "float"
%enddef

%define %nds_doc_param(CALL,TYPE,DESC)
%typemap("doc") CALL "$1_name -- " TYPE " -- " DESC;
%enddef

%define %nds_doc_brief(TEXT)
TEXT "
"
%enddef

%define %nds_doc_details(TEXT)
TEXT %nds_doc_nl( )
%enddef

%define %nds_doc_remark(TEXT)
"REMARK: " TEXT "
"
%enddef
%define %nds_doc_returns(TEXT)
"Returns " TEXT "

"
%enddef

%define %nds_doc_class_seperator()
%feature("autodoc") "";
%feature("docstring") "";
%enddef

#-------

%exception connection::next {
  try
  {
    $action
  }
  catch( const std::out_of_range& E )
  {
    PyErr_SetString(PyExc_StopIteration, "End of iterator");
    return NULL;
  }
  catch( std::bad_alloc& e )
  {
    SWIG_exception( SWIG_MemoryError, const_cast<char*>(e.what( ) ) );
    return NULL;
  }
  catch( std::invalid_argument& e )
  {
    SWIG_exception( SWIG_TypeError, const_cast<char*>(e.what( ) ) );
    return NULL;
  }
  catch( std::overflow_error& e )
  {
    SWIG_exception( SWIG_OverflowError, const_cast<char*>(e.what( ) ) );
    return NULL;
  }
  catch( std::ios_base::failure& e )
  {
    SWIG_exception( SWIG_IOError, const_cast<char*>(e.what( ) ) );
    return NULL;
  }
#if HAVE_STD_SYSTEM_ERROR
  catch( std::system_error& e )
  {
    SWIG_exception( SWIG_SystemError, const_cast<char*>(e.what( ) ) );
    return NULL;
  }
#endif /* HAVE_STD_SYSTEM_ERROR */
  catch( std::exception& e )
  {
    SWIG_exception( SWIG_RuntimeError, const_cast<char*>(e.what( ) ) );
    return NULL;
  }
  catch( ... )
  {
    SWIG_exception( SWIG_RuntimeError, const_cast<char*>( "Unknown exception thrown" ) );
    return NULL;
  }
}

%pythonprepend connection::connection %{
args = _hack_for_python2_strings(args)
%}

%pythonprepend connection::count_channels %{
args = _hack_for_python2_strings(args)
%}

%pythonprepend connection::find_channels %{
args = _hack_for_python2_strings(args)
%}

%pythonprepend connection::set_epoch %{
args = _hack_for_python2_strings(args)
%}

%pythonprepend connection::check %{
if 'args' in dir():
    args = _hack_for_python2_strings(args)
else:
    channel_names = _hack_for_python2_strings(channel_names)
%}

%pythonprepend connection::fetch %{
if 'args' in dir():
    args = _hack_for_python2_strings(args)
else:
    channel_names = _hack_for_python2_strings(channel_names)
%}

%pythonprepend connection::set_parameter %{
if 'args' in dir():
    args = _hack_for_python2_strings(args)
else:
    parameter, value = _hack_for_python2_strings((parameter, value))
%}

%pythonprepend connection::get_parameter %{
if 'args' in dir():
    args = _hack_for_python2_strings(args)
else:
    parameter = _hack_for_python2_strings((parameter,))[0]
%}

%pythonprepend connection::iterate %{
args = _hack_for_python2_strings(args)
%}

%pythonprepend connection::get_availability %{
if 'args' in dir():
    args = _hack_for_python2_strings(args)
else:
    channel_names = _hack_for_python2_strings(channel_names)
%}

%include "nds.i"


%pythoncode %{
import types
import sys

def _unicode_to_str(arg):
    arg_type = type(arg)
    if arg_type == types.UnicodeType:
        return str(arg)
    elif arg_type == types.ListType:
        return [x if type(x) != types.UnicodeType else str(x) for x in arg]
    elif arg_type == types.TupleType:
        return tuple([x if type(x) != types.UnicodeType else str(x) for x in arg])
    return arg

def _bytestr_to_unicode(arg):
        utype = type(u'')
        btype = type(b'')
        ltype = type([])
        ttype = type(())

        arg_type = type(arg)
        if arg_type == btype:
            return arg.decode()
        elif arg_type == ltype:
            return [x if type(x) != btype else x.decode() for x in arg]
        elif arg_type ==ttype:
            return tuple(x if type(x) != btype else x.decode() for x in arg)
        return arg

def _hack_for_python2_strings(args):
    """SWIG < 3.0  with Python 2 does not convert Python unicode strings
    to std::string or char *.  So force unicode strings to be byte strings"""
    if sys.version_info.major < 3 and len(args) > 0:
        args = tuple(map(_unicode_to_str, args))
    elif len(args) > 0:
        args = tuple(map(_bytestr_to_unicode, args))
    return args
%}

%begin %{
extern "C"
{
#define NPY_NO_DEPRECATED_API NPY_API_VERSION

#include <Python.h>
#include <numpy/arrayobject.h>

#if NPY_API_VERSION >= 0x00000007
// Do nothing since the macro is already available
#else // NPY_API_VERSION >= 0x00000007
#define PyArray_SetBaseObject(A,B)	PyArray_BASE(A) = B
#endif // NPY_API_VERSION >= 0x00000007

}

#include <string>
#include <sstream>
#include "nds_channel.hh"
#include "nds_connection.hh"
#include "nds_epoch.hh"
#include "nds_swig.hh"

namespace {
template<typename T>
std::string to_cpp_string(const T* obj)
{
  if (!obj) return std::string("");
  std::ostringstream os;
  os << (*obj);
  return os.str();
}


}

%}

%include "nds_python_buffer.i"

%init %{
  import_array( );
%}


%define NDS_PRINT(type)
  %feature("autodoc") "";
  %feature("docstring") "";
  std::string __str__( )
  {
    return to_cpp_string<type>($self);
  }
  std::string __repr__( )
  {
    return to_cpp_string<type>($self);
  }
%enddef

%extend channel {

  NDS_PRINT(NDS::channel)

  %pythoncode %{
     try:
       __swig_getmethods__
     except NameError:
       __swig_getmethods__ = {}

     __swig_getmethods__['name'] = Name
     __swig_getmethods__['channel_type'] = Type
     __swig_getmethods__['data_type'] = DataType
     __swig_getmethods__['sample_rate'] = SampleRate
     #__swig_getmethods__['gain'] = Gain
     #__swig_getmethods__['slope'] = Slope
     #__swig_getmethods__['offset'] = Offset
     #__swig_getmethods__['units'] = Units
     __swig_getmethods__['signal_gain'] = Gain
     __swig_getmethods__['signal_slope'] = Slope
     __swig_getmethods__['signal_offset'] = Offset
     __swig_getmethods__['signal_units'] = Units

     try:
       object
       name = property(Name)
       channel_type = property(Type)
       data_type = property(DataType)
       sample_rate = property(SampleRate)
       #gain = property(Gain)
       #slope = property(Slope)
       #offset = property(Offset)
       #units = property(Units)
       signal_gain = property(Gain)
       signal_slope = property(Slope)
       signal_offset = property(Offset)
       signal_units = property(Units)
     except AttributeError:
       pass

  %}
}

%extend connection
{
  %insert("python") %{
    def __iter__(self):
      return self

    def __next__(self):
      return self.next()
  %}

  NDS_PRINT(connection)
}

%extend buffer
{
  NDS_PRINT(NDS::buffer)
}

%extend segment
{
  NDS_PRINT(NDS::segment)
}

%extend simple_segment
{
  NDS_PRINT(NDS::simple_segment)
}

%extend simple_segment_list_type
{
  NDS_PRINT(NDS_swig::simple_segment_list_type)
}

%extend simple_availability_list_type
{
  NDS_PRINT(NDS_swig::simple_availability_list_type)
}

%extend segment_list_type
{
  NDS_PRINT(NDS_swig::segment_list_type)
}

%extend availability
{
  NDS_PRINT(NDS_swig::availability)
}

%extend availability_list_type
{
  NDS_PRINT(NDS_swig::availability_list_type)
}

%extend epoch
{
  NDS_PRINT(NDS::epoch)
}

%extend epochs_type
{
  NDS_PRINT(NDS_swig::epochs_type)
}

%{


enum gap_handlers {
  ABORT_HANDLER,
  STATIC_HANDLER_ZERO,
  STATIC_HANDLER_ONE,
  STATIC_HANDLER_NAN,
  STATIC_HANDLER_POS_INF,
  STATIC_HANDLER_NEG_INF,
  SKIP_GAPS_IN_ITERATE,
};

%}

enum gap_handlers {
  ABORT_HANDLER,
  STATIC_HANDLER_ZERO,
  STATIC_HANDLER_ONE,
  STATIC_HANDLER_NAN,
  STATIC_HANDLER_POS_INF,
  STATIC_HANDLER_NEG_INF,
  SKIP_GAPS_IN_ITERATE,
};


%pythoncode %{
    __version__ = version()

    def __resolve_gap_handler(handler):
        mapping = {
                   b'ABORT_HANDLER': ABORT_HANDLER,
                   b'STATIC_HANDLER_ZERO': STATIC_HANDLER_ZERO,
                   b'STATIC_HANDLER_ONE': STATIC_HANDLER_ONE,
                   b'STATIC_HANDLER_NAN': STATIC_HANDLER_NAN,
                   b'STATIC_HANDLER_POS_INF': STATIC_HANDLER_POS_INF,
                   b'STATIC_HANDLER_NEG_INF': STATIC_HANDLER_NEG_INF,
                   b'SKIP_GAPS_IN_ITERATE': SKIP_GAPS_IN_ITERATE,
                   u'ABORT_HANDLER': ABORT_HANDLER,
                   u'STATIC_HANDLER_ZERO': STATIC_HANDLER_ZERO,
                   u'STATIC_HANDLER_ONE': STATIC_HANDLER_ONE,
                   u'STATIC_HANDLER_NAN': STATIC_HANDLER_NAN,
                   u'STATIC_HANDLER_POS_INF': STATIC_HANDLER_POS_INF,
                   u'STATIC_HANDLER_NEG_INF': STATIC_HANDLER_NEG_INF,
                   u'SKIP_GAPS_IN_ITERATE': SKIP_GAPS_IN_ITERATE,
                   }
        return mapping.get(handler, handler)

    def fetch(channels, gps_start, gps_stop, gap_handler=None, allow_data_on_tape=None, hostname=None, port=None, protocol=None, params=None):
        """Request offline data in a single chunk over [gps_start, gps_stop).

        Keyword arguments
        channels -- A list of channels to retrieve data from.
        gps_start -- A gps start time to retrieve data from.
        gps_stop -- The stop time in gps for the request.
        gap_handler -- The gap handling strategy to apply.
        hostname -- The host/server to connect to.
        port -- The port on the server to connect to.
        protocol -- The protocol version to connect with.
        params -- Optional parameters object allowing for the specification
        of gap handlers, servers, ...

        The gap_handler, allow_data_on_tape, hostname, port, and protocol fields
        are resolved as in the following way, with the first match being applied.
         * If the argument is not None it is used.
         * If the params argument is a `parameters` object then its values are used.
         * If the value is set in the environment it is used.
         * Otherwise a default value is used
           gap_handler = ABORT_HANDLER
           allow_data_on_tape = False
           hostname = localhost
           port = 31200
           protocol = PROTOCOL_TRY

        The behavior of the fetch function when a gap in the data is found is
        configurable via the gap_handler parameter.  The following gap_handlers
        are available:
         * ABORT_HANDLER - Abort the call with a data not found error.
         * STATIC_HANDLER_ZERO - Fill the gaps with 0 values.
         * STATIC_HANDLER_ONE - Fill the gaps with the value 1.
         * STATIC_HANDLER_NAN - Fill the gaps with NaN for real values and 0 for integer values.
         * STATIC_HANDLER_POS_INF - Fill the gaps with +Inf or the largest possible value.
         * STATIC_HANDLER_NEG_INF - Fill the gaps with -Inf or the smallest possible value.

        Returns a list of nds2.buffer objects, one per channel.

        Notes:
         * The SKIP_GAPS_IN_ITERATE is not a valid gap handler for fetch.  Currently
         it is treated the same as using the ABORT_HANDLER, but it may be change
         to raising an exception in the future.
        """
        if params is None:
            params = parameters()
        _gap_handler = __resolve_gap_handler(gap_handler)
        if _gap_handler is not None:
            if _gap_handler == ABORT_HANDLER or _gap_handler == SKIP_GAPS_IN_ITERATE:
                params.set('GAP_HANDLER', 'ABORT_HANDLER')
            elif _gap_handler == STATIC_HANDLER_ZERO:
                params.set('GAP_HANDLER', 'STATIC_HANDLER_ZERO')
            elif _gap_handler == STATIC_HANDLER_ONE:
                params.set('GAP_HANDLER', 'STATIC_HANDLER_ONE')
            elif _gap_handler == STATIC_HANDLER_NAN:
                params.set('GAP_HANDLER', 'STATIC_HANDLER_NAN')
            elif _gap_handler == STATIC_HANDLER_POS_INF:
                params.set('GAP_HANDLER', 'STATIC_HANDLER_POS_INF')
            elif _gap_handler == STATIC_HANDLER_NEG_INF:
                params.set('GAP_HANDLER', 'STATIC_HANDLER_NEG_INF')
            else:
                raise RuntimeError('Invalid gap handler selected')
        if not allow_data_on_tape is None:
            val = 'True'
            if not allow_data_on_tape:
                val = 'False'
            params.set('ALLOW_DATA_ON_TAPE', val)
        if not hostname is None:
            params.set('HOSTNAME', hostname)
        if not port is None:
            params.set('PORT', str(port))
        if not protocol is None:
            val = 'PROTOCOL_TRY'
            if protocol == connection.PROTOCOL_ONE:
                val = 'PROTOCOL_ONE'
            elif protocol == connection.PROTOCOL_TWO:
                val = 'PROTOCOL_TWO'
            params.set('PROTOCOL_VERSION', val)
        return _fetch(params, gps_start, gps_stop, channels)

    def find_channels(channel_glob="*", channel_type_mask=channel.DEFAULT_CHANNEL_MASK, data_type_mask=channel.DEFAULT_DATA_MASK, min_sample_rate=channel.MIN_SAMPLE_RATE, max_sample_rate=channel.MAX_SAMPLE_RATE, timespan=None, hostname=None, port=None, protocol=None, params=None):
        """Retrieve a channel list.

        Keyword arguments
        channel_glob -- A bash link glob pattern used to match channel names.
        channel_type_mask -- A mask of channel types (CHANNEL_TYPE_RAW, CHANNEL_TYPE_ONLINE, ...) to limit the search to.
        data_type_mask -- A mask of data types (DATA_TYPE_INT32, DATA_TYPE_FLOAT32, ...)  to limit channels to
        min_sample_rate -- A minimum sample rate to search for.
        max_sample_rate -- A maximum sample rate to search for.
        timespan -- Optional time span to limit available channel search to.  This may be an nds2.epoch or a tuple of [start, stop) times.
        hostname -- The host/server to connect to.
        port -- The port on the server to connect to.
        protocol -- The protocol version to connect with.
        params -- Optional parameters allowing for the specification of a specific server to query.

        The hostname, port, and protocol are resolved as in the
        following way, with the first match being applied.
         * If the argument is not None it is used.
         * If the params argument is a `parameters` object its values are used.
         * If the value is set in the environment it is used.
         * Otherwise a default value is used
           hostname = localhost
           port = 31200
           protocol = PROTOCOL_TRY

        Returns a list of nds2.channel objects that match the given parameters.
        """
        dummy_epoch = epoch()
        timespan_type = type(timespan)

        if params is None:
            params = parameters()
        if timespan is None:
            timespan = epoch("", 0, buffer.GPS_INF)
        elif not timespan_type == type(dummy_epoch):
            timespan = epoch("", *timespan)
        if not hostname is None:
            params.set('HOSTNAME', hostname)
        if not port is None:
            params.set('PORT', str(port))
        if not protocol is None:
            val = 'PROTOCOL_TRY'
            if protocol == connection.PROTOCOL_ONE:
                val = 'PROTOCOL_ONE'
            elif protocol == connection.PROTOCOL_TWO:
                val = 'PROTOCOL_TWO'
            params.set('PROTOCOL_VERSION', val)
        return _find_channels(params, channel_glob, channel_type_mask, data_type_mask, min_sample_rate, max_sample_rate, timespan)

    def iterate(channels, gps_start = 0, gps_stop = 0, stride = 0, gap_handler=None, allow_data_on_tape=None, hostname=None, port=None, protocol=None, params = None):
        """Stream nds data for a given set of channels.

        Keyword arguments
        channels -- A list of channels to stream data from.
        gps_start -- A start time, defaults to 0
        gps_stop -- stop time, defaults to 0
        stride -- Requested stride/buffer size to be returned.  Defaults to 0.
        params -- Optional parameters object allowing for the specification
        of gap handlers, servers, ...

        The gap_handler, allow_data_on_tape, hostname, port, and protocol fields
        are resolved as in the following way, with the first match being applied.
         * If the argument is not None it is used.
         * If the params argument is a `parameters` object its values are used.
         * If the value is set in the environment it is used.
         * Otherwise a default value is used
           gap_handler = ABORT_HANDLER
           allow_data_on_tape = False
           hostname = localhost
           port = 31200
           protocol = PROTOCOL_TRY

        The behavior of the iterate function when a gap in the data is found is
        configurable via the gap_handler parameter.  The following gap_handlers
        are available:
         * ABORT_HANDLER - Abort the call with a data not found error.
         * STATIC_HANDLER_ZERO - Fill the gaps with 0 values.
         * STATIC_HANDLER_ONE - Fill the gaps with the value 1.
         * STATIC_HANDLER_NAN - Fill the gaps with NaN for real values and 0 for integer values.
         * STATIC_HANDLER_POS_INF - Fill the gaps with +Inf or the largest possible value.
         * STATIC_HANDLER_NEG_INF - Fill the gaps with -Inf or the smallest possible value.
         * SKIP_GAPS_IN_ITERATE - Skip over gaps, only return segments of valid data.

        Notes:
         * To request unbounded live data, set gps_start to 0, gps_stop to 0, stride to 0.
         * To request bounded live data, set gps_start to 0, gsp_stop to the duration, stride to 0.
         * To request offline/archived data specify both gps_start and gps_stop.
        Stride is used to influence the block size.  It is a requested size value.  There are two
        constants that help, channel.AUTO_STRIDE asks the nds server to autoconfigure the stride size,
        channel.FAST_STRIDE requests data be sent at 16Hz.  FAST_STRIDE is nds1 only and degrades to
        AUTO_STRIDE if the server cannot support FAST_STRIDE.
         * If gap_handler is set to SKIP_GAPS_IN_ITERATE and channels that do not have identical gaps
        are selected a data not found exception will be raised.

        """
        from contextlib import closing
        if params is None:
            params = parameters()
        _gap_handler = __resolve_gap_handler(gap_handler)
        if _gap_handler is not None:
            if _gap_handler == ABORT_HANDLER:
                params.set('GAP_HANDLER', 'ABORT_HANDLER')
                params.set('ITERATE_USE_GAP_HANDLERS', 'True')
            elif _gap_handler == STATIC_HANDLER_ZERO:
                params.set('GAP_HANDLER', 'STATIC_HANDLER_ZERO')
                params.set('ITERATE_USE_GAP_HANDLERS', 'True')
            elif _gap_handler == STATIC_HANDLER_ONE:
                params.set('GAP_HANDLER', 'STATIC_HANDLER_ONE')
                params.set('ITERATE_USE_GAP_HANDLERS', 'True')
            elif _gap_handler == STATIC_HANDLER_NAN:
                params.set('GAP_HANDLER', 'STATIC_HANDLER_NAN')
                params.set('ITERATE_USE_GAP_HANDLERS', 'True')
            elif _gap_handler == STATIC_HANDLER_POS_INF:
                params.set('GAP_HANDLER', 'STATIC_HANDLER_POS_INF')
                params.set('ITERATE_USE_GAP_HANDLERS', 'True')
            elif _gap_handler == STATIC_HANDLER_NEG_INF:
                params.set('GAP_HANDLER', 'STATIC_HANDLER_NEG_INF')
                params.set('ITERATE_USE_GAP_HANDLERS', 'True')
            elif _gap_handler == SKIP_GAPS_IN_ITERATE:
                params.set('ITERATE_USE_GAP_HANDLERS', 'False')
            else:
                raise RuntimeError('Invalid gap handler selected')
        if not allow_data_on_tape is None:
            val = 'True'
            if not allow_data_on_tape:
                val = 'False'
            params.set('ALLOW_DATA_ON_TAPE', val)
        if not hostname is None:
            params.set('HOSTNAME', hostname)
        if not port is None:
            params.set('PORT', str(port))
        if not protocol is None:
            val = 'PROTOCOL_TRY'
            if protocol == connection.PROTOCOL_ONE:
                val = 'PROTOCOL_ONE'
            elif protocol == connection.PROTOCOL_TWO:
                val = 'PROTOCOL_TWO'
            params.set('PROTOCOL_VERSION', val)
        with closing(connection(params)) as conn:
            for bufs in conn.iterate(gps_start, gps_stop, stride, channels):
                yield bufs

    # Some swig builds promote these static class members to module global level as well as class specific
    # some do not.
    # So provide them if swig does not, this way we do not have to tweak swig more across OS releases
    # We could just define things, but in cases where they exist we would be overwriting the definitions
    # which seems wrong
    try:
        if channel_IsMinuteTrend:
            pass
    except NameError:
        def channel_IsMinuteTrend(Name):
            """Check the channel name to see if it is a minute trend.
            Returns true if Name ends in 'm-trend' else false"""
            return channel.IsMinuteTrend(Name)

    try:
        if channel_IsSecondTrend:
            pass
    except NameError:
        def channel_IsSecondTrend(Name):
            """Check the channel name to see if it is a second trend.
            Returns true if Name ends in 's-trend' else false"""
            return channel.IsSecondTrend(Name)

    try:
        if channel_channel_type_to_string:
            pass
    except NameError:
        def channel_channel_type_to_string(ctype):
            """Return a string version of the channel type value passed in"""
            return channel.channel_type_to_string(ctype)


    try:
        if channel_data_type_to_string:
            pass
    except NameError:
        def channel_data_type_to_string(dtype):
            """Return a string version of the data type value passed in"""
            return channel.data_type_to_string(dtype)

%}

#endif /* defined(SWIGPYTHON) */

