/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.availability;
import nds2.availability_list_type;
import nds2.buffer;
import nds2.connection;

public final class test_ticket_169_java {
    static final int START = 1107878416;
    static final int STOP = 1107878420;
    static final String[] CHANNELS = new String[] {
	"H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend"
    };

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );
	/*
	 * Need to adjust according to parameter
	 */
	int protocol = connection.PROTOCOL_TWO;

	/* ------------------------------------------------------------- *
	 * Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port, protocol );

	conn.setParameter("ALLOW_DATA_ON_TAPE", "true");

	conn.setEpoch( START, STOP );
	availability_list_type avail = conn.getAvailability( CHANNELS );

	unit_test.check( avail.size( ),
			 1,
			 "Availability list size" );
	/*
	unit_test.check( avail.get( 0 ).getName( ),
			 CHANNELS[ 0 ],
			 "Availability name" );
	unit_test.check( avail.get( 0 ).getData( ).size( ),
			 1,
			 "Availability data size" );
	unit_test.check( avail.get( 0 ).getData( ).get( 0 ).getFrameType( ),
			 "H-H1_T",
			 "Availability data frame type" );
	unit_test.check( avail.get( 0 ).getData( ).get( 0 ).getGpsStart( ),
			 START,
			 "Availability data GPS start" );
	unit_test.check( avail.get( 0 ).getData( ).get( 0 ).getGpsStop( ),
			 STOP,
			 "Availability data GPS stop" );
	*/

	buffer[] bufs = conn.fetch( START, STOP, CHANNELS );

	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}