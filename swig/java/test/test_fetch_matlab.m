% -*- mode: octave -*-
% -----------------------------------------------------------------------
% -----------------------------------------------------------------------

unit_test = unittest(  );

hostname = unit_test.hostname( );
port = unit_test.port( );

% -------------------------------------------------------------------
% Establish the connection
% -------------------------------------------------------------------
conn = nds2.connection(hostname, port, nds2.connection.PROTOCOL_ONE );

caught_error = false;

% -------------------------------------------------------------------
% Run test
% -------------------------------------------------------------------
	   
start = 1108835634;
finish = start + 4;

lst = conn.findChannels('H1:SUS-BS*');

channels = {
	    'H1:SUS-BS_BIO_ENCODE_DIO_0_OUT',
	    'H1:SUS-BS_BIO_M1_MSDELAYON',
	    'H1:SUS-BS_COMMISH_STATUS',
	    'H1:SUS-BS_DACKILL_BYPASS_TIMEMON',
	    'H1:SUS-BS_DCU_ID',
	    'H1:SUS-BS_DITHERP2EUL_1_1',
	    'H1:SUS-BS_DITHERP2EUL_2_1',
	    'H1:SUS-BS_DITHERY2EUL_1_1',
	    'H1:SUS-BS_DITHERY2EUL_2_1',
	    'H1:SUS-BS_DITHER_P_IPCERR'
	};

data = conn.fetch( start, finish, channels );

for idx = 1:length(data)
  ch1 = data(idx);
  ch2x = conn.findChannels(ch1.name( ));
  ch2 = ch2x(1);
  unit_test.check( ch1.name( ), ...
		   ch2.name( ), ...
		   'Validate name' );
  unit_test.check( ch1.getDataType( ), ...
		   ch2.getDataType( ), ...
		   'Validate data type' );
  unit_test.check( ch1.getChannelType( ), ...
		   ch2.getChannelType( ), ...
		   'Validate channel type' );
  unit_test.check( ch1.getSampleRate( ), ...
		   ch2.getSampleRate( ), ...
		   'Validate sample rate' );
  unit_test.check( ch1.getSignalGain( ), ...
		   ch2.getSignalGain( ), ...
		   'validate signal gain' );
  unit_test.check( ch1.getSignalOffset( ), ...
		   ch2.getSignalOffset( ), ...
		   'validate signal offset' );
  unit_test.check( ch1.getSignalSlope( ), ...
		   ch2.getSignalSlope( ), ...
		   'validate signal slope' );
  unit_test.check( ch1.getSignalUnits( ), ...
		   ch2.getSignalUnits( ), ...
		   'validate signal units' );
end
% -------------------------------------------------------------------
% Finish
% -------------------------------------------------------------------
conn.close( );

exit( unit_test.exit_code( ) );
