% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

START = 1107878416;
STOP = 1107878420;
CHANNELS =  { 'H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend'
    };

unit_test = unittest( );

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = nds2.connection.PROTOCOL_TWO;

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------
	   
conn = nds2.connection(hostname, port, protocol );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------
conn.setParameter('ALLOW_DATA_ON_TAPE', 'true');

conn.setEpoch( START, STOP );
avail = conn.getAvailability( CHANNELS );

unit_test.check( avail.size( ), ...
		 1, ...
		 'Availability list size' );
unit_test.check( avail.get( 0 ).getName( ), ...
		 CHANNELS( 1 ), ...
		 'Availability name' );
unit_test.check( avail.get( 0 ).getData( ).size( ), ...
		 1, ...
		 'Availability data size' );
unit_test.check( avail.get( 0 ).getData( ).get( 0 ).getFrameType( ), ...
		 'H-H1_T', ...
		 'Availability data frame type' );
unit_test.check( avail.get( 0 ).getData( ).get( 0 ).getGpsStart( ),  ...
		 START,  ...
		 'Availability data GPS start' );
unit_test.check( avail.get( 0 ).getData( ).get( 0 ).getGpsStop( ),  ...
		 STOP,  ...
		 'Availability data GPS stop' );

bufs = conn.fetch( START, STOP, CHANNELS );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );
exit( unit_test.exit_code( ) );