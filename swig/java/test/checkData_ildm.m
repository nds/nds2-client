%------------------------------------------------------------------------
% Utility functions
%------------------------------------------------------------------------

function RETVAL = checkData_ildm( Buf, Value )
  global  unit_test;

  samples_per_sec = Buf.getSampleRate( );
  CHANNEL_TYPE = Buf.getDataType( );

  unit_test.msgInfo( 'samples_per_sec: %d', ...
		     int32(samples_per_sec) );
  offset = 1;
  %======================================================================
  % MATLAB loops are inclusive so need to trim
  % the tail by 1 sec
  %======================================================================
  for cur_gps = Buf.start( ):[ Buf.stop( ) - 1 ]
    expected = Value;
    d = Buf.getData( );
    for i = 1:samples_per_sec
      if ( d( offset ) ~= expected )
	RETVAL = false;
	break
      end
      offset = offset + 1;
    end
  end
  RETVAL = true;
end
