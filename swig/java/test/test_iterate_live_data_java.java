/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.buffer;

public final class test_iterate_live_data_java {

    private static unittest unit_test;

    private static String[] CHANNELS;

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	String use_gap_handler = "true";
	int port = unit_test.port( );
	/* ----------------------------------------------------------- *
	   Need to adjust according to parameter
	 * ----------------------------------------------------------- */
	int protocol = connection.PROTOCOL_TWO;
	int expected_i = 4;
	if ( unit_test.hasOption( "-proto-1" ) )
	{
	    protocol = connection.PROTOCOL_ONE;
	    CHANNELS = new String[] {
		"X1:PEM-1",
		"X1:PEM-2"
	    };
	}
	if ( unit_test.hasOption( "-proto-2" ) )
	{
	    protocol = connection.PROTOCOL_TWO;
	    CHANNELS = new String[] {
		"X1:PEM-1,online",
		"X1:PEM-2,online"
	    };
	}

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port, protocol );

	/* ------------------------------------------------------------- *
	   Run the test
	 * ------------------------------------------------------------- */

	double[] expected = new double[]{ 1.5, 2.75 };

	int i = 0;
	for( buffer[] bufs : conn.iterate(CHANNELS) )
	{
	    /* ------------------------------------------------------- */
	    unit_test.check( bufs[0].samples( ),
			     256,
			     "Verifying samples of buffer 0" );
	    unit_test.check( true,
			     checkData( bufs[0], expected[0] ),
			     "Verifying data of buffer 0" );
	    /* ------------------------------------------------------- */
	    unit_test.check( bufs[1].samples( ),
			     512,
			     "Verifying samples of buffer 0" );
	    unit_test.check( true,
			     checkData( bufs[1], expected[1] ),
			     "Verifying data of buffer 0" );
	    /* ------------------------------------------------------- */
	    ++i;
	    if ( i == expected_i )
	    {
		// On a real system this would continue indefinitly
		// however, this test is set to only send 5 seconds
		// of data.
		break;
	    }
	}
	unit_test.check( expected_i,
			 i,
			 "Verifying number of iterations: " );

	/* ------------------------------------------------------------- *
	   Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }

    static boolean isGapSecond( int GPS )
    {
	return( false );
    }

    static boolean checkData( buffer Buf, double Value )
    {
	int samples_per_sec = (int)Buf.getSampleRate( );
	final int CHANNEL_TYPE = Buf.getDataType( );

	unit_test.msgInfo( "samples_per_sec: %d", samples_per_sec );
	int offset = 0;
	for( int cur_gps = Buf.start( ); cur_gps != Buf.stop( ); ++cur_gps )
	{
	    double expected = Value;
	    if ( isGapSecond( cur_gps ) )
	    {
		expected = 0.0;
	    }
	    for( int i = 0; i < samples_per_sec; ++i )
	    {
		if ( CHANNEL_TYPE == buffer.DATA_TYPE_FLOAT32 )
		{
		    if ( ((float[])Buf.getData( ))[offset] != (float)expected )
		    {
			return( false );
		    }
		}
		else if ( CHANNEL_TYPE == buffer.DATA_TYPE_FLOAT64 )
		{
		    if ( ((double[])Buf.getData( ))[offset] != expected )
		    {
			return( false );
		    }
		}
		offset++;
	    }
	}
	return( true );
    }
}