% -*- mode: octave -*-
%------------------------------------------------------------------------
%------------------------------------------------------------------------

global unit_test;
global START;
global STOP;
global STRIDE;
global CHANNELS;

unit_test = unittest( );

START = 0;
STOP = START + 1;
STRIDE = 1;

hostname = unit_test.hostname( );
port = unit_test.port( );
protocol = nds2.connection.PROTOCOL_ONE;

  CHANNELS = { 'X1:PEM-1', ...
	      };

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------

conn = nds2.connection(hostname, port, protocol );

%------------------------------------------------------------------------
% Run the test
%------------------------------------------------------------------------

i = 0;
EXPECTED_I = 1;
iter = conn.iterate(START, STOP, STRIDE, CHANNELS);
while( iter.hasNext( ) )
  bufs = iter.next( );
  i = i + 1;
end
unit_test.check( i, ...
		 EXPECTED_I, ...
		 'Verifying number of iterations: ' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------

conn.close( );
unit_test.exit( );

