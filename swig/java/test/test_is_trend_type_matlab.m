% -*- mode: octave -*-
% -----------------------------------------------------------------------
%   Simple test to verify version function
% -----------------------------------------------------------------------

% -----------------------------------------------------------------------
%   Minute Trend
% -----------------------------------------------------------------------
unit_test = unittest( );
trends = { 'A,m-trend',
	   'X1:ABC,m-trend',
	   'X1:ABC.mean,m-trend',
	   'A:,m-trend'
	   };
  
for i = trends
  unit_test.check( nds2.channel.isMinuteTrend( i{1} ), ...
		   true, ...
		   'Verify channel as being minute trend' );
  unit_test.check( nds2.channel.isSecondTrend( i{1} ), ...
		  false, ...
		  'Verify channel as NOT being second trend' );
end

% -----------------------------------------------------------------------
%   Second Trend
% -----------------------------------------------------------------------
trends = { 'A,s-trend',
	   'X1:ABC,s-trend',
	   'X1:ABC.mean,s-trend',
	   'A:,s-trend'
	   };
  
for i = trends
  unit_test.check( nds2.channel.isMinuteTrend( i{1} ), ...
		  false, ...
		  'Verify channel is not minute trend' );
  unit_test.check( nds2.channel.isSecondTrend( i{1} ), ...
		  true, ...
		  'Verify channel is second trend' );
end

% -----------------------------------------------------------------------
%   Junk Trend
% -----------------------------------------------------------------------
trends = { '',
	   's-trend',
	   ',s-trend',
	   'm-trend',
	   ',m-trend',
	   'S-TREND',
	   ',S-TREND',
	   'M-TREND',
	   ',M-TREND',
	   ',mtrend',
	   'STREND',
	   ',strend',
	   'MTREND',
	   'X1:ABCCDEFGs-trend',
	   'X1:ABCDEFGS-TREND',
	   'X1:ABCCDEFGm-trend',
	   'X1:ABCDEFGM-TREND',
	   'X1:ABCDEFGM'
	   };
  
for i = trends
  unit_test.check( nds2.channel.isMinuteTrend( i{1} ), ...
		   false, ...
		   'Verify channel is not minute trend' );
  unit_test.check( nds2.channel.isSecondTrend( i{1} ), ...
		   false, ...
		   'Verify channel is not second trend' );
end

% -----------------------------------------------------------------------
% Testing is complete so it is time to leave
% -----------------------------------------------------------------------

exit( unit_test.exit_code( ) );

