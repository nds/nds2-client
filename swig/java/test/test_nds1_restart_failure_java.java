/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.buffer;

public final class test_nds1_restart_failure_java {

    private static unittest unit_test;

    private static String[] CHANNELS;

    public static void main( String[] args )
            throws java.io.UnsupportedEncodingException
    {
        unit_test = new unittest( args );

        String hostname = unit_test.hostname( );
        int port = unit_test.port( );
        int protocol = connection.PROTOCOL_ONE;

        CHANNELS = new String[] {
                "X1:PEM-1",
        };

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */

        connection conn = new connection(hostname, port, protocol );

	/* ------------------------------------------------------------- *
	   Run the test
	 * ------------------------------------------------------------- */

        int i = 0;
        int expected_i = 1;

        for( buffer[] bufs : conn.iterate(0, 1, CHANNELS) )
        {
            ++i;
        }
        unit_test.check( expected_i,
                i,
                "Verifying number of iterations: " );

	/* ------------------------------------------------------------- *
	   Finish
	 * ------------------------------------------------------------- */
        conn.close( );

        unit_test.exit( );
    }
}