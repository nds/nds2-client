% -*- mode: octave -*-
%
% Unit tests against mock-up NDS 1 server
%
% Copyright (C) 2014  Leo Singer <leo.singer@ligo.org>
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program; if not, write to the Free Software Foundation, Inc.,
% 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
%

unit_test = unittest( );

bytepatterns = {
    cast([hex2dec('B0'); hex2dec('00')], 'uint8'),
    cast([hex2dec('DE'); hex2dec('AD'); hex2dec('BE'); hex2dec('EF')], 'uint8'),
    cast([hex2dec('00'); hex2dec('00'); hex2dec('00'); hex2dec('00'); hex2dec('FE'); hex2dec('ED'); hex2dec('FA'); hex2dec('CE')], 'uint8')
};

port = unit_test.port( );

conn = nds2.connection('localhost', port, nds2.connection.PROTOCOL_ONE);
unit_test.check(conn.getPort(), ...
		port, ...
		'Validating port' )
unit_test.check( conn.getProtocol(), ...
		 1, ...
		 'Validating protocol' );

channels = conn.findChannels('X*:{A,B,C}');
unit_test.check( length(channels), ...
		3, ...
		'Validating number of channels matching pattern "X*:{A,B,C}"' );
unit_test.check( channels(1).toString(), ...
		 '<X1:A (16Hz, ONLINE, INT16)>', ...
		 'Validating channel name for channel 1' )
unit_test.check( channels(2).toString(), ...
		 '<X1:B (16Hz, ONLINE, INT32)>', ...
		 'Validating channel name for channel 2' );
unit_test.check( channels(3).toString(), ...
		 '<X1:C (16Hz, ONLINE, INT64)>', ...
		 'Validating channel name for channel 3' );

bufs = conn.fetch(1000000000, 1000000004, {'X1:A', 'X1:B', 'X1:C'});
unit_test.check( length(bufs), ...
		 3, ...
		 'Validating fetch(start, stop, channels)' );
for i = 1:length(bufs)
  lead = sprintf( 'buf[%d] - ', i );
  buf = bufs(i);
  bytepattern = bytepatterns{i};
  unit_test.check( buf.getGpsSeconds(), ...
		   1000000000, ...
		   [lead 'GPS seconds'] );
  unit_test.check( buf.getGpsNanoseconds(), ...
		   0, ...
		   [lead 'GPS Nanoseconds'] );
  unit_test.check( length( buf.getData() ), ...
		   4 * buf.getChannel().getSampleRate(), ...
		   [lead 'Data buffer length'] );
  unit_test.check ( all(typecast(buf.getData(), 'uint8') == repmat(bytepattern, length(buf.getData()), 1)), ...
		    true, ...
		   [lead 'data contents'] );
end

iter = conn.iterate({'X1:A', 'X1:B', 'X1:C'});
bufs = iter.next();
unit_test.check( length(bufs), ...
		 3, ...
		 'Validating return count for iterate(channels)' );
for i = 1:length(bufs)
  buf = bufs(i);
  bytepattern = bytepatterns{i};
  lead = sprintf( 'buf[%d] - ', i );
  unit_test.check ( buf.getGpsSeconds(), ...
		    1000000000, ...
		    [lead 'GPS Seconds'] )
  unit_test.check( buf.getGpsNanoseconds(), ...
		   0, ...
		   [ lead 'GPS Nanoseconds' ] )
  unit_test.check( length(buf.getData()), ...
		    buf.getChannel().getSampleRate(), ...
		    [lead 'Data length' ] )
  unit_test.check( all(typecast(buf.getData(), 'uint8') == repmat(bytepattern, length(buf.getData()), 1)), ...
		   true, ...
		   [ lead 'Data contents' ] )
end

% connection automatically closed when garbage collected,
% but test explicit close anyway
conn.close();
exit( unit_test.exit_code( ) );
