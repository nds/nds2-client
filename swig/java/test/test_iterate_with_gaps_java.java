/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.buffer;

public final class test_iterate_with_gaps_java {

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );
	/* ----------------------------------------------------------- *
	   Need to adjust according to parameter
	 * ----------------------------------------------------------- */
	int protocol = connection.PROTOCOL_TWO;
	if ( unit_test.hasOption( "-proto-1" ) )
	{
	    protocol = connection.PROTOCOL_ONE;
	}
	if ( unit_test.hasOption( "-proto-2" ) )
	{
	    protocol = connection.PROTOCOL_TWO;
	}

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port, protocol );

	/* ------------------------------------------------------------- *
	   Run the test
	 * ------------------------------------------------------------- */

	int start;
	int end;
	int stride;
	int data_start;
	int samples_per_segment;
	String[] channels;

	long last_start = 0;

	if( protocol == connection.PROTOCOL_ONE )
	{
	    /* -------------------------------------------------------- *
	       connection.PROTOCOL_ONE
	     * -------------------------------------------------------- */
	    start = 1130797740;
	    stride = 3000;
	    end = start + stride * 3;
	    samples_per_segment = stride / 60;
	    channels = new String[]{
		"X1:CDS-DACTEST_COSINE_OUT_DQ.n,m-trend"
	    };
	}
	else
	{
	    /* -------------------------------------------------------- *
	       Default is to use connection.PROTOCOL_TWO
	     * -------------------------------------------------------- */
	    start = 1116286100;
	    end = start + 200;
	    stride = 50;
	    data_start = 1116286200;
	    samples_per_segment = stride;
	    channels = new String[] {
		"H1:ISI-BS_ST1_SENSCOR_GND_STS_X_BLRMS_30M_100M.mean,s-trend"
	    };
	}

	conn.setParameter("ITERATE_USE_GAP_HANDLERS", "false");

	for( buffer[] bufs : conn.iterate(start, end, channels) )
	{
	    /* Read off buffers */
	}

	int count = 0;
	for( buffer[] bufs : conn.iterate(start, end, channels) )
	{
	    count += 1;
	}

	conn.setParameter("ITERATE_USE_GAP_HANDLERS", "true");
	conn.setParameter("GAP_HANDLER", "STATIC_HANDLER_ZERO");

	for( buffer[] bufs : conn.iterate(start, end, stride, channels) )
	{
	    unit_test.check( bufs.length,
			     1,
			     "Stride returns 1 buffer" );
	    if( last_start == 0 )
	    {
		unit_test.check( bufs[0].getGpsSeconds( ),
				 start,
				 "Validate initial GPS start" );
	    }
	    else
	    {
		unit_test.check( bufs[0].getGpsSeconds( ),
				 last_start + stride,
				 "Validate offset GPS start" );
	    }
	    unit_test.check( bufs[ 0 ].stop( ) == end ||
			     bufs[ 0 ].samples( ) == samples_per_segment,
			     true,
			     "Verify lenth of buffer" );
	    // assert( bufs[0].Stop() == end or bufs[0].length == samples_per_segment );
	    last_start = bufs[0].getGpsSeconds( );
	}

	/* ------------------------------------------------------------- *
	   Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}