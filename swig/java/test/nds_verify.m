function nds_verify(nds_host, data_set)
    % This is a small demo script to verify core functionality.
    %
    % Before trying to run the command under windows, do the following:
    % 
    % >> addpath( fullfile( winqueryreg('HKEY_LOCAL_MACHINE', 'software\wow6432node\ligo\nds2-client'), 'share', 'matlab' ) )
    % >> nds_verify('nds.ligo.caltech.edu', 'H')
    %
    %======================================================
    %
    % Add jar to Java classpath
    javadir = '';
    if ispc
        % on Windows we get path from the registry
        javadir = fullfile( winqueryreg( 'HKEY_LOCAL_MACHINE', 'software\wow6432node\ligo\nds2-client' ), 'lib', 'java' );
    else
        % on most systems we have a utility to tell us the directory
        [st res] = system('nds-client-config --javaclasspath');
        if (st == 0)
            javadir = strtrim(res);
        end
    end
    if (isempty(javadir))
        javadir = fullfile( '/','opt', 'local', 'share', 'java' );  
    end
    if ( isempty( intersect( javaclasspath('-all'), javadir ) ) )
        javaaddpath(javadir);
    end

    %===========================
    % add some default arguments
    %===========================
    if nargin < 2
        data_set = 'H';
    end
    if nargin < 1
        nds_host = 'nds.ligo.caltech.edu';
    end

    % Some channels we know should exist
    if ( data_set == 'H' )
      channel_pattern = 'H1:*PEM-EY*';
      channel_set = {'H1:PEM-EY_SEISX_OUT_DQ',
             'H1:PEM-EY_SEISY_OUT_DQ',
             'H1:PEM-EY_SEISZ_OUT_DQ'};
    elseif ( data_set == 'L' )
      channel_pattern = 'L1:*PEM-EY*';
      channel_set = {'L1:PEM-EY_SEISX_OUT_DQ',
             'L1:PEM-EY_SEISY_OUT_DQ',
             'L1:PEM-EY_SEISZ_OUT_DQ'};
    end

    % ------------------------------------------------------------
    % Open connection and print some information about the server
    % ------------------------------------------------------------
    conn = nds2.connection(nds_host);
    disp('Connection:'); disp(conn);
    disp(sprintf('Host: %s', conn.getHost()));
    disp(sprintf('Port: %d', conn.getPort()));
    disp(sprintf('Protocol: %d', conn.getProtocol()));

    % ------------------------------------------------------------
    % Get list of available channels and print some information about them
    % ------------------------------------------------------------
    channels = conn.findChannels(channel_pattern);
    disp(sprintf('Number matching of channels: %d', length(channels)));
    disp('First channel:'); disp(channels(1));
    disp(sprintf('First channel name: %s', channels(1).getName()));

    % ------------------------------------------------------------
    % Fetch some data
    % ------------------------------------------------------------
    data = conn.fetch(1038296031, 1038296035, ...
              channel_set );
    disp('A buffer:'); disp(data(1));
    disp('Its data:'); disp(data(1).getData());
    disp('Its channel:'); disp(data(1).getChannel());

    disp('Now block by block...');
    conn.iterate(1038296031, 1038296035, 1, channel_set);
    while conn.hasNext()
        bufs = conn.next();
        disp(bufs);
    end
end
