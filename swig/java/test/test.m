% Add jar to Java classpath
javaaddpath(javadir);

try
    % GPS time for range
    gpsoffset =  315964818;
    gpsend = time() - gpsoffset;
    gpsstart = gpsend - 4;
    disp(sprintf('time: %d gpsstart: %d gpsend: %d', time(), gpsstart, gpsend));

    % Open connection and print some information about the server
    conn = nds2.connection('llocds.ligo-la.caltech.edu');
    disp('Connection:'); disp(conn);
    disp(sprintf('Host: %s', conn.getHost()));
    disp(sprintf('Port: %d', conn.getPort()));
    disp(sprintf('Protocol: %d', conn.getProtocol()));

    % Get list of available channels and print some information about them
    channels = conn.findChannels('X2:HPI*BS*');
    disp(sprintf('Number matching of channels: %d', length(channels)));
    disp('First channel:'); disp(channels(1));
    disp(sprintf('First channel name: %s', channels(1).getName()));

    % Fetch some data
    data = conn.fetch(1038296031, 1038296035, ...
		      {channels(1).getName(), channels(2).getName(), channels(3).getName()});
    disp('A buffer:'); disp(data(1));
    disp('Its data:'); disp(data(1).getData());
    disp('Its channel:'); disp(data(1).getChannel());

    disp('Now block by block...');
    conn.iterate(1038296031, 1038296035, 1, {channels(1).getName(), channels(2).getName(), channels(3).getName()})
    while conn.hasNext()
        bufs = conn.next();
        disp(bufs);
    end
catch ME
    % Trap error
end
