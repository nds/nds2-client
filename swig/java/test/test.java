public final class test {

    private static String matlabToJavaString(char [][] s)
    {
        return new String(s[0]);
    }

    public static void main(String [] args)
    {
	// GPS time for range
	gpsoffset =  315964818;
	gpsend = java.util.Date()/1000 - gpsoffset;
	gpsstart = gpsend - 4;
	disp(sprintf('time: %d gpsstart: %d gpsend: %d', time(), gpsstart, gpsend));

        // Open connection and print some information about the server
        nds2.connection conn = new nds2.connection("llodts0.ligo-la.caltech.edu");
        System.out.println("Connection: " + conn);
        System.out.println("Host: " + matlabToJavaString(conn.getHost()));
        System.out.println("Port: " + conn.getPort());
        System.out.println("Protocol:" + conn.getProtocol());

        // Get list of available channels and print some information about them
        nds2.channel [] channels = conn.findChannels("L1:*PEM-EY*");
        System.out.println("Number matching of channels: " + channels.length);
        System.out.println("First channel: " + channels[0]);
        System.out.println("First channel name: " + matlabToJavaString(channels[0].getName()));

        // Fetch some data
        String [] channelNames = {"L1:PEM-EY_SEISX_OUT_DQ", "L1:PEM-EY_SEISY_OUT_DQ", "L1:PEM-EY_SEISZ_OUT_DQ"};
        nds2.buffer [] data = conn.fetch(1038296031, 1038296035, channelNames);
        System.out.println("A buffer: " + data[0]);
        System.out.println("Its data: " + data[0].getData());
        System.out.println("Its channel: " + data[0].getChannel());

        System.out.println("Now block by block...");
        conn.iterate(1038296031, 1038296035, 1, channelNames);
        while (conn.hasNext())
        {
            data = conn.next();
            System.out.println(data);
        }
    }
}
