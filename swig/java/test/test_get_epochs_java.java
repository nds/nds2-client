/* ------------------------------------------------------------------- *
 *   Simple test to verify version function
 * ------------------------------------------------------------------- */

import java.util.List;
import java.util.ArrayList;

import nds2.connection;
import nds2.epoch;
import nds2.epochs_type;

class expected_data {
    public String name;
    public long start;
    public long stop;

    public expected_data( String Name, long Start, long Stop )
    {
	name = Name;
	start = Start;
	stop = Stop;
    };
};

public final class test_get_epochs_java {

    private static List<expected_data> getExpectedEpochs( int Proto )
    {
	List<expected_data>
	    retval = new ArrayList<expected_data>( );
	if ( Proto == connection.PROTOCOL_ONE )
	{
	    retval.add( new expected_data( "ALL", 0L, 1999999999L) );
	}
	else
	{
	    retval.add( new expected_data("ALL", 0, 1999999999) );
	    retval.add( new expected_data("ER2", 1025636416, 1028563232) );
	    retval.add( new expected_data("ER3", 1042934416, 1045353616) );
	    retval.add( new expected_data("ER4", 1057881616, 1061856016) );
	    retval.add( new expected_data("ER5", 1073606416, 1078790416) );
	    retval.add( new expected_data("ER6", 1102089216, 1102863616) );
	    retval.add( new expected_data("ER7", 1116700672, 1118330880) );
	    retval.add( new expected_data("ER8", 1123856384, 1126621184) );
	    retval.add( new expected_data("NONE", 0, 0) );
	    retval.add( new expected_data("O1", 1126621184, 1999999999) );
	    retval.add( new expected_data("S5", 815153408, 880920032) );
	    retval.add( new expected_data("S6", 930960015, 971654415) );
	    retval.add( new expected_data("S6a", 930960015, 935798415) );
	    retval.add( new expected_data("S6b", 937785615, 947203215) );
	    retval.add( new expected_data("S6c", 947635215, 961545615) );
	    retval.add( new expected_data("S6d", 961545615, 971654415) );
	}
	return retval;
    };

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );
	int protocol = connection.PROTOCOL_TWO;

	if ( args.length == 1 )
	{
	    if ( args[0].equals( "-proto-1" ) )
	    {
		protocol = connection.PROTOCOL_ONE;
	    }
	    else if ( args[0].equals( "-proto-2" ) )
	    {
		protocol = connection.PROTOCOL_TWO;
	    }
	}

	connection conn = new connection(hostname, port, protocol );

	/* ------------------------------------------------------------- *
	 * Establish the connection
	 * ------------------------------------------------------------- */
	   
	epoch cur = conn.currentEpoch( );

	/* ------------------------------------------------------------- *
	 * Verify default as returning ALL epocs
	 * ------------------------------------------------------------- */
	unit_test.check( cur.getGpsStart( ),
			 0,
			 "Check GPS start of current epoch: " );
	unit_test.check( cur.getGpsStop( ),
			 1999999999,
			 "Check GPS start of current epoch: " );

	/* ------------------------------------------------------------- *
	 * Verify list of epoch data
	 * ------------------------------------------------------------- */
	epochs_type available_epochs = conn.getEpochs( );
	List<expected_data> expected_epochs = getExpectedEpochs( conn.getProtocol( ) );
	unit_test.check( expected_epochs.size( ),
			 available_epochs.size( ),
			 "Verify size of epoch lists" );
	/* ------------------------------------------------------------- *
	 * Protocol dependent behaviors
	 * ------------------------------------------------------------- */
	if ( protocol == connection.PROTOCOL_TWO )
	{
	    /* --------------------------------------------------------- *
	       Test all epochs (except "NONE") by name
	     * --------------------------------------------------------- */
	    for ( int i = 0; i < available_epochs.size( ); ++i )
	    {
		epoch epoch = available_epochs.get( i );
		if ( epoch.getName( ).equals( "NONE" ) )
		{
		    continue;
		}
		conn.setEpoch( epoch.getName( ) );

		cur = conn.currentEpoch( );
		unit_test.check( epoch.getGpsStart( ),
				 cur.getGpsStart( ),
				 "Epics by name GPS start" );
		unit_test.check( epoch.getGpsStop( ),
				 cur.getGpsStop( ),
				 "Epics by name GPS stop" );
	    }
	    /* --------------------------------------------------------- *
	       Test all epochs (except 0-0) by time range
	     * --------------------------------------------------------- */
	    for ( int i = 0; i < available_epochs.size( ); ++i )
	    {
		epoch epoch = available_epochs.get( i );

		if ( epoch.getGpsStop( ) == 0L )
		{
		    continue;
		}
		conn.setEpoch( epoch.getGpsStart( ),
			       epoch.getGpsStop( ) );

		cur = conn.currentEpoch( );
		unit_test.check( epoch.getGpsStart( ),
				 cur.getGpsStart( ),
				 "Epics by range GPS start" );
		unit_test.check( epoch.getGpsStop( ),
				 cur.getGpsStop( ),
				 "Epics by range GPS stop" );
	    }
	}

	/* ------------------------------------------------------------- *
	 * Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }
}