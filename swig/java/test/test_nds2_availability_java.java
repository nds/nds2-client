/* ------------------------------------------------------------------- *
 *   Simple test to verify version function
 * ------------------------------------------------------------------- */

import nds2.availability;
import nds2.availability_list_type;
import nds2.buffer;
/* import nds2.buffers_type; */
import nds2.connection;
import nds2.channel;
import nds2.segment;
import nds2.simple_availability_list_type;
import nds2.simple_segment_list_type;

public final class test_nds2_availability_java {

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unittest unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	int port = unit_test.port( );
	int protocol = connection.PROTOCOL_TWO;

	connection conn = new connection(hostname, port, protocol );

	int gps_start = 1116733655;
	int gps_stop = 1116733675;

	String[] cn = {
	    "H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ",
	    "H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ",
	    "H1:SUS-ETMX_M0_MASTER_OUT_F3_DQ",
	    "H1:SUS-ETMX_M0_MASTER_OUT_LF_DQ",
	    "H1:SUS-ETMX_M0_MASTER_OUT_RT_DQ",
	    "H1:SUS-ETMX_M0_MASTER_OUT_SD_DQ",
	    "H1:SUS-ETMX_L1_MASTER_OUT_UL_DQ",
	    "H1:SUS-ETMX_L1_MASTER_OUT_UR_DQ",
	    "H1:SUS-ETMX_L1_MASTER_OUT_LL_DQ",
	    "H1:SUS-ETMX_L1_MASTER_OUT_LR_DQ",
	    "H1:SUS-ETMX_L2_MASTER_OUT_UL_DQ",
	    "H1:SUS-ETMX_L2_MASTER_OUT_UR_DQ",
	    "H1:SUS-ETMX_L2_MASTER_OUT_LL_DQ",
	    "H1:SUS-ETMX_L2_MASTER_OUT_LR_DQ",
	    "H1:SUS-ETMX_L3_MASTER_OUT_UL_DQ",
	    "H1:SUS-ETMX_L3_MASTER_OUT_UR_DQ"
	};

	conn.setEpoch(gps_start, gps_stop);

	availability_list_type avail = conn.getAvailability( cn );

	unit_test.msgInfo( avail.toString( ) );
	unit_test.check( avail.size( ), cn.length, "number of epochs: " );
	unit_test.msgInfo( avail.toString( ) );
	    
	for( int i = 0; i < cn.length; ++i )
	{
	    availability entry = avail.get(i);
	    String lead = String.format( "offset: %d ", i );

	    unit_test.check( entry.getName( ), cn[ i ], lead + "entry.getName(): " );
	    unit_test.check( entry.getData( ).size( ), 2, lead + "entry.getData( ).size( ): " );

	    unit_test.check( entry.getData( ).get( 0 ).getFrameType( ), "H-H1_C",
			     "frameType(0): " );
	    unit_test.check( entry.getData( ).get( 1 ).getFrameType( ), "H-H1_R",
			     "frameType(1): " );
	    /* ------------------------------------------------------- */
	    unit_test.check( entry.getData( ).get( 0 ).getGpsStart( ),
			     entry.getData( ).get( 1 ).getGpsStart( ),
			     lead + "start time equal: offset-0: " );
	    unit_test.check( entry.getData( ).get( 0 ).getGpsStart( ),
			     gps_start,
			     lead + "start time correct: " );
	    /* ------------------------------------------------------- */
	    unit_test.check( entry.getData( ).get( 0 ).getGpsStop( ),
			     entry.getData( ).get( 1 ).getGpsStop( ),
			     lead + "stop time equal: offset-0: " );
	    unit_test.check( entry.getData( ).get( 0 ).getGpsStop( ),
			     gps_stop,
			     lead + "stop time correct: " );
	}

	{
	    String expected = "( <H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ ( H-H1_C:1116733655-1116733675 H-H1_R:1116733655-1116733675 ) >";

	    unit_test.check( avail.toString( ).substring( 0, expected.length( ) ),
			     expected,
			     "Verify availability list name is correct: " );
	}

	{
	    String expected = "<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ ( H-H1_C:1116733655-1116733675 H-H1_R:1116733655-1116733675 ) >";

	    unit_test.check( avail.get( 0 ).toString( ),
			     expected,
			     "Verify availability list name is correct: " );
	}

	{
	    availability_list_type test_avail = new availability_list_type( );
	    availability entry = new availability( );

	    entry.setName( new String("X1:TEST_1") );
	    entry.getData( ).pushBack( new segment( new String("X-X1_C"),  gps_start, gps_start + 1024) );
	    entry.getData( ).pushBack( new segment( new String("X-X1_R"),  gps_start, gps_start + 100) );
	    entry.getData( ).pushBack( new segment( new String("X-X1_R"),  gps_start + 1024, gps_start + 1024+100) );
	    test_avail.pushBack( entry );
	    
	    simple_availability_list_type simple_test = test_avail.simpleList( );
	    
	    unit_test.check( simple_test.toString( ),
			     "( ( <1116733655-1116734779> ) )",
			     "simple_list range: " );

	    unit_test.check( simple_test.size( ),  1, "simple_list size: " );
	    unit_test.check( simple_test.get( 0 ).size( ), 1, "simple_list[ 0 ] size: " );
	    unit_test.check( simple_test.get( 0 ).get( 0 ).getGpsStart( ), gps_start,
			     "simple_list[ 0 ]  GPS Start: " );
	    unit_test.check( simple_test.get( 0 ).get( 0 ).getGpsStop( ), gps_start +1024+100,
			     "simple_list[ 0 ]  GPS Stop: " );
	}

	// --------------------------------------------------------------
	// test the implicit availability call in fetch
	// --------------------------------------------------------------
	buffer[] bufs = conn.fetch( gps_start, gps_stop, cn );

	unit_test.msgInfo( bufs.toString( ) );
	unit_test.msgInfo( bufs[ 0 ].toString( ) );
	/*
	{
	    String expected = "('<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ (GPS time 1116733655, 10240 samples)>','<H1:SUS-ETMX_M0_MASTER_OUT_F2_DQ (GPS time 1116733655, 10240 samples)>','";
	    String actual = bufs.toString( ).substring(0, expected.length( ) );

	    System.out.format( "-- INFO: actual: %s expected: %s%n", actual, expected );

	    // assert( actual.equals( expected ) );
	}
	*/
	unit_test.check( bufs[ 0 ].toString( ),
			 "<H1:SUS-ETMX_M0_MASTER_OUT_F1_DQ (512Hz, RAW, FLOAT32)>",
			 "Check name of first data buffer: " );

    conn.setEpoch(0, 1999999999);
	String[] cn2 = {
	    "H1:GDS-CALIB_STRAIN,reduced"
	};
	availability_list_type avail2 = conn.getAvailability( cn2 );

	conn.close( );
	unit_test.exit( );
    }
}