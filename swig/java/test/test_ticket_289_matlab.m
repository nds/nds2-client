% -*- mode: octave -*-
%------------------------------------------------------------------------
global unit_test


unit_test = unittest(  );
hostname = unit_test.hostname( );
port = unit_test.port( );

%------------------------------------------------------------------------
% Establish the connection
%------------------------------------------------------------------------

conn = nds2.connection(hostname, port );
conn.setParameter('ALLOW_DATA_ON_TAPE', 'true');
conn.setParameter('ITERATE_USE_GAP_HANDLERS', 'false');

%------------------------------------------------------------------------
% Ticket 289
%------------------------------------------------------------------------

chans = conn.findChannels( '*', nds2.channel.CHANNEL_TYPE_RDS );

unit_test.check( chans.length, 3, ...
		 'Correct number of channels' );
unit_test.check( chans( 1 ).name( ), 'X1:PEM-1', ...
		 'Validate channel zero name' );
unit_test.check( chans( 1 ).dataType( ), nds2.channel.DATA_TYPE_UNKNOWN, ...
		 'Validate channel zero data type' );
unit_test.check( chans( 3 ).name( ), 'X1:PEM-3', ...
		 'Validate channel two name' );
unit_test.check( chans( 3 ).dataType( ), nds2.channel.DATA_TYPE_FLOAT32, ...
		 'Validate channel two data type' );

%------------------------------------------------------------------------
% Finish
%------------------------------------------------------------------------
conn.close( );

exit( unit_test.exit_code( ) );
