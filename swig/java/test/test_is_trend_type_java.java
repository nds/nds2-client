/* ------------------------------------------------------------------- *
 *   Simple test to verify version function
 * ------------------------------------------------------------------- */

import java.lang.*;
import java.util.*;

import nds2.channel;

public final class test_is_trend_type_java {

    private static Vector<String> create_mTrends( )
    {
	Vector<String> retval = new Vector<String>( );

	retval.addElement( new String( "A,m-trend" ) );
	retval.addElement( new String( "X1:ABC,m-trend" ) );
	retval.addElement( new String( "X1:ABC.mean,m-trend" ) );
	retval.addElement( new String( "A:,m-trend" ) );

	return retval;
    }

    private static Vector<String> create_sTrends()
    {
	Vector<String> retval = new Vector<String>( );

	retval.addElement( new String( "A,s-trend" ) );
	retval.addElement( new String( "X1:ABC,s-trend" ) );
	retval.addElement( new String( "X1:ABC.mean,s-trend" ) );
	retval.addElement( new String( "A:,s-trend" ) );

	return retval;
    }

    private static Vector<String> create_junk()
    {
	Vector<String> retval = new Vector<String>( );

	retval.addElement( new String( "" ) );
	retval.addElement( new String( "s-trend" ) );
	retval.addElement( new String( ",s-trend" ) );
	retval.addElement( new String( "m-trend" ) );
	retval.addElement( new String( ",m-trend" ) );
	retval.addElement( new String( "S-TREND" ) );
	retval.addElement( new String( ",S-TREND" ) );
	retval.addElement( new String( "M-TREND" ) );
	retval.addElement( new String( ",M-TREND" ) );
	retval.addElement( new String( ",mtrend" ) );
	retval.addElement( new String( "STREND" ) );
	retval.addElement( new String( ",strend" ) );
	retval.addElement( new String( "MTREND" ) );
	retval.addElement( new String( "X1:ABCCDEFGs-trend" ) );
	retval.addElement( new String( "X1:ABCDEFGS-TREND" ) );
	retval.addElement( new String( "X1:ABCCDEFGm-trend" ) );
	retval.addElement( new String( "X1:ABCDEFGM-TREND" ) );
	retval.addElement( new String( "X1:ABCDEFGM" ) );	

	return retval;
    }

    public static void main(String[] args)
	throws java.io.UnsupportedEncodingException
    {
	//---------------------------------------------------------------
	// Minute Trend
	//---------------------------------------------------------------
	{
	    Vector<String> mTrends = create_mTrends( );

	    for ( Iterator< String > i = mTrends.iterator( );
		  i.hasNext( );
		  )
	    {
		String pattern = i.next( );
		assert( channel.isMinuteTrend( pattern ) == true );
		assert( channel.isSecondTrend( pattern ) == false );
	    }
	}
	//---------------------------------------------------------------
	// Second Trend
	//---------------------------------------------------------------
	{
	    Vector<String> sTrends = create_sTrends( );

	    for ( Iterator< String > i = sTrends.iterator( );
		  i.hasNext( );
		  )
	    {
		String pattern = i.next( );
		assert( channel.isMinuteTrend( pattern ) == false );
		assert( channel.isSecondTrend( pattern ) == true );
	    }
	}
	//---------------------------------------------------------------
	// Junk
	//---------------------------------------------------------------
	{
	    Vector<String> junk = create_junk( );

	    for ( Iterator< String > i = junk.iterator( );
		  i.hasNext( );
		  )
	    {
		String pattern = i.next( );
		assert( channel.isMinuteTrend( pattern ) == false );
		assert( channel.isSecondTrend( pattern ) == false );
	    }
	}
    }
}