/* ------------------------------------------------------------------- *
 * ------------------------------------------------------------------- */

import nds2.connection;
import nds2.buffer;

public final class test_iterate_live_data_bounded_java {

    private static unittest unit_test;

    private final static int START = 0;
    private final static int STOP = START + 20;
    private final static int STRIDE = 10;
    private final static int GPS_START = 1770000000;
    private final static int GPS_STOP = GPS_START + 20;
    private static String[] CHANNELS;

    public static void main( String[] args )
	throws java.io.UnsupportedEncodingException
    {
	unit_test = new unittest( args );

	String hostname = unit_test.hostname( );
	String use_gap_handler = "true";
	int port = unit_test.port( );
	/* ----------------------------------------------------------- *
	   Need to adjust according to parameter
	 * ----------------------------------------------------------- */
	int protocol = connection.PROTOCOL_TWO;
	int multiplier = 10;
	int expected_i = 2;
	int tip_point = 0;      /* when does the data do a switch
	                         * this is just part of the test
	                         * to show we are not just carrying the
	                         * same value over and over again.
	                         */
	if ( unit_test.hasOption( "-proto-1" ) )
	{
	    protocol = connection.PROTOCOL_ONE;
	    CHANNELS = new String[] {
		"X1:PEM-1",
		"X1:PEM-2",
	    };
	    multiplier = 1;
	    expected_i = 20;
	    tip_point = 10;
	}
	if ( unit_test.hasOption( "-proto-2" ) )
	{
	    protocol = connection.PROTOCOL_TWO;
	    CHANNELS = new String[] {
		"X1:PEM-1,online",
		"X1:PEM-2,online",
	    };
	    multiplier = 10;
	    expected_i = 2;
	    tip_point = 1;
	}
	if ( unit_test.hasOption( "-no-gap" ) )
	{
	    use_gap_handler = "false";
	}

	/* ------------------------------------------------------------- *
	   Establish the connection
	 * ------------------------------------------------------------- */
	   
	connection conn = new connection(hostname, port, protocol );
	conn.setParameter( "ITERATE_USE_GAP_HANDLERS", use_gap_handler );

	/* ------------------------------------------------------------- *
	   Run the test
	 * ------------------------------------------------------------- */

	double[] expected = new double[]{ 1.5, 2.75 };

	int i = 0;
	for( buffer[] bufs : conn.iterate(START, STOP, STRIDE, CHANNELS) )
	{
	    /* ------------------------------------------------------- */
	    unit_test.check( bufs[0].samples( ),
			     256 * multiplier,
			     "Verifying samples of buffer 0" );
	    unit_test.check( true,
			     checkData( bufs[0], expected[0] ),
			     "Verifying data of buffer 0" );
	    /* ------------------------------------------------------- */
	    unit_test.check( bufs[1].samples( ),
			     512 * multiplier,
			     "Verifying samples of buffer 0" );
	    unit_test.check( true,
			     checkData( bufs[1], expected[1] ),
			     "Verifying data of buffer 1" );
	    /* ------------------------------------------------------- */
	    ++i;
	    if (i == tip_point) {
	        expected = new double[]{ 3.5, 4.75 };
	    }
	}
	unit_test.check( expected_i,
			 i,
			 "Verifying number of iterations: " );

	/* ------------------------------------------------------------- *
	    Make sure we can do other operations
	 * ------------------------------------------------------------- */

	conn.setEpoch( GPS_START, GPS_STOP );
	conn.findChannels( "X1:PEM-1,online" );
	conn.findChannels( "X1:PEM-2,online" );

    expected = new double[]{ 1.5, 2.75 };

	i = 0;
	for( buffer[] bufs : conn.iterate(0, 2, CHANNELS) )
	{
	    unit_test.check( bufs[0].samples( ),
			     256,
			     "Verifying samples of buffer 0 after setEpoch" );
	    unit_test.check( true,
			     checkData( bufs[0], 1.5 ),
			     "Verifying data of buffer 0 after setEpoch" );
	    unit_test.check( bufs[1].samples( ),
			     512,
			     "Verifying samples of buffer 1 after setEpoch" );
	    unit_test.check( true,
			     checkData( bufs[1], 2.75 ),
			     "Verifying data of buffer 1 after setEpoch" );
	    ++i;
	}
	unit_test.check( 2,
			 i,
			 "Verifying second number of iterations: " );

	/* ------------------------------------------------------------- *
	   Finish
	 * ------------------------------------------------------------- */
	conn.close( );

	unit_test.exit( );
    }

    static boolean isGapSecond( int GPS )
    {
	return( false );
    }

    static boolean checkData( buffer Buf, double Value )
    {
	int samples_per_sec = (int)Buf.getSampleRate( );
	final int CHANNEL_TYPE = Buf.getDataType( );

	unit_test.msgInfo( "samples_per_sec: %d", samples_per_sec );
	int offset = 0;
	for( int cur_gps = Buf.start( ); cur_gps != Buf.stop( ); ++cur_gps )
	{
	    double expected = Value;
	    if ( isGapSecond( cur_gps ) )
	    {
		expected = 0.0;
	    }
	    for( int i = 0; i < samples_per_sec; ++i )
	    {
		if ( CHANNEL_TYPE == buffer.DATA_TYPE_FLOAT32 )
		{
		    if ( ((float[])Buf.getData( ))[offset] != (float)expected )
		    {
			return( false );
		    }
		}
		else if ( CHANNEL_TYPE == buffer.DATA_TYPE_FLOAT64 )
		{
		    if ( ((double[])Buf.getData( ))[offset] != expected )
		    {
			return( false );
		    }
		}
		offset++;
	    }
	}
	return( true );
    }
}