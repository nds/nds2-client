/* -*- mode: c++ -*- */
%typemap(javaimports) connection "java.io.Closeable"
%typemap(javainterfaces) connection "java.io.Closeable"

%rename (iterate_base, fullname=1) connection::iterate;
%javamethodmodifiers connection::iterate "protected";

%typemap(javacode) connection %{
  public connection_iterator iterate( String[] Channels)
    throws UnsupportedOperationException
  {
    connection_iterator retval =
      new connection_iterator( iterate_base( Channels ) );
    return retval;
  }

  public connection_iterator iterate( int Stride, String[] Channels)
    throws UnsupportedOperationException
  {
    connection_iterator retval =
      new connection_iterator( iterate_base( Stride, Channels ) );
    return retval;
  }

  public connection_iterator iterate( int Start, int Stop, String[] Channels)
    throws UnsupportedOperationException
  {
    connection_iterator retval =
      new connection_iterator( iterate_base( Start, Stop, Channels ) );
    return retval;
  }

  public connection_iterator iterate( int Start, int Stop, int Stride, String[] Channels)
    throws UnsupportedOperationException
  {
    connection_iterator retval =
      new connection_iterator( iterate_base( Start, Stop, Stride, Channels ) );
    return retval;
  }
%}
