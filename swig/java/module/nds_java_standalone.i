%pragma(java) modulecode=%{
    public static connection_iterator iterate( request_period period, String[] Channels)
    {
        parameters Params = new parameters();
        return iterate( Params, period, Channels );
    }

    public static connection_iterator iterate( parameters Params, request_period period, String[] Channels)
    {
        connection conn = new connection( Params );
        connection_iterator iter = conn.iterate( period.getStart(), period.getStop(), period.getStride(), Channels );
        iter.closeOnFinish();
        return iter;
    }

    public static channel[] findChannels( parameters Params, channel_predicate Pred )
    {
        epoch timeSpan = new epoch( "", Pred.gpsStart(), Pred.gpsStop() );
        return _find_channels(Params, Pred.glob(), Pred.channelTypeMask(), Pred.dataTypeMask(), Pred.minSampleRate(), Pred.maxSampleRate(), timeSpan );
    }

    public static channel[] findChannels( channel_predicate Pred )
    {
        epoch timeSpan = new epoch( "", Pred.gpsStart(), Pred.gpsStop() );
        return _find_channels( Pred.glob(), Pred.channelTypeMask(), Pred.dataTypeMask(), Pred.minSampleRate(), Pred.maxSampleRate(), timeSpan );
    }
%}