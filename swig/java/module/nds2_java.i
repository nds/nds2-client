/* Use methods named '__str__' to supply string representations of objects. */
%typemap(javapackage) nds2;

%rename __str__ toString;

/* Make all functions and non-constant fields camel-case. */
%rename("%(lowercamelcase)s", %$ismember, %$isvariable, %$not %$hasconsttype) "";
%rename("%(lowercamelcase)s", %$isfunction) "";

/* Prevent the user from explicitly calling delete() by making it package-private.
 * Copied from the standard SWIG header java/java.swg, but with
 * 'public synchronized' replaced with 'private synchronized'. */
%typemap(javadestruct, methodname="delete", methodmodifiers="synchronized") SWIGTYPE {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        $jnicall;
      }
      swigCPtr = 0;
    }
  }

%typemap(javadestruct_derived, methodname="delete", methodmodifiers="synchronized") SWIGTYPE {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        $jnicall;
      }
      swigCPtr = 0;
    }
    super.delete();
  }

%include "enumsimple.swg"

/*
 * Support string arrays as char ** input arguments.
 */
%typemap(in, numinputs=1, noblock=1) (const char **channel_names, size_t count_channels) (jstring *jstrings) {
	$2 = JCALL1(GetArrayLength, jenv, $input);
	$1 = (char **) calloc($2 + 1, sizeof(char *));
	jstrings = calloc($2 + 1, sizeof(jstring));
	{
		size_t i;
		for (i = 0; i < $2; i ++) {
			jstrings[i] = (jstring) JCALL2(GetObjectArrayElement, jenv, $input, (jsize) i);
			$1[i] = (char *) JCALL2(GetStringUTFChars, jenv, jstrings[i], 0);
		}
	}
}

%typemap(freearg, noblock=1) (const char **channel_names, size_t count_channels) {
	{
		size_t i;
		for (i = 0; i < $2; i ++)
		{
			JCALL2(ReleaseStringUTFChars, jenv, jstrings$argnum[i], $1[i]);
			JCALL1(DeleteLocalRef, jenv, jstrings$argnum[i]);
		}
	}
	free(jstrings$argnum);
	jstrings$argnum = NULL;
	free($1);
	$1 = NULL;
}

%typemap(jni) char ** "jobjectArray";
%typemap(jtype) char ** "String[]";
%typemap(jstype) char ** "String[]";

%typemap(javain) char ** "$javainput";
%typemap(javaout) char ** {
	$jnicall;
}

/**
 * Support nds2_buffer arrays as buffer ** output arguments in Java.
 */

%typemap(jni) nds2_buffer ** "jobject";
%typemap(jtype) nds2_buffer ** "buffer[]";
%typemap(jstype) nds2_buffer ** "buffer[]";

%typemap(javain) nds2_buffer ** "$javainput";
%typemap(javaout) nds2_buffer ** {
	return $jnicall;
}

/**
 * Support nds2_channel and nds2_buffer arrays double-pointer return values,
 * with the size stored in the second argument of type size_t *count_channels_ptr.
 */

%typemap(jni) SWIGTYPE ** "jobject";
%typemap(jtype) SWIGTYPE ** "$typemap(jstype, $*1_type)[]";
%typemap(jstype) SWIGTYPE ** "$typemap(jstype, $*1_type)[]";

%typemap(javain) SWIGTYPE ** "$javainput";
%typemap(javaout) SWIGTYPE ** {
	return $jnicall;
}

%typemap(in, numinputs=0, noblock=1) size_t *count_channels_ptr (size_t count_channels = 1) {
	$1 = &count_channels;
}

%typemap(out) SWIGTYPE ** {
	/* Locate channel class. */
	jclass clazz;
	jmethodID ctor;
	size_t i;
	size_t count_channels = *arg2;

	clazz = JCALL1(FindClass, jenv, "nds2/$typemap(jstype, $*1_type)");
	if (!clazz)
	{
		$cleanup
		free($1);
		return $null;
	}

	ctor = JCALL3(GetMethodID, jenv, clazz, "<init>", "(JZ)V");
	if (!ctor)
	{
		$cleanup
		free($1);
		return $null;
	}

	/* Initialize the array with the specified length. */
	$result = JCALL3(NewObjectArray, jenv, (jsize) (jsize) count_channels, clazz, NULL);
	if (!$result)
	{
		$cleanup
		free($1);
		return $null;
	}

	for (i = 0; i < count_channels; i ++)
	{
		$typemap(jni, $*1_type) ctor_arg1 = ($typemap(jni, $*1_type)) $1[i];
		jboolean ctor_arg2 = JNI_TRUE;
		jobject obj = JCALL4(NewObject, jenv, clazz, ctor, ctor_arg1, ctor_arg2);
		if (!obj)
		{
			$cleanup
			free($1);
			return $null;
		}
		JCALL3(SetObjectArrayElement, jenv, $result, (jsize) i, obj);
		if (JCALL0(ExceptionCheck, jenv))
		{
			$cleanup
			free($1);
			return $null;
		}
		JCALL1(DeleteLocalRef, jenv, obj);
	}
}

%typemap(newfree, noblock=1) SWIGTYPE ** {
	free($1);
}

%apply SWIGTYPE ** {nds2_channel **, nds2_buffer **};

/**
 * Make buffer.getChannel return a copy.
 */

%typemap(javaout) nds2_channel * {
	return new $javaclassname($jnicall, true);
}
%typemap(out, noblock=1) nds2_channel * (nds2_channel *channel) {
	channel = (nds2_channel *) malloc(sizeof(nds2_channel));
	if (!channel)
	{
		$cleanup
		SWIG_exception(SWIG_MemoryError, nds2_strerror(errno));
	}
	memcpy(channel, $1, sizeof(nds2_channel));
	$result = (jlong)channel;
}

/**
 * Support getting numeric arrays from unsigned char[] using polymorphism.
 */

%typemap(jni) unsigned char [] "jobject";
%typemap(jtype) unsigned char [] "Object";
%typemap(jstype) unsigned char [] "Object";

%typemap(javain) unsigned char [] "$javainput";
%typemap(javaout) unsigned char [] {
	return $jnicall;
}

%typemap(out, noblock=1) unsigned char [] {
	switch (arg1->channel.data_type)
	{
		case NDS2_DATA_TYPE_INT16:
			$result = JCALL1(NewShortArray, jenv, (jsize) arg1->length);
			if (!$result)
			{
				$cleanup
				return $null;
			}
			JCALL4(SetShortArrayRegion, jenv, $result, 0, (jsize) arg1->length, (jshort *) $1);
			if (JCALL0(ExceptionCheck, jenv))
			{
				$cleanup
				return $null;
			}
			break;
		case NDS2_DATA_TYPE_INT32:
		case NDS2_DATA_TYPE_UINT32:
			$result = JCALL1(NewIntArray, jenv, (jsize) arg1->length);
			if (!$result)
			{
				$cleanup
				return $null;
			}
			JCALL4(SetIntArrayRegion, jenv, $result, 0, (jsize) arg1->length, (jint *) $1);
			if (JCALL0(ExceptionCheck, jenv))
			{
				$cleanup
				return $null;
			}
			break;
		case NDS2_DATA_TYPE_INT64:
			$result = JCALL1(NewLongArray, jenv, (jsize) arg1->length);
			if (!$result)
			{
				$cleanup
				return $null;
			}
			JCALL4(SetLongArrayRegion, jenv, $result, 0, (jsize) arg1->length, (jlong *) $1);
			if (JCALL0(ExceptionCheck, jenv))
			{
				$cleanup
				return $null;
			}
			break;
		case NDS2_DATA_TYPE_FLOAT32:
			$result = JCALL1(NewFloatArray, jenv, (jsize) arg1->length);
			if (!$result)
			{
				$cleanup
				return $null;
			}
			JCALL4(SetFloatArrayRegion, jenv, $result, 0, (jsize) arg1->length, (jfloat *) $1);
			if (JCALL0(ExceptionCheck, jenv))
			{
				$cleanup
				return $null;
			}
			break;
		case NDS2_DATA_TYPE_FLOAT64:
			$result = JCALL1(NewDoubleArray, jenv, (jsize) arg1->length);
			if (!$result)
			{
				$cleanup
				return $null;
			}
			JCALL4(SetDoubleArrayRegion, jenv, $result, 0, (jsize) arg1->length, (jdouble *) $1);
			if (JCALL0(ExceptionCheck, jenv))
			{
				$cleanup
				return $null;
			}
			break;
		case NDS2_DATA_TYPE_COMPLEX32:
			SWIG_exception(SWIG_TypeError, "Java does not have a native complex type");
			break;
		default:
			SWIG_exception(SWIG_TypeError, "Unknown NDS data type");
			break;
	}
}

/**
 * Everything below is relevant to MATLAB only.
 * A JAR that is intended for use directly from Java code would make more sense
 * without what follows.  (Note: I'm thinking of creating a separate directory
 * under swig/ for matlab, and creating one JAR for Java and one specifically
 * for Java under MATLAB.
 *
 * MATLAB does not automatically convert java.lang.String return values to its
 * own char arrays, so for convenient calling from MATLAB we turn C strings into
 * double-scripted char arrays, which MATLAB knows to interpret as its own char
 * arrays.
 *
 * Note that MATLAB does seem to know how to convert char arrays into
 * java.lang.String input arguments, so the input typemap could be disabled.
 */

%typemap(jni) CSTRINGTYPE "jobjectArray";
%typemap(jtype) CSTRINGTYPE "char[][]";
%typemap(jstype) CSTRINGTYPE "char[][]";

%typemap(javain) CSTRINGTYPE "$javainput";
%typemap(javaout) CSTRINGTYPE {
	return $jnicall;
}

%typemap(out) CSTRINGTYPE {
	jclass clazz;
	jsize len;
	jcharArray innerArray;
	jchar *innerArrayChars;
	int i;

	/* Look up the class for an array of chars (char []). */
	clazz = JCALL1(FindClass, jenv, "[C");
	if (!clazz)
	{
		$cleanup
		free($1);
		return $null;
	}

	/* Create a new inner array -- which will store the chars in the string. */
	len = (jsize)(strlen($1));
	innerArray = JCALL1(NewCharArray, jenv, len);
	if (!innerArray)
	{
		$cleanup
		free($1);
		return $null;
	}

	/* Since a Java char is 16 bits wide, and in most C compilers a char is
	 * 8 bits wide, it's safest to copy the chars over byte by byte. */

	/* Get a pointer to the underlying array data. */
	innerArrayChars = JCALL2(GetCharArrayElements, jenv, innerArray, NULL);
	if (!innerArrayChars)
	{
		$cleanup
		free($1);
		return $null;
	}

	/* Copy character-for-character. */
	for (i = 0; i < len; i ++)
		innerArrayChars[i] = $1[i];

	/* Release the array data, causing a copy if necessary. */
	JCALL3(ReleaseCharArrayElements, jenv, innerArray, innerArrayChars, 0);

	/* Create a new array of char[] (a char[][]). */
	$result = JCALL3(NewObjectArray, jenv, 1, clazz, (jobject) innerArray);
	if (!$result)
	{
		$cleanup
		free($1);
		return $null;
	}

	/* Let go of innerArray. (GC will do this for us, but let's be tidy.) */
	JCALL1(DeleteLocalRef, jenv, (jobject) innerArray);
}

%typemap(in) CSTRINGTYPE {
	jsize len;
	jcharArray innerArray;
	jchar *innerArrayChars;
	int i;

	/* Get inner array */
	/* FIXME: what to do if this exceeds bounds? */
	innerArray = (jcharArray) JCALL2(GetObjectArrayElement, jenv, $input, 0);
	if (!innerArray)
	{
		$cleanup
		return $null;
	}

	/* Get a pointer to the underlying array data. */
	innerArrayChars = JCALL2(GetCharArrayElements, jenv, innerArray, NULL);
	if (!innerArrayChars)
	{
		$cleanup
		return $null;
	}

	/* Get string length */
	len = JCALL1(GetArrayLength, jenv, innerArray);
	if (len < 0)
	{
		$cleanup
		return $null;
	}

	/* Create char array to store converted string */
	$1 = malloc(sizeof($*1_type) * (len + 1));
	if (!$1)
	{
		$cleanup
		return $null;
	}

	/* Since a Java char is 16 bits wide, and in most C compilers a char is
	 * 8 bits wide, it's safest to copy the chars over byte by byte. */

	/* Copy character-for-character. */
	for (i = 0; i < len; i ++)
		$1[i] = innerArrayChars[i];

	/* Append C string terminator */
	$1[i] = '\0';

	/* Release the array data. */
	JCALL3(ReleaseCharArrayElements, jenv, innerArray, innerArrayChars, JNI_ABORT);

	/* Let go of innerArray. (GC will do this for us, but let's be tidy.) */
	JCALL1(DeleteLocalRef, jenv, (jobject) innerArray);
}

%typemap(freearg, noblock=1) CSTRINGTYPE {
	free($1);
}

%apply CSTRINGTYPE {char *, char[255]};
