#ifndef NDS_JAVA_VECTOR_TO_ARRAY_I
#define NDS_JAVA_VECTOR_TO_ARRAY_I

%define %nds_vector_to_array(Vector, ValueType)

%typemap(jni) Vector "jobjectArray";
%typemap(jtype) Vector "ValueType[]";
%typemap(jstype) Vector "ValueType[]";
%typemap(javaout) Vector {
  return $jnicall;
}

%typemap(out) Vector {
  //---------------------------------------------------------------------
  // find out about the object to convert
  //---------------------------------------------------------------------
  jclass clazz = jenv->FindClass( %nds_namespace( )"/ValueType" );
  if ( ! clazz )
  {
    $cleanup;
    return $null;
  }
  jmethodID ctor = jenv->GetMethodID( clazz, "<init>", "(JZ)V" );
  if ( ! ctor )
  {
    $cleanup;
    return $null;
  }
  //---------------------------------------------------------------------
  // Allocate the space
  //---------------------------------------------------------------------
  $result = jenv->NewObjectArray( $1.size( ), clazz, NULL);
  if ( ! $result )
  {
    $cleanup;
    return $null;
  }
  //---------------------------------------------------------------------
  // Translate vector into array
  //---------------------------------------------------------------------
  jsize         i = 0;
  jlong         cptr;
  jobject       obj;
  for ( $1_basetype::iterator
          cur = $1.begin( ),
          last = $1.end( );
        cur != last;
        ++cur,
	 ++i )
  {
    *($1_basetype::value_type**)(&cptr) = new $1_basetype::value_type(*cur);
    obj = jenv->NewObject( clazz, ctor, cptr, JNI_TRUE );
    if ( ! obj )
    {
      $cleanup;
      return $null;
    }
    jenv->SetObjectArrayElement( $result, i, obj );
  }
}

typedef std::vector< std::shared_ptr< ValueType > > Vector;

%enddef /* %nds_vector_to_array */

#endif /* NDS_JAVA_VECTOR_TO_ARRAY_I */

