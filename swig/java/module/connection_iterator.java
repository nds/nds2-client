package nds2;

import java.io.Closeable;

public class connection_iterator implements java.util.Iterator<buffer[]>,java.lang.Iterable<buffer[]>,java.io.Closeable {
  private connection conn;
  private buffer[] nextBuffers;
  private boolean has_next;
  private boolean initialized;
  private boolean close_conn_on_finish;

  public connection_iterator(connection Container) {
    conn = Container;
    nextBuffers = null;
    has_next = true;
    initialized = false;
    close_conn_on_finish = false;
  }

  protected void closeOnFinish() {
    close_conn_on_finish = true;
  }

  protected void finalize() {
    close();
  }

  public synchronized void delete() {
    close();
  }

  public java.util.Iterator<buffer[]> iterator( )
  {
    return this;
  }

  public void remove() throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  public buffer[] next() throws java.util.NoSuchElementException {
    if (!hasNext()) {
      throw new java.util.NoSuchElementException();
    }

    return nextImpl();
  }

  public boolean hasNext() {
    return has_next;
  }

  private buffer[] nextImpl() {
    buffer[] retval = new buffer[0];

    if (!initialized) {
        setup_next();
    }
    initialized = true;
    if (has_next) {
      retval = nextBuffers;
      setup_next( );
    }
    return retval;
  }

  public void close() {
    if (conn != null) {
      conn.close();
      conn = null;
    }
  }

  private void setup_next() {
    if (conn != null) {
      try {
        nextBuffers = conn.next( );
      } catch (Exception e) {
        has_next = false;
        nextBuffers = null;
        if (close_conn_on_finish) {
          close();
        }
      }
    }
  }

}
