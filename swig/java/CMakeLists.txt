# -*- mode: cmake; coding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=cmake:et:sw=4:ts=4:sts=4
find_package(SWIG 2.0.4 REQUIRED)

#========================================================================
# Discover if JAVA is available and usable
#========================================================================
if ( NOT DEFINED ENABLE_SWIG_JAVA OR ENABLE_SWIG_JAVA )
    find_package(Java)
    find_package(JNI)
endif( )

if ( JAVA_FOUND AND JNI_FOUND )

    #--------------------------------------------------------------------
    # Since it is available and usable, finish up with making it
    #   usable
    #--------------------------------------------------------------------
    include(UseJava)
    set( ENABLE_SWIG_JAVA "yes"
        CACHE BOOL "Control the building of the SWIG bindings for Java" )

    #--------------------------------------------------------------------
    # Test for compatibility
    #--------------------------------------------------------------------
    file( WRITE ${CMAKE_CURRENT_BINARY_DIR}/compatibility_check.java "" )
    set( JAVA_COMPATIBILITY_REGEX_ "^.* (obsolete|no longer supported[.]) .*$" )
    foreach( JAVA_COMPATIBILITY_ 5 6 7 8 9 10 11 )
        message( STATUS "Checking ${Java_JAVAC_EXECUTABLE} for ${JAVA_COMPATIBILITY_}" )
        execute_process(
            COMMAND ${Java_JAVAC_EXECUTABLE}
            -source ${JAVA_COMPATIBILITY_}
            -target ${JAVA_COMPATIBILITY_}
            ${CMAKE_CURRENT_BINARY_DIR}/compatibility_check.java
            OUTPUT_VARIABLE OUTPUT_STRING
            ERROR_VARIABLE ERROR_STRING
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_STRIP_TRAILING_WHITESPACE
            )
        message( STATUS "LOOKING FOR '${JAVA_COMPATIBILITY_REGEX_}' IN: ${ERROR_STRING}" )
        string( REGEX REPLACE "${JAVA_COMPATIBILITY_REGEX_}" "" OBSOLETE_FOUND_STRING "${ERROR_STRING}" )
        message( STATUS "OBSOLETE_FOUND_STRING ${OBSOLETE_FOUND_STRING}" )
        string( LENGTH "${OBSOLETE_FOUND_STRING}" OBSOLETE_FOUND )
        if ( OBSOLETE_FOUND GREATER 0 )
            set( JAVA_COMPATIBILITY ${JAVA_COMPATIBILITY_} CACHE INTERNAL "Backwards compatibility level" )
            break( )
        endif( )
    endforeach()
    file( REMOVE ${CMAKE_CURRENT_BINARY_DIR}/compatibility_check.java )
    message( STATUS "Compiling code to be backwards compatible with Java ${JAVA_COMPATIBILITY}" )

    #--------------------------------------------------------------------
    # 
    #--------------------------------------------------------------------
    if ( ${Java_VERSION_STRING} GREATER "1.8"  )
        set( NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH "" )
    else ( ${Java_VERSION_STRING} GREATER "1.8"  )
        if ( UNIX AND NOT APPLE )
            execute_process(
                COMMAND ${Java_JAVA_EXECUTABLE} -XshowSettings:properties -help
                OUTPUT_QUIET
                ERROR_VARIABLE _PROPERTIES
                )
            # message( STATUS "DEBUG: Setting JAVA PROPERTIES to: ${Java_JAVA_EXECUTABLE}" ${_PROPERTIES} )
            string( REGEX MATCH "sun.boot.library.path = [^\n]*"
                NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH
                ${_PROPERTIES} )
            string( REPLACE "sun.boot.library.path = " ""
                NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH
                ${NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH} )
            string( STRIP
                ${NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH}
                NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH )
            file( TO_CMAKE_PATH
                ${NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH}
                NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH )
            set( NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH
                "LD_LIBRARY_PATH=${NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH}/xawt" )
            message( STATUS "DEBUG: Setting JAVA LIB to: ${NDS_EXTEND_JAVA_BOOT_LIBRARY_PATH}" )
        endif ( UNIX AND NOT APPLE )
    endif ( ${Java_VERSION_STRING} GREATER "1.8"  )
endif( JAVA_FOUND
    AND JNI_FOUND)

add_subdirectory(module)
add_subdirectory(test)

#========================================================================
# Provide additional information regarding components
#========================================================================

cpack_add_component(JAVA
    DISPLAY_NAME "JAVA extensions"
    DESCRIPTION
        "Pieces needed to use the nds2 client from JAVA"
    GROUP Runtime)
